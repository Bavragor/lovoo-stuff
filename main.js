/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
require.config({
    shim: {
        angular: {
            exports: "angular"
        },
        angularAnimate: {
            deps: ["angular"],
            exports: "angular"
        },
        angularCookies: {
            deps: ["angular"],
            exports: "angular"
        },
        angularMocks: {
            deps: ["angular"],
            exports: "angular"
        },
        angularResource: {
            deps: ["angular"],
            exports: "angular"
        },
        angularRoute: {
            deps: ["angular"],
            exports: "angular"
        },
        angularSanitize: {
            deps: ["angular"],
            exports: "angular"
        },
        angularTouch: {
            deps: ["angular"],
            exports: "angular"
        },
        angularLocale: {
            deps: ["angular"],
            exports: "angular"
        },
        bootstrap: [],
        lib: ["angular", "recaptcha", "bootstrap"]
    },
    paths: {
        angular: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular.min", "/angular.min"],
        angularAnimate: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-animate.min", "angular-animate.min"],
        angularCookies: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-cookies.min", "angular-cookies.min"],
        angularMocks: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-mocks.min", "angular-mocks.min"],
        angularResource: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-resource.min", "angular-resource.min"],
        angularRoute: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-route.min", "angular-route.min"],
        angularSanitize: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-sanitize.min", "angular-sanitize.min"],
        angularTouch: ["//ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-touch.min", "angular-touch.min"],
        angularLocale: ["//code.angularjs.org/1.3.6/angular-locale_de_DE.min", "angular-locale_de.min"],
        bootstrap: ["//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min", "bootstrap-3.2.0.min"],
/*        recaptcha: "minified/https://www.google.com/recaptcha/api/js/recaptcha_ajax",
        autocomplete: "minified/autocomplete.min",*/
        challenge: "minified/challenge.min",
        chat: "minified/chat.min",
        contacts: "minified/contacts.min",
        credits: "minified/credits.min",
        eyecatcher: "minified/eyecatcher.min",
        facebook: "minified/facebook.min",
        feed: "minified/feed.min",
        fileupload: "minified/fileupload.min",
        flirtmatch: "minified/flirtmatch.min",
        footer: "minified/footer.min",
        googleplus: "minified/googleplus.min",
        hashtag: "minified/hashtag.min",
        helpcenter: "minified/helpcenter.min",
        homepage: "minified/homepage.min",
        landingpage: "minified/landingpage.min",
        lib: "minified/lib.min",
        list: "minified/list.min",
        location: "minified/location.min",
        lovoo: "minified/lovoo.min",
        packages: "minified/packages.min",
        profile: "minified/profile.min",
        post: "minified/post.min",
        purchase: "minified/purchase.min",
        searchform: "minified/searchform.min",
        self: "minified/self.min",
        settings: "minified/settings.min",
        testview: "minified/testview.min",
        topmenu: "minified/topmenu.min",
        tutorial: "minified/tutorial.min",
        verify: "minified/verify.min",
        widget: "minified/widget.min",
        vip: "minified/vip.min"
    }
}), require(["domReady!", "main.init"]);