/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    ! function() {
        "use strict";

        function ListController($scope, ListService, defaultLimit, $parse, UserActionsFctry) {
            $scope.items = [], $scope.page = $scope.page || 1, $scope.limit = $scope.limit || defaultLimit, $scope.$parent.user && ($scope.user = $scope.$parent.user);
            var typeService = ListService.initCollection($scope.type),
                callParams = angular.extend({
                    type: $scope.type,
                    resultLimit: $scope.limit,
                    resultPage: $scope.page
                }, $scope.serviceParams || {}),
                reloadFunc = function() {
                    typeService.load(callParams)
                };
            reloadFunc(), $scope.action = UserActionsFctry.action, $scope.actionProfile = function(user) {
                $scope.$root.Self.id != user.id && $scope.action(user, "profile")
            }, $scope.$watch(function() {
                return typeService.get()
            }, function(newVal, oldVal) {
                $scope.items = newVal
            })
        }
        angular.module("Lovoo.List", ["ngResource", "TemplateLoader"]).constant("defaultLimit", 20).constant("ITEM_TYPES", ["kiss", "friend", "credits", "pictures"]).controller("ListCtrl", ListController), ListController.$inject = ["$scope", "ListService", "defaultLimit", "$parse", "UserActionsFctry"]
    }(),
    function(angular) {
        "use strict";

        function ItemListDirective(TemplateService) {
            return {
                controller: "ListCtrl",
                restrict: "E",
                link: function(scope, element, attrs) {
                    var src = (attrs.template || "itemlist") + ".html";
                    TemplateService.loadTemplate(src, element, scope), scope.itemAction = function(action, params) {
                        angular.isFunction(scope.$parent.onItemAction) && scope.$parent.onItemAction()(action, params)
                    }
                },
                scope: {
                    type: "@",
                    limit: "@",
                    page: "@",
                    category: "@",
                    size: "@",
                    className: "@className",
                    itemTemplate: "@itemTemplate",
                    sort: "@sort",
                    notFoundMsg: "@notFoundMsg",
                    serviceParams: "=",
                    onItemAction: "&onItemAction"
                }
            }
        }

        function ListItemTypeDirective(TemplateService) {
            return {
                restrict: "E",
                transcluse: !1,
                replace: !0,
                scope: !0,
                link: function(scope, element) {
                    var src = "listitem/" + (scope.itemTemplate || scope.type) + ".html";
                    TemplateService.loadTemplate(src, element, scope), scope.itemAction = function(action, params) {
                        angular.isFunction(scope.$parent.onItemAction) && scope.$parent.onItemAction()(action, params)
                    }
                }
            }
        }

        function ColumnsSwitcherDirective(StorageService) {
            return {
                restrict: "A",
                replace: !0,
                template: '<i ng-click="switchColumns()" ng-class="$root.userListColumns == 3 ? \'fa-th-large\' : \'fa-th\'" class="fa cursor-pointer text-black"></i>',
                link: function(scope) {
                    var setColumns = function(columns) {
                        scope.$root.userListColumns = parseInt(columns), StorageService.set("userListColumns", columns)
                    };
                    scope.switchColumns = function() {
                        setColumns(3 === scope.$root.userListColumns ? 4 : 3)
                    }, setColumns(StorageService.get("userListColumns") || 3)
                }
            }
        }

        function ThumbnailCoverDirective($timeout) {
            function discoverThumbnail(element, timeout) {
                $timeout(function() {
                    $(element).transition({
                        opacity: 0
                    }, 750, "easeOutQuad", function() {
                        element.remove()
                    })
                }, timeout)
            }
            return {
                restrict: "A",
                scope: {
                    index: "="
                },
                link: function(scope, element) {
                    discoverThumbnail(element, 1750 + 75 * scope.index)
                }
            }
        }
        angular.module("Lovoo.List").directive("itemList", ItemListDirective).directive("listItemType", ListItemTypeDirective).directive("columnsSwitcher", ColumnsSwitcherDirective).directive("thumbnailCover", ThumbnailCoverDirective), ItemListDirective.$inject = ["TemplateService"], ListItemTypeDirective.$inject = ["TemplateService"], ColumnsSwitcherDirective.$inject = ["StorageService"], ThumbnailCoverDirective.$inject = ["$timeout"]
    }(angular),
    function() {
        "use strict";

        function ListFactory($resource, ITEM_TYPES) {
            var responseResults = function(response) {
                var data = angular.fromJson(response),
                    dataResponse = data.response;
                return void 0 === dataResponse ? angular.extend(angular.fromJson(response), {
                    allCount: 0
                }) : {
                    data: dataResponse.result ? dataResponse.result : [],
                    allCount: dataResponse.allCount ? dataResponse.allCount : 0
                }
            };
            return {
                query: function(params) {
                    var fn = params.type;
                    if (-1 === ITEM_TYPES.indexOf(fn)) throw new Error("Unsupported list type: " + fn);
                    return this.doRequest()[fn](params).$promise
                },
                doRequest: function() {
                    return $resource("", {
                        userId: "@userId"
                    }, {
                        credits: {
                            method: "GET",
                            transformResponse: responseResults,
                            url: Ajax.api + "/self/creditshistory"
                        },
                        kiss: {
                            method: "GET",
                            transformResponse: responseResults,
                            url: Ajax.api + "/users/:userId/kiss"
                        },
                        pictures: {
                            method: "GET",
                            transformResponse: responseResults,
                            url: Ajax.api + "/users/:userId/pictures",
                            headers: {
                                "public": !0
                            }
                        },
                        friend: {
                            method: "GET",
                            transformResponse: responseResults,
                            url: Ajax.api + "/friends"
                        }
                    })
                }
            }
        }

        function ListService(ListFactory, $collection) {
            var items = {},
                getCollection = function(type) {
                    return "undefined" == typeof items[type] && (items[type] = $collection.getInstance()), items[type]
                };
            return {
                initCollection: function(type) {
                    var objects = getCollection(type),
                        allCount = 0;
                    return {
                        getAllCount: function() {
                            return allCount
                        },
                        getSize: function() {
                            return objects.size()
                        },
                        has: function() {
                            return objects.size() > 0
                        },
                        set: function(data) {
                            objects = data, allCount = objects.size()
                        },
                        add: function(data) {
                            objects.add(data, {
                                prepend: !1
                            }), allCount++
                        },
                        get: function() {
                            return objects.all()
                        },
                        removeById: function(id) {
                            var obj = objects.find("id", id);
                            obj && objects.remove(obj)
                        },
                        load: function(params, clearAll, prepend, callback) {
                            ListFactory.query(params).then(function(data) {
                                "undefined" != typeof callback && callback(data.data, objects.all()), ("undefined" == typeof clearAll || clearAll) && objects.removeAll(), objects.addAll(data.data, {
                                    prepend: "undefined" != typeof prepend ? prepend : !1
                                }), allCount = data.allCount
                            })
                        }
                    }
                }
            }
        }
        angular.module("Lovoo.List").factory("ListFactory", ListFactory).service("ListService", ListService), ListFactory.$inject = ["$resource", "ITEM_TYPES"], ListService.$inject = ["ListFactory", "$collection"]
    }()
});