/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "purchase"], function(angular, purchase) {
    ! function() {
        "use strict";

        function packageListCtrl(PURCHASE_STATES, $scope, $routeParams, $location, RegisterService, PackagesService, methods) {
            $scope.Math = Math, $scope.methods = [], $scope.packages = [], $scope.changeStep = function(step) {
                step = parseInt(step), $scope.purchaseStep = step, $scope.$parent.showPackageDetails = 1 === step, dataLayer.push({
                    event: "sendVirtualPageView",
                    vpv: "/ecommerce/" + PURCHASE_STATES[step - 1]
                }), 3 === step && dataLayer.push({
                    event: "firePurchaseAccomplished"
                })
            }, $scope.clickWizardStep = function(step) {
                $scope.purchaseStep > step && 2 === $scope.purchaseStep && $scope.changeStep(step)
            }, $scope.selectPackage = function(pckg) {
                return $scope.$root.isSecurityUser() ? ($scope.$parent.showPackageDetails = !1, $scope.changeStep(2), void($scope["package"] = pckg)) : void RegisterService.showRegisterDialog()
            }, $scope.getPackageColor = function(pckg) {
                return pckg.bestPrice ? "bg-" + $scope.packageColor + " best-price" : ""
            }, $scope.getButtonColor = function() {
                switch ($scope.packageColor) {
                    case "orange":
                        return "btn-warning";
                    case "green":
                        return "btn-success";
                    default:
                        return "btn-info"
                }
            }, void 0 !== $routeParams.status && "Success" === $routeParams.status ? ($location.url($location.path()).replace(), $scope.changeStep(3)) : void 0 !== $routeParams.status && "Error" === $routeParams.status ? ($location.url($location.path()).replace(), $scope.changeStep(4)) : $scope.changeStep(1), _.each(methods, function(method) {
                -1 === $.inArray("de", method.loc) && method.loc || $scope.methods.push(method)
            }), $scope.$parent.packages = $scope.packages, PackagesService.getPackages($scope.packageType, function(data) {
                2 === $scope.packageType && (data[0].bestPrice = 1), angular.extend($scope.packages, data)
            })
        }
        angular.module("Lovoo.Packages", ["Lovoo.Purchase", "TemplateLoader"]).controller("PackageListCtrl", packageListCtrl).constant("PURCHASE_STATES", ["select-product", "select-payment", "purchase-accomplished"]), packageListCtrl.$inject = ["PURCHASE_STATES", "$scope", "$routeParams", "$location", "RegisterService", "PackagesService", "PurchaseMethods"]
    }(),
    function() {
        "use strict";

        function packageListDirective() {
            return {
                restrict: "E",
                replace: !0,
                scope: {
                    packageColor: "@",
                    packageType: "@"
                },
                controller: "PackageListCtrl",
                templateUrl: "/template/packages/packages.html"
            }
        }

        function packageDirective(TemplateService, PackagesService) {
            function getTemplate(scope) {
                if (1 === scope.pckg.type) {
                    return "vip-weekly"
                }
                return scope.packageDetails = [{
                    icon: "lo lo-user-cursor",
                    translate: "uncover"
                }, {
                    icon: "fa fa-eye",
                    translate: "eyecatcher"
                }, {
                    icon: "lo lo-lips",
                    translate: "kisses"
                }, {
                    icon: "lo lo-comments-up",
                    translate: "topchat"
                }], "credits-default"
            }
            return {
                restrict: "E",
                replace: !0,
                link: function(scope, element) {
                    var src = "packages/" + getTemplate(scope) + ".html";
                    TemplateService.loadTemplate(src, element, scope).success(function() {
                        scope.packageDetails && PackagesService.setPackagePopover(element, scope.packageDetails)
                    })
                }
            }
        }

        function hugePriceDirective() {
            return {
                restrict: "E",
                replace: !0,
                template: "<h1>{{ price | formatNumber }}<i>" + 1.1.toLocaleString().charAt(1) + "</i><span>{{ price | currencyCents }}</span></h1>",
                scope: {
                    price: "="
                }
            }
        }
        angular.module("Lovoo.Packages").directive("packageList", packageListDirective).directive("package", packageDirective).directive("hugePrice", hugePriceDirective), packageDirective.$inject = ["TemplateService", "PackagesService"]
    }(),
    function() {
        "use strict";

        function PackagesQueryFctry($resource) {
            return $resource("", {
                cache: !0
            }, {
                packages: {
                    url: Ajax.api + "/purchase/packages",
                    method: "GET",
                    headers: {
                        "public": !0
                    },
                    params: {
                        type: "@type",
                        category: "@category",
                        platform: "paypal"
                    }
                }
            })
        }

        function PackagesService($filter, PackagesQueryFctry) {
            this.getPackages = function(packageType, callback) {
                PackagesQueryFctry.packages({
                    type: packageType,
                    category: packageType
                }).$promise.then(function(data) {
                    _.each(data.response, function(pckg) {
                        pckg.localePrice = $filter("formatLocalePrice")(pckg.price), "undefined" != typeof pckg.months && 0 !== pckg.months && (pckg.weeklyPrice = Math.ceil(pckg.price / pckg.months / 4 * 100) / 100, pckg.localeWeeklyPrice = $filter("formatLocalePrice")(pckg.weeklyPrice), pckg.monthsHtml = Translator.get("dynamic.months", {
                            months: pckg.months
                        }, pckg.months).replace(" ", "<br />"))
                    }), callback(data.response)
                })
            }, this.setPackagePopover = function(elem, packageDetails) {
                var infoBoxContent = [];
                _.each(packageDetails, function(detail) {
                    var row = $('<div class="padding-sm row"><i class="fa-lg text-success space-right-sm col-xs-1"></i><span class="col-xs-10"></span></div>'),
                        icon = row.children("i"),
                        text = row.children("span");
                    icon.addClass(detail.icon), text.html(Translator.get("credits_details." + detail.translate + "_title")), infoBoxContent.push(row[0].outerHTML)
                }), elem.find(".fa-info-circle").popover({
                    trigger: "hover",
                    placement: "top",
                    container: "body",
                    html: !0,
                    title: Translator.get("credits_details.info"),
                    content: infoBoxContent.join("")
                })
            }
        }
        angular.module("Lovoo.Packages").factory("PackagesQueryFctry", PackagesQueryFctry).service("PackagesService", PackagesService), PackagesQueryFctry.$inject = ["$resource"], PackagesService.$inject = ["$filter", "PackagesQueryFctry"]
    }()
});