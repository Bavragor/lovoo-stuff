/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "credits"], function(angular, credits) {
    ! function() {
        "use strict";

        function TopmenuController($injector, $location, $scope, AuthService) {
            var TopmenuLoggedInService;
            $scope.onLogin = AuthService.showLoginDialog, $scope.onRegister = AuthService.showRegisterDialog, Boolean(ShowLogin) && $scope.onLogin(), $scope.changeLanguageToLocale = function(newLocale) {
                return "prod" === $scope.$root.ENVIRONMENT ? void(window.location.href = "https://" + newLocale + ".lovoo.com") : ($location.search("_locale", newLocale), void(window.location.href = $location.absUrl()))
            }, $scope.checkForOfferwallBeforeRouting = function(newPath) {
                return $scope.$root.isSecurityUser() ? (TopmenuLoggedInService = TopmenuLoggedInService || $injector.get("TopmenuLoggedInService"), void TopmenuLoggedInService.checkForOfferwallBeforeRouting(newPath)) : void(window.location.href = newPath)
            }
        }

        function TopmenuLoggedInController($scope, $interval, $window, convActionService, FavIconService, NotificationService, NotificationNewService, StorageService, TopmenuLoggedInService) {
            function dispatchNotificationRequest() {
                NotificationNewService.getNotifications().then(function(data) {
                    var notificationsResult = data.response.result;
                    0 !== notificationsResult.length && (self.count.info = NotificationNewService.countNotifications(notificationsResult, v3NotificationResponseTypes), self.notifications = notificationsResult)
                })
            }

            function notificationBadge(noti, ele, type) {
                var rootCount = 0;
                noti[ele] && notifications[ele] !== noti[ele] && (self.count[type] += noti[ele] - (notifications[ele] || 0), notifications[ele] = noti[ele], updateNotifications = !0), (!noti[ele] && notifications[ele] || noti[ele] < notifications[ele] || !noti[ele] && notifications[ele] > 0) && (self.count[type] -= noti[ele] || notifications[ele], notifications[ele] -= noti[ele] || notifications[ele], updateNotifications = !0), _.each(self.count, function(value) {
                    rootCount += value
                }), $scope.$root.notificationCount !== rootCount && (FavIconService.badge(rootCount), getExtendedNotificationList()), $scope.$root.notificationCount = rootCount
            }

            function getExtendedNotificationList() {
                if (!($scope.$root.websiteVersion > 2.5)) {
                    var result = TopmenuLoggedInService.getNotification(1, updateNotifications);
                    result.then && result.then(function(data) {
                        var i, notifications = data.response.result;
                        for (self.notifications = [], i = 0; i < notifications.length; ++i) - 1 !== $.inArray(notifications[i].id, notifyInfo) ? self.notifications.push(notifications[i]) : -1 !== $.inArray(notifications[i].type, v3NotificationResponseTypes) && self.notifications.push(notifications[i]);
                        updateNotifications = !1
                    })
                }
            }
            var updateNotifications, self = this,
                notifications = {},
                notifyMessages = ["m", "mr"],
                notifyInfo = $scope.$root.websiteVersion < 3 ? ["v", "mh", "mv", "k", "h"] : ["v", "pl", "fo", "pr"],
                v3NotificationResponseTypes = ["visit", "photo_like", "follower", "reply"];
            this.count = {
                messages: 0,
                info: 0
            }, this.mainmenu = Mainmenu, this.openChatDialog = convActionService.openChatDialog, this.isActive = TopmenuLoggedInService.isActive, this.doLogout = function() {
                StorageService.remove("general_unlock_confirmation." + $scope.$root.Self.id), $window.location.href = $scope.$root.Ajax.url + "/logout"
            }, NotificationService.subscribe(notifyMessages, function(key) {
                notificationBadge(this, key, "messages")
            }), 2.5 === $scope.$root.websiteVersion ? NotificationService.subscribe(notifyInfo, function(key) {
                notificationBadge(this, key, "info")
            }) : (dispatchNotificationRequest(), $interval(function() {
                dispatchNotificationRequest()
            }, 3e4))
        }
        angular.module("Lovoo.Topmenu", ["Lovoo.Auth", "Lovoo.Credits"]).controller("TopmenuCtrl", TopmenuController).controller("TopmenuLoggedInCtrl", TopmenuLoggedInController), TopmenuController.$inject = ["$injector", "$location", "$scope", "AuthService"], TopmenuLoggedInController.$inject = ["$scope", "$interval", "$window", "convActionService", "FavIconService", "NotificationService", "NotificationNewService", "StorageService", "TopmenuLoggedInService"]
    }(),
    function() {
        "use strict";

        function TopmenuDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/topmenu/topmenu.html",
                replace: !0,
                scope: !0,
                controller: "TopmenuCtrl",
                link: function(scope) {
                    var updateTopmenuStyles = function() {
                        switch (scope.$root.routing.topmenu) {
                            case "transparent":
                                scope.topmenuClass = "navbar-transparent", scope.isTransparent = !0, scope.brandLogo = "classic", scope.navigation = "classic";
                                break;
                            case "modern":
                                scope.navigation = "modern", scope.topmenuClass = "navbar-modern", scope.brandLogo = "small-grey", scope.isTransparent = scope.$root.BS.xs;
                                break;
                            case "hidden":
                                scope.topmenuClass = "invisible", scope.isTransparent = !0, scope.brandLogo = "classic", scope.navigation = "classic";
                                break;
                            default:
                                scope.topmenuClass = "navbar-rainbow-top", scope.isTransparent = !1, scope.brandLogo = "classic", scope.navigation = "classic"
                        }
                    };
                    scope.$root.$watch(function() {
                        return scope.$root.BS.size + "-" + scope.$root.routing.topmenu
                    }, updateTopmenuStyles), updateTopmenuStyles()
                }
            }
        }

        function TopmenuLoggedInDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/topmenu/topmenu-logged-in.html",
                replace: !0,
                controller: "TopmenuLoggedInCtrl",
                controllerAs: "loggedIn"
            }
        }

        function TopmenuMobileDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/topmenu/topmenu-mobile.html",
                replace: !0,
                scope: {
                    application: "@"
                }
            }
        }

        function SvgBrandDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/topmenu/svg-brand.html"
            }
        }
        angular.module("Lovoo.Topmenu").directive("topmenu", TopmenuDirective).directive("topmenuLoggedIn", TopmenuLoggedInDirective).directive("topmenuMobile", TopmenuMobileDirective).directive("svgBrand", SvgBrandDirective)
    }(),
    function() {
        "use strict";

        function TopmenuLoggedInService($rootScope, $injector, $location, DialogTutorialService, NotifyQueryFctry) {
            var loading = !1,
                baseUrl = $rootScope.Ajax.url,
                offerwalls = ["sponsorpay", "trialpay", "supersonicads"];
            this.isActive = function(route) {
                var current = $location.path().replace(baseUrl + "/", "");
                return "number" == typeof route ? !1 : (route = route.replace(baseUrl + "/", ""), route === current || route.split("?")[0] === current.split("/")[0])
            }, this.getNotification = function(page, update) {
                var notifyQuery, promiseFunc = function(data) {
                    loading = !1, data.response && data.response.result.length && DialogTutorialService.isTutorialAndEvenTypeEnabled("tutorial_notification") && DialogTutorialService.loadTutorialIfEnabled("tutorial_notification")
                };
                return 1 === page && !update || loading || !_.size($rootScope.Self) || (loading = !0, notifyQuery = NotifyQueryFctry.notification.get({
                    resultLimit: 10,
                    resultPage: page || 1,
                    stream: update || !1
                }, promiseFunc).$promise), notifyQuery || {}
            }, this.checkForOfferwallBeforeRouting = function(newPath) {
                var modal, slashedBaseUrl = baseUrl + "/",
                    CreditsFreeService = $injector.get("CreditsFreeService");
                return $rootScope.AdsEnabled && (this.isActive("vip") || this.isActive("credits")) ? (modal = CreditsFreeService.action(offerwalls[Math.floor(Math.random() * offerwalls.length)]), void modal.result["finally"](function() {
                    return slashedBaseUrl === newPath ? window.location.href = newPath : $location.path(newPath)
                })) : slashedBaseUrl === newPath ? window.location.href = newPath : $location.path(newPath)
            }
        }
        angular.module("Lovoo.Topmenu").service("TopmenuLoggedInService", TopmenuLoggedInService), TopmenuLoggedInService.$inject = ["$rootScope", "$injector", "$location", "DialogTutorialService", "NotifyQueryFctry"]
    }()
});