/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
function _ec_replace(str, key, value) {
    if (str.indexOf("&" + key + "=") > -1 || 0 === str.indexOf(key + "=")) {
        var end, newstr, idx = str.indexOf("&" + key + "=");
        return -1 === idx && (idx = str.indexOf(key + "=")), end = str.indexOf("&", idx + 1), newstr = -1 !== end ? str.substr(0, idx) + str.substr(end + (idx ? 0 : 1)) + "&" + key + "=" + value : str.substr(0, idx) + "&" + key + "=" + value
    }
    return str + "&" + key + "=" + value
}

function hex(num) {
    for (var str = "", j = 7; j >= 0; j--) str += hex_chr.charAt(num >> 4 * j & 15);
    return str
}

function str2blks_SHA1(str) {
    for (var nblk = (str.length + 8 >> 6) + 1, blks = new Array(16 * nblk), i = 0; 16 * nblk > i; i++) blks[i] = 0;
    for (i = 0; i < str.length; i++) blks[i >> 2] |= str.charCodeAt(i) << 24 - i % 4 * 8;
    return blks[i >> 2] |= 128 << 24 - i % 4 * 8, blks[16 * nblk - 1] = 8 * str.length, blks
}

function add(x, y) {
    var lsw = (65535 & x) + (65535 & y),
        msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return msw << 16 | 65535 & lsw
}

function rol(num, cnt) {
    return num << cnt | num >>> 32 - cnt
}

function ft(t, b, c, d) {
    return 20 > t ? b & c | ~b & d : 40 > t ? b ^ c ^ d : 60 > t ? b & c | b & d | c & d : b ^ c ^ d
}

function kt(t) {
    return 20 > t ? 1518500249 : 40 > t ? 1859775393 : 60 > t ? -1894007588 : -899497514
}

function calcSHA1(str) {
        for (var x = str2blks_SHA1(str), w = new Array(80), a = 1732584193, b = -271733879, c = -1732584194, d = 271733878, e = -1009589776, i = 0; i < x.length; i += 16) {
            for (var olda = a, oldb = b, oldc = c, oldd = d, olde = e, j = 0; 80 > j; j++) w[j] = 16 > j ? x[i + j] : rol(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1), t = add(add(rol(a, 5), ft(j, b, c, d)), add(add(e, w[j]), kt(j))), e = d, d = c, c = rol(b, 30), b = a, a = t;
            a = add(a, olda), b = add(b, oldb), c = add(c, oldc), d = add(d, oldd), e = add(e, olde)
        }
        return hex(a) + hex(b) + hex(c) + hex(d) + hex(e)
    }! function(window, angular, undefined) {
        "use strict";
        angular.module("ngCollection", []).factory("$collection", ["$filter", "$parse", function($filter, $parse) {
            function s4() {
                return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
            }

            function guid() {
                return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4()
            }

            function checkValue(item, compareFn) {
                return compareFn(item)
            }

            function Collection(options) {
                options || (options = {}), void 0 !== options.comparator && (this.comparator = options.comparator), void 0 !== options.limit && (this.limit = options.limit), this.idAttribute = options.idAttribute || this.idAttribute, this.current = null, this._reset(), this.initialize.apply(this, arguments)
            }
            return Collection.prototype = {
                idAttribute: "id",
                initialize: function() {},
                add: function(obj, options) {
                    options || (options = {});
                    var id, sort, prepend, limit, existing, index;
                    return sort = options.sort !== !1, prepend = options.prepend || !1, limit = this.limit || options.limit, index = void 0 !== options.index ? options.index : prepend ? 0 : this.array.length, obj[this.idAttribute] || (obj[this.idAttribute] = guid()), (existing = this.get(obj)) ? angular.extend(existing, obj) : (id = obj[this.idAttribute], this.hash[id] = obj, this.array.splice(index, 0, obj), this.length += 1), sort && this.sort(), limit && this.limitResult(limit), this
                },
                addAll: function(objArr, options) {
                    options || (options = {});
                    for (var sort = options.sort !== !1, limit = this.limit || options.limit, i = 0; i < objArr.length; i++) {
                        var obj = objArr[i];
                        this.add(obj, angular.extend(options, {
                            sort: !1,
                            limit: !1
                        })), limit && this.limitResult(limit)
                    }
                    return sort && this.sort(), options.reverse && this.array.reverse(), this
                },
                limitResult: function(limit) {
                    var size = this.array.length,
                        overflow = size - limit;
                    return overflow > 0 && this.remove(this.last()), this
                },
                sort: function() {
                    return this.comparator && (this.array = $filter("orderBy")(this.array, this.comparator)), this
                },
                get: function(obj) {
                    return null == obj ? void 0 : this.hash[obj[this.idAttribute] || obj]
                },
                find: function(expr, value, deepCompare) {
                    var compareFn = expr;
                    if ("string" == typeof expr) {
                        var parse = $parse(expr);
                        compareFn = function(item) {
                            return deepCompare ? parse(item) == value : parse(item) === value
                        }, compareFn.prototype.value = value, compareFn.prototype.deepCompare = deepCompare
                    }
                    for (var i = 0; i < this.array.length; i++)
                        if (checkValue(this.array[i], compareFn)) return this.array[i];
                    return void 0
                },
                where: function(expr, value, deepCompare) {
                    var results = [],
                        compareFn = expr;
                    if ("string" == typeof expr) {
                        var parse = $parse(expr);
                        compareFn = function(item) {
                            return deepCompare ? parse(item) == value : parse(item) === value
                        }, compareFn.prototype.value = value, compareFn.prototype.deepCompare = deepCompare
                    }
                    for (var i = 0; i < this.array.length; i++) checkValue(this.array[i], compareFn) && results.push(this.array[i]);
                    return results
                },
                update: function(obj) {
                    var existing = this.get(obj);
                    return existing && angular.extend(existing, obj), existing || this.add(obj), this
                },
                remove: function(obj) {
                    var index = this.array.indexOf(obj);
                    return -1 === index ? this : (delete this.hash[obj[this.idAttribute]], this.array.splice(index, 1), this.length--, this)
                },
                removeAll: function() {
                    for (var i = this.array.length - 1; i >= 0; i--) this.remove(this.at(i));
                    return this
                },
                last: function() {
                    return this.array[this.length - 1]
                },
                at: function(index) {
                    return this.array[index]
                },
                size: function() {
                    return this.array.length
                },
                all: function() {
                    return this.array
                },
                _reset: function() {
                    this.length = 0, this.hash = {}, this.array = []
                }
            }, Collection.extend = function(protoProps) {
                var child, parent = this;
                child = protoProps && protoProps.hasOwnProperty("constructor") ? protoProps.constructor : function() {
                    return parent.apply(this, arguments)
                };
                var Surrogate = function() {
                    this.constructor = child
                };
                return Surrogate.prototype = parent.prototype, child.prototype = new Surrogate, protoProps && angular.extend(child.prototype, protoProps), child.extend = parent.extend, child.getInstance = Collection.getInstance, child._super = parent.prototype, child
            }, Collection.getInstance = function(options) {
                return new this(options)
            }, Collection
        }])
    }(window, window.angular), ! function() {
        function a(a) {
            if (!a.__listeners) {
                a.upload || (a.upload = {}), a.__listeners = [];
                var b = a.upload.addEventListener;
                a.upload.addEventListener = function(c, d) {
                    a.__listeners[c] = d, b && b.apply(this, arguments)
                }
            }
        }
        var b = function() {
                try {
                    var a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
                    if (a) return !0
                } catch (b) {
                    if (void 0 != navigator.mimeTypes["application/x-shockwave-flash"]) return !0
                }
                return !1
            },
            c = function(a, b) {
                window.XMLHttpRequest.prototype[a] = b(window.XMLHttpRequest.prototype[a])
            };
        if (window.XMLHttpRequest && (!window.FormData || window.FileAPI && FileAPI.forceLoad ? (c("open", function(b) {
                return function(c, d, e) {
                    a(this), this.__url = d;
                    try {
                        b.apply(this, [c, d, e])
                    } catch (f) {
                        f.message.indexOf("Access is denied") > -1 && b.apply(this, [c, "_fix_for_ie_crossdomain__", e])
                    }
                }
            }), c("getResponseHeader", function(a) {
                return function(b) {
                    return this.__fileApiXHR ? this.__fileApiXHR.getResponseHeader(b) : a.apply(this, [b])
                }
            }), c("getAllResponseHeaders", function(a) {
                return function() {
                    return this.__fileApiXHR ? this.__fileApiXHR.abort() : null == a ? null : a.apply(this)
                }
            }), c("abort", function(a) {
                return function() {
                    return this.__fileApiXHR ? this.__fileApiXHR.abort() : null == a ? null : a.apply(this)
                }
            }), c("setRequestHeader", function(b) {
                return function(c, d) {
                    if ("__setXHR_" === c) {
                        a(this);
                        var e = d(this);
                        e instanceof Function && e(this)
                    } else this.__requestHeaders = this.__requestHeaders || {}, this.__requestHeaders[c] = d, b.apply(this, arguments)
                }
            }), c("send", function(a) {
                return function() {
                    var c = this;
                    if (arguments[0] && arguments[0].__isShim) {
                        var d = arguments[0],
                            e = {
                                url: c.__url,
                                complete: function(a, b) {
                                    !a && c.__listeners.load && c.__listeners.load({
                                        type: "load",
                                        loaded: c.__loaded,
                                        total: c.__total,
                                        target: c,
                                        lengthComputable: !0
                                    }), !a && c.__listeners.loadend && c.__listeners.loadend({
                                        type: "loadend",
                                        loaded: c.__loaded,
                                        total: c.__total,
                                        target: c,
                                        lengthComputable: !0
                                    }), "abort" === a && c.__listeners.abort && c.__listeners.abort({
                                        type: "abort",
                                        loaded: c.__loaded,
                                        total: c.__total,
                                        target: c,
                                        lengthComputable: !0
                                    }), void 0 !== b.status && Object.defineProperty(c, "status", {
                                        get: function() {
                                            return b.status
                                        }
                                    }), void 0 !== b.statusText && Object.defineProperty(c, "statusText", {
                                        get: function() {
                                            return b.statusText
                                        }
                                    }), Object.defineProperty(c, "readyState", {
                                        get: function() {
                                            return 4
                                        }
                                    }), void 0 !== b.response && Object.defineProperty(c, "response", {
                                        get: function() {
                                            return b.response
                                        }
                                    }), Object.defineProperty(c, "responseText", {
                                        get: function() {
                                            return b.responseText
                                        }
                                    }), Object.defineProperty(c, "response", {
                                        get: function() {
                                            return b.responseText
                                        }
                                    }), c.__fileApiXHR = b, c.onreadystatechange && c.onreadystatechange()
                                },
                                fileprogress: function(a) {
                                    a.target = c, c.__listeners.progress && c.__listeners.progress(a), c.__total = a.total, c.__loaded = a.loaded
                                },
                                headers: c.__requestHeaders
                            };
                        e.data = {}, e.files = {};
                        for (var f = 0; f < d.data.length; f++) {
                            var g = d.data[f];
                            null != g.val && null != g.val.name && null != g.val.size && null != g.val.type ? e.files[g.key] = g.val : e.data[g.key] = g.val
                        }
                        setTimeout(function() {
                            if (!b()) throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
                            c.__fileApiXHR = FileAPI.upload(e)
                        }, 1)
                    } else a.apply(c, arguments)
                }
            })) : c("setRequestHeader", function(a) {
                return function(b, c) {
                    if ("__setXHR_" === b) {
                        var d = c(this);
                        d instanceof Function && d(this)
                    } else a.apply(this, arguments)
                }
            }), window.XMLHttpRequest.__isShim = !0), !window.FormData || window.FileAPI && FileAPI.forceLoad) {
            var d = function(a) {
                    if (!b()) throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
                    var c = angular.element(a);
                    if (!c.hasClass("js-fileapi-wrapper") && (null != a.getAttribute("ng-file-select") || null != a.getAttribute("data-ng-file-select")))
                        if (FileAPI.wrapInsideDiv) {
                            var d = document.createElement("div");
                            d.innerHTML = '<div class="js-fileapi-wrapper" style="position:relative; overflow:hidden"></div>', d = d.firstChild;
                            var e = a.parentNode;
                            e.insertBefore(d, a), e.removeChild(a), d.appendChild(a)
                        } else c.addClass("js-fileapi-wrapper")
                },
                e = function(a) {
                    return function(b) {
                        for (var c = FileAPI.getFiles(b), d = 0; d < c.length; d++) void 0 === c[d].size && (c[d].size = 0), void 0 === c[d].name && (c[d].name = "file"), void 0 === c[d].type && (c[d].type = "undefined");
                        b.target || (b.target = {}), b.target.files = c, b.target.files != c && (b.__files_ = c), (b.__files_ || b.target.files).item = function(a) {
                            return (b.__files_ || b.target.files)[a] || null
                        }, a && a.apply(this, [b])
                    }
                },
                f = function(a, b) {
                    return ("change" === b.toLowerCase() || "onchange" === b.toLowerCase()) && "file" == a.getAttribute("type")
                };
            HTMLInputElement.prototype.addEventListener && (HTMLInputElement.prototype.addEventListener = function(a) {
                    return function(b, c, g, h) {
                        f(this, b) ? (d(this), a.apply(this, [b, e(c), g, h])) : a.apply(this, [b, c, g, h])
                    }
                }(HTMLInputElement.prototype.addEventListener)), HTMLInputElement.prototype.attachEvent && (HTMLInputElement.prototype.attachEvent = function(a) {
                    return function(b, c) {
                        f(this, b) ? (d(this), window.jQuery ? angular.element(this).bind("change", e(null)) : a.apply(this, [b, e(c)])) : a.apply(this, [b, c])
                    }
                }(HTMLInputElement.prototype.attachEvent)), window.FormData = FormData = function() {
                    return {
                        append: function(a, b, c) {
                            this.data.push({
                                key: a,
                                val: b,
                                name: c
                            })
                        },
                        data: [],
                        __isShim: !0
                    }
                },
                function() {
                    if (window.FileAPI || (window.FileAPI = {}), FileAPI.forceLoad && (FileAPI.html5 = !1), !FileAPI.upload) {
                        var a, c, d, e, f, g = document.createElement("script"),
                            h = document.getElementsByTagName("script");
                        if (window.FileAPI.jsUrl) a = window.FileAPI.jsUrl;
                        else if (window.FileAPI.jsPath) c = window.FileAPI.jsPath;
                        else
                            for (d = 0; d < h.length; d++)
                                if (f = h[d].src, e = f.indexOf("angular-file-upload-shim.js"), -1 == e && (e = f.indexOf("angular-file-upload-shim.min.js")), e > -1) {
                                    c = f.substring(0, e);
                                    break
                                }
                        null == FileAPI.staticPath && (FileAPI.staticPath = c), g.setAttribute("src", a || c + "FileAPI.min.js"), document.getElementsByTagName("head")[0].appendChild(g), FileAPI.hasFlash = b()
                    }
                }()
        }
        window.FileReader || (window.FileReader = function() {
            function a(a, c) {
                var d = {
                    type: a,
                    target: b,
                    loaded: c.loaded,
                    total: c.total,
                    error: c.error
                };
                return null != c.result && (d.target.result = c.result), d
            }
            var b = this,
                c = !1;
            this.listeners = {}, this.addEventListener = function(a, c) {
                b.listeners[a] = b.listeners[a] || [], b.listeners[a].push(c)
            }, this.removeEventListener = function(a, c) {
                b.listeners[a] && b.listeners[a].splice(b.listeners[a].indexOf(c), 1)
            }, this.dispatchEvent = function(a) {
                var c = b.listeners[a.type];
                if (c)
                    for (var d = 0; d < c.length; d++) c[d].call(b, a)
            }, this.onabort = this.onerror = this.onload = this.onloadstart = this.onloadend = this.onprogress = null;
            var d = function(d) {
                if (c || (c = !0, b.onloadstart && this.onloadstart(a("loadstart", d))), "load" === d.type) {
                    b.onloadend && b.onloadend(a("loadend", d));
                    var e = a("load", d);
                    b.onload && b.onload(e), b.dispatchEvent(e)
                } else if ("progress" === d.type) {
                    var e = a("progress", d);
                    b.onprogress && b.onprogress(e), b.dispatchEvent(e)
                } else {
                    var e = a("error", d);
                    b.onerror && b.onerror(e), b.dispatchEvent(e)
                }
            };
            this.readAsArrayBuffer = function(a) {
                FileAPI.readAsBinaryString(a, d)
            }, this.readAsBinaryString = function(a) {
                FileAPI.readAsBinaryString(a, d)
            }, this.readAsDataURL = function(a) {
                FileAPI.readAsDataURL(a, d)
            }, this.readAsText = function(a) {
                FileAPI.readAsText(a, d)
            }
        })
    }(), ! function() {
        var a = angular.module("angularFileUpload", []);
        a.service("$upload", ["$http", "$q", "$timeout", function(a, b, c) {
            function d(d) {
                d.method = d.method || "POST", d.headers = d.headers || {}, d.transformRequest = d.transformRequest || function(b, c) {
                    return window.ArrayBuffer && b instanceof window.ArrayBuffer ? b : a.defaults.transformRequest[0](b, c)
                };
                var e = b.defer();
                window.XMLHttpRequest.__isShim && (d.headers.__setXHR_ = function() {
                    return function(a) {
                        a && (d.__XHR = a, d.xhrFn && d.xhrFn(a), a.upload.addEventListener("progress", function(a) {
                            e.notify(a)
                        }, !1), a.upload.addEventListener("load", function(a) {
                            a.lengthComputable && e.notify(a)
                        }, !1))
                    }
                }), a(d).then(function(a) {
                    e.resolve(a)
                }, function(a) {
                    e.reject(a)
                }, function(a) {
                    e.notify(a)
                });
                var f = e.promise;
                return f.success = function(a) {
                    return f.then(function(b) {
                        a(b.data, b.status, b.headers, d)
                    }), f
                }, f.error = function(a) {
                    return f.then(null, function(b) {
                        a(b.data, b.status, b.headers, d)
                    }), f
                }, f.progress = function(a) {
                    return f.then(null, null, function(b) {
                        a(b)
                    }), f
                }, f.abort = function() {
                    return d.__XHR && c(function() {
                        d.__XHR.abort()
                    }), f
                }, f.xhr = function(a) {
                    return d.xhrFn = function(b) {
                        return function() {
                            b && b.apply(f, arguments), a.apply(f, arguments)
                        }
                    }(d.xhrFn), f
                }, f
            }
            this.upload = function(b) {
                b.headers = b.headers || {}, b.headers["Content-Type"] = void 0, b.transformRequest = b.transformRequest || a.defaults.transformRequest;
                var c = new FormData,
                    e = b.transformRequest,
                    f = b.data;
                return b.transformRequest = function(a, c) {
                    if (f)
                        if (b.formDataAppender)
                            for (var d in f) {
                                var g = f[d];
                                b.formDataAppender(a, d, g)
                            } else
                                for (var d in f) {
                                    var g = f[d];
                                    if ("function" == typeof e) g = e(g, c);
                                    else
                                        for (var h = 0; h < e.length; h++) {
                                            var i = e[h];
                                            "function" == typeof i && (g = i(g, c))
                                        }
                                    a.append(d, g)
                                }
                        if (null != b.file) {
                            var j = b.fileFormDataName || "file";
                            if ("[object Array]" === Object.prototype.toString.call(b.file))
                                for (var k = "[object String]" === Object.prototype.toString.call(j), h = 0; h < b.file.length; h++) a.append(k ? j : j[h], b.file[h], b.fileName && b.fileName[h] || b.file[h].name);
                            else a.append(j, b.file, b.fileName || b.file.name)
                        }
                    return a
                }, b.data = c, d(b)
            }, this.http = function(a) {
                return d(a)
            }
        }]), a.directive("ngFileSelect", ["$parse", "$timeout", function(a, b) {
            return function(c, d, e) {
                var f = a(e.ngFileSelect);
                if ("input" !== d[0].tagName.toLowerCase() || "file" !== (d.attr("type") && d.attr("type").toLowerCase())) {
                    for (var g = angular.element('<input type="file">'), h = 0; h < d[0].attributes.length; h++) g.attr(d[0].attributes[h].name, d[0].attributes[h].value);
                    d.attr("data-multiple") && g.attr("multiple", "true"), g.css("top", 0).css("bottom", 0).css("left", 0).css("right", 0).css("width", "100%").css("opacity", 0).css("position", "absolute").css("filter", "alpha(opacity=0)"), d.append(g), ("" === d.css("position") || "static" === d.css("position")) && d.css("position", "relative"), d = g
                }
                d.bind("change", function(a) {
                    var d, e, g = [];
                    if (d = a.__files_ || a.target.files, null != d)
                        for (e = 0; e < d.length; e++) g.push(d.item(e));
                    b(function() {
                        f(c, {
                            $files: g,
                            $event: a
                        })
                    })
                })
            }
        }]), a.directive("ngFileDropAvailable", ["$parse", "$timeout", function(a, b) {
            return function(c, d, e) {
                if ("draggable" in document.createElement("span")) {
                    var f = a(e.ngFileDropAvailable);
                    b(function() {
                        f(c)
                    })
                }
            }
        }]), a.directive("ngFileDrop", ["$parse", "$timeout", "$location", function(a, b, c) {
            return function(d, e, f) {
                function g(a) {
                    return /^[\000-\177]*$/.test(a)
                }

                function h(a, d) {
                    var e = [],
                        f = a.dataTransfer.items;
                    if (f && f.length > 0 && f[0].webkitGetAsEntry && "file" != c.protocol())
                        for (var h = 0; h < f.length; h++) {
                            var j = f[h].webkitGetAsEntry();
                            null != j && (g(j.name) ? i(e, j) : f[h].webkitGetAsEntry().isDirectory || e.push(f[h].getAsFile()))
                        } else {
                            var k = a.dataTransfer.files;
                            if (null != k)
                                for (var h = 0; h < k.length; h++) e.push(k.item(h))
                        }! function m(a) {
                            b(function() {
                                l ? m(10) : d(e)
                            }, a || 0)
                        }()
                }

                function i(a, b, c) {
                    if (null != b)
                        if (b.isDirectory) {
                            var d = b.createReader();
                            l++, d.readEntries(function(d) {
                                for (var e = 0; e < d.length; e++) i(a, d[e], (c ? c : "") + b.name + "/");
                                l--
                            })
                        } else l++, b.file(function(b) {
                            l--, b._relativePath = (c ? c : "") + b.name, a.push(b)
                        })
                }
                if ("draggable" in document.createElement("span")) {
                    var j = null;
                    e[0].addEventListener("dragover", function(c) {
                        if (c.stopPropagation(), c.preventDefault(), b.cancel(j), !e[0].__drag_over_class_)
                            if (f.ngFileDragOverClass.search(/\) *$/) > -1) {
                                dragOverClassFn = a(f.ngFileDragOverClass);
                                var g = dragOverClassFn(d, {
                                    $event: c
                                });
                                e[0].__drag_over_class_ = g
                            } else e[0].__drag_over_class_ = f.ngFileDragOverClass || "dragover";
                        e.addClass(e[0].__drag_over_class_)
                    }, !1), e[0].addEventListener("dragenter", function(a) {
                        a.stopPropagation(), a.preventDefault()
                    }, !1), e[0].addEventListener("dragleave", function() {
                        j = b(function() {
                            e.removeClass(e[0].__drag_over_class_), e[0].__drag_over_class_ = null
                        }, f.ngFileDragOverDelay || 1)
                    }, !1);
                    var k = a(f.ngFileDrop);
                    e[0].addEventListener("drop", function(a) {
                        a.stopPropagation(), a.preventDefault(), e.removeClass(e[0].__drag_over_class_), e[0].__drag_over_class_ = null, h(a, function(b) {
                            k(d, {
                                $files: b,
                                $event: a
                            })
                        })
                    }, !1);
                    var l = 0
                }
            }
        }])
    }(), angular.module("angular-growl", []), angular.module("angular-growl").directive("growl", ["$rootScope", function(a) {
        "use strict";
        return {
            restrict: "A",
            template: '<div class="growl">	<div class="growl-item alert" ng-repeat="message in messages" ng-class="computeClasses(message)">		<button type="button" class="close" ng-click="deleteMessage(message)">&times;</button>       <div ng-switch="message.enableHtml">           <div ng-switch-when="true" ng-bind-html="message.text"></div>           <div ng-switch-default ng-bind="message.text"></div>       </div>	</div></div>',
            replace: !1,
            scope: !0,
            controller: ["$scope", "$timeout", "growl", function(b, c, d) {
                function e(a) {
                    b.messages.push(a), a.ttl && -1 !== a.ttl && c(function() {
                        b.deleteMessage(a)
                    }, a.ttl)
                }
                var f = d.onlyUnique();
                b.messages = [], a.$on("growlMessage", function(a, c) {
                    var d;
                    f ? (angular.forEach(b.messages, function(a) {
                        c.text === a.text && c.severity === a.severity && (d = !0)
                    }), d || e(c)) : e(c)
                }), b.deleteMessage = function(a) {
                    var c = b.messages.indexOf(a);
                    c > -1 && b.messages.splice(c, 1)
                }, b.computeClasses = function(a) {
                    return {
                        "alert-success": "success" === a.severity,
                        "alert-error": "error" === a.severity,
                        "alert-danger": "error" === a.severity,
                        "alert-info": "info" === a.severity,
                        "alert-warning": "warn" === a.severity
                    }
                }
            }]
        }
    }]), angular.module("angular-growl").provider("growl", function() {
        "use strict";
        var a = null,
            b = !1,
            c = "messages",
            d = "text",
            e = "severity",
            f = !0;
        this.globalTimeToLive = function(b) {
            a = b
        }, this.globalEnableHtml = function(a) {
            b = a
        }, this.messagesKey = function(a) {
            c = a
        }, this.messageTextKey = function(a) {
            d = a
        }, this.messageSeverityKey = function(a) {
            e = a
        }, this.onlyUniqueMessages = function(a) {
            f = a
        }, this.serverMessagesInterceptor = ["$q", "growl", function(a, b) {
            function d(a) {
                a.data[c] && a.data[c].length > 0 && b.addServerMessages(a.data[c])
            }

            function e(a) {
                return d(a), a
            }

            function f(b) {
                return d(b), a.reject(b)
            }
            return function(a) {
                return a.then(e, f)
            }
        }], this.$get = ["$rootScope", "$filter", function(c, g) {
            function h(a) {
                p && (a.text = p(a.text)), c.$broadcast("growlMessage", a)
            }

            function i(c, d, e) {
                var f, g = d || {};
                f = {
                    text: c,
                    severity: e,
                    ttl: g.ttl || a,
                    enableHtml: g.enableHtml || b
                }, h(f)
            }

            function j(a, b) {
                i(a, b, "warn")
            }

            function k(a, b) {
                i(a, b, "error")
            }

            function l(a, b) {
                i(a, b, "info")
            }

            function m(a, b) {
                i(a, b, "success")
            }

            function n(a) {
                var b, c, f, g;
                for (g = a.length, b = 0; g > b; b++)
                    if (c = a[b], c[d] && c[e]) {
                        switch (c[e]) {
                            case "warn":
                                f = "warn";
                                break;
                            case "success":
                                f = "success";
                                break;
                            case "info":
                                f = "info";
                                break;
                            case "error":
                                f = "error"
                        }
                        i(c[d], void 0, f)
                    }
            }

            function o() {
                return f
            }
            var p;
            try {
                p = g("translate")
            } catch (q) {}
            return {
                addWarnMessage: j,
                addErrorMessage: k,
                addInfoMessage: l,
                addSuccessMessage: m,
                addServerMessages: n,
                onlyUnique: o
            }
        }]
    }), ! function() {
        "use strict";
        angular.module("wu.masonry", []).controller("MasonryCtrl", ["$scope", "$element", "$timeout", function(a, b, c) {
            function d(a) {
                a.addClass("loaded")
            }
            var e = {},
                f = [],
                g = !1,
                h = this,
                i = null;
            this.preserveOrder = !1, this.scheduleMasonryOnce = function() {
                var a = arguments,
                    b = f.filter(function(b) {
                        return b[0] === a[0]
                    }).length > 0;
                b || this.scheduleMasonry.apply(null, arguments)
            }, this.scheduleMasonry = function() {
                i && c.cancel(i), f.push([].slice.call(arguments)), i = c(function() {
                    g || (f.forEach(function(a) {
                        b.masonry.apply(b, a)
                    }), f = [])
                }, 30)
            }, this.appendBrick = function(a, c) {
                function f() {
                    0 === Object.keys(e).length && b.masonry("resize"), void 0 === e[c] && (e[c] = !0, d(a), b.masonry("appended", a, !0))
                }

                function i() {
                    h.scheduleMasonryOnce("layout")
                }
                g || (h.preserveOrder ? (f(), a.imagesLoaded(i)) : a.imagesLoaded(function() {
                    f(), i()
                }))
            }, this.removeBrick = function(a, c) {
                g || (delete e[a], b.masonry("remove", c), this.scheduleMasonryOnce("layout"))
            }, this.destroy = function() {
                g = !0, b.data("masonry") && b.masonry("destroy"), a.$emit("masonry.destroyed"), e = []
            }, this.reload = function() {
                b.masonry(), a.$emit("masonry.reloaded")
            }
        }]).directive("masonry", function() {
            return {
                restrict: "AE",
                controller: "MasonryCtrl",
                link: {
                    pre: function(a, b, c, d) {
                        var e = a.$eval(c.masonry || c.masonryOptions),
                            f = angular.extend({
                                itemSelector: c.itemSelector || ".masonry-brick",
                                columnWidth: parseInt(c.columnWidth, 10)
                            }, e || {});
                        b.masonry(f);
                        var g = a.$eval(c.preserveOrder);
                        d.preserveOrder = g !== !1 && void 0 !== c.preserveOrder, a.$emit("masonry.created", b), a.$on("$destroy", d.destroy)
                    }
                }
            }
        }).directive("masonryBrick", function() {
            return {
                restrict: "AC",
                require: "^masonry",
                scope: !0,
                link: {
                    pre: function(a, b, c, d) {
                        var e, f = a.$id;
                        d.appendBrick(b, f), b.on("$destroy", function() {
                            d.removeBrick(f, b)
                        }), a.$on("masonry.reload", function() {
                            d.reload()
                        }), a.$watch("$index", function() {
                            void 0 !== e && e !== a.$index && (d.scheduleMasonryOnce("reloadItems"), d.scheduleMasonryOnce("layout")), e = a.$index
                        })
                    }
                }
            }
        })
    }(),
    function(ng) {
        "use strict";
        ng.module("vcRecaptcha", [])
    }(angular),
    function(ng, Recaptcha) {
        "use strict";
        var app = ng.module("vcRecaptcha");
        app.service("vcRecaptchaService", ["$timeout", "$log", function($timeout, $log) {
            var callback;
            return {
                create: function(elm, key, fn, conf) {
                    callback = fn, conf.callback = fn, Recaptcha.create(key, elm, conf)
                },
                reload: function(should_focus) {
                    Recaptcha.reload(should_focus && "t"), $timeout(callback, 1e3)
                },
                data: function() {
                    return {
                        response: Recaptcha.get_response(),
                        challenge: Recaptcha.get_challenge()
                    }
                },
                destroy: function() {
                    Recaptcha.destroy()
                }
            }
        }])
    }(angular, Recaptcha),
    function(ng, Recaptcha) {
        "use strict";
        var app = ng.module("vcRecaptcha");
        app.directive("vcRecaptcha", ["$log", "$timeout", "vcRecaptchaService", function($log, $timeout, vcRecaptchaService) {
            return {
                restrict: "A",
                require: "?ngModel",
                scope: {
                    key: "="
                },
                link: function(scope, elm, attrs, ctrl) {
                    {
                        var response_input, challenge_input, captcha_created = !1,
                            refresh = function() {
                                ctrl && ctrl.$setViewValue({
                                    response: response_input.val(),
                                    challenge: challenge_input.val()
                                })
                            },
                            reload = function() {
                                var inputs = elm.find("input");
                                challenge_input = angular.element(inputs[0]), response_input = angular.element(inputs[1]), refresh()
                            },
                            callback = function() {
                                reload(), response_input.bind("keyup", function() {
                                    scope.$apply(refresh)
                                }), ctrl && (ctrl.$render = function() {
                                    response_input.val(ctrl.$viewValue.response), challenge_input.val(ctrl.$viewValue.challenge)
                                }), elm.bind("click", function() {
                                    $timeout(function() {
                                        scope.$apply(reload)
                                    }, 1e3)
                                })
                            };
                        Recaptcha.reload
                    }
                    if (!attrs.hasOwnProperty("key")) throw 'You need to set the "key" attribute to your public reCaptcha key. If you don\'t have a key, please get one from https://www.google.com/recaptcha/admin/create';
                    scope.$watch("key", function(key, old) {
                        if (key && !captcha_created) {
                            if (40 !== key.length) throw 'The "key" should be set to your public reCaptcha key. If you don\'t have a key, please get one from https://www.google.com/recaptcha/admin/create';
                            vcRecaptchaService.create(elm[0], scope.key, callback, {
                                tabindex: attrs.tabindex,
                                theme: attrs.theme,
                                lang: attrs.lang || null
                            }), captcha_created = !0
                        }
                    })
                }
            }
        }])
    }(angular, Recaptcha),
    function() {
        var MODULE_NAME, SLIDER_TAG, angularize, bindHtml, gap, halfWidth, hide, inputEvents, module, offset, offsetLeft, pixelize, qualifiedDirectiveDefinition, roundStep, show, sliderDirective, width;
        MODULE_NAME = "uiSlider", SLIDER_TAG = "slider", angularize = function(element) {
            return angular.element(element)
        }, pixelize = function(position) {
            return "" + position + "px"
        }, hide = function(element) {
            return element.css({
                opacity: 0
            })
        }, show = function(element) {
            return element.css({
                opacity: 1
            })
        }, offset = function(element, position) {
            return element.css({
                left: position
            })
        }, halfWidth = function(element) {
            return element[0].offsetWidth / 2
        }, offsetLeft = function(element) {
            return element[0].offsetLeft
        }, width = function(element) {
            return element[0].offsetWidth
        }, gap = function(element1, element2) {
            return offsetLeft(element2) - offsetLeft(element1) - width(element1)
        }, bindHtml = function(element, html) {
            return element.attr("ng-bind-html", html)
        }, roundStep = function(value, precision, step, floor) {
            var decimals, remainder, roundedValue, steppedValue;
            return null == floor && (floor = 0), null == step && (step = 1 / Math.pow(10, precision)), remainder = (value - floor) % step, steppedValue = remainder > step / 2 ? value + step - remainder : value - remainder, decimals = Math.pow(10, precision), roundedValue = steppedValue * decimals / decimals, roundedValue.toFixed(precision)
        }, inputEvents = {
            mouse: {
                start: "mousedown",
                move: "mousemove",
                end: "mouseup"
            },
            touch: {
                start: "touchstart",
                move: "touchmove",
                end: "touchend"
            }
        }, sliderDirective = function($timeout) {
            return {
                restrict: "EA",
                scope: {
                    floor: "@",
                    ceiling: "@",
                    step: "@",
                    precision: "@",
                    ngModel: "=?",
                    ngModelLow: "=?",
                    ngModelHigh: "=?",
                    translate: "&",
                    moveBegin: "=?",
                    moveEnd: "=?"
                },
                template: '<span class="bar"></span><span class="bar selection"></span><span class="pointer"></span><span class="pointer"></span><span class="sliderbubble selection"></span><span ng-bind-html="translate({value: floor})" class="sliderbubble limit"></span><span ng-bind-html="translate({value: ceiling})" class="sliderbubble limit"></span><span class="sliderbubble"></span><span class="sliderbubble"></span><span class="sliderbubble"></span>',
                compile: function(element, attributes) {
                    var ceilBub, cmbBub, e, flrBub, fullBar, highBub, lowBub, maxPtr, minPtr, range, refHigh, refLow, selBar, selBub, watchables, _i, _len, _ref, _ref1;
                    if (attributes.translate && -1 === attributes.translate.indexOf("(value)") && attributes.$set("translate", "" + attributes.translate + "(value)"), range = null == attributes.ngModel && null != attributes.ngModelLow && null != attributes.ngModelHigh, _ref = function() {
                            var _i, _len, _ref, _results;
                            for (_ref = element.children(), _results = [], _i = 0, _len = _ref.length; _len > _i; _i++) e = _ref[_i], _results.push(angularize(e));
                            return _results
                        }(), fullBar = _ref[0], selBar = _ref[1], minPtr = _ref[2], maxPtr = _ref[3], selBub = _ref[4], flrBub = _ref[5], ceilBub = _ref[6], lowBub = _ref[7], highBub = _ref[8], cmbBub = _ref[9], refLow = range ? "ngModelLow" : "ngModel", refHigh = "ngModelHigh", bindHtml(selBub, "'" + Translator.get("Range") + ": ' + translate({value: diff})"), bindHtml(lowBub, "translate({value: " + refLow + "})"), bindHtml(highBub, "translate({value: " + refHigh + "})"), bindHtml(cmbBub, "translate({value: " + refLow + "}) + ' - ' + translate({value: " + refHigh + "})"), !range)
                        for (_ref1 = [selBar, maxPtr, selBub, highBub, cmbBub], _i = 0, _len = _ref1.length; _len > _i; _i++)
                            if (element = _ref1[_i], _ref1[_i] !== selBar) {
                                var rangeOneDir = !0;
                                element.remove()
                            }
                    return watchables = [refLow, "floor", "ceiling"], range && watchables.push(refHigh), {
                        post: function(scope, element, attributes) {
                            var barWidth, boundToInputs, dimensions, maxOffset, maxValue, minOffset, minValue, ngDocument, offsetRange, pointerHalfWidth, updateDOM, valueRange, w, _j, _len1;
                            for (boundToInputs = !1, ngDocument = angularize(document), attributes.translate || (scope.translate = function(value) {
                                    return value.value
                                }), pointerHalfWidth = barWidth = minOffset = maxOffset = minValue = maxValue = valueRange = offsetRange = void 0, dimensions = function() {
                                    var value, _j, _len1, _ref2, _ref3;
                                    for (null == (_ref2 = scope.precision) && (scope.precision = 0), null == (_ref3 = scope.step) && (scope.step = 1), _j = 0, _len1 = watchables.length; _len1 > _j; _j++) value = watchables[_j], scope[value] = roundStep(parseFloat(scope[value]), parseInt(scope.precision), parseFloat(scope.step), parseFloat(scope.floor));
                                    return scope.diff = roundStep(scope[refHigh] - scope[refLow], parseInt(scope.precision), parseFloat(scope.step), parseFloat(scope.floor)), pointerHalfWidth = halfWidth(minPtr), barWidth = width(fullBar), minOffset = 0, maxOffset = barWidth - width(minPtr), minValue = parseFloat(attributes.floor), maxValue = parseFloat(attributes.ceiling), valueRange = maxValue - minValue, offsetRange = maxOffset - minOffset
                                }, updateDOM = function() {
                                    var adjustSliderBubbles, bindToInputEvents, fitToBar, percentOffset, percentToOffset, percentValue, setBindings, setPointers;
                                    return dimensions(), percentOffset = function(offset) {
                                        return (offset - minOffset) / offsetRange * 100
                                    }, percentValue = function(value) {
                                        return (value - minValue) / valueRange * 100
                                    }, percentToOffset = function(percent) {
                                        return pixelize(percent * offsetRange / 100)
                                    }, fitToBar = function(element) {
                                        return offset(element, pixelize(Math.min(Math.max(0, offsetLeft(element)), barWidth - width(element))))
                                    }, setPointers = function() {
                                        var newHighValue, newLowValue;
                                        return offset(ceilBub, pixelize(barWidth - width(ceilBub))), newLowValue = percentValue(scope[refLow]), offset(minPtr, percentToOffset(newLowValue)), offset(lowBub, pixelize(offsetLeft(minPtr) - halfWidth(lowBub) + pointerHalfWidth)), range ? (newHighValue = percentValue(scope[refHigh]), offset(maxPtr, percentToOffset(newHighValue)), offset(highBub, pixelize(offsetLeft(maxPtr) - halfWidth(highBub) + pointerHalfWidth)), offset(selBar, pixelize(offsetLeft(minPtr) + pointerHalfWidth)), selBar.css({
                                            width: percentToOffset(newHighValue - newLowValue)
                                        }), offset(selBub, pixelize(offsetLeft(selBar) + halfWidth(selBar) - halfWidth(selBub))), offset(cmbBub, pixelize(offsetLeft(selBar) + halfWidth(selBar) - halfWidth(cmbBub)))) : void(rangeOneDir && selBar.css({
                                            width: percentToOffset(newLowValue + 4)
                                        }))
                                    }, adjustSliderBubbles = function() {
                                        var bubToAdjust;
                                        return fitToBar(lowBub), bubToAdjust = highBub, range && (fitToBar(highBub), fitToBar(selBub), gap(lowBub, highBub) < 10 ? (hide(lowBub), hide(highBub), fitToBar(cmbBub), show(cmbBub), bubToAdjust = cmbBub) : (show(lowBub), show(highBub), hide(cmbBub), bubToAdjust = highBub)), gap(flrBub, lowBub) < 5 ? hide(flrBub) : range && gap(flrBub, bubToAdjust) < 5 ? hide(flrBub) : show(flrBub), gap(lowBub, ceilBub) < 5 ? hide(ceilBub) : range && gap(bubToAdjust, ceilBub) < 5 ? hide(ceilBub) : show(ceilBub)
                                    }, bindToInputEvents = function(pointer, ref, events) {
                                        var onEnd, onMove, onStart;
                                        return onEnd = function() {
                                            return pointer.removeClass("active"), "function" == typeof scope.moveEnd ? scope.moveEnd() : void 0 !== scope.moveEnd && scope.moveEnd, ngDocument.unbind(events.move), ngDocument.unbind(events.end)
                                        }, onMove = function(event) {
                                            var eventX, newOffset, newPercent, newValue;
                                            return eventX = event.clientX || event.touches[0].clientX, newOffset = eventX - element[0].getBoundingClientRect().left - pointerHalfWidth, newOffset = Math.max(Math.min(newOffset, maxOffset), minOffset), newPercent = percentOffset(newOffset), newValue = minValue + valueRange * newPercent / 100, range && (ref === refLow ? newValue > scope[refHigh] && (ref = refHigh, minPtr.removeClass("active"), maxPtr.addClass("active")) : newValue < scope[refLow] && (ref = refLow, maxPtr.removeClass("active"), minPtr.addClass("active"))), newValue = roundStep(newValue, parseInt(scope.precision), parseFloat(scope.step), parseFloat(scope.floor)), scope[ref] = newValue, scope.$apply()
                                        }, onStart = function(event) {
                                            return pointer.addClass("active"), dimensions(), event.stopPropagation(), event.preventDefault(), "function" == typeof scope.moveBegin ? scope.moveBegin() : void 0 !== scope.moveBegin && scope.moveBegin, ngDocument.bind(events.move, function(event) {
                                                onMove(event.originalEvent)
                                            }), ngDocument.bind(events.end, onEnd)
                                        }, pointer.bind(events.start, onStart)
                                    }, setBindings = function() {
                                        var bind, inputMethod, _j, _len1, _ref2, _results;
                                        for (boundToInputs = !0, bind = function(method) {
                                                return bindToInputEvents(minPtr, refLow, inputEvents[method]), bindToInputEvents(maxPtr, refHigh, inputEvents[method])
                                            }, _ref2 = ["touch", "mouse"], _results = [], _j = 0, _len1 = _ref2.length; _len1 > _j; _j++) inputMethod = _ref2[_j], _results.push(bind(inputMethod));
                                        return _results
                                    }, setPointers(), adjustSliderBubbles(), boundToInputs ? void 0 : setBindings()
                                }, $timeout(updateDOM), _j = 0, _len1 = watchables.length; _len1 > _j; _j++) w = watchables[_j], scope.$watch(w, updateDOM);
                            return window.addEventListener("resize", updateDOM)
                        }
                    }
                }
            }
        }, qualifiedDirectiveDefinition = ["$timeout", sliderDirective], (module = function(window, angular) {
            return angular.module(MODULE_NAME, ["ngSanitize"]).directive(SLIDER_TAG, qualifiedDirectiveDefinition)
        })(window, window.angular)
    }.call(this), angular.module("ngLocale", [], ["$provide", function($provide) {
        function getDecimals(n) {
            n += "";
            var i = n.indexOf(".");
            return -1 == i ? 0 : n.length - i - 1
        }

        function getVF(n, opt_precision) {
            var v = opt_precision;
            void 0 === v && (v = Math.min(getDecimals(n), 3));
            var base = Math.pow(10, v),
                f = (n * base | 0) % base;
            return {
                v: v,
                f: f
            }
        }
        var PLURAL_CATEGORY = {
            ZERO: "zero",
            ONE: "one",
            TWO: "two",
            FEW: "few",
            MANY: "many",
            OTHER: "other"
        };
        $provide.value("$locale", {
            DATETIME_FORMATS: {
                AMPMS: ["vorm.", "nachm."],
                DAY: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                MONTH: ["Januar", "Februar", "MÃ¤rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
                SHORTDAY: ["So.", "Mo.", "Di.", "Mi.", "Do.", "Fr.", "Sa."],
                SHORTMONTH: ["Jan.", "Feb.", "MÃ¤rz", "Apr.", "Mai", "Juni", "Juli", "Aug.", "Sep.", "Okt.", "Nov.", "Dez."],
                fullDate: "EEEE, d. MMMM y",
                longDate: "d. MMMM y",
                medium: "dd.MM.y HH:mm:ss",
                mediumDate: "dd.MM.y",
                mediumTime: "HH:mm:ss",
                "short": "dd.MM.yy HH:mm",
                shortDate: "dd.MM.yy",
                shortTime: "HH:mm"
            },
            NUMBER_FORMATS: {
                CURRENCY_SYM: "â‚¬",
                DECIMAL_SEP: ",",
                GROUP_SEP: ".",
                PATTERNS: [{
                    gSize: 3,
                    lgSize: 3,
                    maxFrac: 3,
                    minFrac: 0,
                    minInt: 1,
                    negPre: "-",
                    negSuf: "",
                    posPre: "",
                    posSuf: ""
                }, {
                    gSize: 3,
                    lgSize: 3,
                    maxFrac: 2,
                    minFrac: 2,
                    minInt: 1,
                    negPre: "-",
                    negSuf: "Â Â¤",
                    posPre: "",
                    posSuf: "Â Â¤"
                }]
            },
            id: "de",
            pluralCat: function(n, opt_precision) {
                var i = 0 | n,
                    vf = getVF(n, opt_precision);
                return 1 == i && 0 == vf.v ? PLURAL_CATEGORY.ONE : PLURAL_CATEGORY.OTHER
            }
        })
    }]), angular.module("monospaced.elastic", []).constant("msdElasticConfig", {
        append: ""
    }).directive("msdElastic", ["$timeout", "$window", "msdElasticConfig", function($timeout, $window, config) {
        "use strict";
        return {
            require: "ngModel",
            restrict: "A, C",
            link: function(scope, element, attrs, ngModel) {
                function initMirror() {
                    mirrored = ta, taStyle = getComputedStyle(ta), angular.forEach(copyStyle, function(val) {
                        mirrorStyle += val + ":" + taStyle.getPropertyValue(val) + ";"
                    }), mirror.setAttribute("style", mirrorStyle)
                }

                function adjust() {
                    var taHeight, mirrorHeight, width, overflow;
                    mirrored !== ta && initMirror(), active || (active = !0, mirror.value = ta.value + append, mirror.style.overflowY = ta.style.overflowY, taHeight = "" === ta.style.height ? "auto" : parseInt(ta.style.height, 10), width = parseInt(getComputedStyle(ta).getPropertyValue("width"), 10) - boxOuter.width, mirror.style.width = width + "px", mirrorHeight = mirror.scrollHeight, mirrorHeight > maxHeight ? (mirrorHeight = maxHeight, overflow = "scroll") : minHeight > mirrorHeight && (mirrorHeight = minHeight), mirrorHeight += boxOuter.height, ta.style.overflowY = overflow || "hidden", taHeight !== mirrorHeight && (ta.style.height = mirrorHeight + "px", scope.$emit("elastic:resize", $ta)), $timeout(function() {
                        active = !1
                    }, 1))
                }

                function forceAdjust() {
                    active = !1, adjust()
                }
                var ta = element[0],
                    $ta = element;
                if ("TEXTAREA" === ta.nodeName && $window.getComputedStyle) {
                    $ta.css({
                        overflow: "hidden",
                        "overflow-y": "hidden",
                        "word-wrap": "break-word"
                    });
                    var text = ta.value;
                    ta.value = "", ta.value = text;
                    var mirrored, active, fs = getComputedStyle(ta).getPropertyValue("font-size").replace("px", ""),
                        lh = ta.offsetHeight,
                        appendText = attrs.msdElastic || config.append,
                        append = "\\n" === appendText ? "\n" : appendText,
                        $win = angular.element($window),
                        mirrorStyle = "position: absolute; top: -999px; right: auto; bottom: auto; left: 0 ;overflow: hidden; -webkit-box-sizing: content-box;-moz-box-sizing: content-box; box-sizing: content-box;min-height: 0 !important; height: 0 !important; padding: 0;word-wrap: break-word; border: 0;",
                        $mirror = angular.element('<textarea tabindex="-1" style="' + mirrorStyle + '"/>').data("elastic", !0),
                        mirror = $mirror[0],
                        taStyle = getComputedStyle(ta),
                        resize = taStyle.getPropertyValue("resize"),
                        borderBox = "border-box" === taStyle.getPropertyValue("box-sizing") || "border-box" === taStyle.getPropertyValue("-moz-box-sizing") || "border-box" === taStyle.getPropertyValue("-webkit-box-sizing"),
                        boxOuter = borderBox ? {
                            width: parseInt(taStyle.getPropertyValue("border-right-width"), 10) + parseInt(taStyle.getPropertyValue("padding-right"), 10) + parseInt(taStyle.getPropertyValue("padding-left"), 10) + parseInt(taStyle.getPropertyValue("border-left-width"), 10),
                            height: parseInt(taStyle.getPropertyValue("border-top-width"), 10) + parseInt(taStyle.getPropertyValue("padding-top"), 10) + parseInt(taStyle.getPropertyValue("padding-bottom"), 10) + parseInt(taStyle.getPropertyValue("border-bottom-width"), 10)
                        } : {
                            width: 0,
                            height: 0
                        },
                        minHeightValue = parseInt(taStyle.getPropertyValue("min-height"), 10),
                        minHeight = Math.max(minHeightValue, lh) - boxOuter.height,
                        maxHeight = attrs.maxRows ? Math.ceil(fs * Math.round(attrs.maxRows / 2) * Math.round(lh / fs * 100) / 100 + 200 * fs / 300) : parseInt(taStyle.getPropertyValue("max-height"), 10),
                        copyStyle = ["font-family", "font-size", "font-weight", "font-style", "letter-spacing", "line-height", "text-transform", "word-spacing", "text-indent"];
                    $ta.data("elastic") || (maxHeight = maxHeight && maxHeight > 0 ? maxHeight : 9e4, mirror.parentNode !== document.body && angular.element(document.body).append(mirror), $ta.css({
                        resize: "none" === resize || "vertical" === resize ? "none" : "horizontal"
                    }).data("elastic", !0), ta.oninput = "onpropertychange" in ta && "oninput" in ta ? ta.onkeyup = adjust : adjust, $win.bind("resize", forceAdjust), scope.$watch(function() {
                        return ngModel.$modelValue
                    }, function(newValue) {
                        forceAdjust()
                    }), scope.$on("elastic:adjust", function() {
                        forceAdjust()
                    }), $timeout(adjust), scope.$on("$destroy", function() {
                        $mirror.remove(), $win.unbind("resize", forceAdjust)
                    }))
                }
            }
        }
    }]),
    function() {
        "use strict";
        angular.module("angular-loading-bar", ["cfp.loadingBarInterceptor"]), angular.module("chieffancypants.loadingBar", ["cfp.loadingBarInterceptor"]), angular.module("cfp.loadingBarInterceptor", ["cfp.loadingBar"]).config(["$httpProvider", function($httpProvider) {
            var interceptor = ["$q", "$cacheFactory", "$timeout", "$rootScope", "cfpLoadingBar", function($q, $cacheFactory, $timeout, $rootScope, cfpLoadingBar) {
                function setComplete() {
                    $timeout.cancel(startTimeout), cfpLoadingBar.complete(), reqsCompleted = 0, reqsTotal = 0
                }

                function isCached(config) {
                    var cache, defaultCache = $cacheFactory.get("$http"),
                        defaults = $httpProvider.defaults;
                    !config.cache && !defaults.cache || config.cache === !1 || "GET" !== config.method && "JSONP" !== config.method || (cache = angular.isObject(config.cache) ? config.cache : angular.isObject(defaults.cache) ? defaults.cache : defaultCache);
                    var cached = void 0 !== cache ? void 0 !== cache.get(config.url) : !1;
                    return void 0 !== config.cached && cached !== config.cached ? config.cached : (config.cached = cached, cached)
                }
                var startTimeout, reqsTotal = 0,
                    reqsCompleted = 0,
                    latencyThreshold = cfpLoadingBar.latencyThreshold;
                return {
                    request: function(config) {
                        return config.ignoreLoadingBar || isCached(config) || ($rootScope.$broadcast("cfpLoadingBar:loading", {
                            url: config.url
                        }), 0 === reqsTotal && (startTimeout = $timeout(function() {
                            cfpLoadingBar.start()
                        }, latencyThreshold)), reqsTotal++, cfpLoadingBar.set(reqsCompleted / reqsTotal)), config
                    },
                    response: function(response) {
                        return response.config.ignoreLoadingBar || isCached(response.config) || (reqsCompleted++, $rootScope.$broadcast("cfpLoadingBar:loaded", {
                            url: response.config.url
                        }), reqsCompleted >= reqsTotal ? setComplete() : cfpLoadingBar.set(reqsCompleted / reqsTotal)), response
                    },
                    responseError: function(rejection) {
                        return rejection.config.ignoreLoadingBar || isCached(rejection.config) || (reqsCompleted++, $rootScope.$broadcast("cfpLoadingBar:loaded", {
                            url: rejection.config.url
                        }), reqsCompleted >= reqsTotal ? setComplete() : cfpLoadingBar.set(reqsCompleted / reqsTotal)), $q.reject(rejection)
                    }
                }
            }];
            $httpProvider.interceptors.push(interceptor)
        }]), angular.module("cfp.loadingBar", []).provider("cfpLoadingBar", function() {
            this.includeSpinner = !0, this.includeBar = !0, this.latencyThreshold = 100, this.startSize = .02, this.parentSelector = "body", this.spinnerTemplate = '<div id="loading-bar-spinner"><div class="spinner-icon"></div></div>', this.loadingBarTemplate = '<div id="loading-bar"><div class="bar"><div class="peg"></div></div></div>', this.$get = ["$injector", "$document", "$timeout", "$rootScope", function($injector, $document, $timeout, $rootScope) {
                function _start() {
                    $animate || ($animate = $injector.get("$animate"));
                    var $parent = $document.find($parentSelector).eq(0);
                    $timeout.cancel(completeTimeout), started || ($rootScope.$broadcast("cfpLoadingBar:started"), started = !0, includeBar && $animate.enter(loadingBarContainer, $parent), includeSpinner && $animate.enter(spinner, $parent), _set(startSize))
                }

                function _set(n) {
                    if (started) {
                        var pct = 100 * n + "%";
                        loadingBar.css("width", pct), status = n, $timeout.cancel(incTimeout), incTimeout = $timeout(function() {
                            _inc()
                        }, 250)
                    }
                }

                function _inc() {
                    if (!(_status() >= 1)) {
                        var rnd = 0,
                            stat = _status();
                        rnd = stat >= 0 && .25 > stat ? (3 * Math.random() + 3) / 100 : stat >= .25 && .65 > stat ? 3 * Math.random() / 100 : stat >= .65 && .9 > stat ? 2 * Math.random() / 100 : stat >= .9 && .99 > stat ? .005 : 0;
                        var pct = _status() + rnd;
                        _set(pct)
                    }
                }

                function _status() {
                    return status
                }

                function _completeAnimation() {
                    status = 0, started = !1
                }

                function _complete() {
                    $animate || ($animate = $injector.get("$animate")), $rootScope.$broadcast("cfpLoadingBar:completed"), _set(1), $timeout.cancel(completeTimeout), completeTimeout = $timeout(function() {
                        var promise = $animate.leave(loadingBarContainer, _completeAnimation);
                        promise && promise.then && promise.then(_completeAnimation), $animate.leave(spinner)
                    }, 500)
                }
                var $animate, incTimeout, completeTimeout, $parentSelector = this.parentSelector,
                    loadingBarContainer = angular.element(this.loadingBarTemplate),
                    loadingBar = loadingBarContainer.find("div").eq(0),
                    spinner = angular.element(this.spinnerTemplate),
                    started = !1,
                    status = 0,
                    includeSpinner = this.includeSpinner,
                    includeBar = this.includeBar,
                    startSize = this.startSize;
                return {
                    start: _start,
                    set: _set,
                    status: _status,
                    inc: _inc,
                    complete: _complete,
                    includeSpinner: this.includeSpinner,
                    latencyThreshold: this.latencyThreshold,
                    parentSelector: this.parentSelector,
                    startSize: this.startSize
                }
            }]
        })
    }(),
    function() {
        "use strict";

        function getRequires(module) {
            var requires = [];
            return angular.forEach(module.requires, function(requireModule) {
                -1 === regModules.indexOf(requireModule) && requires.push(requireModule)
            }), requires
        }

        function moduleExists(moduleName) {
            if (!angular.isString(moduleName)) return !1;
            try {
                return ngModuleFct(moduleName)
            } catch (e) {
                if (/No module/.test(e) || e.message.indexOf("$injector:nomod") > -1) return !1
            }
        }

        function getModule(moduleName) {
            try {
                return ngModuleFct(moduleName)
            } catch (e) {
                throw (/No module/.test(e) || e.message.indexOf("$injector:nomod") > -1) && (e.message = 'The module "' + moduleName + '" that you are trying to load does not exist. ' + e.message), e
            }
        }

        function invokeQueue(providers, queue, moduleName, reconfig) {
            if (queue) {
                var i, len, args, provider;
                for (i = 0, len = queue.length; len > i; i++)
                    if (args = queue[i], angular.isArray(args)) {
                        if (null !== providers) {
                            if (!providers.hasOwnProperty(args[0])) throw new Error("unsupported provider " + args[0]);
                            provider = providers[args[0]]
                        }
                        var isNew = registerInvokeList(args, moduleName);
                        if ("invoke" !== args[1]) isNew && angular.isDefined(provider) && provider[args[1]].apply(provider, args[2]);
                        else {
                            var callInvoke = function(fct) {
                                var invoked = regConfigs.indexOf(moduleName + "-" + fct);
                                (-1 === invoked || reconfig) && (-1 === invoked && regConfigs.push(moduleName + "-" + fct), angular.isDefined(provider) && provider[args[1]].apply(provider, args[2]))
                            };
                            if (angular.isFunction(args[2][0])) callInvoke(args[2][0]);
                            else if (angular.isArray(args[2][0]))
                                for (var j = 0, jlen = args[2][0].length; jlen > j; j++) angular.isFunction(args[2][0][j]) && callInvoke(args[2][0][j])
                        }
                    }
            }
        }

        function register(providers, registerModules, params) {
            if (registerModules) {
                var k, moduleName, moduleFn, tempRunBlocks = [];
                for (k = registerModules.length - 1; k >= 0; k--)
                    if (moduleName = registerModules[k], "string" != typeof moduleName && (moduleName = getModuleName(moduleName)), moduleName && -1 === justLoaded.indexOf(moduleName)) {
                        var newModule = -1 === regModules.indexOf(moduleName);
                        if (moduleFn = ngModuleFct(moduleName), newModule && (regModules.push(moduleName), register(providers, moduleFn.requires, params)), moduleFn._runBlocks.length > 0)
                            for (runBlocks[moduleName] = []; moduleFn._runBlocks.length > 0;) runBlocks[moduleName].push(moduleFn._runBlocks.shift());
                        angular.isDefined(runBlocks[moduleName]) && (newModule || params.rerun) && (tempRunBlocks = tempRunBlocks.concat(runBlocks[moduleName])), invokeQueue(providers, moduleFn._invokeQueue, moduleName, params.reconfig), invokeQueue(providers, moduleFn._configBlocks, moduleName, params.reconfig), broadcast(newModule ? "ocLazyLoad.moduleLoaded" : "ocLazyLoad.moduleReloaded", moduleName), registerModules.pop(), justLoaded.push(moduleName)
                    }
                var instanceInjector = providers.getInstanceInjector();
                angular.forEach(tempRunBlocks, function(fn) {
                    instanceInjector.invoke(fn)
                })
            }
        }

        function registerInvokeList(args, moduleName) {
            var invokeList = args[2][0],
                type = args[1],
                newInvoke = !1;
            angular.isUndefined(regInvokes[moduleName]) && (regInvokes[moduleName] = {}), angular.isUndefined(regInvokes[moduleName][type]) && (regInvokes[moduleName][type] = {});
            var onInvoke = function(invokeName, signature) {
                    newInvoke = !0, regInvokes[moduleName][type][invokeName].push(signature), broadcast("ocLazyLoad.componentLoaded", [moduleName, type, invokeName])
                },
                signature = function(data) {
                    return angular.isArray(data) ? data.toString() : angular.isObject(data) ? JSON.stringify(data) : data.toString()
                };
            if (angular.isString(invokeList)) angular.isUndefined(regInvokes[moduleName][type][invokeList]) && (regInvokes[moduleName][type][invokeList] = []), -1 === regInvokes[moduleName][type][invokeList].indexOf(signature(args[2][1])) && onInvoke(invokeList, signature(args[2][1]));
            else {
                if (!angular.isObject(invokeList)) return !1;
                angular.forEach(invokeList, function(invoke) {
                    angular.isString(invoke) && (angular.isUndefined(regInvokes[moduleName][type][invoke]) && (regInvokes[moduleName][type][invoke] = []), -1 === regInvokes[moduleName][type][invoke].indexOf(signature(invokeList[1])) && onInvoke(invoke, signature(invokeList[1])))
                })
            }
            return newInvoke
        }

        function getModuleName(module) {
            var moduleName = null;
            return angular.isString(module) ? moduleName = module : angular.isObject(module) && module.hasOwnProperty("name") && angular.isString(module.name) && (moduleName = module.name), moduleName
        }

        function init(element) {
            if (0 === initModules.length) {
                var elements = [element],
                    names = ["ng:app", "ng-app", "x-ng-app", "data-ng-app"],
                    NG_APP_CLASS_REGEXP = /\sng[:\-]app(:\s*([\w\d_]+);?)?\s/,
                    append = function(elm) {
                        return elm && elements.push(elm)
                    };
                angular.forEach(names, function(name) {
                    names[name] = !0, append(document.getElementById(name)), name = name.replace(":", "\\:"), element[0].querySelectorAll && (angular.forEach(element[0].querySelectorAll("." + name), append), angular.forEach(element[0].querySelectorAll("." + name + "\\:"), append), angular.forEach(element[0].querySelectorAll("[" + name + "]"), append))
                }), angular.forEach(elements, function(elm) {
                    if (0 === initModules.length) {
                        var className = " " + element.className + " ",
                            match = NG_APP_CLASS_REGEXP.exec(className);
                        match ? initModules.push((match[2] || "").replace(/\s+/g, ",")) : angular.forEach(elm.attributes, function(attr) {
                            0 === initModules.length && names[attr.name] && initModules.push(attr.value)
                        })
                    }
                })
            }
            if (0 === initModules.length) throw "No module found during bootstrap, unable to init ocLazyLoad";
            var addReg = function addReg(moduleName) {
                if (-1 === regModules.indexOf(moduleName)) {
                    regModules.push(moduleName);
                    var mainModule = angular.module(moduleName);
                    invokeQueue(null, mainModule._invokeQueue, moduleName), invokeQueue(null, mainModule._configBlocks, moduleName), angular.forEach(mainModule.requires, addReg)
                }
            };
            angular.forEach(initModules, function(moduleName) {
                addReg(moduleName)
            }), initModules = [], angular.module = ngModuleFct
        }
        var regModules = ["ng"],
            initModules = [],
            regInvokes = {},
            regConfigs = [],
            justLoaded = [],
            runBlocks = {},
            ocLazyLoad = angular.module("oc.lazyLoad", ["ng"]),
            broadcast = angular.noop;
        ocLazyLoad.provider("$ocLazyLoad", ["$controllerProvider", "$provide", "$compileProvider", "$filterProvider", "$injector", "$animateProvider", function($controllerProvider, $provide, $compileProvider, $filterProvider, $injector, $animateProvider) {
            var jsLoader, cssLoader, templatesLoader, modules = {},
                providers = {
                    $controllerProvider: $controllerProvider,
                    $compileProvider: $compileProvider,
                    $filterProvider: $filterProvider,
                    $provide: $provide,
                    $injector: $injector,
                    $animateProvider: $animateProvider
                },
                anchor = document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0],
                debug = !1,
                events = !1;
            init(angular.element(window.document)), this.$get = ["$log", "$q", "$templateCache", "$http", "$rootElement", "$rootScope", "$cacheFactory", "$interval", function($log, $q, $templateCache, $http, $rootElement, $rootScope, $cacheFactory, $interval) {
                var instanceInjector, filesCache = $cacheFactory("ocLazyLoad"),
                    uaCssChecked = !1,
                    useCssLoadPatch = !1;
                debug || ($log = {}, $log.error = angular.noop, $log.warn = angular.noop, $log.info = angular.noop), providers.getInstanceInjector = function() {
                    return instanceInjector ? instanceInjector : instanceInjector = $rootElement.data("$injector") || angular.injector()
                }, broadcast = function(eventName, params) {
                    events && $rootScope.$broadcast(eventName, params), debug && $log.info(eventName, params)
                };
                var buildElement = function(type, path, params) {
                    var el, loaded, deferred = $q.defer(),
                        cacheBuster = function(url) {
                            var dc = (new Date).getTime();
                            return url.indexOf("?") >= 0 ? "&" === url.substring(0, url.length - 1) ? url + "_dc=" + dc : url + "&_dc=" + dc : url + "?_dc=" + dc
                        };
                    switch (angular.isUndefined(filesCache.get(path)) && filesCache.put(path, deferred.promise), type) {
                        case "css":
                            el = document.createElement("link"), el.type = "text/css", el.rel = "stylesheet", el.href = params.cache === !1 ? cacheBuster(path) : path;
                            break;
                        case "js":
                            el = document.createElement("script"), el.src = params.cache === !1 ? cacheBuster(path) : path;
                            break;
                        default:
                            deferred.reject(new Error('Requested type "' + type + '" is not known. Could not inject "' + path + '"'))
                    }
                    el.onload = el.onreadystatechange = function(e) {
                        el.readyState && !/^c|loade/.test(el.readyState) || loaded || (el.onload = el.onreadystatechange = null, loaded = 1, broadcast("ocLazyLoad.fileLoaded", path), deferred.resolve())
                    }, el.onerror = function(e) {
                        deferred.reject(new Error("Unable to load " + path))
                    }, el.async = params.serie ? 0 : 1;
                    var insertBeforeElem = anchor.lastChild;
                    if (params.insertBefore) {
                        var element = angular.element(params.insertBefore);
                        element && element.length > 0 && (insertBeforeElem = element[0])
                    }
                    if (anchor.insertBefore(el, insertBeforeElem), "css" == type) {
                        if (!uaCssChecked) {
                            var ua = navigator.userAgent.toLowerCase();
                            if (/iP(hone|od|ad)/.test(navigator.platform)) {
                                var v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/),
                                    iOSVersion = parseFloat([parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)].join("."));
                                useCssLoadPatch = 6 > iOSVersion
                            } else if (ua.indexOf("android") > -1) {
                                var androidVersion = parseFloat(ua.slice(ua.indexOf("android") + 8));
                                useCssLoadPatch = 4.4 > androidVersion
                            } else if (ua.indexOf("safari") > -1 && -1 == ua.indexOf("chrome")) {
                                var safariVersion = parseFloat(ua.match(/version\/([\.\d]+)/i)[1]);
                                useCssLoadPatch = 6 > safariVersion
                            }
                        }
                        if (useCssLoadPatch) var tries = 1e3,
                            interval = $interval(function() {
                                try {
                                    el.sheet.cssRules, $interval.cancel(interval), el.onload()
                                } catch (e) {
                                    --tries <= 0 && el.onerror()
                                }
                            }, 20)
                    }
                    return deferred.promise
                };
                angular.isUndefined(jsLoader) && (jsLoader = function(paths, callback, params) {
                    var promises = [];
                    angular.forEach(paths, function(path) {
                        promises.push(buildElement("js", path, params))
                    }), $q.all(promises).then(function() {
                        callback()
                    }, function(err) {
                        callback(err)
                    })
                }, jsLoader.ocLazyLoadLoader = !0), angular.isUndefined(cssLoader) && (cssLoader = function(paths, callback, params) {
                    var promises = [];
                    angular.forEach(paths, function(path) {
                        promises.push(buildElement("css", path, params))
                    }), $q.all(promises).then(function() {
                        callback()
                    }, function(err) {
                        callback(err)
                    })
                }, cssLoader.ocLazyLoadLoader = !0), angular.isUndefined(templatesLoader) && (templatesLoader = function(paths, callback, params) {
                    var promises = [];
                    return angular.forEach(paths, function(url) {
                        var deferred = $q.defer();
                        promises.push(deferred.promise), $http.get(url, params).success(function(data) {
                            angular.isString(data) && data.length > 0 && angular.forEach(angular.element(data), function(node) {
                                "SCRIPT" === node.nodeName && "text/ng-template" === node.type && $templateCache.put(node.id, node.innerHTML)
                            }), angular.isUndefined(filesCache.get(url)) && filesCache.put(url, !0), deferred.resolve()
                        }).error(function(err) {
                            deferred.reject(new Error('Unable to load template file "' + url + '": ' + err))
                        })
                    }), $q.all(promises).then(function() {
                        callback()
                    }, function(err) {
                        callback(err)
                    })
                }, templatesLoader.ocLazyLoadLoader = !0);
                var filesLoader = function(config, params) {
                    var cssFiles = [],
                        templatesFiles = [],
                        jsFiles = [],
                        promises = [],
                        cachePromise = null;
                    angular.extend(params || {}, config);
                    var pushFile = function(path) {
                        var m, file_type = null;
                        if ("object" == typeof path && (file_type = path.type, path = path.path), cachePromise = filesCache.get(path), angular.isUndefined(cachePromise) || params.cache === !1) {
                            if (null !== (m = /^(css|less|html|htm|js)?(?=!)/.exec(path)) && (file_type = m[1], path = path.substr(m[1].length + 1, path.length)), !file_type) {
                                if (null === (m = /[.](css|less|html|htm|js)?$/.exec(path))) return void $log.error("File type could not be determined. " + path);
                                file_type = m[1]
                            }
                            "css" !== file_type && "less" !== file_type || -1 !== cssFiles.indexOf(path) ? "html" !== file_type && "htm" !== file_type || -1 !== templatesFiles.indexOf(path) ? "js" === file_type || -1 === jsFiles.indexOf(path) ? jsFiles.push(path) : $log.error("File type is not valid. " + path) : templatesFiles.push(path) : cssFiles.push(path)
                        } else cachePromise && promises.push(cachePromise)
                    };
                    if (params.serie ? pushFile(params.files.shift()) : angular.forEach(params.files, function(path) {
                            pushFile(path)
                        }), cssFiles.length > 0) {
                        var cssDeferred = $q.defer();
                        cssLoader(cssFiles, function(err) {
                            angular.isDefined(err) && cssLoader.hasOwnProperty("ocLazyLoadLoader") ? ($log.error(err), cssDeferred.reject(err)) : cssDeferred.resolve()
                        }, params), promises.push(cssDeferred.promise)
                    }
                    if (templatesFiles.length > 0) {
                        var templatesDeferred = $q.defer();
                        templatesLoader(templatesFiles, function(err) {
                            angular.isDefined(err) && templatesLoader.hasOwnProperty("ocLazyLoadLoader") ? ($log.error(err), templatesDeferred.reject(err)) : templatesDeferred.resolve()
                        }, params), promises.push(templatesDeferred.promise)
                    }
                    if (jsFiles.length > 0) {
                        var jsDeferred = $q.defer();
                        jsLoader(jsFiles, function(err) {
                            angular.isDefined(err) && jsLoader.hasOwnProperty("ocLazyLoadLoader") ? ($log.error(err), jsDeferred.reject(err)) : jsDeferred.resolve()
                        }, params), promises.push(jsDeferred.promise)
                    }
                    return params.serie && params.files.length > 0 ? $q.all(promises).then(function() {
                        return filesLoader(config, params)
                    }) : $q.all(promises)
                };
                return {
                    getModuleConfig: function(moduleName) {
                        if (!angular.isString(moduleName)) throw new Error("You need to give the name of the module to get");
                        return modules[moduleName] ? modules[moduleName] : null
                    },
                    setModuleConfig: function(moduleConfig) {
                        if (!angular.isObject(moduleConfig)) throw new Error("You need to give the module config object to set");
                        return modules[moduleConfig.name] = moduleConfig, moduleConfig
                    },
                    getModules: function() {
                        return regModules
                    },
                    isLoaded: function(modulesNames) {
                        var moduleLoaded = function(module) {
                            var isLoaded = regModules.indexOf(module) > -1;
                            return isLoaded || (isLoaded = !!moduleExists(module)), isLoaded
                        };
                        if (angular.isString(modulesNames) && (modulesNames = [modulesNames]), angular.isArray(modulesNames)) {
                            var i, len;
                            for (i = 0, len = modulesNames.length; len > i; i++)
                                if (!moduleLoaded(modulesNames[i])) return !1;
                            return !0
                        }
                        throw new Error("You need to define the module(s) name(s)")
                    },
                    load: function(module, params) {
                        var moduleName, errText, self = this,
                            config = null,
                            moduleCache = [],
                            deferredList = [],
                            deferred = $q.defer();
                        if (angular.isUndefined(params) && (params = {}), angular.isArray(module)) return angular.forEach(module, function(m) {
                            m && deferredList.push(self.load(m, params))
                        }), $q.all(deferredList).then(function() {
                            deferred.resolve(module)
                        }, function(err) {
                            deferred.reject(err)
                        }), deferred.promise;
                        if (moduleName = getModuleName(module), "string" == typeof module ? (config = self.getModuleConfig(module), config || (config = {
                                files: [module]
                            }, moduleName = null)) : "object" == typeof module && (config = self.setModuleConfig(module)), null === config ? (errText = 'Module "' + moduleName + '" is not configured, cannot load.', $log.error(errText), deferred.reject(new Error(errText))) : angular.isDefined(config.template) && (angular.isUndefined(config.files) && (config.files = []), angular.isString(config.template) ? config.files.push(config.template) : angular.isArray(config.template) && config.files.concat(config.template)), moduleCache.push = function(value) {
                                -1 === this.indexOf(value) && Array.prototype.push.apply(this, arguments)
                            }, angular.isDefined(moduleName) && moduleExists(moduleName) && -1 !== regModules.indexOf(moduleName) && (moduleCache.push(moduleName), angular.isUndefined(config.files))) return deferred.resolve(), deferred.promise;
                        var localParams = {};
                        angular.extend(localParams, params, config);
                        var loadDependencies = function loadDependencies(module) {
                            var moduleName, loadedModule, requires, diff, promisesList = [];
                            if (moduleName = getModuleName(module), null === moduleName) return $q.when();
                            try {
                                loadedModule = getModule(moduleName)
                            } catch (e) {
                                var deferred = $q.defer();
                                return $log.error(e.message), deferred.reject(e), deferred.promise
                            }
                            return requires = getRequires(loadedModule), angular.forEach(requires, function(requireEntry) {
                                if ("string" == typeof requireEntry) {
                                    var config = self.getModuleConfig(requireEntry);
                                    if (null === config) return void moduleCache.push(requireEntry);
                                    requireEntry = config
                                }
                                return moduleExists(requireEntry.name) ? void("string" != typeof module && (diff = requireEntry.files.filter(function(n) {
                                    return self.getModuleConfig(requireEntry.name).files.indexOf(n) < 0
                                }), 0 !== diff.length && $log.warn('Module "', moduleName, '" attempted to redefine configuration for dependency. "', requireEntry.name, '"\n Additional Files Loaded:', diff), promisesList.push(filesLoader(requireEntry.files, localParams).then(function() {
                                    return loadDependencies(requireEntry)
                                })))) : ("object" == typeof requireEntry && (requireEntry.hasOwnProperty("name") && requireEntry.name && (self.setModuleConfig(requireEntry), moduleCache.push(requireEntry.name)), requireEntry.hasOwnProperty("css") && 0 !== requireEntry.css.length && angular.forEach(requireEntry.css, function(path) {
                                    buildElement("css", path, localParams)
                                })), void(requireEntry.hasOwnProperty("files") && 0 !== requireEntry.files.length && requireEntry.files && promisesList.push(filesLoader(requireEntry, localParams).then(function() {
                                    return loadDependencies(requireEntry)
                                }))))
                            }), $q.all(promisesList)
                        };
                        return filesLoader(config, localParams).then(function() {
                            null === moduleName ? deferred.resolve(module) : (moduleCache.push(moduleName), loadDependencies(moduleName).then(function() {
                                try {
                                    justLoaded = [], register(providers, moduleCache, localParams)
                                } catch (e) {
                                    return $log.error(e.message), void deferred.reject(e)
                                }
                                deferred.resolve(module)
                            }, function(err) {
                                deferred.reject(err)
                            }))
                        }, function(err) {
                            deferred.reject(err)
                        }), deferred.promise
                    }
                }
            }], this.config = function(config) {
                if (angular.isDefined(config.jsLoader) || angular.isDefined(config.asyncLoader)) {
                    if (!angular.isFunction(config.jsLoader || config.asyncLoader)) throw "The js loader needs to be a function";
                    jsLoader = config.jsLoader || config.asyncLoader
                }
                if (angular.isDefined(config.cssLoader)) {
                    if (!angular.isFunction(config.cssLoader)) throw "The css loader needs to be a function";
                    cssLoader = config.cssLoader
                }
                if (angular.isDefined(config.templatesLoader)) {
                    if (!angular.isFunction(config.templatesLoader)) throw "The template loader needs to be a function";
                    templatesLoader = config.templatesLoader
                }
                angular.isDefined(config.modules) && (angular.isArray(config.modules) ? angular.forEach(config.modules, function(moduleConfig) {
                    modules[moduleConfig.name] = moduleConfig
                }) : modules[config.modules.name] = config.modules), angular.isDefined(config.debug) && (debug = config.debug), angular.isDefined(config.events) && (events = config.events)
            }
        }]), ocLazyLoad.directive("ocLazyLoad", ["$ocLazyLoad", "$compile", "$animate", "$parse", function($ocLazyLoad, $compile, $animate, $parse) {
            return {
                restrict: "A",
                terminal: !0,
                priority: 1e3,
                compile: function(element, attrs) {
                    var content = element[0].innerHTML;
                    return element.html(""),
                        function($scope, $element, $attr) {
                            var model = $parse($attr.ocLazyLoad);
                            $scope.$watch(function() {
                                return model($scope) || $attr.ocLazyLoad
                            }, function(moduleName) {
                                angular.isDefined(moduleName) && $ocLazyLoad.load(moduleName).then(function(moduleConfig) {
                                    $animate.enter($compile(content)($scope), null, $element)
                                })
                            }, !0)
                        }
                }
            }
        }]);
        var bootstrapFct = angular.bootstrap;
        angular.bootstrap = function(element, modules, config) {
            return initModules = modules.slice(), bootstrapFct(element, modules, config)
        };
        var addToInit = function(name) {
                angular.isString(name) && -1 === initModules.indexOf(name) && initModules.push(name)
            },
            ngModuleFct = angular.module;
        if (angular.module = function(name, requires, configFn) {
                return addToInit(name), ngModuleFct(name, requires, configFn)
            }, (window.jasmine || window.mocha) && angular.isDefined(angular.mock)) {
            {
                var ngMockModuleFct = angular.mock.module;
                window.module
            }
            window.module = angular.mock.module = function(module) {
                Array.prototype.slice.call(arguments, 0);
                angular.isObject(module) && !angular.isArray(module) ? angular.forEach(module, function(value, key) {
                    addToInit(key)
                }) : angular.isString(module) && addToInit(module), ngMockModuleFct(module)
            }
        }
        Array.prototype.indexOf || (Array.prototype.indexOf = function(searchElement, fromIndex) {
            var k;
            if (null == this) throw new TypeError('"this" is null or not defined');
            var O = Object(this),
                len = O.length >>> 0;
            if (0 === len) return -1;
            var n = +fromIndex || 0;
            if (Math.abs(n) === 1 / 0 && (n = 0), n >= len) return -1;
            for (k = Math.max(n >= 0 ? n : len - Math.abs(n), 0); len > k;) {
                if (k in O && O[k] === searchElement) return k;
                k++
            }
            return -1
        })
    }(), angular.module("ui.bootstrap", ["ui.bootstrap.tpls", "ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.dateparser", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdown", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead"]), angular.module("ui.bootstrap.tpls", ["template/accordion/accordion-group.html", "template/accordion/accordion.html", "template/alert/alert.html", "template/carousel/carousel.html", "template/carousel/slide.html", "template/datepicker/datepicker.html", "template/datepicker/day.html", "template/datepicker/month.html", "template/datepicker/popup.html", "template/datepicker/year.html", "template/modal/backdrop.html", "template/modal/window.html", "template/pagination/pager.html", "template/pagination/pagination.html", "template/tooltip/tooltip-html-unsafe-popup.html", "template/tooltip/tooltip-popup.html", "template/popover/popover.html", "template/progressbar/bar.html", "template/progressbar/progress.html", "template/progressbar/progressbar.html", "template/rating/rating.html", "template/tabs/tab.html", "template/tabs/tabset.html", "template/timepicker/timepicker.html", "template/typeahead/typeahead-match.html", "template/typeahead/typeahead-popup.html"]), angular.module("ui.bootstrap.transition", []).factory("$transition", ["$q", "$timeout", "$rootScope", function($q, $timeout, $rootScope) {
        function findEndEventName(endEventNames) {
            for (var name in endEventNames)
                if (void 0 !== transElement.style[name]) return endEventNames[name]
        }
        var $transition = function(element, trigger, options) {
                options = options || {};
                var deferred = $q.defer(),
                    endEventName = $transition[options.animation ? "animationEndEventName" : "transitionEndEventName"],
                    transitionEndHandler = function(event) {
                        $rootScope.$apply(function() {
                            element.unbind(endEventName, transitionEndHandler), deferred.resolve(element)
                        })
                    };
                return endEventName && element.bind(endEventName, transitionEndHandler), $timeout(function() {
                    angular.isString(trigger) ? element.addClass(trigger) : angular.isFunction(trigger) ? trigger(element) : angular.isObject(trigger) && element.css(trigger), endEventName || deferred.resolve(element)
                }), deferred.promise.cancel = function() {
                    endEventName && element.unbind(endEventName, transitionEndHandler), deferred.reject("Transition cancelled")
                }, deferred.promise
            },
            transElement = document.createElement("trans"),
            transitionEndEventNames = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd",
                transition: "transitionend"
            },
            animationEndEventNames = {
                WebkitTransition: "webkitAnimationEnd",
                MozTransition: "animationend",
                OTransition: "oAnimationEnd",
                transition: "animationend"
            };
        return $transition.transitionEndEventName = findEndEventName(transitionEndEventNames), $transition.animationEndEventName = findEndEventName(animationEndEventNames), $transition
    }]), angular.module("ui.bootstrap.collapse", ["ui.bootstrap.transition"]).directive("collapse", ["$transition", function($transition) {
        return {
            link: function(scope, element, attrs) {
                function doTransition(change) {
                    function newTransitionDone() {
                        currentTransition === newTransition && (currentTransition = void 0)
                    }
                    var newTransition = $transition(element, change);
                    return currentTransition && currentTransition.cancel(), currentTransition = newTransition, newTransition.then(newTransitionDone, newTransitionDone), newTransition
                }

                function expand() {
                    initialAnimSkip ? (initialAnimSkip = !1, expandDone()) : (element.removeClass("collapse").addClass("collapsing"), doTransition({
                        height: element[0].scrollHeight + "px"
                    }).then(expandDone))
                }

                function expandDone() {
                    element.removeClass("collapsing"), element.addClass("collapse in"), element.css({
                        height: "auto"
                    })
                }

                function collapse() {
                    if (initialAnimSkip) initialAnimSkip = !1, collapseDone(), element.css({
                        height: 0
                    });
                    else {
                        element.css({
                            height: element[0].scrollHeight + "px"
                        }); {
                            element[0].offsetWidth
                        }
                        element.removeClass("collapse in").addClass("collapsing"), doTransition({
                            height: 0
                        }).then(collapseDone)
                    }
                }

                function collapseDone() {
                    element.removeClass("collapsing"), element.addClass("collapse")
                }
                var currentTransition, initialAnimSkip = !0;
                scope.$watch(attrs.collapse, function(shouldCollapse) {
                    shouldCollapse ? collapse() : expand()
                })
            }
        }
    }]), angular.module("ui.bootstrap.accordion", ["ui.bootstrap.collapse"]).constant("accordionConfig", {
        closeOthers: !0
    }).controller("AccordionController", ["$scope", "$attrs", "accordionConfig", function($scope, $attrs, accordionConfig) {
        this.groups = [], this.closeOthers = function(openGroup) {
            var closeOthers = angular.isDefined($attrs.closeOthers) ? $scope.$eval($attrs.closeOthers) : accordionConfig.closeOthers;
            closeOthers && angular.forEach(this.groups, function(group) {
                group !== openGroup && (group.isOpen = !1)
            })
        }, this.addGroup = function(groupScope) {
            var that = this;
            this.groups.push(groupScope), groupScope.$on("$destroy", function(event) {
                that.removeGroup(groupScope)
            })
        }, this.removeGroup = function(group) {
            var index = this.groups.indexOf(group); - 1 !== index && this.groups.splice(index, 1)
        }
    }]).directive("accordion", function() {
        return {
            restrict: "EA",
            controller: "AccordionController",
            transclude: !0,
            replace: !1,
            templateUrl: "template/accordion/accordion.html"
        }
    }).directive("accordionGroup", function() {
        return {
            require: "^accordion",
            restrict: "EA",
            transclude: !0,
            replace: !0,
            templateUrl: "template/accordion/accordion-group.html",
            scope: {
                heading: "@",
                isOpen: "=?",
                isDisabled: "=?"
            },
            controller: function() {
                this.setHeading = function(element) {
                    this.heading = element
                }
            },
            link: function(scope, element, attrs, accordionCtrl) {
                accordionCtrl.addGroup(scope), scope.$watch("isOpen", function(value) {
                    value && accordionCtrl.closeOthers(scope)
                }), scope.toggleOpen = function() {
                    scope.isDisabled || (scope.isOpen = !scope.isOpen)
                }
            }
        }
    }).directive("accordionHeading", function() {
        return {
            restrict: "EA",
            transclude: !0,
            template: "",
            replace: !0,
            require: "^accordionGroup",
            link: function(scope, element, attr, accordionGroupCtrl, transclude) {
                accordionGroupCtrl.setHeading(transclude(scope, function() {}))
            }
        }
    }).directive("accordionTransclude", function() {
        return {
            require: "^accordionGroup",
            link: function(scope, element, attr, controller) {
                scope.$watch(function() {
                    return controller[attr.accordionTransclude]
                }, function(heading) {
                    heading && (element.html(""), element.append(heading))
                })
            }
        }
    }), angular.module("ui.bootstrap.alert", []).controller("AlertController", ["$scope", "$attrs", function($scope, $attrs) {
        $scope.closeable = "close" in $attrs, this.close = $scope.close
    }]).directive("alert", function() {
        return {
            restrict: "EA",
            controller: "AlertController",
            templateUrl: "template/alert/alert.html",
            transclude: !0,
            replace: !0,
            scope: {
                type: "@",
                close: "&"
            }
        }
    }).directive("dismissOnTimeout", ["$timeout", function($timeout) {
        return {
            require: "alert",
            link: function(scope, element, attrs, alertCtrl) {
                $timeout(function() {
                    alertCtrl.close()
                }, parseInt(attrs.dismissOnTimeout, 10))
            }
        }
    }]), angular.module("ui.bootstrap.bindHtml", []).directive("bindHtmlUnsafe", function() {
        return function(scope, element, attr) {
            element.addClass("ng-binding").data("$binding", attr.bindHtmlUnsafe), scope.$watch(attr.bindHtmlUnsafe, function(value) {
                element.html(value || "")
            })
        }
    }), angular.module("ui.bootstrap.buttons", []).constant("buttonConfig", {
        activeClass: "active",
        toggleEvent: "click"
    }).controller("ButtonsController", ["buttonConfig", function(buttonConfig) {
        this.activeClass = buttonConfig.activeClass || "active", this.toggleEvent = buttonConfig.toggleEvent || "click"
    }]).directive("btnRadio", function() {
        return {
            require: ["btnRadio", "ngModel"],
            controller: "ButtonsController",
            link: function(scope, element, attrs, ctrls) {
                var buttonsCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                ngModelCtrl.$render = function() {
                    element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, scope.$eval(attrs.btnRadio)))
                }, element.bind(buttonsCtrl.toggleEvent, function() {
                    var isActive = element.hasClass(buttonsCtrl.activeClass);
                    (!isActive || angular.isDefined(attrs.uncheckable)) && scope.$apply(function() {
                        ngModelCtrl.$setViewValue(isActive ? null : scope.$eval(attrs.btnRadio)), ngModelCtrl.$render()
                    })
                })
            }
        }
    }).directive("btnCheckbox", function() {
        return {
            require: ["btnCheckbox", "ngModel"],
            controller: "ButtonsController",
            link: function(scope, element, attrs, ctrls) {
                function getTrueValue() {
                    return getCheckboxValue(attrs.btnCheckboxTrue, !0)
                }

                function getFalseValue() {
                    return getCheckboxValue(attrs.btnCheckboxFalse, !1)
                }

                function getCheckboxValue(attributeValue, defaultValue) {
                    var val = scope.$eval(attributeValue);
                    return angular.isDefined(val) ? val : defaultValue
                }
                var buttonsCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                ngModelCtrl.$render = function() {
                    element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, getTrueValue()))
                }, element.bind(buttonsCtrl.toggleEvent, function() {
                    scope.$apply(function() {
                        ngModelCtrl.$setViewValue(element.hasClass(buttonsCtrl.activeClass) ? getFalseValue() : getTrueValue()), ngModelCtrl.$render()
                    })
                })
            }
        }
    }), angular.module("ui.bootstrap.carousel", ["ui.bootstrap.transition"]).controller("CarouselController", ["$scope", "$timeout", "$interval", "$transition", function($scope, $timeout, $interval, $transition) {
        function restartTimer() {
            resetTimer();
            var interval = +$scope.interval;
            !isNaN(interval) && interval > 0 && (currentInterval = $interval(timerFn, interval))
        }

        function resetTimer() {
            currentInterval && ($interval.cancel(currentInterval), currentInterval = null)
        }

        function timerFn() {
            var interval = +$scope.interval;
            isPlaying && !isNaN(interval) && interval > 0 ? $scope.next() : $scope.pause()
        }
        var currentInterval, isPlaying, self = this,
            slides = self.slides = $scope.slides = [],
            currentIndex = -1;
        self.currentSlide = null;
        var destroyed = !1;
        self.select = $scope.select = function(nextSlide, direction) {
            function goNext() {
                if (!destroyed) {
                    if (self.currentSlide && angular.isString(direction) && !$scope.noTransition && nextSlide.$element) {
                        nextSlide.$element.addClass(direction); {
                            nextSlide.$element[0].offsetWidth
                        }
                        angular.forEach(slides, function(slide) {
                                angular.extend(slide, {
                                    direction: "",
                                    entering: !1,
                                    leaving: !1,
                                    active: !1
                                })
                            }), angular.extend(nextSlide, {
                                direction: direction,
                                active: !0,
                                entering: !0
                            }), angular.extend(self.currentSlide || {}, {
                                direction: direction,
                                leaving: !0
                            }), $scope.$currentTransition = $transition(nextSlide.$element, {}),
                            function(next, current) {
                                $scope.$currentTransition.then(function() {
                                    transitionDone(next, current)
                                }, function() {
                                    transitionDone(next, current)
                                })
                            }(nextSlide, self.currentSlide)
                    } else transitionDone(nextSlide, self.currentSlide);
                    self.currentSlide = nextSlide, currentIndex = nextIndex, restartTimer()
                }
            }

            function transitionDone(next, current) {
                angular.extend(next, {
                    direction: "",
                    active: !0,
                    leaving: !1,
                    entering: !1
                }), angular.extend(current || {}, {
                    direction: "",
                    active: !1,
                    leaving: !1,
                    entering: !1
                }), $scope.$currentTransition = null
            }
            var nextIndex = slides.indexOf(nextSlide);
            void 0 === direction && (direction = nextIndex > currentIndex ? "next" : "prev"), nextSlide && nextSlide !== self.currentSlide && ($scope.$currentTransition ? ($scope.$currentTransition.cancel(), $timeout(goNext)) : goNext())
        }, $scope.$on("$destroy", function() {
            destroyed = !0
        }), self.indexOfSlide = function(slide) {
            return slides.indexOf(slide)
        }, $scope.next = function() {
            var newIndex = (currentIndex + 1) % slides.length;
            return $scope.$currentTransition ? void 0 : self.select(slides[newIndex], "next")
        }, $scope.prev = function() {
            var newIndex = 0 > currentIndex - 1 ? slides.length - 1 : currentIndex - 1;
            return $scope.$currentTransition ? void 0 : self.select(slides[newIndex], "prev")
        }, $scope.isActive = function(slide) {
            return self.currentSlide === slide
        }, $scope.$watch("interval", restartTimer), $scope.$on("$destroy", resetTimer), $scope.play = function() {
            isPlaying || (isPlaying = !0, restartTimer())
        }, $scope.pause = function() {
            $scope.noPause || (isPlaying = !1, resetTimer())
        }, self.addSlide = function(slide, element) {
            slide.$element = element, slides.push(slide), 1 === slides.length || slide.active ? (self.select(slides[slides.length - 1]), 1 == slides.length && $scope.play()) : slide.active = !1
        }, self.removeSlide = function(slide) {
            var index = slides.indexOf(slide);
            slides.splice(index, 1), slides.length > 0 && slide.active ? self.select(index >= slides.length ? slides[index - 1] : slides[index]) : currentIndex > index && currentIndex--
        }
    }]).directive("carousel", [function() {
        return {
            restrict: "EA",
            transclude: !0,
            replace: !0,
            controller: "CarouselController",
            require: "carousel",
            templateUrl: "template/carousel/carousel.html",
            scope: {
                interval: "=",
                noTransition: "=",
                noPause: "="
            }
        }
    }]).directive("slide", function() {
        return {
            require: "^carousel",
            restrict: "EA",
            transclude: !0,
            replace: !0,
            templateUrl: "template/carousel/slide.html",
            scope: {
                active: "=?"
            },
            link: function(scope, element, attrs, carouselCtrl) {
                carouselCtrl.addSlide(scope, element), scope.$on("$destroy", function() {
                    carouselCtrl.removeSlide(scope)
                }), scope.$watch("active", function(active) {
                    active && carouselCtrl.select(scope)
                })
            }
        }
    }), angular.module("ui.bootstrap.dateparser", []).service("dateParser", ["$locale", "orderByFilter", function($locale, orderByFilter) {
        function createParser(format) {
            var map = [],
                regex = format.split("");
            return angular.forEach(formatCodeToRegex, function(data, code) {
                var index = format.indexOf(code);
                if (index > -1) {
                    format = format.split(""), regex[index] = "(" + data.regex + ")", format[index] = "$";
                    for (var i = index + 1, n = index + code.length; n > i; i++) regex[i] = "", format[i] = "$";
                    format = format.join(""), map.push({
                        index: index,
                        apply: data.apply
                    })
                }
            }), {
                regex: new RegExp("^" + regex.join("") + "$"),
                map: orderByFilter(map, "index")
            }
        }

        function isValid(year, month, date) {
            return 1 === month && date > 28 ? 29 === date && (year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) : 3 === month || 5 === month || 8 === month || 10 === month ? 31 > date : !0
        }
        this.parsers = {};
        var formatCodeToRegex = {
            yyyy: {
                regex: "\\d{4}",
                apply: function(value) {
                    this.year = +value
                }
            },
            yy: {
                regex: "\\d{2}",
                apply: function(value) {
                    this.year = +value + 2e3
                }
            },
            y: {
                regex: "\\d{1,4}",
                apply: function(value) {
                    this.year = +value
                }
            },
            MMMM: {
                regex: $locale.DATETIME_FORMATS.MONTH.join("|"),
                apply: function(value) {
                    this.month = $locale.DATETIME_FORMATS.MONTH.indexOf(value)
                }
            },
            MMM: {
                regex: $locale.DATETIME_FORMATS.SHORTMONTH.join("|"),
                apply: function(value) {
                    this.month = $locale.DATETIME_FORMATS.SHORTMONTH.indexOf(value)
                }
            },
            MM: {
                regex: "0[1-9]|1[0-2]",
                apply: function(value) {
                    this.month = value - 1
                }
            },
            M: {
                regex: "[1-9]|1[0-2]",
                apply: function(value) {
                    this.month = value - 1
                }
            },
            dd: {
                regex: "[0-2][0-9]{1}|3[0-1]{1}",
                apply: function(value) {
                    this.date = +value
                }
            },
            d: {
                regex: "[1-2]?[0-9]{1}|3[0-1]{1}",
                apply: function(value) {
                    this.date = +value
                }
            },
            EEEE: {
                regex: $locale.DATETIME_FORMATS.DAY.join("|")
            },
            EEE: {
                regex: $locale.DATETIME_FORMATS.SHORTDAY.join("|")
            }
        };
        this.parse = function(input, format) {
            if (!angular.isString(input) || !format) return input;
            format = $locale.DATETIME_FORMATS[format] || format, this.parsers[format] || (this.parsers[format] = createParser(format));
            var parser = this.parsers[format],
                regex = parser.regex,
                map = parser.map,
                results = input.match(regex);
            if (results && results.length) {
                for (var dt, fields = {
                        year: 1900,
                        month: 0,
                        date: 1,
                        hours: 0
                    }, i = 1, n = results.length; n > i; i++) {
                    var mapper = map[i - 1];
                    mapper.apply && mapper.apply.call(fields, results[i])
                }
                return isValid(fields.year, fields.month, fields.date) && (dt = new Date(fields.year, fields.month, fields.date, fields.hours)), dt
            }
        }
    }]), angular.module("ui.bootstrap.position", []).factory("$position", ["$document", "$window", function($document, $window) {
        function getStyle(el, cssprop) {
            return el.currentStyle ? el.currentStyle[cssprop] : $window.getComputedStyle ? $window.getComputedStyle(el)[cssprop] : el.style[cssprop]
        }

        function isStaticPositioned(element) {
            return "static" === (getStyle(element, "position") || "static")
        }
        var parentOffsetEl = function(element) {
            for (var docDomEl = $document[0], offsetParent = element.offsetParent || docDomEl; offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent);) offsetParent = offsetParent.offsetParent;
            return offsetParent || docDomEl
        };
        return {
            position: function(element) {
                var elBCR = this.offset(element),
                    offsetParentBCR = {
                        top: 0,
                        left: 0
                    },
                    offsetParentEl = parentOffsetEl(element[0]);
                offsetParentEl != $document[0] && (offsetParentBCR = this.offset(angular.element(offsetParentEl)), offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop, offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft);
                var boundingClientRect = element[0].getBoundingClientRect();
                return {
                    width: boundingClientRect.width || element.prop("offsetWidth"),
                    height: boundingClientRect.height || element.prop("offsetHeight"),
                    top: elBCR.top - offsetParentBCR.top,
                    left: elBCR.left - offsetParentBCR.left
                }
            },
            offset: function(element) {
                var boundingClientRect = element[0].getBoundingClientRect();
                return {
                    width: boundingClientRect.width || element.prop("offsetWidth"),
                    height: boundingClientRect.height || element.prop("offsetHeight"),
                    top: boundingClientRect.top + ($window.pageYOffset || $document[0].documentElement.scrollTop),
                    left: boundingClientRect.left + ($window.pageXOffset || $document[0].documentElement.scrollLeft)
                }
            },
            positionElements: function(hostEl, targetEl, positionStr, appendToBody) {
                var hostElPos, targetElWidth, targetElHeight, targetElPos, positionStrParts = positionStr.split("-"),
                    pos0 = positionStrParts[0],
                    pos1 = positionStrParts[1] || "center";
                hostElPos = appendToBody ? this.offset(hostEl) : this.position(hostEl), targetElWidth = targetEl.prop("offsetWidth"), targetElHeight = targetEl.prop("offsetHeight");
                var shiftWidth = {
                        center: function() {
                            return hostElPos.left + hostElPos.width / 2 - targetElWidth / 2
                        },
                        left: function() {
                            return hostElPos.left
                        },
                        right: function() {
                            return hostElPos.left + hostElPos.width
                        }
                    },
                    shiftHeight = {
                        center: function() {
                            return hostElPos.top + hostElPos.height / 2 - targetElHeight / 2
                        },
                        top: function() {
                            return hostElPos.top
                        },
                        bottom: function() {
                            return hostElPos.top + hostElPos.height
                        }
                    };
                switch (pos0) {
                    case "right":
                        targetElPos = {
                            top: shiftHeight[pos1](),
                            left: shiftWidth[pos0]()
                        };
                        break;
                    case "left":
                        targetElPos = {
                            top: shiftHeight[pos1](),
                            left: hostElPos.left - targetElWidth
                        };
                        break;
                    case "bottom":
                        targetElPos = {
                            top: shiftHeight[pos0](),
                            left: shiftWidth[pos1]()
                        };
                        break;
                    default:
                        targetElPos = {
                            top: hostElPos.top - targetElHeight,
                            left: shiftWidth[pos1]()
                        }
                }
                return targetElPos
            }
        }
    }]), angular.module("ui.bootstrap.datepicker", ["ui.bootstrap.dateparser", "ui.bootstrap.position"]).constant("datepickerConfig", {
        formatDay: "dd",
        formatMonth: "MMMM",
        formatYear: "yyyy",
        formatDayHeader: "EEE",
        formatDayTitle: "MMMM yyyy",
        formatMonthTitle: "yyyy",
        datepickerMode: "day",
        minMode: "day",
        maxMode: "year",
        showWeeks: !0,
        startingDay: 0,
        yearRange: 20,
        minDate: null,
        maxDate: null
    }).controller("DatepickerController", ["$scope", "$attrs", "$parse", "$interpolate", "$timeout", "$log", "dateFilter", "datepickerConfig", function($scope, $attrs, $parse, $interpolate, $timeout, $log, dateFilter, datepickerConfig) {
        var self = this,
            ngModelCtrl = {
                $setViewValue: angular.noop
            };
        this.modes = ["day", "month", "year"], angular.forEach(["formatDay", "formatMonth", "formatYear", "formatDayHeader", "formatDayTitle", "formatMonthTitle", "minMode", "maxMode", "showWeeks", "startingDay", "yearRange"], function(key, index) {
            self[key] = angular.isDefined($attrs[key]) ? 8 > index ? $interpolate($attrs[key])($scope.$parent) : $scope.$parent.$eval($attrs[key]) : datepickerConfig[key]
        }), angular.forEach(["minDate", "maxDate"], function(key) {
            $attrs[key] ? $scope.$parent.$watch($parse($attrs[key]), function(value) {
                self[key] = value ? new Date(value) : null, self.refreshView()
            }) : self[key] = datepickerConfig[key] ? new Date(datepickerConfig[key]) : null
        }), $scope.datepickerMode = $scope.datepickerMode || datepickerConfig.datepickerMode, $scope.uniqueId = "datepicker-" + $scope.$id + "-" + Math.floor(1e4 * Math.random()), this.activeDate = angular.isDefined($attrs.initDate) ? $scope.$parent.$eval($attrs.initDate) : new Date, $scope.isActive = function(dateObject) {
            return 0 === self.compare(dateObject.date, self.activeDate) ? ($scope.activeDateId = dateObject.uid, !0) : !1
        }, this.init = function(ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_, ngModelCtrl.$render = function() {
                self.render()
            }
        }, this.render = function() {
            if (ngModelCtrl.$modelValue) {
                var date = new Date(ngModelCtrl.$modelValue),
                    isValid = !isNaN(date);
                isValid ? this.activeDate = date : $log.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.'), ngModelCtrl.$setValidity("date", isValid)
            }
            this.refreshView()
        }, this.refreshView = function() {
            if (this.element) {
                this._refreshView();
                var date = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;
                ngModelCtrl.$setValidity("date-disabled", !date || this.element && !this.isDisabled(date))
            }
        }, this.createDateObject = function(date, format) {
            var model = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;
            return {
                date: date,
                label: dateFilter(date, format),
                selected: model && 0 === this.compare(date, model),
                disabled: this.isDisabled(date),
                current: 0 === this.compare(date, new Date)
            }
        }, this.isDisabled = function(date) {
            return this.minDate && this.compare(date, this.minDate) < 0 || this.maxDate && this.compare(date, this.maxDate) > 0 || $attrs.dateDisabled && $scope.dateDisabled({
                date: date,
                mode: $scope.datepickerMode
            })
        }, this.split = function(arr, size) {
            for (var arrays = []; arr.length > 0;) arrays.push(arr.splice(0, size));
            return arrays
        }, $scope.select = function(date) {
            if ($scope.datepickerMode === self.minMode) {
                var dt = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : new Date(0, 0, 0, 0, 0, 0, 0);
                dt.setFullYear(date.getFullYear(), date.getMonth(), date.getDate()), ngModelCtrl.$setViewValue(dt), ngModelCtrl.$render()
            } else self.activeDate = date, $scope.datepickerMode = self.modes[self.modes.indexOf($scope.datepickerMode) - 1]
        }, $scope.move = function(direction) {
            var year = self.activeDate.getFullYear() + direction * (self.step.years || 0),
                month = self.activeDate.getMonth() + direction * (self.step.months || 0);
            self.activeDate.setFullYear(year, month, 1), self.refreshView()
        }, $scope.toggleMode = function(direction) {
            direction = direction || 1, $scope.datepickerMode === self.maxMode && 1 === direction || $scope.datepickerMode === self.minMode && -1 === direction || ($scope.datepickerMode = self.modes[self.modes.indexOf($scope.datepickerMode) + direction])
        }, $scope.keys = {
            13: "enter",
            32: "space",
            33: "pageup",
            34: "pagedown",
            35: "end",
            36: "home",
            37: "left",
            38: "up",
            39: "right",
            40: "down"
        };
        var focusElement = function() {
            $timeout(function() {
                self.element[0].focus()
            }, 0, !1)
        };
        $scope.$on("datepicker.focus", focusElement), $scope.keydown = function(evt) {
            var key = $scope.keys[evt.which];
            if (key && !evt.shiftKey && !evt.altKey)
                if (evt.preventDefault(), evt.stopPropagation(), "enter" === key || "space" === key) {
                    if (self.isDisabled(self.activeDate)) return;
                    $scope.select(self.activeDate), focusElement()
                } else !evt.ctrlKey || "up" !== key && "down" !== key ? (self.handleKeyDown(key, evt), self.refreshView()) : ($scope.toggleMode("up" === key ? 1 : -1), focusElement())
        }
    }]).directive("datepicker", function() {
        return {
            restrict: "EA",
            replace: !0,
            templateUrl: "template/datepicker/datepicker.html",
            scope: {
                datepickerMode: "=?",
                dateDisabled: "&"
            },
            require: ["datepicker", "?^ngModel"],
            controller: "DatepickerController",
            link: function(scope, element, attrs, ctrls) {
                var datepickerCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                ngModelCtrl && datepickerCtrl.init(ngModelCtrl)
            }
        }
    }).directive("daypicker", ["dateFilter", function(dateFilter) {
        return {
            restrict: "EA",
            replace: !0,
            templateUrl: "template/datepicker/day.html",
            require: "^datepicker",
            link: function(scope, element, attrs, ctrl) {
                function getDaysInMonth(year, month) {
                    return 1 !== month || year % 4 !== 0 || year % 100 === 0 && year % 400 !== 0 ? DAYS_IN_MONTH[month] : 29
                }

                function getDates(startDate, n) {
                    var dates = new Array(n),
                        current = new Date(startDate),
                        i = 0;
                    for (current.setHours(12); n > i;) dates[i++] = new Date(current), current.setDate(current.getDate() + 1);
                    return dates
                }

                function getISO8601WeekNumber(date) {
                    var checkDate = new Date(date);
                    checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));
                    var time = checkDate.getTime();
                    return checkDate.setMonth(0), checkDate.setDate(1), Math.floor(Math.round((time - checkDate) / 864e5) / 7) + 1
                }
                scope.showWeeks = ctrl.showWeeks, ctrl.step = {
                    months: 1
                }, ctrl.element = element;
                var DAYS_IN_MONTH = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                ctrl._refreshView = function() {
                    var year = ctrl.activeDate.getFullYear(),
                        month = ctrl.activeDate.getMonth(),
                        firstDayOfMonth = new Date(year, month, 1),
                        difference = ctrl.startingDay - firstDayOfMonth.getDay(),
                        numDisplayedFromPreviousMonth = difference > 0 ? 7 - difference : -difference,
                        firstDate = new Date(firstDayOfMonth);
                    numDisplayedFromPreviousMonth > 0 && firstDate.setDate(-numDisplayedFromPreviousMonth + 1);
                    for (var days = getDates(firstDate, 42), i = 0; 42 > i; i++) days[i] = angular.extend(ctrl.createDateObject(days[i], ctrl.formatDay), {
                        secondary: days[i].getMonth() !== month,
                        uid: scope.uniqueId + "-" + i
                    });
                    scope.labels = new Array(7);
                    for (var j = 0; 7 > j; j++) scope.labels[j] = {
                        abbr: dateFilter(days[j].date, ctrl.formatDayHeader),
                        full: dateFilter(days[j].date, "EEEE")
                    };
                    if (scope.title = dateFilter(ctrl.activeDate, ctrl.formatDayTitle), scope.rows = ctrl.split(days, 7), scope.showWeeks) {
                        scope.weekNumbers = [];
                        for (var weekNumber = getISO8601WeekNumber(scope.rows[0][0].date), numWeeks = scope.rows.length; scope.weekNumbers.push(weekNumber++) < numWeeks;);
                    }
                }, ctrl.compare = function(date1, date2) {
                    return new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate())
                }, ctrl.handleKeyDown = function(key, evt) {
                    var date = ctrl.activeDate.getDate();
                    if ("left" === key) date -= 1;
                    else if ("up" === key) date -= 7;
                    else if ("right" === key) date += 1;
                    else if ("down" === key) date += 7;
                    else if ("pageup" === key || "pagedown" === key) {
                        var month = ctrl.activeDate.getMonth() + ("pageup" === key ? -1 : 1);
                        ctrl.activeDate.setMonth(month, 1), date = Math.min(getDaysInMonth(ctrl.activeDate.getFullYear(), ctrl.activeDate.getMonth()), date)
                    } else "home" === key ? date = 1 : "end" === key && (date = getDaysInMonth(ctrl.activeDate.getFullYear(), ctrl.activeDate.getMonth()));
                    ctrl.activeDate.setDate(date)
                }, ctrl.refreshView()
            }
        }
    }]).directive("monthpicker", ["dateFilter", function(dateFilter) {
        return {
            restrict: "EA",
            replace: !0,
            templateUrl: "template/datepicker/month.html",
            require: "^datepicker",
            link: function(scope, element, attrs, ctrl) {
                ctrl.step = {
                    years: 1
                }, ctrl.element = element, ctrl._refreshView = function() {
                    for (var months = new Array(12), year = ctrl.activeDate.getFullYear(), i = 0; 12 > i; i++) months[i] = angular.extend(ctrl.createDateObject(new Date(year, i, 1), ctrl.formatMonth), {
                        uid: scope.uniqueId + "-" + i
                    });
                    scope.title = dateFilter(ctrl.activeDate, ctrl.formatMonthTitle), scope.rows = ctrl.split(months, 3)
                }, ctrl.compare = function(date1, date2) {
                    return new Date(date1.getFullYear(), date1.getMonth()) - new Date(date2.getFullYear(), date2.getMonth())
                }, ctrl.handleKeyDown = function(key, evt) {
                    var date = ctrl.activeDate.getMonth();
                    if ("left" === key) date -= 1;
                    else if ("up" === key) date -= 3;
                    else if ("right" === key) date += 1;
                    else if ("down" === key) date += 3;
                    else if ("pageup" === key || "pagedown" === key) {
                        var year = ctrl.activeDate.getFullYear() + ("pageup" === key ? -1 : 1);
                        ctrl.activeDate.setFullYear(year)
                    } else "home" === key ? date = 0 : "end" === key && (date = 11);
                    ctrl.activeDate.setMonth(date)
                }, ctrl.refreshView()
            }
        }
    }]).directive("yearpicker", ["dateFilter", function(dateFilter) {
        return {
            restrict: "EA",
            replace: !0,
            templateUrl: "template/datepicker/year.html",
            require: "^datepicker",
            link: function(scope, element, attrs, ctrl) {
                function getStartingYear(year) {
                    return parseInt((year - 1) / range, 10) * range + 1
                }
                var range = ctrl.yearRange;
                ctrl.step = {
                    years: range
                }, ctrl.element = element, ctrl._refreshView = function() {
                    for (var years = new Array(range), i = 0, start = getStartingYear(ctrl.activeDate.getFullYear()); range > i; i++) years[i] = angular.extend(ctrl.createDateObject(new Date(start + i, 0, 1), ctrl.formatYear), {
                        uid: scope.uniqueId + "-" + i
                    });
                    scope.title = [years[0].label, years[range - 1].label].join(" - "), scope.rows = ctrl.split(years, 5)
                }, ctrl.compare = function(date1, date2) {
                    return date1.getFullYear() - date2.getFullYear()
                }, ctrl.handleKeyDown = function(key, evt) {
                    var date = ctrl.activeDate.getFullYear();
                    "left" === key ? date -= 1 : "up" === key ? date -= 5 : "right" === key ? date += 1 : "down" === key ? date += 5 : "pageup" === key || "pagedown" === key ? date += ("pageup" === key ? -1 : 1) * ctrl.step.years : "home" === key ? date = getStartingYear(ctrl.activeDate.getFullYear()) : "end" === key && (date = getStartingYear(ctrl.activeDate.getFullYear()) + range - 1), ctrl.activeDate.setFullYear(date)
                }, ctrl.refreshView()
            }
        }
    }]).constant("datepickerPopupConfig", {
        datepickerPopup: "yyyy-MM-dd",
        currentText: "Today",
        clearText: "Clear",
        closeText: "Done",
        closeOnDateSelection: !0,
        appendToBody: !1,
        showButtonBar: !0
    }).directive("datepickerPopup", ["$compile", "$parse", "$document", "$position", "dateFilter", "dateParser", "datepickerPopupConfig", function($compile, $parse, $document, $position, dateFilter, dateParser, datepickerPopupConfig) {
        return {
            restrict: "EA",
            require: "ngModel",
            scope: {
                isOpen: "=?",
                currentText: "@",
                clearText: "@",
                closeText: "@",
                dateDisabled: "&"
            },
            link: function(scope, element, attrs, ngModel) {
                function cameltoDash(string) {
                    return string.replace(/([A-Z])/g, function($1) {
                        return "-" + $1.toLowerCase()
                    })
                }

                function parseDate(viewValue) {
                    if (viewValue) {
                        if (angular.isDate(viewValue) && !isNaN(viewValue)) return ngModel.$setValidity("date", !0), viewValue;
                        if (angular.isString(viewValue)) {
                            var date = dateParser.parse(viewValue, dateFormat) || new Date(viewValue);
                            return isNaN(date) ? void ngModel.$setValidity("date", !1) : (ngModel.$setValidity("date", !0), date)
                        }
                        return void ngModel.$setValidity("date", !1)
                    }
                    return ngModel.$setValidity("date", !0), null
                }
                var dateFormat, closeOnDateSelection = angular.isDefined(attrs.closeOnDateSelection) ? scope.$parent.$eval(attrs.closeOnDateSelection) : datepickerPopupConfig.closeOnDateSelection,
                    appendToBody = angular.isDefined(attrs.datepickerAppendToBody) ? scope.$parent.$eval(attrs.datepickerAppendToBody) : datepickerPopupConfig.appendToBody;
                scope.showButtonBar = angular.isDefined(attrs.showButtonBar) ? scope.$parent.$eval(attrs.showButtonBar) : datepickerPopupConfig.showButtonBar, scope.getText = function(key) {
                    return scope[key + "Text"] || datepickerPopupConfig[key + "Text"]
                }, attrs.$observe("datepickerPopup", function(value) {
                    dateFormat = value || datepickerPopupConfig.datepickerPopup, ngModel.$render()
                });
                var popupEl = angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");
                popupEl.attr({
                    "ng-model": "date",
                    "ng-change": "dateSelection()"
                });
                var datepickerEl = angular.element(popupEl.children()[0]);
                attrs.datepickerOptions && angular.forEach(scope.$parent.$eval(attrs.datepickerOptions), function(value, option) {
                    datepickerEl.attr(cameltoDash(option), value)
                }), scope.watchData = {}, angular.forEach(["minDate", "maxDate", "datepickerMode"], function(key) {
                    if (attrs[key]) {
                        var getAttribute = $parse(attrs[key]);
                        if (scope.$parent.$watch(getAttribute, function(value) {
                                scope.watchData[key] = value
                            }), datepickerEl.attr(cameltoDash(key), "watchData." + key), "datepickerMode" === key) {
                            var setAttribute = getAttribute.assign;
                            scope.$watch("watchData." + key, function(value, oldvalue) {
                                value !== oldvalue && setAttribute(scope.$parent, value)
                            })
                        }
                    }
                }), attrs.dateDisabled && datepickerEl.attr("date-disabled", "dateDisabled({ date: date, mode: mode })"), ngModel.$parsers.unshift(parseDate), scope.dateSelection = function(dt) {
                    angular.isDefined(dt) && (scope.date = dt), ngModel.$setViewValue(scope.date), ngModel.$render(), closeOnDateSelection && (scope.isOpen = !1, element[0].focus())
                }, element.bind("input change keyup", function() {
                    scope.$apply(function() {
                        scope.date = ngModel.$modelValue
                    })
                }), ngModel.$render = function() {
                    var date = ngModel.$viewValue ? dateFilter(ngModel.$viewValue, dateFormat) : "";
                    element.val(date), scope.date = parseDate(ngModel.$modelValue)
                };
                var documentClickBind = function(event) {
                        scope.isOpen && event.target !== element[0] && scope.$apply(function() {
                            scope.isOpen = !1
                        })
                    },
                    keydown = function(evt, noApply) {
                        scope.keydown(evt)
                    };
                element.bind("keydown", keydown), scope.keydown = function(evt) {
                    27 === evt.which ? (evt.preventDefault(), evt.stopPropagation(), scope.close()) : 40 !== evt.which || scope.isOpen || (scope.isOpen = !0)
                }, scope.$watch("isOpen", function(value) {
                    value ? (scope.$broadcast("datepicker.focus"), scope.position = appendToBody ? $position.offset(element) : $position.position(element), scope.position.top = scope.position.top + element.prop("offsetHeight"), $document.bind("click", documentClickBind)) : $document.unbind("click", documentClickBind)
                }), scope.select = function(date) {
                    if ("today" === date) {
                        var today = new Date;
                        angular.isDate(ngModel.$modelValue) ? (date = new Date(ngModel.$modelValue), date.setFullYear(today.getFullYear(), today.getMonth(), today.getDate())) : date = new Date(today.setHours(0, 0, 0, 0))
                    }
                    scope.dateSelection(date)
                }, scope.close = function() {
                    scope.isOpen = !1, element[0].focus()
                };
                var $popup = $compile(popupEl)(scope);
                popupEl.remove(), appendToBody ? $document.find("body").append($popup) : element.after($popup), scope.$on("$destroy", function() {
                    $popup.remove(), element.unbind("keydown", keydown), $document.unbind("click", documentClickBind)
                })
            }
        }
    }]).directive("datepickerPopupWrap", function() {
        return {
            restrict: "EA",
            replace: !0,
            transclude: !0,
            templateUrl: "template/datepicker/popup.html",
            link: function(scope, element, attrs) {
                element.bind("click", function(event) {
                    event.preventDefault(), event.stopPropagation()
                })
            }
        }
    }), angular.module("ui.bootstrap.dropdown", []).constant("dropdownConfig", {
        openClass: "open"
    }).service("dropdownService", ["$document", function($document) {
        var openScope = null;
        this.open = function(dropdownScope) {
            openScope || ($document.bind("click", closeDropdown), $document.bind("keydown", escapeKeyBind)), openScope && openScope !== dropdownScope && (openScope.isOpen = !1), openScope = dropdownScope
        }, this.close = function(dropdownScope) {
            openScope === dropdownScope && (openScope = null, $document.unbind("click", closeDropdown), $document.unbind("keydown", escapeKeyBind))
        };
        var closeDropdown = function(evt) {
                if (openScope) {
                    var toggleElement = openScope.getToggleElement();
                    evt && toggleElement && toggleElement[0].contains(evt.target) || openScope.$apply(function() {
                        openScope.isOpen = !1
                    })
                }
            },
            escapeKeyBind = function(evt) {
                27 === evt.which && (openScope.focusToggleElement(), closeDropdown())
            }
    }]).controller("DropdownController", ["$scope", "$attrs", "$parse", "dropdownConfig", "dropdownService", "$animate", function($scope, $attrs, $parse, dropdownConfig, dropdownService, $animate) {
        var getIsOpen, self = this,
            scope = $scope.$new(),
            openClass = dropdownConfig.openClass,
            setIsOpen = angular.noop,
            toggleInvoker = $attrs.onToggle ? $parse($attrs.onToggle) : angular.noop;
        this.init = function(element) {
            self.$element = element, $attrs.isOpen && (getIsOpen = $parse($attrs.isOpen), setIsOpen = getIsOpen.assign, $scope.$watch(getIsOpen, function(value) {
                scope.isOpen = !!value
            }))
        }, this.toggle = function(open) {
            return scope.isOpen = arguments.length ? !!open : !scope.isOpen
        }, this.isOpen = function() {
            return scope.isOpen
        }, scope.getToggleElement = function() {
            return self.toggleElement
        }, scope.focusToggleElement = function() {
            self.toggleElement && self.toggleElement[0].focus()
        }, scope.$watch("isOpen", function(isOpen, wasOpen) {
            $animate[isOpen ? "addClass" : "removeClass"](self.$element, openClass), isOpen ? (scope.focusToggleElement(), dropdownService.open(scope)) : dropdownService.close(scope), setIsOpen($scope, isOpen), angular.isDefined(isOpen) && isOpen !== wasOpen && toggleInvoker($scope, {
                open: !!isOpen
            })
        }), $scope.$on("$locationChangeSuccess", function() {
            scope.isOpen = !1
        }), $scope.$on("$destroy", function() {
            scope.$destroy()
        })
    }]).directive("dropdown", function() {
        return {
            controller: "DropdownController",
            link: function(scope, element, attrs, dropdownCtrl) {
                dropdownCtrl.init(element)
            }
        }
    }).directive("dropdownToggle", function() {
        return {
            require: "?^dropdown",
            link: function(scope, element, attrs, dropdownCtrl) {
                if (dropdownCtrl) {
                    dropdownCtrl.toggleElement = element;
                    var toggleDropdown = function(event) {
                        event.preventDefault(), element.hasClass("disabled") || attrs.disabled || scope.$apply(function() {
                            dropdownCtrl.toggle()
                        })
                    };
                    element.bind("click", toggleDropdown), element.attr({
                        "aria-haspopup": !0,
                        "aria-expanded": !1
                    }), scope.$watch(dropdownCtrl.isOpen, function(isOpen) {
                        element.attr("aria-expanded", !!isOpen)
                    }), scope.$on("$destroy", function() {
                        element.unbind("click", toggleDropdown)
                    })
                }
            }
        }
    }), angular.module("ui.bootstrap.modal", ["ui.bootstrap.transition"]).factory("$$stackedMap", function() {
        return {
            createNew: function() {
                var stack = [];
                return {
                    add: function(key, value) {
                        stack.push({
                            key: key,
                            value: value
                        })
                    },
                    get: function(key) {
                        for (var i = 0; i < stack.length; i++)
                            if (key == stack[i].key) return stack[i]
                    },
                    keys: function() {
                        for (var keys = [], i = 0; i < stack.length; i++) keys.push(stack[i].key);
                        return keys
                    },
                    top: function() {
                        return stack[stack.length - 1]
                    },
                    remove: function(key) {
                        for (var idx = -1, i = 0; i < stack.length; i++)
                            if (key == stack[i].key) {
                                idx = i;
                                break
                            }
                        return stack.splice(idx, 1)[0]
                    },
                    removeTop: function() {
                        return stack.splice(stack.length - 1, 1)[0]
                    },
                    length: function() {
                        return stack.length
                    }
                }
            }
        }
    }).directive("modalBackdrop", ["$timeout", function($timeout) {
        return {
            restrict: "EA",
            replace: !0,
            templateUrl: "template/modal/backdrop.html",
            link: function(scope, element, attrs) {
                scope.backdropClass = attrs.backdropClass || "", scope.animate = !1,
                    $timeout(function() {
                        scope.animate = !0
                    })
            }
        }
    }]).directive("modalWindow", ["$modalStack", "$timeout", function($modalStack, $timeout) {
        return {
            restrict: "EA",
            scope: {
                index: "@",
                animate: "="
            },
            replace: !0,
            transclude: !0,
            templateUrl: function(tElement, tAttrs) {
                return tAttrs.templateUrl || "template/modal/window.html"
            },
            link: function(scope, element, attrs) {
                element.addClass(attrs.windowClass || ""), scope.size = attrs.size, $timeout(function() {
                    scope.animate = !0, element[0].querySelectorAll("[autofocus]").length || element[0].focus()
                }), scope.close = function(evt) {
                    var modal = $modalStack.getTop();
                    modal && modal.value.backdrop && "static" != modal.value.backdrop && evt.target === evt.currentTarget && (evt.preventDefault(), evt.stopPropagation(), $modalStack.dismiss(modal.key, "backdrop click"))
                }
            }
        }
    }]).directive("modalTransclude", function() {
        return {
            link: function($scope, $element, $attrs, controller, $transclude) {
                $transclude($scope.$parent, function(clone) {
                    $element.empty(), $element.append(clone)
                })
            }
        }
    }).factory("$modalStack", ["$transition", "$timeout", "$document", "$compile", "$rootScope", "$$stackedMap", function($transition, $timeout, $document, $compile, $rootScope, $$stackedMap) {
        function backdropIndex() {
            for (var topBackdropIndex = -1, opened = openedWindows.keys(), i = 0; i < opened.length; i++) openedWindows.get(opened[i]).value.backdrop && (topBackdropIndex = i);
            return topBackdropIndex
        }

        function removeModalWindow(modalInstance) {
            var body = $document.find("body").eq(0),
                modalWindow = openedWindows.get(modalInstance).value;
            openedWindows.remove(modalInstance), removeAfterAnimate(modalWindow.modalDomEl, modalWindow.modalScope, 300, function() {
                modalWindow.modalScope.$destroy(), body.toggleClass(OPENED_MODAL_CLASS, openedWindows.length() > 0), checkRemoveBackdrop()
            })
        }

        function checkRemoveBackdrop() {
            if (backdropDomEl && -1 == backdropIndex()) {
                var backdropScopeRef = backdropScope;
                removeAfterAnimate(backdropDomEl, backdropScope, 150, function() {
                    backdropScopeRef.$destroy(), backdropScopeRef = null
                }), backdropDomEl = void 0, backdropScope = void 0
            }
        }

        function removeAfterAnimate(domEl, scope, emulateTime, done) {
            function afterAnimating() {
                afterAnimating.done || (afterAnimating.done = !0, domEl.remove(), done && done())
            }
            scope.animate = !1;
            var transitionEndEventName = $transition.transitionEndEventName;
            if (transitionEndEventName) {
                var timeout = $timeout(afterAnimating, emulateTime);
                domEl.bind(transitionEndEventName, function() {
                    $timeout.cancel(timeout), afterAnimating(), scope.$apply()
                })
            } else $timeout(afterAnimating)
        }
        var backdropDomEl, backdropScope, OPENED_MODAL_CLASS = "modal-open",
            openedWindows = $$stackedMap.createNew(),
            $modalStack = {};
        return $rootScope.$watch(backdropIndex, function(newBackdropIndex) {
            backdropScope && (backdropScope.index = newBackdropIndex)
        }), $document.bind("keydown", function(evt) {
            var modal;
            27 === evt.which && (modal = openedWindows.top(), modal && modal.value.keyboard && (evt.preventDefault(), $rootScope.$apply(function() {
                $modalStack.dismiss(modal.key, "escape key press")
            })))
        }), $modalStack.open = function(modalInstance, modal) {
            openedWindows.add(modalInstance, {
                deferred: modal.deferred,
                modalScope: modal.scope,
                backdrop: modal.backdrop,
                keyboard: modal.keyboard
            });
            var body = $document.find("body").eq(0),
                currBackdropIndex = backdropIndex();
            if (currBackdropIndex >= 0 && !backdropDomEl) {
                backdropScope = $rootScope.$new(!0), backdropScope.index = currBackdropIndex;
                var angularBackgroundDomEl = angular.element("<div modal-backdrop></div>");
                angularBackgroundDomEl.attr("backdrop-class", modal.backdropClass), backdropDomEl = $compile(angularBackgroundDomEl)(backdropScope), body.append(backdropDomEl)
            }
            var angularDomEl = angular.element("<div modal-window></div>");
            angularDomEl.attr({
                "template-url": modal.windowTemplateUrl,
                "window-class": modal.windowClass,
                size: modal.size,
                index: openedWindows.length() - 1,
                animate: "animate"
            }).html(modal.content);
            var modalDomEl = $compile(angularDomEl)(modal.scope);
            openedWindows.top().value.modalDomEl = modalDomEl, body.append(modalDomEl), body.addClass(OPENED_MODAL_CLASS)
        }, $modalStack.close = function(modalInstance, result) {
            var modalWindow = openedWindows.get(modalInstance);
            modalWindow && (modalWindow.value.deferred.resolve(result), removeModalWindow(modalInstance))
        }, $modalStack.dismiss = function(modalInstance, reason) {
            var modalWindow = openedWindows.get(modalInstance);
            modalWindow && (modalWindow.value.deferred.reject(reason), removeModalWindow(modalInstance))
        }, $modalStack.dismissAll = function(reason) {
            for (var topModal = this.getTop(); topModal;) this.dismiss(topModal.key, reason), topModal = this.getTop()
        }, $modalStack.getTop = function() {
            return openedWindows.top()
        }, $modalStack
    }]).provider("$modal", function() {
        var $modalProvider = {
            options: {
                backdrop: !0,
                keyboard: !0
            },
            $get: ["$injector", "$rootScope", "$q", "$http", "$templateCache", "$controller", "$modalStack", function($injector, $rootScope, $q, $http, $templateCache, $controller, $modalStack) {
                function getTemplatePromise(options) {
                    return options.template ? $q.when(options.template) : $http.get(angular.isFunction(options.templateUrl) ? options.templateUrl() : options.templateUrl, {
                        cache: $templateCache
                    }).then(function(result) {
                        return result.data
                    })
                }

                function getResolvePromises(resolves) {
                    var promisesArr = [];
                    return angular.forEach(resolves, function(value) {
                        (angular.isFunction(value) || angular.isArray(value)) && promisesArr.push($q.when($injector.invoke(value)))
                    }), promisesArr
                }
                var $modal = {};
                return $modal.open = function(modalOptions) {
                    var modalResultDeferred = $q.defer(),
                        modalOpenedDeferred = $q.defer(),
                        modalInstance = {
                            result: modalResultDeferred.promise,
                            opened: modalOpenedDeferred.promise,
                            close: function(result) {
                                $modalStack.close(modalInstance, result)
                            },
                            dismiss: function(reason) {
                                $modalStack.dismiss(modalInstance, reason)
                            }
                        };
                    if (modalOptions = angular.extend({}, $modalProvider.options, modalOptions), modalOptions.resolve = modalOptions.resolve || {}, !modalOptions.template && !modalOptions.templateUrl) throw new Error("One of template or templateUrl options is required.");
                    var templateAndResolvePromise = $q.all([getTemplatePromise(modalOptions)].concat(getResolvePromises(modalOptions.resolve)));
                    return templateAndResolvePromise.then(function(tplAndVars) {
                        var modalScope = (modalOptions.scope || $rootScope).$new();
                        modalScope.$close = modalInstance.close, modalScope.$dismiss = modalInstance.dismiss;
                        var ctrlInstance, ctrlLocals = {},
                            resolveIter = 1;
                        modalOptions.controller && (ctrlLocals.$scope = modalScope, ctrlLocals.$modalInstance = modalInstance, angular.forEach(modalOptions.resolve, function(value, key) {
                            ctrlLocals[key] = tplAndVars[resolveIter++]
                        }), ctrlInstance = $controller(modalOptions.controller, ctrlLocals), modalOptions.controllerAs && (modalScope[modalOptions.controllerAs] = ctrlInstance)), $modalStack.open(modalInstance, {
                            scope: modalScope,
                            deferred: modalResultDeferred,
                            content: tplAndVars[0],
                            backdrop: modalOptions.backdrop,
                            keyboard: modalOptions.keyboard,
                            backdropClass: modalOptions.backdropClass,
                            windowClass: modalOptions.windowClass,
                            windowTemplateUrl: modalOptions.windowTemplateUrl,
                            size: modalOptions.size
                        })
                    }, function(reason) {
                        modalResultDeferred.reject(reason)
                    }), templateAndResolvePromise.then(function() {
                        modalOpenedDeferred.resolve(!0)
                    }, function() {
                        modalOpenedDeferred.reject(!1)
                    }), modalInstance
                }, $modal
            }]
        };
        return $modalProvider
    }), angular.module("ui.bootstrap.pagination", []).controller("PaginationController", ["$scope", "$attrs", "$parse", function($scope, $attrs, $parse) {
        var self = this,
            ngModelCtrl = {
                $setViewValue: angular.noop
            },
            setNumPages = $attrs.numPages ? $parse($attrs.numPages).assign : angular.noop;
        this.init = function(ngModelCtrl_, config) {
            ngModelCtrl = ngModelCtrl_, this.config = config, ngModelCtrl.$render = function() {
                self.render()
            }, $attrs.itemsPerPage ? $scope.$parent.$watch($parse($attrs.itemsPerPage), function(value) {
                self.itemsPerPage = parseInt(value, 10), $scope.totalPages = self.calculateTotalPages()
            }) : this.itemsPerPage = config.itemsPerPage
        }, this.calculateTotalPages = function() {
            var totalPages = this.itemsPerPage < 1 ? 1 : Math.ceil($scope.totalItems / this.itemsPerPage);
            return Math.max(totalPages || 0, 1)
        }, this.render = function() {
            $scope.page = parseInt(ngModelCtrl.$viewValue, 10) || 1
        }, $scope.selectPage = function(page) {
            $scope.page !== page && page > 0 && page <= $scope.totalPages && (ngModelCtrl.$setViewValue(page), ngModelCtrl.$render())
        }, $scope.getText = function(key) {
            return $scope[key + "Text"] || self.config[key + "Text"]
        }, $scope.noPrevious = function() {
            return 1 === $scope.page
        }, $scope.noNext = function() {
            return $scope.page === $scope.totalPages
        }, $scope.$watch("totalItems", function() {
            $scope.totalPages = self.calculateTotalPages()
        }), $scope.$watch("totalPages", function(value) {
            setNumPages($scope.$parent, value), $scope.page > value ? $scope.selectPage(value) : ngModelCtrl.$render()
        })
    }]).constant("paginationConfig", {
        itemsPerPage: 10,
        boundaryLinks: !1,
        directionLinks: !0,
        firstText: "First",
        previousText: "Previous",
        nextText: "Next",
        lastText: "Last",
        rotate: !0
    }).directive("pagination", ["$parse", "paginationConfig", function($parse, paginationConfig) {
        return {
            restrict: "EA",
            scope: {
                totalItems: "=",
                firstText: "@",
                previousText: "@",
                nextText: "@",
                lastText: "@"
            },
            require: ["pagination", "?ngModel"],
            controller: "PaginationController",
            templateUrl: "template/pagination/pagination.html",
            replace: !0,
            link: function(scope, element, attrs, ctrls) {
                function makePage(number, text, isActive) {
                    return {
                        number: number,
                        text: text,
                        active: isActive
                    }
                }

                function getPages(currentPage, totalPages) {
                    var pages = [],
                        startPage = 1,
                        endPage = totalPages,
                        isMaxSized = angular.isDefined(maxSize) && totalPages > maxSize;
                    isMaxSized && (rotate ? (startPage = Math.max(currentPage - Math.floor(maxSize / 2), 1), endPage = startPage + maxSize - 1, endPage > totalPages && (endPage = totalPages, startPage = endPage - maxSize + 1)) : (startPage = (Math.ceil(currentPage / maxSize) - 1) * maxSize + 1, endPage = Math.min(startPage + maxSize - 1, totalPages)));
                    for (var number = startPage; endPage >= number; number++) {
                        var page = makePage(number, number, number === currentPage);
                        pages.push(page)
                    }
                    if (isMaxSized && !rotate) {
                        if (startPage > 1) {
                            var previousPageSet = makePage(startPage - 1, "...", !1);
                            pages.unshift(previousPageSet)
                        }
                        if (totalPages > endPage) {
                            var nextPageSet = makePage(endPage + 1, "...", !1);
                            pages.push(nextPageSet)
                        }
                    }
                    return pages
                }
                var paginationCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                if (ngModelCtrl) {
                    var maxSize = angular.isDefined(attrs.maxSize) ? scope.$parent.$eval(attrs.maxSize) : paginationConfig.maxSize,
                        rotate = angular.isDefined(attrs.rotate) ? scope.$parent.$eval(attrs.rotate) : paginationConfig.rotate;
                    scope.boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$parent.$eval(attrs.boundaryLinks) : paginationConfig.boundaryLinks, scope.directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$parent.$eval(attrs.directionLinks) : paginationConfig.directionLinks, paginationCtrl.init(ngModelCtrl, paginationConfig), attrs.maxSize && scope.$parent.$watch($parse(attrs.maxSize), function(value) {
                        maxSize = parseInt(value, 10), paginationCtrl.render()
                    });
                    var originalRender = paginationCtrl.render;
                    paginationCtrl.render = function() {
                        originalRender(), scope.page > 0 && scope.page <= scope.totalPages && (scope.pages = getPages(scope.page, scope.totalPages))
                    }
                }
            }
        }
    }]).constant("pagerConfig", {
        itemsPerPage: 10,
        previousText: "Â« Previous",
        nextText: "Next Â»",
        align: !0
    }).directive("pager", ["pagerConfig", function(pagerConfig) {
        return {
            restrict: "EA",
            scope: {
                totalItems: "=",
                previousText: "@",
                nextText: "@"
            },
            require: ["pager", "?ngModel"],
            controller: "PaginationController",
            templateUrl: "template/pagination/pager.html",
            replace: !0,
            link: function(scope, element, attrs, ctrls) {
                var paginationCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                ngModelCtrl && (scope.align = angular.isDefined(attrs.align) ? scope.$parent.$eval(attrs.align) : pagerConfig.align, paginationCtrl.init(ngModelCtrl, pagerConfig))
            }
        }
    }]), angular.module("ui.bootstrap.tooltip", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).provider("$tooltip", function() {
        function snake_case(name) {
            var regexp = /[A-Z]/g,
                separator = "-";
            return name.replace(regexp, function(letter, pos) {
                return (pos ? separator : "") + letter.toLowerCase()
            })
        }
        var defaultOptions = {
                placement: "top",
                animation: !0,
                popupDelay: 0
            },
            triggerMap = {
                mouseenter: "mouseleave",
                click: "click",
                focus: "blur"
            },
            globalOptions = {};
        this.options = function(value) {
            angular.extend(globalOptions, value)
        }, this.setTriggers = function(triggers) {
            angular.extend(triggerMap, triggers)
        }, this.$get = ["$window", "$compile", "$timeout", "$document", "$position", "$interpolate", function($window, $compile, $timeout, $document, $position, $interpolate) {
            return function(type, prefix, defaultTriggerShow) {
                function getTriggers(trigger) {
                    var show = trigger || options.trigger || defaultTriggerShow,
                        hide = triggerMap[show] || show;
                    return {
                        show: show,
                        hide: hide
                    }
                }
                var options = angular.extend({}, defaultOptions, globalOptions),
                    directiveName = snake_case(type),
                    startSym = $interpolate.startSymbol(),
                    endSym = $interpolate.endSymbol(),
                    template = "<div " + directiveName + '-popup title="' + startSym + "title" + endSym + '" content="' + startSym + "content" + endSym + '" placement="' + startSym + "placement" + endSym + '" animation="animation" is-open="isOpen"></div>';
                return {
                    restrict: "EA",
                    compile: function(tElem, tAttrs) {
                        var tooltipLinker = $compile(template);
                        return function(scope, element, attrs) {
                            function toggleTooltipBind() {
                                ttScope.isOpen ? hideTooltipBind() : showTooltipBind()
                            }

                            function showTooltipBind() {
                                (!hasEnableExp || scope.$eval(attrs[prefix + "Enable"])) && (prepareTooltip(), ttScope.popupDelay ? popupTimeout || (popupTimeout = $timeout(show, ttScope.popupDelay, !1), popupTimeout.then(function(reposition) {
                                    reposition()
                                })) : show()())
                            }

                            function hideTooltipBind() {
                                scope.$apply(function() {
                                    hide()
                                })
                            }

                            function show() {
                                return popupTimeout = null, transitionTimeout && ($timeout.cancel(transitionTimeout), transitionTimeout = null), ttScope.content ? (createTooltip(), tooltip.css({
                                    top: 0,
                                    left: 0,
                                    display: "block"
                                }), appendToBody ? $document.find("body").append(tooltip) : element.after(tooltip), positionTooltip(), ttScope.isOpen = !0, ttScope.$digest(), positionTooltip) : angular.noop
                            }

                            function hide() {
                                ttScope.isOpen = !1, $timeout.cancel(popupTimeout), popupTimeout = null, ttScope.animation ? transitionTimeout || (transitionTimeout = $timeout(removeTooltip, 500)) : removeTooltip()
                            }

                            function createTooltip() {
                                tooltip && removeTooltip(), tooltipLinkedScope = ttScope.$new(), tooltip = tooltipLinker(tooltipLinkedScope, angular.noop)
                            }

                            function removeTooltip() {
                                transitionTimeout = null, tooltip && (tooltip.remove(), tooltip = null), tooltipLinkedScope && (tooltipLinkedScope.$destroy(), tooltipLinkedScope = null)
                            }

                            function prepareTooltip() {
                                prepPlacement(), prepPopupDelay()
                            }

                            function prepPlacement() {
                                var val = attrs[prefix + "Placement"];
                                ttScope.placement = angular.isDefined(val) ? val : options.placement
                            }

                            function prepPopupDelay() {
                                var val = attrs[prefix + "PopupDelay"],
                                    delay = parseInt(val, 10);
                                ttScope.popupDelay = isNaN(delay) ? options.popupDelay : delay
                            }

                            function prepTriggers() {
                                var val = attrs[prefix + "Trigger"];
                                unregisterTriggers(), triggers = getTriggers(val), triggers.show === triggers.hide ? element.bind(triggers.show, toggleTooltipBind) : (element.bind(triggers.show, showTooltipBind), element.bind(triggers.hide, hideTooltipBind))
                            }
                            var tooltip, tooltipLinkedScope, transitionTimeout, popupTimeout, appendToBody = angular.isDefined(options.appendToBody) ? options.appendToBody : !1,
                                triggers = getTriggers(void 0),
                                hasEnableExp = angular.isDefined(attrs[prefix + "Enable"]),
                                ttScope = scope.$new(!0),
                                positionTooltip = function() {
                                    var ttPosition = $position.positionElements(element, tooltip, ttScope.placement, appendToBody);
                                    ttPosition.top += "px", ttPosition.left += "px", tooltip.css(ttPosition)
                                };
                            ttScope.isOpen = !1, attrs.$observe(type, function(val) {
                                ttScope.content = val, !val && ttScope.isOpen && hide()
                            }), attrs.$observe(prefix + "Title", function(val) {
                                ttScope.title = val
                            });
                            var unregisterTriggers = function() {
                                element.unbind(triggers.show, showTooltipBind), element.unbind(triggers.hide, hideTooltipBind)
                            };
                            prepTriggers();
                            var animation = scope.$eval(attrs[prefix + "Animation"]);
                            ttScope.animation = angular.isDefined(animation) ? !!animation : options.animation;
                            var appendToBodyVal = scope.$eval(attrs[prefix + "AppendToBody"]);
                            appendToBody = angular.isDefined(appendToBodyVal) ? appendToBodyVal : appendToBody, appendToBody && scope.$on("$locationChangeSuccess", function() {
                                ttScope.isOpen && hide()
                            }), scope.$on("$destroy", function() {
                                $timeout.cancel(transitionTimeout), $timeout.cancel(popupTimeout), unregisterTriggers(), removeTooltip(), ttScope = null
                            })
                        }
                    }
                }
            }
        }]
    }).directive("tooltipPopup", function() {
        return {
            restrict: "EA",
            replace: !0,
            scope: {
                content: "@",
                placement: "@",
                animation: "&",
                isOpen: "&"
            },
            templateUrl: "template/tooltip/tooltip-popup.html"
        }
    }).directive("tooltip", ["$tooltip", function($tooltip) {
        return $tooltip("tooltip", "tooltip", "mouseenter")
    }]).directive("tooltipHtmlUnsafePopup", function() {
        return {
            restrict: "EA",
            replace: !0,
            scope: {
                content: "@",
                placement: "@",
                animation: "&",
                isOpen: "&"
            },
            templateUrl: "template/tooltip/tooltip-html-unsafe-popup.html"
        }
    }).directive("tooltipHtmlUnsafe", ["$tooltip", function($tooltip) {
        return $tooltip("tooltipHtmlUnsafe", "tooltip", "mouseenter")
    }]), angular.module("ui.bootstrap.popover", ["ui.bootstrap.tooltip"]).directive("popoverPopup", function() {
        return {
            restrict: "EA",
            replace: !0,
            scope: {
                title: "@",
                content: "@",
                placement: "@",
                animation: "&",
                isOpen: "&"
            },
            templateUrl: "template/popover/popover.html"
        }
    }).directive("popover", ["$tooltip", function($tooltip) {
        return $tooltip("popover", "popover", "click")
    }]), angular.module("ui.bootstrap.progressbar", []).constant("progressConfig", {
        animate: !0,
        max: 100
    }).controller("ProgressController", ["$scope", "$attrs", "progressConfig", function($scope, $attrs, progressConfig) {
        var self = this,
            animate = angular.isDefined($attrs.animate) ? $scope.$parent.$eval($attrs.animate) : progressConfig.animate;
        this.bars = [], $scope.max = angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : progressConfig.max, this.addBar = function(bar, element) {
            animate || element.css({
                transition: "none"
            }), this.bars.push(bar), bar.$watch("value", function(value) {
                bar.percent = +(100 * value / $scope.max).toFixed(2)
            }), bar.$on("$destroy", function() {
                element = null, self.removeBar(bar)
            })
        }, this.removeBar = function(bar) {
            this.bars.splice(this.bars.indexOf(bar), 1)
        }
    }]).directive("progress", function() {
        return {
            restrict: "EA",
            replace: !0,
            transclude: !0,
            controller: "ProgressController",
            require: "progress",
            scope: {},
            templateUrl: "template/progressbar/progress.html"
        }
    }).directive("bar", function() {
        return {
            restrict: "EA",
            replace: !0,
            transclude: !0,
            require: "^progress",
            scope: {
                value: "=",
                type: "@"
            },
            templateUrl: "template/progressbar/bar.html",
            link: function(scope, element, attrs, progressCtrl) {
                progressCtrl.addBar(scope, element)
            }
        }
    }).directive("progressbar", function() {
        return {
            restrict: "EA",
            replace: !0,
            transclude: !0,
            controller: "ProgressController",
            scope: {
                value: "=",
                type: "@"
            },
            templateUrl: "template/progressbar/progressbar.html",
            link: function(scope, element, attrs, progressCtrl) {
                progressCtrl.addBar(scope, angular.element(element.children()[0]))
            }
        }
    }), angular.module("ui.bootstrap.rating", []).constant("ratingConfig", {
        max: 5,
        stateOn: null,
        stateOff: null
    }).controller("RatingController", ["$scope", "$attrs", "ratingConfig", function($scope, $attrs, ratingConfig) {
        var ngModelCtrl = {
            $setViewValue: angular.noop
        };
        this.init = function(ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_, ngModelCtrl.$render = this.render, this.stateOn = angular.isDefined($attrs.stateOn) ? $scope.$parent.$eval($attrs.stateOn) : ratingConfig.stateOn, this.stateOff = angular.isDefined($attrs.stateOff) ? $scope.$parent.$eval($attrs.stateOff) : ratingConfig.stateOff;
            var ratingStates = angular.isDefined($attrs.ratingStates) ? $scope.$parent.$eval($attrs.ratingStates) : new Array(angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : ratingConfig.max);
            $scope.range = this.buildTemplateObjects(ratingStates)
        }, this.buildTemplateObjects = function(states) {
            for (var i = 0, n = states.length; n > i; i++) states[i] = angular.extend({
                index: i
            }, {
                stateOn: this.stateOn,
                stateOff: this.stateOff
            }, states[i]);
            return states
        }, $scope.rate = function(value) {
            !$scope.readonly && value >= 0 && value <= $scope.range.length && (ngModelCtrl.$setViewValue(value), ngModelCtrl.$render())
        }, $scope.enter = function(value) {
            $scope.readonly || ($scope.value = value), $scope.onHover({
                value: value
            })
        }, $scope.reset = function() {
            $scope.value = ngModelCtrl.$viewValue, $scope.onLeave()
        }, $scope.onKeydown = function(evt) {
            /(37|38|39|40)/.test(evt.which) && (evt.preventDefault(), evt.stopPropagation(), $scope.rate($scope.value + (38 === evt.which || 39 === evt.which ? 1 : -1)))
        }, this.render = function() {
            $scope.value = ngModelCtrl.$viewValue
        }
    }]).directive("rating", function() {
        return {
            restrict: "EA",
            require: ["rating", "ngModel"],
            scope: {
                readonly: "=?",
                onHover: "&",
                onLeave: "&"
            },
            controller: "RatingController",
            templateUrl: "template/rating/rating.html",
            replace: !0,
            link: function(scope, element, attrs, ctrls) {
                var ratingCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                ngModelCtrl && ratingCtrl.init(ngModelCtrl)
            }
        }
    }), angular.module("ui.bootstrap.tabs", []).controller("TabsetController", ["$scope", function($scope) {
        var ctrl = this,
            tabs = ctrl.tabs = $scope.tabs = [];
        ctrl.select = function(selectedTab) {
            angular.forEach(tabs, function(tab) {
                tab.active && tab !== selectedTab && (tab.active = !1, tab.onDeselect())
            }), selectedTab.active = !0, selectedTab.onSelect()
        }, ctrl.addTab = function(tab) {
            tabs.push(tab), 1 === tabs.length ? tab.active = !0 : tab.active && ctrl.select(tab)
        }, ctrl.removeTab = function(tab) {
            var index = tabs.indexOf(tab);
            if (tab.active && tabs.length > 1 && !destroyed) {
                var newActiveIndex = index == tabs.length - 1 ? index - 1 : index + 1;
                ctrl.select(tabs[newActiveIndex])
            }
            tabs.splice(index, 1)
        };
        var destroyed;
        $scope.$on("$destroy", function() {
            destroyed = !0
        })
    }]).directive("tabset", function() {
        return {
            restrict: "EA",
            transclude: !0,
            replace: !0,
            scope: {
                type: "@"
            },
            controller: "TabsetController",
            templateUrl: "template/tabs/tabset.html",
            link: function(scope, element, attrs) {
                scope.vertical = angular.isDefined(attrs.vertical) ? scope.$parent.$eval(attrs.vertical) : !1, scope.justified = angular.isDefined(attrs.justified) ? scope.$parent.$eval(attrs.justified) : !1
            }
        }
    }).directive("tab", ["$parse", function($parse) {
        return {
            require: "^tabset",
            restrict: "EA",
            replace: !0,
            templateUrl: "template/tabs/tab.html",
            transclude: !0,
            scope: {
                active: "=?",
                heading: "@",
                onSelect: "&select",
                onDeselect: "&deselect"
            },
            controller: function() {},
            compile: function(elm, attrs, transclude) {
                return function(scope, elm, attrs, tabsetCtrl) {
                    scope.$watch("active", function(active) {
                        active && tabsetCtrl.select(scope)
                    }), scope.disabled = !1, attrs.disabled && scope.$parent.$watch($parse(attrs.disabled), function(value) {
                        scope.disabled = !!value
                    }), scope.select = function() {
                        scope.disabled || (scope.active = !0)
                    }, tabsetCtrl.addTab(scope), scope.$on("$destroy", function() {
                        tabsetCtrl.removeTab(scope)
                    }), scope.$transcludeFn = transclude
                }
            }
        }
    }]).directive("tabHeadingTransclude", [function() {
        return {
            restrict: "A",
            require: "^tab",
            link: function(scope, elm, attrs, tabCtrl) {
                scope.$watch("headingElement", function(heading) {
                    heading && (elm.html(""), elm.append(heading))
                })
            }
        }
    }]).directive("tabContentTransclude", function() {
        function isTabHeading(node) {
            return node.tagName && (node.hasAttribute("tab-heading") || node.hasAttribute("data-tab-heading") || "tab-heading" === node.tagName.toLowerCase() || "data-tab-heading" === node.tagName.toLowerCase())
        }
        return {
            restrict: "A",
            require: "^tabset",
            link: function(scope, elm, attrs) {
                var tab = scope.$eval(attrs.tabContentTransclude);
                tab.$transcludeFn(tab.$parent, function(contents) {
                    angular.forEach(contents, function(node) {
                        isTabHeading(node) ? tab.headingElement = node : elm.append(node)
                    })
                })
            }
        }
    }), angular.module("ui.bootstrap.timepicker", []).constant("timepickerConfig", {
        hourStep: 1,
        minuteStep: 1,
        showMeridian: !0,
        meridians: null,
        readonlyInput: !1,
        mousewheel: !0
    }).controller("TimepickerController", ["$scope", "$attrs", "$parse", "$log", "$locale", "timepickerConfig", function($scope, $attrs, $parse, $log, $locale, timepickerConfig) {
        function getHoursFromTemplate() {
            var hours = parseInt($scope.hours, 10),
                valid = $scope.showMeridian ? hours > 0 && 13 > hours : hours >= 0 && 24 > hours;
            return valid ? ($scope.showMeridian && (12 === hours && (hours = 0), $scope.meridian === meridians[1] && (hours += 12)), hours) : void 0
        }

        function getMinutesFromTemplate() {
            var minutes = parseInt($scope.minutes, 10);
            return minutes >= 0 && 60 > minutes ? minutes : void 0
        }

        function pad(value) {
            return angular.isDefined(value) && value.toString().length < 2 ? "0" + value : value
        }

        function refresh(keyboardChange) {
            makeValid(), ngModelCtrl.$setViewValue(new Date(selected)), updateTemplate(keyboardChange)
        }

        function makeValid() {
            ngModelCtrl.$setValidity("time", !0), $scope.invalidHours = !1, $scope.invalidMinutes = !1
        }

        function updateTemplate(keyboardChange) {
            var hours = selected.getHours(),
                minutes = selected.getMinutes();
            $scope.showMeridian && (hours = 0 === hours || 12 === hours ? 12 : hours % 12), $scope.hours = "h" === keyboardChange ? hours : pad(hours), $scope.minutes = "m" === keyboardChange ? minutes : pad(minutes), $scope.meridian = selected.getHours() < 12 ? meridians[0] : meridians[1]
        }

        function addMinutes(minutes) {
            var dt = new Date(selected.getTime() + 6e4 * minutes);
            selected.setHours(dt.getHours(), dt.getMinutes()), refresh()
        }
        var selected = new Date,
            ngModelCtrl = {
                $setViewValue: angular.noop
            },
            meridians = angular.isDefined($attrs.meridians) ? $scope.$parent.$eval($attrs.meridians) : timepickerConfig.meridians || $locale.DATETIME_FORMATS.AMPMS;
        this.init = function(ngModelCtrl_, inputs) {
            ngModelCtrl = ngModelCtrl_, ngModelCtrl.$render = this.render;
            var hoursInputEl = inputs.eq(0),
                minutesInputEl = inputs.eq(1),
                mousewheel = angular.isDefined($attrs.mousewheel) ? $scope.$parent.$eval($attrs.mousewheel) : timepickerConfig.mousewheel;
            mousewheel && this.setupMousewheelEvents(hoursInputEl, minutesInputEl), $scope.readonlyInput = angular.isDefined($attrs.readonlyInput) ? $scope.$parent.$eval($attrs.readonlyInput) : timepickerConfig.readonlyInput, this.setupInputEvents(hoursInputEl, minutesInputEl)
        };
        var hourStep = timepickerConfig.hourStep;
        $attrs.hourStep && $scope.$parent.$watch($parse($attrs.hourStep), function(value) {
            hourStep = parseInt(value, 10)
        });
        var minuteStep = timepickerConfig.minuteStep;
        $attrs.minuteStep && $scope.$parent.$watch($parse($attrs.minuteStep), function(value) {
            minuteStep = parseInt(value, 10)
        }), $scope.showMeridian = timepickerConfig.showMeridian, $attrs.showMeridian && $scope.$parent.$watch($parse($attrs.showMeridian), function(value) {
            if ($scope.showMeridian = !!value, ngModelCtrl.$error.time) {
                var hours = getHoursFromTemplate(),
                    minutes = getMinutesFromTemplate();
                angular.isDefined(hours) && angular.isDefined(minutes) && (selected.setHours(hours), refresh())
            } else updateTemplate()
        }), this.setupMousewheelEvents = function(hoursInputEl, minutesInputEl) {
            var isScrollingUp = function(e) {
                e.originalEvent && (e = e.originalEvent);
                var delta = e.wheelDelta ? e.wheelDelta : -e.deltaY;
                return e.detail || delta > 0
            };
            hoursInputEl.bind("mousewheel wheel", function(e) {
                $scope.$apply(isScrollingUp(e) ? $scope.incrementHours() : $scope.decrementHours()), e.preventDefault()
            }), minutesInputEl.bind("mousewheel wheel", function(e) {
                $scope.$apply(isScrollingUp(e) ? $scope.incrementMinutes() : $scope.decrementMinutes()), e.preventDefault()
            })
        }, this.setupInputEvents = function(hoursInputEl, minutesInputEl) {
            if ($scope.readonlyInput) return $scope.updateHours = angular.noop, void($scope.updateMinutes = angular.noop);
            var invalidate = function(invalidHours, invalidMinutes) {
                ngModelCtrl.$setViewValue(null), ngModelCtrl.$setValidity("time", !1), angular.isDefined(invalidHours) && ($scope.invalidHours = invalidHours), angular.isDefined(invalidMinutes) && ($scope.invalidMinutes = invalidMinutes)
            };
            $scope.updateHours = function() {
                var hours = getHoursFromTemplate();
                angular.isDefined(hours) ? (selected.setHours(hours), refresh("h")) : invalidate(!0)
            }, hoursInputEl.bind("blur", function(e) {
                !$scope.invalidHours && $scope.hours < 10 && $scope.$apply(function() {
                    $scope.hours = pad($scope.hours)
                })
            }), $scope.updateMinutes = function() {
                var minutes = getMinutesFromTemplate();
                angular.isDefined(minutes) ? (selected.setMinutes(minutes), refresh("m")) : invalidate(void 0, !0)
            }, minutesInputEl.bind("blur", function(e) {
                !$scope.invalidMinutes && $scope.minutes < 10 && $scope.$apply(function() {
                    $scope.minutes = pad($scope.minutes)
                })
            })
        }, this.render = function() {
            var date = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;
            isNaN(date) ? (ngModelCtrl.$setValidity("time", !1), $log.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')) : (date && (selected = date), makeValid(), updateTemplate())
        }, $scope.incrementHours = function() {
            addMinutes(60 * hourStep)
        }, $scope.decrementHours = function() {
            addMinutes(60 * -hourStep)
        }, $scope.incrementMinutes = function() {
            addMinutes(minuteStep)
        }, $scope.decrementMinutes = function() {
            addMinutes(-minuteStep)
        }, $scope.toggleMeridian = function() {
            addMinutes(720 * (selected.getHours() < 12 ? 1 : -1))
        }
    }]).directive("timepicker", function() {
        return {
            restrict: "EA",
            require: ["timepicker", "?^ngModel"],
            controller: "TimepickerController",
            replace: !0,
            scope: {},
            templateUrl: "template/timepicker/timepicker.html",
            link: function(scope, element, attrs, ctrls) {
                var timepickerCtrl = ctrls[0],
                    ngModelCtrl = ctrls[1];
                ngModelCtrl && timepickerCtrl.init(ngModelCtrl, element.find("input"))
            }
        }
    }), angular.module("ui.bootstrap.typeahead", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).factory("typeaheadParser", ["$parse", function($parse) {
        var TYPEAHEAD_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;
        return {
            parse: function(input) {
                var match = input.match(TYPEAHEAD_REGEXP);
                if (!match) throw new Error('Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_" but got "' + input + '".');
                return {
                    itemName: match[3],
                    source: $parse(match[4]),
                    viewMapper: $parse(match[2] || match[1]),
                    modelMapper: $parse(match[1])
                }
            }
        }
    }]).directive("typeahead", ["$compile", "$parse", "$q", "$timeout", "$document", "$position", "typeaheadParser", function($compile, $parse, $q, $timeout, $document, $position, typeaheadParser) {
        var HOT_KEYS = [9, 13, 27, 38, 40];
        return {
            require: "ngModel",
            link: function(originalScope, element, attrs, modelCtrl) {
                var hasFocus, minSearch = originalScope.$eval(attrs.typeaheadMinLength) || 1,
                    waitTime = originalScope.$eval(attrs.typeaheadWaitMs) || 0,
                    isEditable = originalScope.$eval(attrs.typeaheadEditable) !== !1,
                    isLoadingSetter = $parse(attrs.typeaheadLoading).assign || angular.noop,
                    onSelectCallback = $parse(attrs.typeaheadOnSelect),
                    inputFormatter = attrs.typeaheadInputFormatter ? $parse(attrs.typeaheadInputFormatter) : void 0,
                    appendToBody = attrs.typeaheadAppendToBody ? originalScope.$eval(attrs.typeaheadAppendToBody) : !1,
                    focusFirst = originalScope.$eval(attrs.typeaheadFocusFirst) !== !1,
                    $setModelValue = $parse(attrs.ngModel).assign,
                    parserResult = typeaheadParser.parse(attrs.typeahead),
                    scope = originalScope.$new();
                originalScope.$on("$destroy", function() {
                    scope.$destroy()
                });
                var popupId = "typeahead-" + scope.$id + "-" + Math.floor(1e4 * Math.random());
                element.attr({
                    "aria-autocomplete": "list",
                    "aria-expanded": !1,
                    "aria-owns": popupId
                });
                var popUpEl = angular.element("<div typeahead-popup></div>");
                popUpEl.attr({
                    id: popupId,
                    matches: "matches",
                    active: "activeIdx",
                    select: "select(activeIdx)",
                    query: "query",
                    position: "position"
                }), angular.isDefined(attrs.typeaheadTemplateUrl) && popUpEl.attr("template-url", attrs.typeaheadTemplateUrl);
                var resetMatches = function() {
                        scope.matches = [], scope.activeIdx = -1, element.attr("aria-expanded", !1)
                    },
                    getMatchId = function(index) {
                        return popupId + "-option-" + index
                    };
                scope.$watch("activeIdx", function(index) {
                    0 > index ? element.removeAttr("aria-activedescendant") : element.attr("aria-activedescendant", getMatchId(index))
                });
                var getMatchesAsync = function(inputValue) {
                    var locals = {
                        $viewValue: inputValue
                    };
                    isLoadingSetter(originalScope, !0), $q.when(parserResult.source(originalScope, locals)).then(function(matches) {
                        var onCurrentRequest = inputValue === modelCtrl.$viewValue;
                        if (onCurrentRequest && hasFocus)
                            if (matches.length > 0) {
                                scope.activeIdx = focusFirst ? 0 : -1, scope.matches.length = 0;
                                for (var i = 0; i < matches.length; i++) locals[parserResult.itemName] = matches[i], scope.matches.push({
                                    id: getMatchId(i),
                                    label: parserResult.viewMapper(scope, locals),
                                    model: matches[i]
                                });
                                scope.query = inputValue, scope.position = appendToBody ? $position.offset(element) : $position.position(element), scope.position.top = scope.position.top + element.prop("offsetHeight"), element.attr("aria-expanded", !0)
                            } else resetMatches();
                        onCurrentRequest && isLoadingSetter(originalScope, !1)
                    }, function() {
                        resetMatches(), isLoadingSetter(originalScope, !1)
                    })
                };
                resetMatches(), scope.query = void 0;
                var timeoutPromise, scheduleSearchWithTimeout = function(inputValue) {
                        timeoutPromise = $timeout(function() {
                            getMatchesAsync(inputValue)
                        }, waitTime)
                    },
                    cancelPreviousTimeout = function() {
                        timeoutPromise && $timeout.cancel(timeoutPromise)
                    };
                modelCtrl.$parsers.unshift(function(inputValue) {
                    return hasFocus = !0, inputValue && inputValue.length >= minSearch ? waitTime > 0 ? (cancelPreviousTimeout(), scheduleSearchWithTimeout(inputValue)) : getMatchesAsync(inputValue) : (isLoadingSetter(originalScope, !1), cancelPreviousTimeout(), resetMatches()), isEditable ? inputValue : inputValue ? void modelCtrl.$setValidity("editable", !1) : (modelCtrl.$setValidity("editable", !0), inputValue)
                }), modelCtrl.$formatters.push(function(modelValue) {
                    var candidateViewValue, emptyViewValue, locals = {};

                    return inputFormatter ? (locals.$model = modelValue, inputFormatter(originalScope, locals)) : (locals[parserResult.itemName] = modelValue, candidateViewValue = parserResult.viewMapper(originalScope, locals), locals[parserResult.itemName] = void 0, emptyViewValue = parserResult.viewMapper(originalScope, locals), candidateViewValue !== emptyViewValue ? candidateViewValue : modelValue)
                }), scope.select = function(activeIdx) {
                    var model, item, locals = {};
                    locals[parserResult.itemName] = item = scope.matches[activeIdx].model, model = parserResult.modelMapper(originalScope, locals), $setModelValue(originalScope, model), modelCtrl.$setValidity("editable", !0), onSelectCallback(originalScope, {
                        $item: item,
                        $model: model,
                        $label: parserResult.viewMapper(originalScope, locals)
                    }), resetMatches(), $timeout(function() {
                        element[0].focus()
                    }, 0, !1)
                }, element.bind("keydown", function(evt) {
                    0 !== scope.matches.length && -1 !== HOT_KEYS.indexOf(evt.which) && (-1 != scope.activeIdx || 13 !== evt.which && 9 !== evt.which) && (evt.preventDefault(), 40 === evt.which ? (scope.activeIdx = (scope.activeIdx + 1) % scope.matches.length, scope.$digest()) : 38 === evt.which ? (scope.activeIdx = (scope.activeIdx > 0 ? scope.activeIdx : scope.matches.length) - 1, scope.$digest()) : 13 === evt.which || 9 === evt.which ? scope.$apply(function() {
                        scope.select(scope.activeIdx)
                    }) : 27 === evt.which && (evt.stopPropagation(), resetMatches(), scope.$digest()))
                }), element.bind("blur", function(evt) {
                    hasFocus = !1
                });
                var dismissClickHandler = function(evt) {
                    element[0] !== evt.target && (resetMatches(), scope.$digest())
                };
                $document.bind("click", dismissClickHandler), originalScope.$on("$destroy", function() {
                    $document.unbind("click", dismissClickHandler), appendToBody && $popup.remove()
                });
                var $popup = $compile(popUpEl)(scope);
                appendToBody ? $document.find("body").append($popup) : element.after($popup)
            }
        }
    }]).directive("typeaheadPopup", function() {
        return {
            restrict: "EA",
            scope: {
                matches: "=",
                query: "=",
                active: "=",
                position: "=",
                select: "&"
            },
            replace: !0,
            templateUrl: "template/typeahead/typeahead-popup.html",
            link: function(scope, element, attrs) {
                scope.templateUrl = attrs.templateUrl, scope.isOpen = function() {
                    return scope.matches.length > 0
                }, scope.isActive = function(matchIdx) {
                    return scope.active == matchIdx
                }, scope.selectActive = function(matchIdx) {
                    scope.active = matchIdx
                }, scope.selectMatch = function(activeIdx) {
                    scope.select({
                        activeIdx: activeIdx
                    })
                }
            }
        }
    }).directive("typeaheadMatch", ["$http", "$templateCache", "$compile", "$parse", function($http, $templateCache, $compile, $parse) {
        return {
            restrict: "EA",
            scope: {
                index: "=",
                match: "=",
                query: "="
            },
            link: function(scope, element, attrs) {
                var tplUrl = $parse(attrs.templateUrl)(scope.$parent) || "template/typeahead/typeahead-match.html";
                $http.get(tplUrl, {
                    cache: $templateCache
                }).success(function(tplContent) {
                    element.replaceWith($compile(tplContent.trim())(scope))
                })
            }
        }
    }]).filter("typeaheadHighlight", function() {
        function escapeRegexp(queryToEscape) {
            return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
        }
        return function(matchItem, query) {
            return query ? ("" + matchItem).replace(new RegExp(escapeRegexp(query), "gi"), "<strong>$&</strong>") : matchItem
        }
    }), angular.module("template/accordion/accordion-group.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/accordion/accordion-group.html", '<div class="panel panel-default">\n  <div class="panel-heading">\n    <h4 class="panel-title">\n      <a href class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading"><span ng-class="{\'text-muted\': isDisabled}">{{heading}}</span></a>\n    </h4>\n  </div>\n  <div class="panel-collapse" collapse="!isOpen">\n	  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>\n')
    }]), angular.module("template/accordion/accordion.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/accordion/accordion.html", '<div class="panel-group" ng-transclude></div>')
    }]), angular.module("template/alert/alert.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/alert/alert.html", '<div class="alert" ng-class="[\'alert-\' + (type || \'warning\'), closeable ? \'alert-dismissable\' : null]" role="alert">\n    <button ng-show="closeable" type="button" class="close" ng-click="close()">\n        <span aria-hidden="true">&times;</span>\n        <span class="sr-only">Close</span>\n    </button>\n    <div ng-transclude></div>\n</div>\n')
    }]), angular.module("template/carousel/carousel.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/carousel/carousel.html", '<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel" ng-swipe-right="prev()" ng-swipe-left="next()">\n    <ol class="carousel-indicators" ng-show="slides.length > 1">\n        <li ng-repeat="slide in slides track by $index" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a class="left carousel-control" ng-click="prev()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-left"></span></a>\n    <a class="right carousel-control" ng-click="next()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-right"></span></a>\n</div>\n')
    }]), angular.module("template/carousel/slide.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/carousel/slide.html", "<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item text-center\" ng-transclude></div>\n")
    }]), angular.module("template/datepicker/datepicker.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/datepicker/datepicker.html", '<div ng-switch="datepickerMode" role="application" ng-keydown="keydown($event)">\n  <daypicker ng-switch-when="day" tabindex="0"></daypicker>\n  <monthpicker ng-switch-when="month" tabindex="0"></monthpicker>\n  <yearpicker ng-switch-when="year" tabindex="0"></yearpicker>\n</div>')
    }]), angular.module("template/datepicker/day.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/datepicker/day.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="{{5 + showWeeks}}"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n    <tr>\n      <th ng-show="showWeeks" class="text-center"></th>\n      <th ng-repeat="label in labels track by $index" class="text-center"><small aria-label="{{label.full}}">{{label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-show="showWeeks" class="text-center h6"><em>{{ weekNumbers[$index] }}</em></td>\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
    }]), angular.module("template/datepicker/month.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/datepicker/month.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
    }]), angular.module("template/datepicker/popup.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/datepicker/popup.html", '<ul class="dropdown-menu" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top+\'px\', left: position.left+\'px\'}" ng-keydown="keydown($event)">\n	<li ng-transclude></li>\n	<li ng-if="showButtonBar" style="padding:10px 9px 2px">\n		<span class="btn-group pull-left">\n			<button type="button" class="btn btn-sm btn-info" ng-click="select(\'today\')">{{ getText(\'current\') }}</button>\n			<button type="button" class="btn btn-sm btn-danger" ng-click="select(null)">{{ getText(\'clear\') }}</button>\n		</span>\n		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="close()">{{ getText(\'close\') }}</button>\n	</li>\n</ul>\n')
    }]), angular.module("template/datepicker/year.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/datepicker/year.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="3"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
    }]), angular.module("template/modal/backdrop.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/modal/backdrop.html", '<div class="modal-backdrop fade {{ backdropClass }}"\n     ng-class="{in: animate}"\n     ng-style="{\'z-index\': 1040 + (index && 1 || 0) + index*10}"\n></div>\n')
    }]), angular.module("template/modal/window.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/modal/window.html", '<div tabindex="-1" role="dialog" class="modal fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n    <div class="modal-dialog" ng-class="{\'modal-sm\': size == \'sm\', \'modal-lg\': size == \'lg\'}"><div class="modal-content" modal-transclude></div></div>\n</div>')
    }]), angular.module("template/pagination/pager.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/pagination/pager.html", '<ul class="pager">\n  <li ng-class="{disabled: noPrevious(), previous: align}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-class="{disabled: noNext(), next: align}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n</ul>')
    }]), angular.module("template/pagination/pagination.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/pagination/pagination.html", '<ul class="pagination">\n  <li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(1)">{{getText(\'first\')}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-repeat="page in pages track by $index" ng-class="{active: page.active}"><a href ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n  <li ng-if="boundaryLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(totalPages)">{{getText(\'last\')}}</a></li>\n</ul>')
    }]), angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/tooltip/tooltip-html-unsafe-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n</div>\n')
    }]), angular.module("template/tooltip/tooltip-popup.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/tooltip/tooltip-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')
    }]), angular.module("template/popover/popover.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/popover/popover.html", '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')
    }]), angular.module("template/progressbar/bar.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/progressbar/bar.html", '<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>')
    }]), angular.module("template/progressbar/progress.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/progressbar/progress.html", '<div class="progress" ng-transclude></div>')
    }]), angular.module("template/progressbar/progressbar.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/progressbar/progressbar.html", '<div class="progress">\n  <div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>\n</div>')
    }]), angular.module("template/rating/rating.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/rating/rating.html", '<span ng-mouseleave="reset()" ng-keydown="onKeydown($event)" tabindex="0" role="slider" aria-valuemin="0" aria-valuemax="{{range.length}}" aria-valuenow="{{value}}">\n    <i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < value && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')">\n        <span class="sr-only">({{ $index < value ? \'*\' : \' \' }})</span>\n    </i>\n</span>')
    }]), angular.module("template/tabs/tab.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/tabs/tab.html", '<li ng-class="{active: active, disabled: disabled}">\n  <a href ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n')
    }]), angular.module("template/tabs/tabset.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/tabs/tabset.html", '<div>\n  <ul class="nav nav-{{type || \'tabs\'}}" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n</div>\n')
    }]), angular.module("template/timepicker/timepicker.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/timepicker/timepicker.html", '<table>\n	<tbody>\n		<tr class="text-center">\n			<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n		<tr>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidHours}">\n				<input type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td>:</td>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n				<input type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n		</tr>\n		<tr class="text-center">\n			<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n	</tbody>\n</table>\n')
    }]), angular.module("template/typeahead/typeahead-match.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/typeahead/typeahead-match.html", '<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')
    }]), angular.module("template/typeahead/typeahead-popup.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("template/typeahead/typeahead-popup.html", '<ul class="dropdown-menu" ng-show="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\'}" style="display: block;" role="listbox" aria-hidden="{{!isOpen()}}">\n    <li ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)" role="option" id="{{match.id}}">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>\n')
    }]), ! function($) {
        var Datepicker = function(element, options) {
            if (this.element = $(element), this.format = DPGlobal.parseFormat(options.format || this.element.data("date-format") || "mm/dd/yyyy"), this.picker = $(DPGlobal.template).appendTo("body").on({
                    click: $.proxy(this.click, this)
                }), this.isInput = this.element.is("input"), this.component = this.element.is(".date") ? this.element.find(".input-group-addon") : !1, this.isInput ? this.element.on({
                    focus: $.proxy(this.show, this),
                    keyup: $.proxy(this.update, this)
                }) : this.component ? this.component.on("click", $.proxy(this.show, this)) : this.element.on("click", $.proxy(this.show, this)), this.minViewMode = options.minViewMode || this.element.data("date-minviewmode") || 0, "string" == typeof this.minViewMode) switch (this.minViewMode) {
                case "months":
                    this.minViewMode = 1;
                    break;
                case "years":
                    this.minViewMode = 2;
                    break;
                default:
                    this.minViewMode = 0
            }
            if (this.viewMode = options.viewMode || this.element.data("date-viewmode") || 0, "string" == typeof this.viewMode) switch (this.viewMode) {
                case "months":
                    this.viewMode = 1;
                    break;
                case "years":
                    this.viewMode = 2;
                    break;
                default:
                    this.viewMode = 0
            }
            this.startViewMode = this.viewMode, this.weekStart = options.weekStart || this.element.data("date-weekstart") || 0, this.weekEnd = 0 === this.weekStart ? 6 : this.weekStart - 1, this.onRender = options.onRender, this.fillDow(), this.fillMonths(), this.update(), this.showMode()
        };
        Datepicker.prototype = {
            constructor: Datepicker,
            show: function(e) {
                this.picker.show(), this.height = this.component ? this.component.outerHeight() : this.element.outerHeight(), this.place(), $(window).on("resize", $.proxy(this.place, this)), e && (e.stopPropagation(), e.preventDefault()), !this.isInput;
                var that = this;
                $(document).on("mousedown", function(ev) {
                    0 == $(ev.target).closest(".datepicker").length && that.hide()
                }), this.element.trigger({
                    type: "show",
                    date: this.date
                })
            },
            hide: function() {
                this.picker.hide(), $(window).off("resize", this.place), this.viewMode = this.startViewMode, this.showMode(), this.isInput || $(document).off("mousedown", this.hide), this.element.trigger({
                    type: "hide",
                    date: this.date
                })
            },
            set: function() {
                var formated = DPGlobal.formatDate(this.date, this.format);
                this.isInput ? this.element.prop("value", formated) : (this.component && this.element.find("input").prop("value", formated), this.element.data("date", formated))
            },
            setValue: function(newDate) {
                this.date = "string" == typeof newDate ? DPGlobal.parseDate(newDate, this.format) : new Date(newDate), this.set(), this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0), this.fill()
            },
            place: function() {
                var offset = this.component ? this.component.offset() : this.element.offset();
                this.picker.css({
                    top: offset.top + this.height,
                    left: offset.left
                })
            },
            update: function(newDate) {
                this.date = DPGlobal.parseDate("string" == typeof newDate ? newDate : this.isInput ? this.element.prop("value") : this.element.data("date"), this.format), this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0), this.fill()
            },
            fillDow: function() {
                for (var dowCnt = this.weekStart, html = "<tr>"; dowCnt < this.weekStart + 7;) html += '<th class="dow">' + DPGlobal.dates.daysMin[dowCnt++ % 7] + "</th>";
                html += "</tr>", this.picker.find(".datepicker-days thead").append(html)
            },
            fillMonths: function() {
                for (var html = "", i = 0; 12 > i;) html += '<span class="month">' + DPGlobal.dates.monthsShort[i++] + "</span>";
                this.picker.find(".datepicker-months td").append(html)
            },
            fill: function() {
                var d = new Date(this.viewDate),
                    year = d.getFullYear(),
                    month = d.getMonth(),
                    currentDate = this.date.valueOf();
                this.picker.find(".datepicker-days th:eq(1)").text(DPGlobal.dates.months[month] + " " + year);
                var prevMonth = new Date(year, month - 1, 28, 0, 0, 0, 0),
                    day = DPGlobal.getDaysInMonth(prevMonth.getFullYear(), prevMonth.getMonth());
                prevMonth.setDate(day), prevMonth.setDate(day - (prevMonth.getDay() - this.weekStart + 7) % 7);
                var nextMonth = new Date(prevMonth);
                nextMonth.setDate(nextMonth.getDate() + 42), nextMonth = nextMonth.valueOf();
                for (var clsName, prevY, prevM, html = []; prevMonth.valueOf() < nextMonth;) prevMonth.getDay() === this.weekStart && html.push("<tr>"), clsName = this.onRender(prevMonth), prevY = prevMonth.getFullYear(), prevM = prevMonth.getMonth(), month > prevM && prevY === year || year > prevY ? clsName += " old" : (prevM > month && prevY === year || prevY > year) && (clsName += " new"), prevMonth.valueOf() === currentDate && (clsName += " active"), html.push('<td class="day ' + clsName + '">' + prevMonth.getDate() + "</td>"), prevMonth.getDay() === this.weekEnd && html.push("</tr>"), prevMonth.setDate(prevMonth.getDate() + 1);
                this.picker.find(".datepicker-days tbody").empty().append(html.join(""));
                var currentYear = this.date.getFullYear(),
                    months = this.picker.find(".datepicker-months").find("th:eq(1)").text(year).end().find("span").removeClass("active");
                currentYear === year && months.eq(this.date.getMonth()).addClass("active"), html = "", year = 10 * parseInt(year / 10, 10);
                var yearCont = this.picker.find(".datepicker-years").find("th:eq(1)").text(year + "-" + (year + 9)).end().find("td");
                year -= 1;
                for (var i = -1; 11 > i; i++) html += '<span class="year' + (-1 === i || 10 === i ? " old" : "") + (currentYear === year ? " active" : "") + '">' + year + "</span>", year += 1;
                yearCont.html(html)
            },
            click: function(e) {
                e.stopPropagation(), e.preventDefault();
                var target = $(e.target).closest("span, td, th");
                if (1 === target.length) switch (target[0].nodeName.toLowerCase()) {
                    case "th":
                        switch (target[0].className) {
                            case "switch":
                                this.showMode(1);
                                break;
                            case "prev":
                            case "next":
                                this.viewDate["set" + DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate, this.viewDate["get" + DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate) + DPGlobal.modes[this.viewMode].navStep * ("prev" === target[0].className ? -1 : 1)), this.fill(), this.set()
                        }
                        break;
                    case "span":
                        if (target.is(".month")) {
                            var month = target.parent().find("span").index(target);
                            this.viewDate.setMonth(month)
                        } else {
                            var year = parseInt(target.text(), 10) || 0;
                            this.viewDate.setFullYear(year)
                        }
                        0 !== this.viewMode && (this.date = new Date(this.viewDate), this.element.trigger({
                            type: "changeDate",
                            date: this.date,
                            viewMode: DPGlobal.modes[this.viewMode].clsName
                        })), this.showMode(-1), this.fill(), this.set();
                        break;
                    case "td":
                        if (target.is(".day") && !target.is(".disabled")) {
                            var day = parseInt(target.text(), 10) || 1,
                                month = this.viewDate.getMonth();
                            target.is(".old") ? month -= 1 : target.is(".new") && (month += 1);
                            var year = this.viewDate.getFullYear();
                            this.date = new Date(year, month, day, 0, 0, 0, 0), this.viewDate = new Date(year, month, Math.min(28, day), 0, 0, 0, 0), this.fill(), this.set(), this.element.trigger({
                                type: "changeDate",
                                date: this.date,
                                viewMode: DPGlobal.modes[this.viewMode].clsName
                            })
                        }
                }
            },
            mousedown: function(e) {
                e.stopPropagation(), e.preventDefault()
            },
            showMode: function(dir) {
                dir && (this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir))), this.picker.find(">div").hide().filter(".datepicker-" + DPGlobal.modes[this.viewMode].clsName).show()
            }
        }, $.fn.datepicker = function(option, val) {
            return this.each(function() {
                var $this = $(this),
                    data = $this.data("datepicker"),
                    options = "object" == typeof option && option;
                data || $this.data("datepicker", data = new Datepicker(this, $.extend({}, $.fn.datepicker.defaults, options))), "string" == typeof option && data[option](val)
            })
        }, $.fn.datepicker.defaults = {
            onRender: function(date) {
                return ""
            }
        }, $.fn.datepicker.Constructor = Datepicker;
        var DPGlobal = {
            modes: [{
                clsName: "days",
                navFnc: "Month",
                navStep: 1
            }, {
                clsName: "months",
                navFnc: "FullYear",
                navStep: 1
            }, {
                clsName: "years",
                navFnc: "FullYear",
                navStep: 10
            }],
            dates: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            },
            isLeapYear: function(year) {
                return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0
            },
            getDaysInMonth: function(year, month) {
                return [31, DPGlobal.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
            },
            parseFormat: function(format) {
                var separator = format.match(/[.\/\-\s].*?/),
                    parts = format.split(/\W+/);
                if (!separator || !parts || 0 === parts.length) throw new Error("Invalid date format.");
                return {
                    separator: separator,
                    parts: parts
                }
            },
            parseDate: function(date, format) {
                var val, parts = date.split(format.separator),
                    date = new Date;
                if (date.setHours(0), date.setMinutes(0), date.setSeconds(0), date.setMilliseconds(0), parts.length === format.parts.length) {
                    for (var year = date.getFullYear(), day = date.getDate(), month = date.getMonth(), i = 0, cnt = format.parts.length; cnt > i; i++) switch (val = parseInt(parts[i], 10) || 1, format.parts[i]) {
                        case "dd":
                        case "d":
                            day = val, date.setDate(val);
                            break;
                        case "mm":
                        case "m":
                            month = val - 1, date.setMonth(val - 1);
                            break;
                        case "yy":
                            year = 2e3 + val, date.setFullYear(2e3 + val);
                            break;
                        case "yyyy":
                            year = val, date.setFullYear(val)
                    }
                    date = new Date(year, month, day, 0, 0, 0)
                }
                return date
            },
            formatDate: function(date, format) {
                var val = {
                    d: date.getDate(),
                    m: date.getMonth() + 1,
                    yy: date.getFullYear().toString().substring(2),
                    yyyy: date.getFullYear()
                };
                val.dd = (val.d < 10 ? "0" : "") + val.d, val.mm = (val.m < 10 ? "0" : "") + val.m;
                for (var date = [], i = 0, cnt = format.parts.length; cnt > i; i++) date.push(val[format.parts[i]]);
                return date.join(format.separator)
            },
            headTemplate: '<thead><tr><th class="prev">&lsaquo;</th><th colspan="5" class="switch"></th><th class="next">&rsaquo;</th></tr></thead>',
            contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
        };
        DPGlobal.template = '<div class="datepicker dropdown-menu"><div class="datepicker-days"><table class=" table-condensed">' + DPGlobal.headTemplate + '<tbody></tbody></table></div><div class="datepicker-months"><table class="table-condensed">' + DPGlobal.headTemplate + DPGlobal.contTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + DPGlobal.headTemplate + DPGlobal.contTemplate + "</table></div></div>"
    }(window.jQuery),
    function($, window, undefined) {
        var $allDropdowns = $();
        $.fn.dropdownHover = function(options) {
            return "ontouchstart" in document ? this : ($allDropdowns = $allDropdowns.add(this.parent()), this.each(function() {
                var timeout, $this = $(this),
                    $parent = $this.parent(),
                    defaults = {
                        delay: 500,
                        instantlyCloseOthers: !0
                    },
                    data = {
                        delay: $(this).data("delay"),
                        instantlyCloseOthers: $(this).data("close-others")
                    },
                    showEvent = "show.bs.dropdown",
                    hideEvent = "hide.bs.dropdown",
                    settings = $.extend(!0, {}, defaults, options, data);
                $parent.hover(function(event) {
                    return $parent.hasClass("open") || $this.is(event.target) ? (settings.instantlyCloseOthers === !0 && $allDropdowns.removeClass("open"), window.clearTimeout(timeout), $parent.addClass("open"), void $this.trigger(showEvent)) : !0
                }, function() {
                    timeout = window.setTimeout(function() {
                        $parent.removeClass("open"), $this.trigger(hideEvent)
                    }, settings.delay)
                }), $this.hover(function() {
                    settings.instantlyCloseOthers === !0 && $allDropdowns.removeClass("open"), window.clearTimeout(timeout), $parent.addClass("open"), $this.trigger(showEvent)
                }), $parent.find(".dropdown-submenu").each(function() {
                    var subTimeout, $this = $(this);
                    $this.hover(function() {
                        window.clearTimeout(subTimeout), $this.children(".dropdown-menu").show(), $this.siblings().children(".dropdown-menu").hide()
                    }, function() {
                        var $submenu = $this.children(".dropdown-menu");
                        subTimeout = window.setTimeout(function() {
                            $submenu.hide()
                        }, settings.delay)
                    })
                })
            }))
        }, $(document).ready(function() {
            $('[data-hover="dropdown"]').dropdownHover()
        })
    }(jQuery, this), ! function($) {
        var Slider = function(element, options) {
            this.element = $(element), this.picker = $('<div class="slider"><span class="val-min"></span><div class="slider-track"><div class="slider-selection"></div><div class="slider-handle"></div><div class="slider-handle"></div></div><span class="val-max"></span><div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div></div>').insertBefore(this.element).append(this.element), this.id = this.element.data("slider-id") || options.id, this.id && (this.picker[0].id = this.id), "undefined" != typeof Modernizr && Modernizr.touch && (this.touchCapable = !0);
            var tooltip = this.element.data("slider-tooltip") || options.tooltip;
            switch (this.tooltip = this.picker.find(".tooltip"), this.tooltipInner = this.tooltip.find("div.tooltip-inner"), this.orientation = this.element.data("slider-orientation") || options.orientation, this.orientation) {
                case "vertical":
                    this.picker.addClass("slider-vertical"), this.stylePos = "top", this.mousePos = "pageY", this.sizePos = "offsetHeight", this.tooltip.addClass("right")[0].style.left = "100%";
                    break;
                default:
                    this.picker.addClass("slider-horizontal").css("width", this.element.outerWidth()), this.orientation = "horizontal", this.stylePos = "left", this.mousePos = "pageX", this.sizePos = "offsetWidth", this.tooltip.addClass("top")[0].style.top = -this.tooltip.outerHeight() - 14 + "px";

            }
            this.min = this.element.data("slider-min") || options.min, this.max = this.element.data("slider-max") || options.max, this.step = this.element.data("slider-step") || options.step, this.value = this.element.data("slider-value") || options.value, this.picker.find("span.val-min").html(this.min), this.picker.find("span.val-max").html(this.max), this.value[1] && (this.range = !0), this.selection = this.element.data("slider-selection") || options.selection, this.selectionEl = this.picker.find(".slider-selection"), "none" === this.selection && this.selectionEl.addClass("hide"), this.selectionElStyle = this.selectionEl[0].style, this.handle1 = this.picker.find(".slider-handle:first"), this.handle1Stype = this.handle1[0].style, this.handle2 = this.picker.find(".slider-handle:last"), this.handle2Stype = this.handle2[0].style;
            var handle = this.element.data("slider-handle") || options.handle;
            switch (handle) {
                case "round":
                    this.handle1.addClass("round"), this.handle2.addClass("round");
                    break;
                case "triangle":
                    this.handle1.addClass("triangle"), this.handle2.addClass("triangle")
            }
            this.range ? (this.value[0] = Math.max(this.min, Math.min(this.max, this.value[0])), this.value[1] = Math.max(this.min, Math.min(this.max, this.value[1]))) : (this.value = [Math.max(this.min, Math.min(this.max, this.value))], this.handle2.addClass("hide"), this.value[1] = "after" == this.selection ? this.max : this.min), this.diff = this.max - this.min, this.percentage = [100 * (this.value[0] - this.min) / this.diff, 100 * (this.value[1] - this.min) / this.diff, 100 * this.step / this.diff], this.offset = this.picker.offset(), this.size = this.picker[0][this.sizePos], this.formater = options.formater, this.layout(), this.picker.on(this.touchCapable ? {
                touchstart: $.proxy(this.mousedown, this)
            } : {
                mousedown: $.proxy(this.mousedown, this)
            }), "show" === tooltip ? this.picker.on({
                mouseenter: $.proxy(this.showTooltip, this),
                mouseleave: $.proxy(this.hideTooltip, this)
            }) : this.tooltip.addClass("hide")
        };
        Slider.prototype = {
            constructor: Slider,
            over: !1,
            inDrag: !1,
            showTooltip: function() {
                this.tooltip.addClass("in"), this.over = !0
            },
            hideTooltip: function() {
                this.inDrag === !1 && this.tooltip.removeClass("in"), this.over = !1
            },
            layout: function() {
                this.handle1Stype[this.stylePos] = this.percentage[0] + "%", this.handle2Stype[this.stylePos] = this.percentage[1] + "%", "vertical" == this.orientation ? (this.selectionElStyle.top = Math.min(this.percentage[0], this.percentage[1]) + "%", this.selectionElStyle.height = Math.abs(this.percentage[0] - this.percentage[1]) + "%") : (this.selectionElStyle.left = Math.min(this.percentage[0], this.percentage[1]) + "%", this.selectionElStyle.width = Math.abs(this.percentage[0] - this.percentage[1]) + "%"), this.range ? (this.tooltipInner.text(this.formater(this.value[0]) + " â€“ " + this.formater(this.value[1])), this.tooltip[0].style[this.stylePos] = this.size * (this.percentage[0] + (this.percentage[1] - this.percentage[0]) / 2) / 100 - ("vertical" === this.orientation ? this.tooltip.outerHeight() / 2 : this.tooltip.outerWidth() / 2) + "px") : (this.tooltipInner.text(this.formater(this.value[0])), this.tooltip[0].style[this.stylePos] = this.size * this.percentage[0] / 100 - ("vertical" === this.orientation ? this.tooltip.outerHeight() / 2 : this.tooltip.outerWidth() / 2) + "px")
            },
            mousedown: function(ev) {
                this.touchCapable && "touchstart" === ev.type && (ev = ev.originalEvent), this.offset = this.picker.offset(), this.size = this.picker[0][this.sizePos];
                var percentage = this.getPercentage(ev);
                if (this.range) {
                    var diff1 = Math.abs(this.percentage[0] - percentage),
                        diff2 = Math.abs(this.percentage[1] - percentage);
                    this.dragged = diff2 > diff1 ? 0 : 1
                } else this.dragged = 0;
                this.percentage[this.dragged] = percentage, this.layout(), $(document).on(this.touchCapable ? {
                    touchmove: $.proxy(this.mousemove, this),
                    touchend: $.proxy(this.mouseup, this)
                } : {
                    mousemove: $.proxy(this.mousemove, this),
                    mouseup: $.proxy(this.mouseup, this)
                }), this.inDrag = !0;
                var val = this.calculateValue();
                return this.element.trigger({
                    type: "slideStart",
                    value: val
                }).trigger({
                    type: "slide",
                    value: val
                }), !1
            },
            mousemove: function(ev) {
                this.touchCapable && "touchmove" === ev.type && (ev = ev.originalEvent);
                var percentage = this.getPercentage(ev);
                this.range && (0 === this.dragged && this.percentage[1] < percentage ? (this.percentage[0] = this.percentage[1], this.dragged = 1) : 1 === this.dragged && this.percentage[0] > percentage && (this.percentage[1] = this.percentage[0], this.dragged = 0)), this.percentage[this.dragged] = percentage, this.layout();
                var val = this.calculateValue();
                return this.element.trigger({
                    type: "slide",
                    value: val
                }).data("value", val).prop("value", val), !1
            },
            mouseup: function(ev) {
                $(document).off(this.touchCapable ? {
                    touchmove: this.mousemove,
                    touchend: this.mouseup
                } : {
                    mousemove: this.mousemove,
                    mouseup: this.mouseup
                }), this.inDrag = !1, 0 == this.over && this.hideTooltip(), this.element;
                var val = this.calculateValue();
                return this.element.trigger({
                    type: "slideStop",
                    value: val
                }).data("value", val).prop("value", val), !1
            },
            calculateValue: function() {
                var val;
                return this.range ? (val = [this.min + Math.round(this.diff * this.percentage[0] / 100 / this.step) * this.step, this.min + Math.round(this.diff * this.percentage[1] / 100 / this.step) * this.step], this.value = val) : (val = this.min + Math.round(this.diff * this.percentage[0] / 100 / this.step) * this.step, this.value = [val, this.value[1]]), val
            },
            getPercentage: function(ev) {
                this.touchCapable && (ev = ev.touches[0]);
                var percentage = 100 * (ev[this.mousePos] - this.offset[this.stylePos]) / this.size;
                return percentage = Math.round(percentage / this.percentage[2]) * this.percentage[2], Math.max(0, Math.min(100, percentage))
            },
            getValue: function() {
                return this.range ? this.value : this.value[0]
            },
            setValue: function(val) {
                this.value = val, this.range ? (this.value[0] = Math.max(this.min, Math.min(this.max, this.value[0])), this.value[1] = Math.max(this.min, Math.min(this.max, this.value[1]))) : (this.value = [Math.max(this.min, Math.min(this.max, this.value))], this.handle2.addClass("hide"), this.value[1] = "after" == this.selection ? this.max : this.min), this.diff = this.max - this.min, this.percentage = [100 * (this.value[0] - this.min) / this.diff, 100 * (this.value[1] - this.min) / this.diff, 100 * this.step / this.diff], this.layout()
            }
        }, $.fn.slider = function(option, val) {
            return this.each(function() {
                var $this = $(this),
                    data = $this.data("slider"),
                    options = "object" == typeof option && option;
                data || $this.data("slider", data = new Slider(this, $.extend({}, $.fn.slider.defaults, options))), "string" == typeof option && data[option](val)
            })
        }, $.fn.slider.defaults = {
            min: 0,
            max: 10,
            step: 1,
            orientation: "horizontal",
            value: 5,
            selection: "before",
            tooltip: "show",
            handle: "round",
            formater: function(value) {
                return value
            }
        }, $.fn.slider.Constructor = Slider
    }(window.jQuery);
var _ec_history = 0,
    _ec_tests = 10,
    evercookie = function(window) {
        var document = window.document,
            Image = window.Image,
            localStorage = window.localStorage,
            globalStorage = window.globalStorage;
        try {
            var sessionStorage = window.sessionStorage
        } catch (e) {}
        return this._class = function() {
            function newImage(src) {
                var img = new Image;
                img.style.visibility = "hidden", img.style.position = "absolute", img.src = src
            }
            var self = this,
                _baseKeyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            this._ec = {}, this.get = function(name, cb, dont_reset) {
                self._evercookie(name, cb, void 0, void 0, dont_reset)
            }, this.set = function(name, value) {
                self._evercookie(name, function() {}, value)
            }, this._evercookie = function(name, cb, value, i, dont_reset) {
                if (void 0 === self._evercookie && (self = this), void 0 === i && (i = 0), 0 === i && (self.evercookie_database_storage(name, value), self.evercookie_cache(name, value), self._ec.userData = self.evercookie_userdata(name, value), self._ec.cookieData = self.evercookie_cookie(name, value), self._ec.localData = self.evercookie_local_storage(name, value), self._ec.globalData = self.evercookie_global_storage(name, value), self._ec.sessionData = self.evercookie_session_storage(name, value), self._ec.windowData = self.evercookie_window(name, value), _ec_history && (self._ec.historyData = self.evercookie_history(name, value))), void 0 !== value) i++ < _ec_tests && setTimeout(function() {
                    self._evercookie(name, cb, value, i, dont_reset)
                }, 300);
                else if ((window.openDatabase && "undefined" == typeof self._ec.dbData || "undefined" == typeof self._ec.cacheData) && i++ < _ec_tests) setTimeout(function() {
                    self._evercookie(name, cb, value, i, dont_reset)
                }, 300);
                else {
                    var candidate, item, tmpec = self._ec,
                        candidates = [],
                        bestnum = 0;
                    self._ec = {};
                    for (item in tmpec) tmpec[item] && "null" !== tmpec[item] && "undefined" !== tmpec[item] && (candidates[tmpec[item]] = void 0 === candidates[tmpec[item]] ? 1 : candidates[tmpec[item]] + 1);
                    for (item in candidates) candidates[item] > bestnum && (bestnum = candidates[item], candidate = item);
                    (void 0 === dont_reset || 1 !== dont_reset) && self.set(name, candidate), "function" == typeof cb && cb(candidate, tmpec)
                }
            }, this.evercookie_window = function(name, value) {
                try {
                    if (void 0 === value) return this.getFromStr(name, window.name);
                    window.name = _ec_replace(window.name, name, value)
                } catch (e) {}
            }, this.evercookie_userdata = function(name, value) {
                try {
                    var elm = this.createElem("div", "userdata_el", 1);
                    if (elm.style.behavior = "url(#default#userData)", void 0 === value) return elm.load(name), elm.getAttribute(name);
                    elm.setAttribute(name, value), elm.save(name)
                } catch (e) {}
            }, this.ajax = function(settings) {
                var headers, name, transports, transport, i, length;
                for (headers = {
                        "X-Requested-With": "XMLHttpRequest",
                        Accept: "text/javascript, text/html, application/xml, text/xml, */*"
                    }, transports = [function() {
                        return new XMLHttpRequest
                    }, function() {
                        return new ActiveXObject("Msxml2.XMLHTTP")
                    }, function() {
                        return new ActiveXObject("Microsoft.XMLHTTP")
                    }], i = 0, length = transports.length; length > i; i++) {
                    transport = transports[i];
                    try {
                        transport = transport();
                        break
                    } catch (e) {}
                }
                transport.onreadystatechange = function() {
                    4 === transport.readyState && settings.success(transport.responseText)
                }, transport.open("get", settings.url, !0);
                for (name in headers) transport.setRequestHeader(name, headers[name]);
                transport.send()
            }, this.evercookie_cache = function(name, value) {
                if (void 0 !== value) document.cookie = "ldt=" + value, newImage(Ajax.url + "/_c_/c?name=" + name);
                else {
                    var origvalue = this.getFromStr("ldt", document.cookie);
                    self._ec.cacheData = void 0, document.cookie = "ldt=; expires=Tue, 01 Jan 2013 00:00:00 GMT; path=/", $.ajax({
                        url: Ajax.url + "/_c_/c?name=" + name,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            document.cookie = "ldt=" + origvalue + "; expires=Tue, 31 Dec 2030 23:59:59 GMT; path=/", self._ec.cacheData = "undefined" != typeof data ? data.statusMessage : ""
                        }
                    })
                }
            }, this.evercookie_local_storage = function(name, value) {
                try {
                    if (localStorage) {
                        if (void 0 === value) return localStorage.getItem(name);
                        localStorage.setItem(name, value)
                    }
                } catch (e) {}
            }, this.evercookie_database_storage = function(name, value) {
                try {
                    if (window.openDatabase) {
                        var database = window.openDatabase("sqlite_evercookie", "", "evercookie", 1048576);
                        database.transaction(void 0 !== value ? function(tx) {
                            tx.executeSql("CREATE TABLE IF NOT EXISTS cache(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, value TEXT NOT NULL, UNIQUE (name))", [], function(tx, rs) {}, function(tx, err) {}), tx.executeSql("INSERT OR REPLACE INTO cache(name, value) VALUES(?, ?)", [name, value], function(tx, rs) {}, function(tx, err) {})
                        } : function(tx) {
                            tx.executeSql("SELECT value FROM cache WHERE name=?", [name], function(tx, result1) {
                                self._ec.dbData = result1.rows.length >= 1 ? result1.rows.item(0).value : ""
                            }, function(tx, err) {})
                        })
                    }
                } catch (e) {}
            }, this.evercookie_session_storage = function(name, value) {
                try {
                    if (sessionStorage) {
                        if (void 0 === value) return sessionStorage.getItem(name);
                        sessionStorage.setItem(name, value)
                    }
                } catch (e) {}
            }, this.evercookie_global_storage = function(name, value) {
                if (globalStorage) {
                    var host = this.getHost();
                    try {
                        if (void 0 === value) return globalStorage[host][name];
                        globalStorage[host][name] = value
                    } catch (e) {}
                }
            }, this.encode = function(input) {
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4, output = "",
                    i = 0;
                for (input = this._utf8_encode(input); i < input.length;) chr1 = input.charCodeAt(i++), chr2 = input.charCodeAt(i++), chr3 = input.charCodeAt(i++), enc1 = chr1 >> 2, enc2 = (3 & chr1) << 4 | chr2 >> 4, enc3 = (15 & chr2) << 2 | chr3 >> 6, enc4 = 63 & chr3, isNaN(chr2) ? enc3 = enc4 = 64 : isNaN(chr3) && (enc4 = 64), output = output + _baseKeyStr.charAt(enc1) + _baseKeyStr.charAt(enc2) + _baseKeyStr.charAt(enc3) + _baseKeyStr.charAt(enc4);
                return output
            }, this.decode = function(input) {
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4, output = "",
                    i = 0;
                for (input = input.replace(/[^A-Za-z0-9\+\/\=]/g, ""); i < input.length;) enc1 = _baseKeyStr.indexOf(input.charAt(i++)), enc2 = _baseKeyStr.indexOf(input.charAt(i++)), enc3 = _baseKeyStr.indexOf(input.charAt(i++)), enc4 = _baseKeyStr.indexOf(input.charAt(i++)), chr1 = enc1 << 2 | enc2 >> 4, chr2 = (15 & enc2) << 4 | enc3 >> 2, chr3 = (3 & enc3) << 6 | enc4, output += String.fromCharCode(chr1), 64 !== enc3 && (output += String.fromCharCode(chr2)), 64 !== enc4 && (output += String.fromCharCode(chr3));
                return output = this._utf8_decode(output)
            }, this._utf8_encode = function(str) {
                str = str.replace(/\r\n/g, "\n");
                for (var c, utftext = "", i = 0, n = str.length; n > i; i++) c = str.charCodeAt(i), 128 > c ? utftext += String.fromCharCode(c) : c > 127 && 2048 > c ? (utftext += String.fromCharCode(c >> 6 | 192), utftext += String.fromCharCode(63 & c | 128)) : (utftext += String.fromCharCode(c >> 12 | 224), utftext += String.fromCharCode(c >> 6 & 63 | 128), utftext += String.fromCharCode(63 & c | 128));
                return utftext
            }, this._utf8_decode = function(utftext) {
                for (var str = "", i = 0, n = utftext.length, c = 0, c2 = 0, c3 = 0; n > i;) c = utftext.charCodeAt(i), 128 > c ? (str += String.fromCharCode(c), i += 1) : c > 191 && 224 > c ? (c2 = utftext.charCodeAt(i + 1), str += String.fromCharCode((31 & c) << 6 | 63 & c2), i += 2) : (c2 = utftext.charCodeAt(i + 1), c3 = utftext.charCodeAt(i + 2), str += String.fromCharCode((15 & c) << 12 | (63 & c2) << 6 | 63 & c3), i += 3);
                return str
            }, this.createElem = function(type, name, append) {
                var el;
                return el = void 0 !== name && document.getElementById(name) ? document.getElementById(name) : document.createElement(type), el.style.visibility = "hidden", el.style.position = "absolute", name && el.setAttribute("id", name), append && document.body.appendChild(el), el
            }, this.evercookie_cookie = function(name, value) {
                return void 0 === value ? this.getFromStr(name, document.cookie) : (document.cookie = name + "=; expires=Mon, 20 Sep 2010 00:00:00 UTC; path=/", void(document.cookie = name + "=" + value + "; expires=Tue, 31 Dec 2030 00:00:00 UTC; path=/"))
            }, this.getFromStr = function(name, text) {
                if ("string" == typeof text) {
                    var i, c, nameEQ = name + "=",
                        ca = text.split(/[;&]/);
                    for (i = 0; i < ca.length; i++) {
                        for (c = ca[i];
                            " " === c.charAt(0);) c = c.substring(1, c.length);
                        if (0 === c.indexOf(nameEQ)) return c.substring(nameEQ.length, c.length)
                    }
                }
            }, this.getHost = function() {
                return window.location.host.replace(/:\d+/, "")
            }, this.toHex = function(str) {
                for (var h, r = "", e = str.length, c = 0; e > c;) {
                    for (h = str.charCodeAt(c++).toString(16); h.length < 2;) h = "0" + h;
                    r += h
                }
                return r
            }, this.fromHex = function(str) {
                for (var s, r = "", e = str.length; e >= 0;) s = e - 2, r = String.fromCharCode("0x" + str.substring(s, e)) + r, e = s;
                return r
            }
        }, this._class
    }(window),
    swfobject = function() {
        function f() {
            if (!J) {
                try {
                    var Z = j.getElementsByTagName("body")[0].appendChild(C("span"));
                    Z.parentNode.removeChild(Z)
                } catch (aa) {
                    return
                }
                J = !0;
                for (var X = U.length, Y = 0; X > Y; Y++) U[Y]()
            }
        }

        function K(X) {
            J ? X() : U[U.length] = X
        }

        function s(Y) {
            if (typeof O.addEventListener != D) O.addEventListener("load", Y, !1);
            else if (typeof j.addEventListener != D) j.addEventListener("load", Y, !1);
            else if (typeof O.attachEvent != D) i(O, "onload", Y);
            else if ("function" == typeof O.onload) {
                var X = O.onload;
                O.onload = function() {
                    X(), Y()
                }
            } else O.onload = Y
        }

        function h() {
            T ? V() : H()
        }

        function V() {
            var X = j.getElementsByTagName("body")[0],
                aa = C(r);
            aa.setAttribute("type", q);
            var Z = X.appendChild(aa);
            if (Z) {
                var Y = 0;
                ! function() {
                    if (typeof Z.GetVariable != D) {
                        var ab = Z.GetVariable("$version");
                        ab && (ab = ab.split(" ")[1].split(","), M.pv = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)])
                    } else if (10 > Y) return Y++, void setTimeout(arguments.callee, 10);
                    X.removeChild(aa), Z = null, H()
                }()
            } else H()
        }

        function H() {
            var ag = o.length;
            if (ag > 0)
                for (var af = 0; ag > af; af++) {
                    var Y = o[af].id,
                        ab = o[af].callbackFn,
                        aa = {
                            success: !1,
                            id: Y
                        };
                    if (M.pv[0] > 0) {
                        var ae = c(Y);
                        if (ae)
                            if (!F(o[af].swfVersion) || M.wk && M.wk < 312)
                                if (o[af].expressInstall && A()) {
                                    var ai = {};
                                    ai.data = o[af].expressInstall, ai.width = ae.getAttribute("width") || "0", ai.height = ae.getAttribute("height") || "0", ae.getAttribute("class") && (ai.styleclass = ae.getAttribute("class")), ae.getAttribute("align") && (ai.align = ae.getAttribute("align"));
                                    for (var ah = {}, X = ae.getElementsByTagName("param"), ac = X.length, ad = 0; ac > ad; ad++) "movie" != X[ad].getAttribute("name").toLowerCase() && (ah[X[ad].getAttribute("name")] = X[ad].getAttribute("value"));
                                    P(ai, ah, Y, ab)
                                } else p(ae), ab && ab(aa);
                        else w(Y, !0), ab && (aa.success = !0, aa.ref = z(Y), ab(aa))
                    } else if (w(Y, !0), ab) {
                        var Z = z(Y);
                        Z && typeof Z.SetVariable != D && (aa.success = !0, aa.ref = Z), ab(aa)
                    }
                }
        }

        function z(aa) {
            var X = null,
                Y = c(aa);
            if (Y && "OBJECT" == Y.nodeName)
                if (typeof Y.SetVariable != D) X = Y;
                else {
                    var Z = Y.getElementsByTagName(r)[0];
                    Z && (X = Z)
                }
            return X
        }

        function A() {
            return !a && F("6.0.65") && (M.win || M.mac) && !(M.wk && M.wk < 312)
        }

        function P(aa, ab, X, Z) {
            a = !0, E = Z || null, B = {
                success: !1,
                id: X
            };
            var ae = c(X);
            if (ae) {
                "OBJECT" == ae.nodeName ? (l = g(ae), Q = null) : (l = ae, Q = X), aa.id = R, (typeof aa.width == D || !/%$/.test(aa.width) && parseInt(aa.width, 10) < 310) && (aa.width = "310"), (typeof aa.height == D || !/%$/.test(aa.height) && parseInt(aa.height, 10) < 137) && (aa.height = "137"), j.title = j.title.slice(0, 47) + " - Flash Player Installation";
                var ad = M.ie && M.win ? "ActiveX" : "PlugIn",
                    ac = "MMredirectURL=" + O.location.toString().replace(/&/g, "%26") + "&MMplayerType=" + ad + "&MMdoctitle=" + j.title;
                if (typeof ab.flashvars != D ? ab.flashvars += "&" + ac : ab.flashvars = ac, M.ie && M.win && 4 != ae.readyState) {
                    var Y = C("div");
                    X += "SWFObjectNew", Y.setAttribute("id", X), ae.parentNode.insertBefore(Y, ae), ae.style.display = "none",
                        function() {
                            4 == ae.readyState ? ae.parentNode.removeChild(ae) : setTimeout(arguments.callee, 10)
                        }()
                }
                u(aa, ab, X)
            }
        }

        function p(Y) {
            if (M.ie && M.win && 4 != Y.readyState) {
                var X = C("div");
                Y.parentNode.insertBefore(X, Y), X.parentNode.replaceChild(g(Y), X), Y.style.display = "none",
                    function() {
                        4 == Y.readyState ? Y.parentNode.removeChild(Y) : setTimeout(arguments.callee, 10)
                    }()
            } else Y.parentNode.replaceChild(g(Y), Y)
        }

        function g(ab) {
            var aa = C("div");
            if (M.win && M.ie) aa.innerHTML = ab.innerHTML;
            else {
                var Y = ab.getElementsByTagName(r)[0];
                if (Y) {
                    var ad = Y.childNodes;
                    if (ad)
                        for (var X = ad.length, Z = 0; X > Z; Z++) 1 == ad[Z].nodeType && "PARAM" == ad[Z].nodeName || 8 == ad[Z].nodeType || aa.appendChild(ad[Z].cloneNode(!0))
                }
            }
            return aa
        }

        function u(ai, ag, Y) {
            var X, aa = c(Y);
            if (M.wk && M.wk < 312) return X;
            if (aa)
                if (typeof ai.id == D && (ai.id = Y), M.ie && M.win) {
                    var ah = "";
                    for (var ae in ai) ai[ae] != Object.prototype[ae] && ("data" == ae.toLowerCase() ? ag.movie = ai[ae] : "styleclass" == ae.toLowerCase() ? ah += ' class="' + ai[ae] + '"' : "classid" != ae.toLowerCase() && (ah += " " + ae + '="' + ai[ae] + '"'));
                    var af = "";
                    for (var ad in ag) ag[ad] != Object.prototype[ad] && (af += '<param name="' + ad + '" value="' + ag[ad] + '" />');
                    aa.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + ah + ">" + af + "</object>", N[N.length] = ai.id, X = c(ai.id)
                } else {
                    var Z = C(r);
                    Z.setAttribute("type", q);
                    for (var ac in ai) ai[ac] != Object.prototype[ac] && ("styleclass" == ac.toLowerCase() ? Z.setAttribute("class", ai[ac]) : "classid" != ac.toLowerCase() && Z.setAttribute(ac, ai[ac]));
                    for (var ab in ag) ag[ab] != Object.prototype[ab] && "movie" != ab.toLowerCase() && e(Z, ab, ag[ab]);
                    aa.parentNode.replaceChild(Z, aa), X = Z
                }
            return X
        }

        function e(Z, X, Y) {
            var aa = C("param");
            aa.setAttribute("name", X), aa.setAttribute("value", Y), Z.appendChild(aa)
        }

        function y(Y) {
            var X = c(Y);
            X && "OBJECT" == X.nodeName && (M.ie && M.win ? (X.style.display = "none", function() {
                4 == X.readyState ? b(Y) : setTimeout(arguments.callee, 10)
            }()) : X.parentNode.removeChild(X))
        }

        function b(Z) {
            var Y = c(Z);
            if (Y) {
                for (var X in Y) "function" == typeof Y[X] && (Y[X] = null);
                Y.parentNode.removeChild(Y)
            }
        }

        function c(Z) {
            var X = null;
            try {
                X = j.getElementById(Z)
            } catch (Y) {}
            return X
        }

        function C(X) {
            return j.createElement(X)
        }

        function i(Z, X, Y) {
            Z.attachEvent(X, Y), I[I.length] = [Z, X, Y]
        }

        function F(Z) {
            var Y = M.pv,
                X = Z.split(".");
            return X[0] = parseInt(X[0], 10), X[1] = parseInt(X[1], 10) || 0, X[2] = parseInt(X[2], 10) || 0, Y[0] > X[0] || Y[0] == X[0] && Y[1] > X[1] || Y[0] == X[0] && Y[1] == X[1] && Y[2] >= X[2] ? !0 : !1
        }

        function v(ac, Y, ad, ab) {
            if (!M.ie || !M.mac) {
                var aa = j.getElementsByTagName("head")[0];
                if (aa) {
                    var X = ad && "string" == typeof ad ? ad : "screen";
                    if (ab && (n = null, G = null), !n || G != X) {
                        var Z = C("style");
                        Z.setAttribute("type", "text/css"), Z.setAttribute("media", X), n = aa.appendChild(Z), M.ie && M.win && typeof j.styleSheets != D && j.styleSheets.length > 0 && (n = j.styleSheets[j.styleSheets.length - 1]), G = X
                    }
                    M.ie && M.win ? n && typeof n.addRule == r && n.addRule(ac, Y) : n && typeof j.createTextNode != D && n.appendChild(j.createTextNode(ac + " {" + Y + "}"))
                }
            }
        }

        function w(Z, X) {
            if (m) {
                var Y = X ? "visible" : "hidden";
                J && c(Z) ? c(Z).style.visibility = Y : v("#" + Z, "visibility:" + Y)
            }
        }

        function L(Y) {
            var Z = /[\\\"<>\.;]/,
                X = null != Z.exec(Y);
            return X && typeof encodeURIComponent != D ? encodeURIComponent(Y) : Y
        } {
            var l, Q, E, B, n, G, D = "undefined",
                r = "object",
                S = "Shockwave Flash",
                W = "ShockwaveFlash.ShockwaveFlash",
                q = "application/x-shockwave-flash",
                R = "SWFObjectExprInst",
                x = "onreadystatechange",
                O = window,
                j = document,
                t = navigator,
                T = !1,
                U = [h],
                o = [],
                N = [],
                I = [],
                J = !1,
                a = !1,
                m = !0,
                M = function() {
                    var aa = typeof j.getElementById != D && typeof j.getElementsByTagName != D && typeof j.createElement != D,
                        ah = t.userAgent.toLowerCase(),
                        Y = t.platform.toLowerCase(),
                        ae = /win/.test(Y ? Y : ah),
                        ac = /mac/.test(Y ? Y : ah),
                        af = /webkit/.test(ah) ? parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : !1,
                        X = !1,
                        ag = [0, 0, 0],
                        ab = null;
                    if (typeof t.plugins != D && typeof t.plugins[S] == r) ab = t.plugins[S].description, !ab || typeof t.mimeTypes != D && t.mimeTypes[q] && !t.mimeTypes[q].enabledPlugin || (T = !0, X = !1, ab = ab.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), ag[0] = parseInt(ab.replace(/^(.*)\..*$/, "$1"), 10), ag[1] = parseInt(ab.replace(/^.*\.(.*)\s.*$/, "$1"), 10), ag[2] = /[a-zA-Z]/.test(ab) ? parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0);
                    else if (typeof O.ActiveXObject != D) try {
                        var ad = new ActiveXObject(W);
                        ad && (ab = ad.GetVariable("$version"), ab && (X = !0, ab = ab.split(" ")[1].split(","), ag = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)]))
                    } catch (Z) {}
                    return {
                        w3: aa,
                        pv: ag,
                        wk: af,
                        ie: X,
                        win: ae,
                        mac: ac
                    }
                }();
            ! function() {
                M.w3 && ((typeof j.readyState != D && "complete" == j.readyState || typeof j.readyState == D && (j.getElementsByTagName("body")[0] || j.body)) && f(), J || (typeof j.addEventListener != D && j.addEventListener("DOMContentLoaded", f, !1), M.ie && M.win && (j.attachEvent(x, function() {
                    "complete" == j.readyState && (j.detachEvent(x, arguments.callee), f())
                }), O == top && ! function() {
                    if (!J) {
                        try {
                            j.documentElement.doScroll("left")
                        } catch (X) {
                            return void setTimeout(arguments.callee, 0)
                        }
                        f()
                    }
                }()), M.wk && ! function() {
                    return J ? void 0 : /loaded|complete/.test(j.readyState) ? void f() : void setTimeout(arguments.callee, 0)
                }(), s(f)))
            }(),
            function() {
                M.ie && M.win && window.attachEvent("onunload", function() {
                    for (var ac = I.length, ab = 0; ac > ab; ab++) I[ab][0].detachEvent(I[ab][1], I[ab][2]);
                    for (var Z = N.length, aa = 0; Z > aa; aa++) y(N[aa]);
                    for (var Y in M) M[Y] = null;
                    M = null;
                    for (var X in swfobject) swfobject[X] = null;
                    swfobject = null
                })
            }()
        }
        return {
            registerObject: function(ab, X, aa, Z) {
                if (M.w3 && ab && X) {
                    var Y = {};
                    Y.id = ab, Y.swfVersion = X, Y.expressInstall = aa, Y.callbackFn = Z, o[o.length] = Y, w(ab, !1)
                } else Z && Z({
                    success: !1,
                    id: ab
                })
            },
            getObjectById: function(X) {
                return M.w3 ? z(X) : void 0
            },
            embedSWF: function(ab, ah, ae, ag, Y, aa, Z, ad, af, ac) {
                var X = {
                    success: !1,
                    id: ah
                };
                M.w3 && !(M.wk && M.wk < 312) && ab && ah && ae && ag && Y ? (w(ah, !1), K(function() {
                    ae += "", ag += "";
                    var aj = {};
                    if (af && typeof af === r)
                        for (var al in af) aj[al] = af[al];
                    aj.data = ab, aj.width = ae, aj.height = ag;
                    var am = {};
                    if (ad && typeof ad === r)
                        for (var ak in ad) am[ak] = ad[ak];
                    if (Z && typeof Z === r)
                        for (var ai in Z) typeof am.flashvars != D ? am.flashvars += "&" + ai + "=" + Z[ai] : am.flashvars = ai + "=" + Z[ai];
                    if (F(Y)) {
                        var an = u(aj, am, ah);
                        aj.id == ah && w(ah, !0), X.success = !0, X.ref = an
                    } else {
                        if (aa && A()) return aj.data = aa, void P(aj, am, ah, ac);
                        w(ah, !0)
                    }
                    ac && ac(X)
                })) : ac && ac(X)
            },
            switchOffAutoHideShow: function() {
                m = !1
            },
            ua: M,
            getFlashPlayerVersion: function() {
                return {
                    major: M.pv[0],
                    minor: M.pv[1],
                    release: M.pv[2]
                }
            },
            hasFlashPlayerVersion: F,
            createSWF: function(Z, Y, X) {
                return M.w3 ? u(Z, Y, X) : void 0
            },
            showExpressInstall: function(Z, aa, X, Y) {
                M.w3 && A() && P(Z, aa, X, Y)
            },
            removeSWF: function(X) {
                M.w3 && y(X)
            },
            createCSS: function(aa, Z, Y, X) {
                M.w3 && v(aa, Z, Y, X)
            },
            addDomLoadEvent: K,
            addLoadEvent: s,
            getQueryParamValue: function(aa) {
                var Z = j.location.search || j.location.hash;
                if (Z) {
                    if (/\?/.test(Z) && (Z = Z.split("?")[1]), null == aa) return L(Z);
                    for (var Y = Z.split("&"), X = 0; X < Y.length; X++)
                        if (Y[X].substring(0, Y[X].indexOf("=")) == aa) return L(Y[X].substring(Y[X].indexOf("=") + 1))
                }
                return ""
            },
            expressInstallCallback: function() {
                if (a) {
                    var X = c(R);
                    X && l && (X.parentNode.replaceChild(l, X), Q && (w(Q, !0), M.ie && M.win && (l.style.display = "block")), E && E(B)), a = !1
                }
            }
        }
    }(),
    swfobject = function() {
        function callDomLoadFunctions() {
            if (!isDomLoaded) {
                try {
                    var t = doc.getElementsByTagName("body")[0].appendChild(createElement("span"));
                    t.parentNode.removeChild(t)
                } catch (e) {
                    return
                }
                isDomLoaded = !0;
                for (var dl = domLoadFnArr.length, i = 0; dl > i; i++) domLoadFnArr[i]()
            }
        }

        function addDomLoadEvent(fn) {
            isDomLoaded ? fn() : domLoadFnArr[domLoadFnArr.length] = fn
        }

        function addLoadEvent(fn) {
            if (typeof win.addEventListener != UNDEF) win.addEventListener("load", fn, !1);
            else if (typeof doc.addEventListener != UNDEF) doc.addEventListener("load", fn, !1);
            else if (typeof win.attachEvent != UNDEF) addListener(win, "onload", fn);
            else if ("function" == typeof win.onload) {
                var fnOld = win.onload;
                win.onload = function() {
                    fnOld(), fn()
                }
            } else win.onload = fn
        }

        function main() {
            plugin ? testPlayerVersion() : matchVersions()
        }

        function testPlayerVersion() {
            var b = doc.getElementsByTagName("body")[0],
                o = createElement(OBJECT);
            o.setAttribute("type", FLASH_MIME_TYPE);
            var t = b.appendChild(o);
            if (t) {
                var counter = 0;
                ! function() {
                    if (typeof t.GetVariable != UNDEF) {
                        var d = t.GetVariable("$version");
                        d && (d = d.split(" ")[1].split(","), ua.pv = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)])
                    } else if (10 > counter) return counter++, void setTimeout(arguments.callee, 10);
                    b.removeChild(o), t = null, matchVersions()
                }()
            } else matchVersions()
        }

        function matchVersions() {
            var rl = regObjArr.length;
            if (rl > 0)
                for (var i = 0; rl > i; i++) {
                    var id = regObjArr[i].id,
                        cb = regObjArr[i].callbackFn,
                        cbObj = {
                            success: !1,
                            id: id
                        };
                    if (ua.pv[0] > 0) {
                        var obj = getElementById(id);
                        if (obj)
                            if (!hasPlayerVersion(regObjArr[i].swfVersion) || ua.wk && ua.wk < 312)
                                if (regObjArr[i].expressInstall && canExpressInstall()) {
                                    var att = {};
                                    att.data = regObjArr[i].expressInstall, att.width = obj.getAttribute("width") || "0", att.height = obj.getAttribute("height") || "0", obj.getAttribute("class") && (att.styleclass = obj.getAttribute("class")), obj.getAttribute("align") && (att.align = obj.getAttribute("align"));
                                    for (var par = {}, p = obj.getElementsByTagName("param"), pl = p.length, j = 0; pl > j; j++) "movie" != p[j].getAttribute("name").toLowerCase() && (par[p[j].getAttribute("name")] = p[j].getAttribute("value"));
                                    showExpressInstall(att, par, id, cb)
                                } else displayAltContent(obj), cb && cb(cbObj);
                        else setVisibility(id, !0), cb && (cbObj.success = !0, cbObj.ref = getObjectById(id), cb(cbObj))
                    } else if (setVisibility(id, !0), cb) {
                        var o = getObjectById(id);
                        o && typeof o.SetVariable != UNDEF && (cbObj.success = !0, cbObj.ref = o), cb(cbObj)
                    }
                }
        }

        function getObjectById(objectIdStr) {
            var r = null,
                o = getElementById(objectIdStr);
            if (o && "OBJECT" == o.nodeName)
                if (typeof o.SetVariable != UNDEF) r = o;
                else {
                    var n = o.getElementsByTagName(OBJECT)[0];
                    n && (r = n)
                }
            return r
        }

        function canExpressInstall() {
            return !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac) && !(ua.wk && ua.wk < 312)
        }

        function showExpressInstall(att, par, replaceElemIdStr, callbackFn) {
            isExpressInstallActive = !0, storedCallbackFn = callbackFn || null, storedCallbackObj = {
                success: !1,
                id: replaceElemIdStr
            };
            var obj = getElementById(replaceElemIdStr);
            if (obj) {
                "OBJECT" == obj.nodeName ? (storedAltContent = abstractAltContent(obj), storedAltContentId = null) : (storedAltContent = obj, storedAltContentId = replaceElemIdStr), att.id = EXPRESS_INSTALL_ID, (typeof att.width == UNDEF || !/%$/.test(att.width) && parseInt(att.width, 10) < 310) && (att.width = "310"), (typeof att.height == UNDEF || !/%$/.test(att.height) && parseInt(att.height, 10) < 137) && (att.height = "137"), doc.title = doc.title.slice(0, 47) + " - Flash Player Installation";
                var pt = ua.ie && ua.win ? "ActiveX" : "PlugIn",
                    fv = "MMredirectURL=" + encodeURI(window.location).toString().replace(/&/g, "%26") + "&MMplayerType=" + pt + "&MMdoctitle=" + doc.title;
                if (typeof par.flashvars != UNDEF ? par.flashvars += "&" + fv : par.flashvars = fv, ua.ie && ua.win && 4 != obj.readyState) {
                    var newObj = createElement("div");
                    replaceElemIdStr += "SWFObjectNew", newObj.setAttribute("id", replaceElemIdStr), obj.parentNode.insertBefore(newObj, obj), obj.style.display = "none",
                        function() {
                            4 == obj.readyState ? obj.parentNode.removeChild(obj) : setTimeout(arguments.callee, 10)
                        }()
                }
                createSWF(att, par, replaceElemIdStr)
            }
        }

        function displayAltContent(obj) {
            if (ua.ie && ua.win && 4 != obj.readyState) {
                var el = createElement("div");
                obj.parentNode.insertBefore(el, obj), el.parentNode.replaceChild(abstractAltContent(obj), el), obj.style.display = "none",
                    function() {
                        4 == obj.readyState ? obj.parentNode.removeChild(obj) : setTimeout(arguments.callee, 10)
                    }()
            } else obj.parentNode.replaceChild(abstractAltContent(obj), obj)
        }

        function abstractAltContent(obj) {
            var ac = createElement("div");
            if (ua.win && ua.ie) ac.innerHTML = obj.innerHTML;
            else {
                var nestedObj = obj.getElementsByTagName(OBJECT)[0];
                if (nestedObj) {
                    var c = nestedObj.childNodes;
                    if (c)
                        for (var cl = c.length, i = 0; cl > i; i++) 1 == c[i].nodeType && "PARAM" == c[i].nodeName || 8 == c[i].nodeType || ac.appendChild(c[i].cloneNode(!0))
                }
            }
            return ac
        }

        function createSWF(attObj, parObj, id) {
            var r, el = getElementById(id);
            if (ua.wk && ua.wk < 312) return r;
            if (el)
                if (typeof attObj.id == UNDEF && (attObj.id = id), ua.ie && ua.win) {
                    var att = "";
                    for (var i in attObj) attObj[i] != Object.prototype[i] && ("data" == i.toLowerCase() ? parObj.movie = attObj[i] : "styleclass" == i.toLowerCase() ? att += ' class="' + attObj[i] + '"' : "classid" != i.toLowerCase() && (att += " " + i + '="' + attObj[i] + '"'));
                    var par = "";
                    for (var j in parObj) parObj[j] != Object.prototype[j] && (par += '<param name="' + j + '" value="' + parObj[j] + '" />');
                    el.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + att + ">" + par + "</object>", objIdArr[objIdArr.length] = attObj.id, r = getElementById(attObj.id)
                } else {
                    var o = createElement(OBJECT);
                    o.setAttribute("type", FLASH_MIME_TYPE);
                    for (var m in attObj) attObj[m] != Object.prototype[m] && ("styleclass" == m.toLowerCase() ? o.setAttribute("class", attObj[m]) : "classid" != m.toLowerCase() && o.setAttribute(m, attObj[m]));
                    for (var n in parObj) parObj[n] != Object.prototype[n] && "movie" != n.toLowerCase() && createObjParam(o, n, parObj[n]);
                    el.parentNode.replaceChild(o, el), r = o
                }
            return r
        }

        function createObjParam(el, pName, pValue) {
            var p = createElement("param");
            p.setAttribute("name", pName), p.setAttribute("value", pValue), el.appendChild(p)
        }

        function removeSWF(id) {
            var obj = getElementById(id);
            obj && "OBJECT" == obj.nodeName && (ua.ie && ua.win ? (obj.style.display = "none", function() {
                4 == obj.readyState ? removeObjectInIE(id) : setTimeout(arguments.callee, 10)
            }()) : obj.parentNode.removeChild(obj))
        }

        function removeObjectInIE(id) {
            var obj = getElementById(id);
            if (obj) {
                for (var i in obj) "function" == typeof obj[i] && (obj[i] = null);
                obj.parentNode.removeChild(obj)
            }
        }

        function getElementById(id) {
            var el = null;
            try {
                el = doc.getElementById(id)
            } catch (e) {}
            return el
        }

        function createElement(el) {
            return doc.createElement(el)
        }

        function addListener(target, eventType, fn) {
            target.attachEvent(eventType, fn), listenersArr[listenersArr.length] = [target, eventType, fn]
        }

        function hasPlayerVersion(rv) {
            var pv = ua.pv,
                v = rv.split(".");
            return v[0] = parseInt(v[0], 10), v[1] = parseInt(v[1], 10) || 0, v[2] = parseInt(v[2], 10) || 0, pv[0] > v[0] || pv[0] == v[0] && pv[1] > v[1] || pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2] ? !0 : !1
        }

        function createCSS(sel, decl, media, newStyle) {
            if (!ua.ie || !ua.mac) {
                var h = doc.getElementsByTagName("head")[0];
                if (h) {
                    var m = media && "string" == typeof media ? media : "screen";
                    if (newStyle && (dynamicStylesheet = null, dynamicStylesheetMedia = null), !dynamicStylesheet || dynamicStylesheetMedia != m) {
                        var s = createElement("style");
                        s.setAttribute("type", "text/css"), s.setAttribute("media", m), dynamicStylesheet = h.appendChild(s), ua.ie && ua.win && typeof doc.styleSheets != UNDEF && doc.styleSheets.length > 0 && (dynamicStylesheet = doc.styleSheets[doc.styleSheets.length - 1]), dynamicStylesheetMedia = m
                    }
                    ua.ie && ua.win ? dynamicStylesheet && typeof dynamicStylesheet.addRule == OBJECT && dynamicStylesheet.addRule(sel, decl) : dynamicStylesheet && typeof doc.createTextNode != UNDEF && dynamicStylesheet.appendChild(doc.createTextNode(sel + " {" + decl + "}"))
                }
            }
        }

        function setVisibility(id, isVisible) {
            if (autoHideShow) {
                var v = isVisible ? "visible" : "hidden";
                isDomLoaded && getElementById(id) ? getElementById(id).style.visibility = v : createCSS("#" + id, "visibility:" + v)
            }
        }

        function urlEncodeIfNecessary(s) {
            var regex = /[\\\"<>\.;]/,
                hasBadChars = null != regex.exec(s);
            return hasBadChars && typeof encodeURIComponent != UNDEF ? encodeURIComponent(s) : s
        } {
            var storedAltContent, storedAltContentId, storedCallbackFn, storedCallbackObj, dynamicStylesheet, dynamicStylesheetMedia, UNDEF = "undefined",
                OBJECT = "object",
                SHOCKWAVE_FLASH = "Shockwave Flash",
                SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
                FLASH_MIME_TYPE = "application/x-shockwave-flash",
                EXPRESS_INSTALL_ID = "SWFObjectExprInst",
                ON_READY_STATE_CHANGE = "onreadystatechange",
                win = window,
                doc = document,
                nav = navigator,
                plugin = !1,
                domLoadFnArr = [main],
                regObjArr = [],
                objIdArr = [],
                listenersArr = [],
                isDomLoaded = !1,
                isExpressInstallActive = !1,
                autoHideShow = !0,
                ua = function() {
                    var w3cdom = typeof doc.getElementById != UNDEF && typeof doc.getElementsByTagName != UNDEF && typeof doc.createElement != UNDEF,
                        u = nav.userAgent.toLowerCase(),
                        p = nav.platform.toLowerCase(),
                        windows = /win/.test(p ? p : u),
                        mac = /mac/.test(p ? p : u),
                        webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : !1,
                        ie = !1,
                        playerVersion = [0, 0, 0],
                        d = null;
                    if (typeof nav.plugins != UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] == OBJECT) d = nav.plugins[SHOCKWAVE_FLASH].description, !d || typeof nav.mimeTypes != UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && !nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin || (plugin = !0, ie = !1, d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), playerVersion[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10), playerVersion[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10), playerVersion[2] = /[a-zA-Z]/.test(d) ? parseInt(d.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0);
                    else if (typeof win.ActiveXObject != UNDEF) try {
                        var a = new ActiveXObject(SHOCKWAVE_FLASH_AX);
                        a && (d = a.GetVariable("$version"), d && (ie = !0, d = d.split(" ")[1].split(","), playerVersion = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)]))
                    } catch (e) {}
                    return {
                        w3: w3cdom,
                        pv: playerVersion,
                        wk: webkit,
                        ie: ie,
                        win: windows,
                        mac: mac
                    }
                }();
            ! function() {
                ua.w3 && ((typeof doc.readyState != UNDEF && "complete" == doc.readyState || typeof doc.readyState == UNDEF && (doc.getElementsByTagName("body")[0] || doc.body)) && callDomLoadFunctions(), isDomLoaded || (typeof doc.addEventListener != UNDEF && doc.addEventListener("DOMContentLoaded", callDomLoadFunctions, !1), ua.ie && ua.win && (doc.attachEvent(ON_READY_STATE_CHANGE, function() {
                    "complete" == doc.readyState && (doc.detachEvent(ON_READY_STATE_CHANGE, arguments.callee), callDomLoadFunctions())
                }), win == top && ! function() {
                    if (!isDomLoaded) {
                        try {
                            doc.documentElement.doScroll("left")
                        } catch (e) {
                            return void setTimeout(arguments.callee, 0)
                        }
                        callDomLoadFunctions()
                    }
                }()), ua.wk && ! function() {
                    return isDomLoaded ? void 0 : /loaded|complete/.test(doc.readyState) ? void callDomLoadFunctions() : void setTimeout(arguments.callee, 0)
                }(), addLoadEvent(callDomLoadFunctions)))
            }(),
            function() {
                ua.ie && ua.win && window.attachEvent("onunload", function() {
                    for (var ll = listenersArr.length, i = 0; ll > i; i++) listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2]);
                    for (var il = objIdArr.length, j = 0; il > j; j++) removeSWF(objIdArr[j]);
                    for (var k in ua) ua[k] = null;
                    ua = null;
                    for (var l in swfobject) swfobject[l] = null;
                    swfobject = null
                })
            }()
        }
        return {
            registerObject: function(objectIdStr, swfVersionStr, xiSwfUrlStr, callbackFn) {
                if (ua.w3 && objectIdStr && swfVersionStr) {
                    var regObj = {};
                    regObj.id = objectIdStr, regObj.swfVersion = swfVersionStr, regObj.expressInstall = xiSwfUrlStr, regObj.callbackFn = callbackFn, regObjArr[regObjArr.length] = regObj, setVisibility(objectIdStr, !1)
                } else callbackFn && callbackFn({
                    success: !1,
                    id: objectIdStr
                })
            },
            getObjectById: function(objectIdStr) {
                return ua.w3 ? getObjectById(objectIdStr) : void 0
            },
            embedSWF: function(swfUrlStr, replaceElemIdStr, widthStr, heightStr, swfVersionStr, xiSwfUrlStr, flashvarsObj, parObj, attObj, callbackFn) {
                var callbackObj = {
                    success: !1,
                    id: replaceElemIdStr
                };
                ua.w3 && !(ua.wk && ua.wk < 312) && swfUrlStr && replaceElemIdStr && widthStr && heightStr && swfVersionStr ? (setVisibility(replaceElemIdStr, !1), addDomLoadEvent(function() {
                    widthStr += "", heightStr += "";
                    var att = {};
                    if (attObj && typeof attObj === OBJECT)
                        for (var i in attObj) att[i] = attObj[i];
                    att.data = swfUrlStr, att.width = widthStr, att.height = heightStr;
                    var par = {};
                    if (parObj && typeof parObj === OBJECT)
                        for (var j in parObj) par[j] = parObj[j];
                    if (flashvarsObj && typeof flashvarsObj === OBJECT)
                        for (var k in flashvarsObj) typeof par.flashvars != UNDEF ? par.flashvars += "&" + k + "=" + flashvarsObj[k] : par.flashvars = k + "=" + flashvarsObj[k];
                    if (hasPlayerVersion(swfVersionStr)) {
                        var obj = createSWF(att, par, replaceElemIdStr);
                        att.id == replaceElemIdStr && setVisibility(replaceElemIdStr, !0), callbackObj.success = !0, callbackObj.ref = obj
                    } else {
                        if (xiSwfUrlStr && canExpressInstall()) return att.data = xiSwfUrlStr, void showExpressInstall(att, par, replaceElemIdStr, callbackFn);
                        setVisibility(replaceElemIdStr, !0)
                    }
                    callbackFn && callbackFn(callbackObj)
                })) : callbackFn && callbackFn(callbackObj)
            },
            switchOffAutoHideShow: function() {
                autoHideShow = !1
            },
            ua: ua,
            getFlashPlayerVersion: function() {
                return {
                    major: ua.pv[0],
                    minor: ua.pv[1],
                    release: ua.pv[2]
                }
            },
            hasFlashPlayerVersion: hasPlayerVersion,
            createSWF: function(attObj, parObj, replaceElemIdStr) {
                return ua.w3 ? createSWF(attObj, parObj, replaceElemIdStr) : void 0
            },
            showExpressInstall: function(att, par, replaceElemIdStr, callbackFn) {
                ua.w3 && canExpressInstall() && showExpressInstall(att, par, replaceElemIdStr, callbackFn)
            },
            removeSWF: function(objElemIdStr) {
                ua.w3 && removeSWF(objElemIdStr)
            },
            createCSS: function(selStr, declStr, mediaStr, newStyleBoolean) {
                ua.w3 && createCSS(selStr, declStr, mediaStr, newStyleBoolean)
            },
            addDomLoadEvent: addDomLoadEvent,
            addLoadEvent: addLoadEvent,
            getQueryParamValue: function(param) {
                var q = doc.location.search || doc.location.hash;
                if (q) {
                    if (/\?/.test(q) && (q = q.split("?")[1]), null == param) return urlEncodeIfNecessary(q);
                    for (var pairs = q.split("&"), i = 0; i < pairs.length; i++)
                        if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) return urlEncodeIfNecessary(pairs[i].substring(pairs[i].indexOf("=") + 1))
                }
                return ""
            },
            expressInstallCallback: function() {
                if (isExpressInstallActive) {
                    var obj = getElementById(EXPRESS_INSTALL_ID);
                    obj && storedAltContent && (obj.parentNode.replaceChild(storedAltContent, obj), storedAltContentId && (setVisibility(storedAltContentId, !0), ua.ie && ua.win && (storedAltContent.style.display = "block")), storedCallbackFn && storedCallbackFn(storedCallbackObj)), isExpressInstallActive = !1
                }
            }
        }
    }(),
    WebDevice = {
        cookie: null,
        device: "",
        token: "ld",
        init: function() {
            this.cookie = new evercookie, WebDevice.createCookie()
        },
        createCookie: function() {
            this.cookie.get(WebDevice.token, function(value) {
                if ("undefined" == typeof value) {
                    for (var hash = "", possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", i = 0; 15 > i; i++) hash += possible.charAt(Math.floor(Math.random() * possible.length));
                    WebDevice.ajaxRequest(calcSHA1(hash))
                }
            })
        },
        ajaxRequest: function(hash) {
            WebDevice.cookie.set(WebDevice.token, hash), $.ajax({
                url: Ajax.url + "/_c_/v",
                type: "POST",
                data: {
                    data: hash
                },
                dataType: "json"
            })
        }
    },
    hex_chr = "0123456789abcdef";
! function(w) {
    "use strict";
    w.DfpSlotStorage = function(slots, targeting) {
        this.slots = {};
        for (var i in slots) this.slots[slots[i].id] = new DfpSlot(slots[i], targeting);
        googletag.cmd.push(function() {
            googletag.pubads().enableSingleRequest(), googletag.enableServices()
        })
    };
    var DfpSlot = function(options, targeting) {
        var self = this;
        this.id = options.id, googletag.cmd.push(function() {
            self.slot = googletag.defineSlot(options.name, options.size, options.id).addService(googletag.pubads()).setTargeting("gender", targeting.gender).setTargeting("vip", targeting.vip).setTargeting("verified", targeting.verified)
        }), this.lastRefresh = (new Date).getTime()
    };
    DfpSlot.prototype.refreshSlot = function() {
        var newRefresh = (new Date).getTime();
        newRefresh - this.lastRefresh >= 3e4 && (this.lastRefresh = newRefresh, googletag.pubads().refresh([this.slot]))
    }, DfpSlot.prototype.initializeSlot = function() {
        var self = this;
        googletag.cmd.push(function() {
            googletag.display(self.id)
        })
    }
}(window), ! function(a, b, c, d, e) {
    "use strict";

    function f(a) {
        var b = "bez_" + d.makeArray(arguments).join("_").replace(".", "p");
        if ("function" != typeof d.easing[b]) {
            var c = function(a, b) {
                var c = [null, null],
                    d = [null, null],
                    e = [null, null],
                    f = function(f, g) {
                        return e[g] = 3 * a[g], d[g] = 3 * (b[g] - a[g]) - e[g], c[g] = 1 - e[g] - d[g], f * (e[g] + f * (d[g] + f * c[g]))
                    },
                    g = function(a) {
                        return e[0] + a * (2 * d[0] + 3 * c[0] * a)
                    },
                    h = function(a) {
                        for (var b, c = a, d = 0; ++d < 14 && (b = f(c, 0) - a, !(Math.abs(b) < .001));) c -= b / g(c);
                        return c
                    };
                return function(a) {
                    return f(h(a), 1)
                }
            };
            d.easing[b] = function(b, d, e, f, g) {
                return f * c([a[0], a[1]], [a[2], a[3]])(d / g) + e
            }
        }
        return b
    }

    function g() {}

    function h(a, b, c) {
        return Math.max(isNaN(b) ? -1 / 0 : b, Math.min(isNaN(c) ? 1 / 0 : c, a))
    }

    function i(a) {
        return a.match(/ma/) && a.match(/-?\d+(?!d)/g)[a.match(/3d/) ? 12 : 4]
    }

    function j(a) {
        return Bc ? +i(a.css("transform")) : +a.css("left").replace("px", "")
    }

    function k(a, b) {
        var c = {};
        return Bc ? c.transform = "translate3d(" + (a + (b ? .001 : 0)) + "px,0,0)" : c.left = a, c
    }

    function l(a) {
        return {
            "transition-duration": a + "ms"
        }
    }

    function m(a, b) {
        return +String(a).replace(b || "px", "") || e
    }

    function n(a) {
        return /%$/.test(a) && m(a, "%")
    }

    function o(a) {
        return (!!m(a) || !!m(a, "%")) && a
    }

    function p(a, b, c, d) {
        return (a - (d || 0)) * (b + (c || 0))
    }

    function q(a, b, c, d) {
        return -Math.round(a / (b + (c || 0)) - (d || 0))
    }

    function r(a) {
        var b = a.data();
        if (!b.tEnd) {
            var c = a[0],
                d = {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd otransitionend",
                    msTransition: "MSTransitionEnd",
                    transition: "transitionend"
                };
            c.addEventListener(d[jc.prefixed("transition")], function(a) {
                b.tProp && a.propertyName.match(b.tProp) && b.onEndFn()
            }, !1), b.tEnd = !0
        }
    }

    function s(a, b, c, d) {
        var e, f = a.data();
        f && (f.onEndFn = function() {
            e || (e = !0, clearTimeout(f.tT), c())
        }, f.tProp = b, clearTimeout(f.tT), f.tT = setTimeout(function() {
            f.onEndFn()
        }, 1.5 * d), r(a))
    }

    function t(a, b, c) {
        if (a.length) {
            var d = a.data();
            Bc ? (a.css(l(0)), d.onEndFn = g, clearTimeout(d.tT)) : a.stop();
            var e = u(b, function() {
                return j(a)
            });
            return a.css(k(e, c)), e
        }
    }

    function u() {
        for (var a, b = 0, c = arguments.length; c > b && (a = b ? arguments[b]() : arguments[b], "number" != typeof a); b++);
        return a
    }

    function v(a, b) {
        return Math.round(a + (b - a) / 1.5)
    }

    function w() {
        return w.p = w.p || ("https://" === c.protocol ? "https://" : "http://"), w.p
    }

    function x(a) {
        var c = b.createElement("a");
        return c.href = a, c
    }

    function y(a, b) {
        if ("string" != typeof a) return a;
        a = x(a);
        var c, d;
        if (a.host.match(/youtube\.com/) && a.search) {
            if (c = a.search.split("v=")[1]) {
                var e = c.indexOf("&"); - 1 !== e && (c = c.substring(0, e)), d = "youtube"
            }
        } else a.host.match(/youtube\.com|youtu\.be/) ? (c = a.pathname.replace(/^\/(embed\/|v\/)?/, "").replace(/\/.*/, ""), d = "youtube") : a.host.match(/vimeo\.com/) && (d = "vimeo", c = a.pathname.replace(/^\/(video\/)?/, "").replace(/\/.*/, ""));
        return c && d || !b || (c = a.href, d = "custom"), c ? {
            id: c,
            type: d,
            s: a.search.replace(/^\?/, "")
        } : !1
    }

    function z(a, b, c) {
        var e, f, g = a.video;
        return "youtube" === g.type ? (f = w() + "img.youtube.com/vi/" + g.id + "/default.jpg", e = f.replace(/\/default.jpg$/, "/hqdefault.jpg"), a.thumbsReady = !0) : "vimeo" === g.type ? d.ajax({
            url: w() + "vimeo.com/api/v2/video/" + g.id + ".json",
            dataType: "jsonp",
            success: function(d) {
                a.thumbsReady = !0, A(b, {
                    img: d[0].thumbnail_large,
                    thumb: d[0].thumbnail_small
                }, a.i, c)
            }
        }) : a.thumbsReady = !0, {
            img: e,
            thumb: f
        }
    }

    function A(a, b, c, e) {
        for (var f = 0, g = a.length; g > f; f++) {
            var h = a[f];
            if (h.i === c && h.thumbsReady) {
                var i = {
                    videoReady: !0
                };
                i[Rc] = i[Tc] = i[Sc] = !1, e.splice(f, 1, d.extend({}, h, i, b));
                break
            }
        }
    }

    function B(a) {
        function b(a, b, e) {
            var f = a.children("img").eq(0),
                g = a.attr("href"),
                h = a.attr("src"),
                i = f.attr("src"),
                j = b.video,
                k = e ? y(g, j === !0) : !1;
            k ? g = !1 : k = j, c(a, f, d.extend(b, {
                video: k,
                img: b.img || g || h || i,
                thumb: b.thumb || i || h || g
            }))
        }

        function c(a, b, c) {
            var e = c.thumb && c.img !== c.thumb,
                f = m(c.width || a.attr("width")),
                g = m(c.height || a.attr("height"));
            d.extend(c, {
                width: f,
                height: g,
                thumbratio: Q(c.thumbratio || m(c.thumbwidth || b && b.attr("width") || e || f) / m(c.thumbheight || b && b.attr("height") || e || g))
            })
        }
        var e = [];
        return a.children().each(function() {
            var a = d(this),
                f = P(d.extend(a.data(), {
                    id: a.attr("id")
                }));
            if (a.is("a, img")) b(a, f, !0);
            else {
                if (a.is(":empty")) return;
                c(a, null, d.extend(f, {
                    html: this,
                    _html: a.html()
                }))
            }
            e.push(f)
        }), e
    }

    function C(a) {
        return 0 === a.offsetWidth && 0 === a.offsetHeight
    }

    function D(a) {
        return !d.contains(b.documentElement, a)
    }

    function E(a, b, c) {
        a() ? b() : setTimeout(function() {
            E(a, b)
        }, c || 100)
    }

    function F(a) {
        c.replace(c.protocol + "//" + c.host + c.pathname.replace(/^\/?/, "/") + c.search + "#" + a)
    }

    function G(a, b, c) {
        var d = a.data(),
            e = d.measures;
        if (e && (!d.l || d.l.W !== e.width || d.l.H !== e.height || d.l.r !== e.ratio || d.l.w !== b.w || d.l.h !== b.h || d.l.m !== c)) {
            var f = e.width,
                g = e.height,
                i = b.w / b.h,
                j = e.ratio >= i,
                k = "scaledown" === c,
                l = "contain" === c,
                m = "cover" === c;
            j && (k || l) || !j && m ? (f = h(b.w, 0, k ? f : 1 / 0), g = f / e.ratio) : (j && m || !j && (k || l)) && (g = h(b.h, 0, k ? g : 1 / 0), f = g * e.ratio), a.css({
                width: Math.ceil(f),
                height: Math.ceil(g),
                marginLeft: Math.floor(-f / 2),
                marginTop: Math.floor(-g / 2)
            }), d.l = {
                W: e.width,
                H: e.height,
                r: e.ratio,
                w: b.w,
                h: b.h,
                m: c
            }
        }
        return !0
    }

    function H(a, b) {
        var c = a[0];
        c.styleSheet ? c.styleSheet.cssText = b : a.html(b)
    }

    function I(a, b, c) {
        return b === c ? !1 : b >= a ? "left" : a >= c ? "right" : "left right"
    }

    function J(a, b, c, d) {
        if (!c) return !1;
        if (!isNaN(a)) return a - (d ? 0 : 1);
        for (var e, f = 0, g = b.length; g > f; f++) {
            var h = b[f];
            if (h.id === a) {
                e = f;
                break
            }
        }
        return e
    }

    function K(a, b, c) {
        c = c || {}, a.each(function() {
            var a, e = d(this),
                f = e.data();
            f.clickOn || (f.clickOn = !0, d.extend(W(e, {
                onStart: function(b) {
                    a = b, (c.onStart || g).call(this, b)
                },
                onMove: c.onMove || g,
                onTouchEnd: c.onTouchEnd || g,
                onEnd: function(d) {
                    d.moved || c.tail.checked || b.call(this, a)
                }
            }), c.tail))
        })
    }

    function L(a, b) {
        return '<div class="' + a + '">' + (b || "") + "</div>"
    }

    function M(a) {
        for (var b = a.length; b;) {
            var c = Math.floor(Math.random() * b--),
                d = a[b];
            a[b] = a[c], a[c] = d
        }
        return a
    }

    function N(a) {
        return "[object Array]" == Object.prototype.toString.call(a) && d.map(a, function(a) {
            return d.extend({}, a)
        })
    }

    function O(a, b) {
        xc.scrollLeft(a).scrollTop(b)
    }

    function P(a) {
        if (a) {
            var b = {};
            return d.each(a, function(a, c) {
                b[a.toLowerCase()] = c
            }), b
        }
    }

    function Q(a) {
        if (a) {
            var b = +a;
            return isNaN(b) ? (b = a.split("/"), +b[0] / +b[1] || e) : b
        }
    }

    function R(a, b) {
        a.preventDefault(), b && a.stopPropagation()
    }

    function S(a) {
        return a ? ">" : "<"
    }

    function T(a, b) {
        var c = Math.round(b.pos),
            e = b.onEnd || g;
        "undefined" != typeof b.overPos && b.overPos !== b.pos && (c = b.overPos, e = function() {
            T(a, d.extend({}, b, {
                overPos: b.pos,
                time: Math.max(Kc, b.time / 2)
            }))
        });
        var f = d.extend(k(c, b._001), b.width && {
            width: b.width
        });
        Bc ? (a.css(d.extend(l(b.time), f)), b.time > 10 ? s(a, "transform", e, b.time) : e()) : a.stop().animate(f, b.time, Uc, e)
    }

    function U(a, b, c, e, f, h) {
        var i = "undefined" != typeof h;
        if (i || (f.push(arguments), Array.prototype.push.call(arguments, f.length), !(f.length > 1))) {
            a = a || d(a), b = b || d(b);
            var j = a[0],
                k = b[0],
                l = "crossfade" === e.method,
                m = function() {
                    if (!m.done) {
                        m.done = !0;
                        var a = (i || f.shift()) && f.shift();
                        a && U.apply(this, a), (e.onEnd || g)(!!a)
                    }
                },
                n = e.time / (h || 1);
            c.not(a.addClass(Hb).removeClass(Gb)).not(b.addClass(Gb).removeClass(Hb)).removeClass(Hb + " " + Gb), a.stop(), b.stop(), l && k && a.fadeTo(0, 0), a.fadeTo(l ? n : 1, 1, l && m), b.fadeTo(n, 0, m), j && l || k || m()
        }
    }

    function V(a) {
        var b = (a.touches || [])[0] || a;
        a._x = b.pageX, a._y = b.clientY
    }

    function W(c, e) {
        function f(a) {
            return n = d(a.target), t.checked = q = r = !1, l || t.flow || a.touches && a.touches.length > 1 || a.which > 1 || tc && tc.type !== a.type && vc || (q = e.select && n.is(e.select, s)) ? q : (p = "touchstart" === a.type, r = n.is("a, a *", s), V(a), m = tc = a, uc = a.type.replace(/down|start/, "move").replace(/Down/, "Move"), o = t.control, (e.onStart || g).call(s, a, {
                control: o,
                $target: n
            }), l = t.flow = !0, void((!p || t.go) && R(a)))
        }

        function h(a) {
            if (a.touches && a.touches.length > 1 || Hc && !a.isPrimary || uc !== a.type || !l) return l && i(), void(e.onTouchEnd || g)();
            V(a);
            var b = Math.abs(a._x - m._x),
                c = Math.abs(a._y - m._y),
                d = b - c,
                f = (t.go || t.x || d >= 0) && !t.noSwipe,
                h = 0 > d;
            p && !t.checked ? (l = f) && R(a) : (R(a), (e.onMove || g).call(s, a, {
                touch: p
            })), t.checked = t.checked || f || h
        }

        function i(a) {
            (e.onTouchEnd || g)();
            var b = l;
            t.control = l = !1, b && (t.flow = !1), !b || r && !t.checked || (a && R(a), vc = !0, clearTimeout(wc), wc = setTimeout(function() {
                vc = !1
            }, 1e3), (e.onEnd || g).call(s, {
                moved: t.checked,
                $target: n,
                control: o,
                touch: p,
                startEvent: m,
                aborted: !a || "MSPointerCancel" === a.type
            }))
        }

        function j() {
            t.flow || setTimeout(function() {
                t.flow = !0
            }, 10)
        }

        function k() {
            t.flow && setTimeout(function() {
                t.flow = !1
            }, Jc)
        }
        var l, m, n, o, p, q, r, s = c[0],
            t = {};
        return Hc ? (s[Gc]("MSPointerDown", f, !1), b[Gc]("MSPointerMove", h, !1), b[Gc]("MSPointerCancel", i, !1), b[Gc]("MSPointerUp", i, !1)) : (s[Gc] && (s[Gc]("touchstart", f, !1), s[Gc]("touchmove", h, !1), s[Gc]("touchend", i, !1), b[Gc]("touchstart", j, !1), b[Gc]("touchend", k, !1), b[Gc]("touchcancel", k, !1), a[Gc]("scroll", k, !1)), c.on("mousedown", f), yc.on("mousemove", h).on("mouseup", i)), c.on("click", "a", function(a) {
            t.checked && R(a)
        }), t
    }

    function X(a, b) {
        function c(c) {
            j = l = c._x, q = d.now(), p = [
                [q, j]
            ], m = n = C.noMove ? 0 : t(a, (b.getPos || g)(), b._001), (b.onStart || g).call(A, c)
        }

        function e(a, b) {
            s = B.min, u = B.max, w = B.snap, x = a.altKey, z = !1, y = b.control, y || c(a)
        }

        function f(e, f) {
            y && (y = !1, c(e)), C.noSwipe || (l = e._x, p.push([d.now(), l]), n = m - (j - l), o = I(n, s, u), s >= n ? n = v(n, s) : n >= u && (n = v(n, u)), C.noMove || (a.css(k(n, b._001)), z || (z = !0, f.touch || Hc || a.addClass(Wb)), (b.onMove || g).call(A, e, {
                pos: n,
                edge: o
            })))
        }

        function i(c) {
            if (!y) {
                c.touch || Hc || a.removeClass(Wb), r = (new Date).getTime();
                for (var e, f, i, j, k, o, q, t, v, z = r - Jc, B = null, C = Kc, D = b.friction, E = p.length - 1; E >= 0; E--) {
                    if (e = p[E][0], f = Math.abs(e - z), null === B || i > f) B = e, j = p[E][1];
                    else if (B === z || f > i) break;
                    i = f
                }
                q = h(n, s, u);
                var F = j - l,
                    G = F >= 0,
                    H = r - B,
                    I = H > Jc,
                    J = !I && n !== m && q === n;
                w && (q = h(Math[J ? G ? "floor" : "ceil" : "round"](n / w) * w, s, u), s = u = q), J && (w || q === n) && (v = -(F / H), C *= h(Math.abs(v), b.timeLow, b.timeHigh), k = Math.round(n + v * C / D), w || (q = k), (!G && k > u || G && s > k) && (o = G ? s : u, t = k - o, w || (q = o), t = h(q + .03 * t, o - 50, o + 50), C = Math.abs((n - t) / (v / D)))), C *= x ? 10 : 1, (b.onEnd || g).call(A, d.extend(c, {
                    pos: n,
                    newPos: q,
                    overPos: t,
                    time: C,
                    moved: I ? w : Math.abs(n - m) > (w ? 0 : 3)
                }))
            }
        }
        var j, l, m, n, o, p, q, r, s, u, w, x, y, z, A = a[0],
            B = a.data(),
            C = {};
        return C = d.extend(W(b.$wrap, {
            onStart: e,
            onMove: f,
            onTouchEnd: b.onTouchEnd,
            onEnd: i,
            select: b.select,
            control: b.control
        }), C)
    }

    function Y(a, b) {
        var c, e, f, h = a[0],
            i = {
                prevent: {}
            };
        return h[Gc] && h[Gc](Ic, function(a) {
            var h = a.wheelDeltaY || -1 * a.deltaY || 0,
                j = a.wheelDeltaX || -1 * a.deltaX || 0,
                k = Math.abs(j) > Math.abs(h),
                l = S(0 > j),
                m = e === l,
                n = d.now(),
                o = Jc > n - f;
            e = l, f = n, k && i.ok && (!i.prevent[l] || c) && (R(a, !0), c && m && o || (b.shift && (c = !0, clearTimeout(i.t), i.t = setTimeout(function() {
                c = !1
            }, Lc)), (b.onEnd || g)(a, b.shift ? l : j)))
        }, !1), i
    }

    function Z() {
        d.each(d.Fotorama.instances, function(a, b) {
            b.index = a
        })
    }

    function $(a) {
        d.Fotorama.instances.push(a), Z()
    }

    function _(a) {
        d.Fotorama.instances.splice(a.index, 1), Z()
    }
    var ab = "fotorama",
        bb = "fullscreen",
        cb = ab + "__wrap",
        db = cb + "--css2",
        eb = cb + "--css3",
        fb = cb + "--video",
        gb = cb + "--fade",
        hb = cb + "--slide",
        ib = cb + "--no-controls",
        jb = cb + "--no-shadows",
        kb = cb + "--pan-y",
        lb = cb + "--rtl",
        mb = ab + "__stage",
        nb = mb + "__frame",
        ob = nb + "--video",
        pb = mb + "__shaft",
        qb = mb + "--only-active",
        rb = ab + "__grab",
        sb = ab + "__pointer",
        tb = ab + "__arr",
        ub = tb + "--disabled",
        vb = tb + "--prev",
        wb = tb + "--next",
        xb = ab + "__nav",
        yb = xb + "-wrap",
        zb = xb + "__shaft",
        Ab = xb + "--dots",
        Bb = xb + "--thumbs",
        Cb = xb + "__frame",
        Db = Cb + "--dot",
        Eb = Cb + "--thumb",
        Fb = ab + "__fade",
        Gb = Fb + "-front",
        Hb = Fb + "-rear",
        Ib = ab + "__shadow",
        Jb = Ib + "s",
        Kb = Jb + "--left",
        Lb = Jb + "--right",
        Mb = ab + "__active",
        Nb = ab + "__select",
        Ob = ab + "--hidden",
        Pb = ab + "--fullscreen",
        Qb = ab + "__fullscreen-icon",
        Rb = ab + "__error",
        Sb = ab + "__loading",
        Tb = ab + "__loaded",
        Ub = Tb + "--full",
        Vb = Tb + "--img",
        Wb = ab + "__grabbing",
        Xb = ab + "__img",
        Yb = Xb + "--full",
        Zb = ab + "__dot",
        $b = ab + "__thumb",
        _b = $b + "-border",
        ac = ab + "__html",
        bc = ab + "__video",
        cc = bc + "-play",
        dc = bc + "-close",
        ec = ab + "__caption",
        fc = ab + "__caption__wrap",
        gc = ab + "__spinner",
        hc = d && d.fn.jquery.split(".");
    if (!hc || hc[0] < 1 || 1 == hc[0] && hc[1] < 8) throw "Fotorama requires jQuery 1.8 or later and will not run without it.";
    var ic = {},
        jc = function(a, b, c) {
            function d(a) {
                r.cssText = a
            }

            function e(a, b) {
                return typeof a === b
            }

            function f(a, b) {
                return !!~("" + a).indexOf(b)
            }

            function g(a, b) {
                for (var d in a) {
                    var e = a[d];
                    if (!f(e, "-") && r[e] !== c) return "pfx" == b ? e : !0
                }
                return !1
            }

            function h(a, b, d) {
                for (var f in a) {
                    var g = b[a[f]];
                    if (g !== c) return d === !1 ? a[f] : e(g, "function") ? g.bind(d || b) : g
                }
                return !1
            }

            function i(a, b, c) {
                var d = a.charAt(0).toUpperCase() + a.slice(1),
                    f = (a + " " + u.join(d + " ") + d).split(" ");
                return e(b, "string") || e(b, "undefined") ? g(f, b) : (f = (a + " " + v.join(d + " ") + d).split(" "), h(f, b, c))
            }
            var j, k, l, m = "2.6.2",
                n = {},
                o = b.documentElement,
                p = "modernizr",
                q = b.createElement(p),
                r = q.style,
                s = ({}.toString, " -webkit- -moz- -o- -ms- ".split(" ")),
                t = "Webkit Moz O ms",
                u = t.split(" "),
                v = t.toLowerCase().split(" "),
                w = {},
                x = [],
                y = x.slice,
                z = function(a, c, d, e) {
                    var f, g, h, i, j = b.createElement("div"),
                        k = b.body,
                        l = k || b.createElement("body");
                    if (parseInt(d, 10))
                        for (; d--;) h = b.createElement("div"), h.id = e ? e[d] : p + (d + 1), j.appendChild(h);
                    return f = ["&#173;", '<style id="s', p, '">', a, "</style>"].join(""), j.id = p, (k ? j : l).innerHTML += f, l.appendChild(j), k || (l.style.background = "", l.style.overflow = "hidden", i = o.style.overflow, o.style.overflow = "hidden", o.appendChild(l)), g = c(j, a), k ? j.parentNode.removeChild(j) : (l.parentNode.removeChild(l), o.style.overflow = i), !!g
                },
                A = {}.hasOwnProperty;
            l = e(A, "undefined") || e(A.call, "undefined") ? function(a, b) {
                return b in a && e(a.constructor.prototype[b], "undefined")
            } : function(a, b) {
                return A.call(a, b)
            }, Function.prototype.bind || (Function.prototype.bind = function(a) {
                var b = this;
                if ("function" != typeof b) throw new TypeError;
                var c = y.call(arguments, 1),
                    d = function() {
                        if (this instanceof d) {
                            var e = function() {};
                            e.prototype = b.prototype;
                            var f = new e,
                                g = b.apply(f, c.concat(y.call(arguments)));
                            return Object(g) === g ? g : f
                        }
                        return b.apply(a, c.concat(y.call(arguments)))
                    };
                return d
            }), w.csstransforms3d = function() {
                var a = !!i("perspective");
                return a
            };
            for (var B in w) l(w, B) && (k = B.toLowerCase(), n[k] = w[B](), x.push((n[k] ? "" : "no-") + k));
            return n.addTest = function(a, b) {
                if ("object" == typeof a)
                    for (var d in a) l(a, d) && n.addTest(d, a[d]);
                else {
                    if (a = a.toLowerCase(), n[a] !== c) return n;
                    b = "function" == typeof b ? b() : b, "undefined" != typeof enableClasses && enableClasses && (o.className += " " + (b ? "" : "no-") + a), n[a] = b
                }
                return n
            }, d(""), q = j = null, n._version = m, n._prefixes = s, n._domPrefixes = v, n._cssomPrefixes = u, n.testProp = function(a) {
                return g([a])
            }, n.testAllProps = i, n.testStyles = z, n.prefixed = function(a, b, c) {
                return b ? i(a, b, c) : i(a, "pfx")
            }, n
        }(a, b),
        kc = {
            ok: !1,
            is: function() {
                return !1
            },
            request: function() {},
            cancel: function() {},
            event: "",
            prefix: ""
        },
        lc = "webkit moz o ms khtml".split(" ");
    if ("undefined" != typeof b.cancelFullScreen) kc.ok = !0;
    else
        for (var mc = 0, nc = lc.length; nc > mc; mc++)
            if (kc.prefix = lc[mc], "undefined" != typeof b[kc.prefix + "CancelFullScreen"]) {
                kc.ok = !0;
                break
            }
    kc.ok && (kc.event = kc.prefix + "fullscreenchange", kc.is = function() {
        switch (this.prefix) {
            case "":
                return b.fullScreen;
            case "webkit":
                return b.webkitIsFullScreen;
            default:
                return b[this.prefix + "FullScreen"]
        }
    }, kc.request = function(a) {
        return "" === this.prefix ? a.requestFullScreen() : a[this.prefix + "RequestFullScreen"]()
    }, kc.cancel = function() {
        return "" === this.prefix ? b.cancelFullScreen() : b[this.prefix + "CancelFullScreen"]()
    });
    var oc, pc = {
            lines: 12,
            length: 5,
            width: 2,
            radius: 7,
            corners: 1,
            rotate: 15,
            color: "rgba(128, 128, 128, .75)",
            hwaccel: !0
        },
        qc = {
            top: "auto",
            left: "auto",
            className: ""
        };
    ! function(a, b) {
        oc = b()
    }(this, function() {
        function a(a, c) {
            var d, e = b.createElement(a || "div");
            for (d in c) e[d] = c[d];
            return e
        }

        function c(a) {
            for (var b = 1, c = arguments.length; c > b; b++) a.appendChild(arguments[b]);
            return a
        }

        function d(a, b, c, d) {
            var e = ["opacity", b, ~~(100 * a), c, d].join("-"),
                f = .01 + 100 * (c / d),
                g = Math.max(1 - (1 - a) / b * (100 - f), a),
                h = m.substring(0, m.indexOf("Animation")).toLowerCase(),
                i = h && "-" + h + "-" || "";
            return o[e] || (p.insertRule("@" + i + "keyframes " + e + "{0%{opacity:" + g + "}" + f + "%{opacity:" + a + "}" + (f + .01) + "%{opacity:1}" + (f + b) % 100 + "%{opacity:" + a + "}100%{opacity:" + g + "}}", p.cssRules.length), o[e] = 1), e
        }

        function f(a, b) {
            var c, d, f = a.style;
            for (b = b.charAt(0).toUpperCase() + b.slice(1), d = 0; d < n.length; d++)
                if (c = n[d] + b, f[c] !== e) return c;
            return f[b] !== e ? b : void 0
        }

        function g(a, b) {
            for (var c in b) a.style[f(a, c) || c] = b[c];
            return a
        }

        function h(a) {
            for (var b = 1; b < arguments.length; b++) {
                var c = arguments[b];
                for (var d in c) a[d] === e && (a[d] = c[d])
            }
            return a
        }

        function i(a) {
            for (var b = {
                    x: a.offsetLeft,
                    y: a.offsetTop
                }; a = a.offsetParent;) b.x += a.offsetLeft, b.y += a.offsetTop;
            return b
        }

        function j(a, b) {
            return "string" == typeof a ? a : a[b % a.length]
        }

        function k(a) {
            return "undefined" == typeof this ? new k(a) : void(this.opts = h(a || {}, k.defaults, q))
        }

        function l() {
            function b(b, c) {
                return a("<" + b + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', c)
            }
            p.addRule(".spin-vml", "behavior:url(#default#VML)"), k.prototype.lines = function(a, d) {
                function e() {
                    return g(b("group", {
                        coordsize: k + " " + k,
                        coordorigin: -i + " " + -i
                    }), {
                        width: k,
                        height: k
                    })
                }

                function f(a, f, h) {
                    c(m, c(g(e(), {
                        rotation: 360 / d.lines * a + "deg",
                        left: ~~f
                    }), c(g(b("roundrect", {
                        arcsize: d.corners
                    }), {
                        width: i,
                        height: d.width,
                        left: d.radius,
                        top: -d.width >> 1,
                        filter: h
                    }), b("fill", {
                        color: j(d.color, a),
                        opacity: d.opacity
                    }), b("stroke", {
                        opacity: 0
                    }))))
                }
                var h, i = d.length + d.width,
                    k = 2 * i,
                    l = 2 * -(d.width + d.length) + "px",
                    m = g(e(), {
                        position: "absolute",
                        top: l,
                        left: l
                    });
                if (d.shadow)
                    for (h = 1; h <= d.lines; h++) f(h, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");
                for (h = 1; h <= d.lines; h++) f(h);
                return c(a, m)
            }, k.prototype.opacity = function(a, b, c, d) {
                var e = a.firstChild;
                d = d.shadow && d.lines || 0, e && b + d < e.childNodes.length && (e = e.childNodes[b + d], e = e && e.firstChild, e = e && e.firstChild, e && (e.opacity = c))
            }
        }
        var m, n = ["webkit", "Moz", "ms", "O"],
            o = {},
            p = function() {
                var d = a("style", {
                    type: "text/css"
                });
                return c(b.getElementsByTagName("head")[0], d), d.sheet || d.styleSheet
            }(),
            q = {
                lines: 12,
                length: 7,
                width: 5,
                radius: 10,
                rotate: 0,
                corners: 1,
                color: "#000",
                direction: 1,
                speed: 1,
                trail: 100,
                opacity: .25,
                fps: 20,
                zIndex: 2e9,
                className: "spinner",
                top: "auto",
                left: "auto",
                position: "relative"
            };
        k.defaults = {}, h(k.prototype, {
            spin: function(b) {
                this.stop();
                var c, d, e = this,
                    f = e.opts,
                    h = e.el = g(a(0, {
                        className: f.className
                    }), {
                        position: f.position,
                        width: 0,
                        zIndex: f.zIndex
                    }),
                    j = f.radius + f.length + f.width;
                if (b && (b.insertBefore(h, b.firstChild || null), d = i(b), c = i(h), g(h, {
                        left: ("auto" == f.left ? d.x - c.x + (b.offsetWidth >> 1) : parseInt(f.left, 10) + j) + "px",
                        top: ("auto" == f.top ? d.y - c.y + (b.offsetHeight >> 1) : parseInt(f.top, 10) + j) + "px"
                    })), h.setAttribute("role", "progressbar"), e.lines(h, e.opts), !m) {
                    var k, l = 0,
                        n = (f.lines - 1) * (1 - f.direction) / 2,
                        o = f.fps,
                        p = o / f.speed,
                        q = (1 - f.opacity) / (p * f.trail / 100),
                        r = p / f.lines;
                    ! function s() {
                        l++;
                        for (var a = 0; a < f.lines; a++) k = Math.max(1 - (l + (f.lines - a) * r) % p * q, f.opacity), e.opacity(h, a * f.direction + n, k, f);
                        e.timeout = e.el && setTimeout(s, ~~(1e3 / o))
                    }()
                }
                return e
            },
            stop: function() {
                var a = this.el;
                return a && (clearTimeout(this.timeout), a.parentNode && a.parentNode.removeChild(a), this.el = e), this
            },
            lines: function(b, e) {
                function f(b, c) {
                    return g(a(), {
                        position: "absolute",
                        width: e.length + e.width + "px",
                        height: e.width + "px",
                        background: b,
                        boxShadow: c,
                        transformOrigin: "left",
                        transform: "rotate(" + ~~(360 / e.lines * i + e.rotate) + "deg) translate(" + e.radius + "px,0)",
                        borderRadius: (e.corners * e.width >> 1) + "px"
                    })
                }
                for (var h, i = 0, k = (e.lines - 1) * (1 - e.direction) / 2; i < e.lines; i++) h = g(a(), {
                    position: "absolute",
                    top: 1 + ~(e.width / 2) + "px",
                    transform: e.hwaccel ? "translate3d(0,0,0)" : "",
                    opacity: e.opacity,
                    animation: m && d(e.opacity, e.trail, k + i * e.direction, e.lines) + " " + 1 / e.speed + "s linear infinite"
                }), e.shadow && c(h, g(f("#000", "0 0 4px #000"), {
                    top: "2px"
                })), c(b, c(h, f(j(e.color, i), "0 0 1px rgba(0,0,0,.1)")));
                return b
            },
            opacity: function(a, b, c) {
                b < a.childNodes.length && (a.childNodes[b].style.opacity = c)
            }
        });
        var r = g(a("group"), {
            behavior: "url(#default#VML)"
        });
        return !f(r, "transform") && r.adj ? l() : m = f(r, "animation"), k
    });
    var rc, sc, tc, uc, vc, wc, xc = d(a),
        yc = d(b),
        zc = "quirks" === c.hash.replace("#", ""),
        Ac = jc.csstransforms3d,
        Bc = Ac && !zc,
        Cc = Ac || "CSS1Compat" === b.compatMode,
        Dc = kc.ok,
        Ec = navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i),
        Fc = !Bc || Ec,
        Gc = "addEventListener",
        Hc = a.navigator.msPointerEnabled,
        Ic = "onwheel" in b.createElement("div") ? "wheel" : b.onmousewheel !== e ? "mousewheel" : "DOMMouseScroll",
        Jc = 250,
        Kc = 300,
        Lc = 1400,
        Mc = 5e3,
        Nc = 2,
        Oc = 64,
        Pc = 500,
        Qc = 333,
        Rc = "$stageFrame",
        Sc = "$navDotFrame",
        Tc = "$navThumbFrame",
        Uc = f([.1, 0, .25, 1]);
    jQuery.Fotorama = function(a, e) {
        function f() {
            d.each(kd, function(a, b) {
                if (!b.i) {
                    b.i = Xd++;
                    var c = y(b.video, !0);
                    if (c) {
                        var d = {};
                        b.video = c, b.img || b.thumb ? b.thumbsReady = !0 : d = z(b, kd, Td), A(kd, {
                            img: d.img,
                            thumb: d.thumb
                        }, b.i, Td)
                    }
                }
            })
        }

        function g(a) {
            var b = "keydown." + ab,
                c = "keydown." + ab + Ud,
                d = "resize." + ab + Ud;
            a ? (yc.on(c, function(a) {
                od && 27 === a.keyCode ? (R(a), bd(od, !0, !0)) : (Td.fullScreen || e.keyboard && !Td.index) && (27 === a.keyCode ? (R(a), Td.cancelFullScreen()) : 39 === a.keyCode || 40 === a.keyCode && Td.fullScreen ? (R(a), Td.show({
                    index: ">",
                    slow: a.altKey,
                    user: !0
                })) : (37 === a.keyCode || 38 === a.keyCode && Td.fullScreen) && (R(a), Td.show({
                    index: "<",
                    slow: a.altKey,
                    user: !0
                })))
            }), Td.index || yc.off(b).on(b, "textarea, input, select", function(a) {
                !sc.hasClass(bb) && a.stopPropagation()
            }), xc.on(d, Td.resize)) : (yc.off(c), xc.off(d))
        }

        function i(b) {
            b !== i.f && (b ? (a.html("").addClass(ab + " " + Vd).append(_d).before(Zd).before($d), $(Td)) : (_d.detach(), Zd.detach(), $d.detach(), a.html(Yd.urtext).removeClass(Vd), _(Td)), g(b), i.f = b)
        }

        function j() {
            kd = Td.data = kd || N(e.data) || B(a), ld = Td.size = kd.length, !jd.ok && e.shuffle && M(kd), f(), ue = C(ue), ld && i(!0)
        }

        function r() {
            var a = 2 > ld || od;
            xe.noMove = a || Dd, xe.noSwipe = a || !e.swipe, be.toggleClass(rb, !xe.noMove && !xe.noSwipe), Hc && _d.toggleClass(kb, !xe.noSwipe)
        }

        function s(a) {
            a === !0 && (a = ""), e.autoplay = Math.max(+a || Mc, 1.5 * Gd)
        }

        function v(a) {
            return a ? "add" : "remove"
        }

        function w() {
            Td.options = e = P(e), Dd = "crossfade" === e.transition || "dissolve" === e.transition, xd = e.loop && (ld > 2 || Dd), Gd = +e.transitionduration || Kc, Id = "rtl" === e.direction;
            var a = {
                add: [],
                remove: []
            };
            ld > 1 ? (yd = e.nav, Ad = "top" === e.navposition, a.remove.push(Nb), fe.toggle(e.arrows)) : (yd = !1, fe.hide()), hc(), nd = new oc(d.extend(pc, e.spinner, qc, {
                direction: Id ? -1 : 1
            })), wc(), zc(), e.autoplay && s(e.autoplay), Ed = m(e.thumbwidth) || Oc, Fd = m(e.thumbheight) || Oc, ye.ok = Ae.ok = e.trackpad && !Fc, r(), Vc(e, !0), zd = "thumbs" === yd, zd ? (jc(ld, "navThumb"), md = ke, Sd = Tc, H(Zd, d.Fotorama.jst.style({
                w: Ed,
                h: Fd,
                b: e.thumbborderwidth,
                m: e.thumbmargin,
                s: Ud,
                q: !Cc
            })), he.addClass(Bb).removeClass(Ab)) : "dots" === yd ? (jc(ld, "navDot"), md = je, Sd = Sc, he.addClass(Ab).removeClass(Bb)) : (yd = !1, he.removeClass(Bb + " " + Ab)), yd && (Ad ? ge.insertBefore(ae) : ge.insertAfter(ae), tc.nav = !1, tc(md, ie, "nav")), Bd = e.allowfullscreen, Bd ? (oe.appendTo(ae), Cd = Dc && "native" === Bd) : (oe.detach(), Cd = !1), a[v(Dd)].push(gb), a[v(!Dd)].push(hb), a[v(Id)].push(lb), Hd = e.shadows && !Fc, a[v(!Hd)].push(jb), _d.addClass(a.add.join(" ")).removeClass(a.remove.join(" ")), ve = d.extend({}, e)
        }

        function x(a) {
            return 0 > a ? (ld + a % ld) % ld : a >= ld ? a % ld : a
        }

        function C(a) {
            return h(a, 0, ld - 1)
        }

        function V(a) {
            return xd ? x(a) : C(a)
        }

        function W(a) {
            return a > 0 || xd ? a - 1 : !1
        }

        function Z(a) {
            return ld - 1 > a || xd ? a + 1 : !1
        }

        function Fb() {
            le.min = xd ? -1 / 0 : -p(ld - 1, we.w, e.margin, rd), le.max = xd ? 1 / 0 : -p(0, we.w, e.margin, rd), le.snap = we.w + e.margin
        }

        function Gb() {
            me.min = Math.min(0, we.W - ie.width()), me.max = 0, ie.toggleClass(rb, !(ze.noMove = me.min === me.max))
        }

        function Hb(a, b, c) {
            if ("number" == typeof a) {
                a = new Array(a);
                var e = !0
            }
            return d.each(a, function(a, d) {
                if (e && (d = a), "number" == typeof d) {
                    var f = kd[x(d)];
                    if (f) {
                        var g = "$" + b + "Frame",
                            h = f[g];
                        c.call(this, a, d, f, h, g, h && h.data())
                    }
                }
            })
        }

        function Ib(a, b, c, d) {
            (!Jd || "*" === Jd && d === wd) && (a = o(e.width) || o(a) || Pc, b = o(e.height) || o(b) || Qc, Td.resize({
                width: a,
                ratio: e.ratio || c || a / b
            }, 0, d === wd ? !0 : "*"))
        }

        function Wb(a, b, c, f, g) {
            Hb(a, b, function(a, h, i, j, k, l) {
                function m(a) {
                    var b = x(h);
                    Wc(a, {
                        index: b,
                        src: v,
                        frame: kd[b]
                    })
                }

                function n() {
                    s.remove(), d.Fotorama.cache[v] = "error", i.html && "stage" === b || !w || w === v ? (!v || i.html || q ? "stage" === b && (j.trigger("f:load").removeClass(Sb + " " + Rb).addClass(Tb), m("load"), Ib()) : (j.trigger("f:error").removeClass(Sb).addClass(Rb), m("error")), l.state = "error", !(ld > 1 && kd[h] === i) || i.html || i.deleted || i.video || q || (i.deleted = !0, Td.splice(h, 1))) : (i[u] = v = w, Wb([h], b, c, f, !0))
                }

                function o() {
                    d.Fotorama.measures[v] = t.measures = d.Fotorama.measures[v] || {
                        width: r.width,
                        height: r.height,
                        ratio: r.width / r.height
                    }, Ib(t.measures.width, t.measures.height, t.measures.ratio, h), s.off("load error").addClass(Xb + (q ? " " + Yb : "")).prependTo(j), G(s, c || we, f || i.fit || e.fit), d.Fotorama.cache[v] = l.state = "loaded", setTimeout(function() {
                        j.trigger("f:load").removeClass(Sb + " " + Rb).addClass(Tb + " " + (q ? Ub : Vb)), "stage" === b && m("load")
                    }, 5)
                }

                function p() {
                    var a = 10;
                    E(function() {
                        return !Qd || !a-- && !Fc
                    }, function() {
                        o()
                    })
                }
                if (j) {
                    var q = Td.fullScreen && i.full && i.full !== i.img && !l.$full && "stage" === b;
                    if (!l.$img || g || q) {
                        var r = new Image,
                            s = d(r),
                            t = s.data();
                        l[q ? "$full" : "$img"] = s;
                        var u = "stage" === b ? q ? "full" : "img" : "thumb",
                            v = i[u],
                            w = q ? null : i["stage" === b ? "thumb" : "img"];
                        if ("navThumb" === b && (j = l.$wrap), !v) return void n();
                        d.Fotorama.cache[v] ? ! function y() {
                            "error" === d.Fotorama.cache[v] ? n() : "loaded" === d.Fotorama.cache[v] ? setTimeout(p, 0) : setTimeout(y, 100)
                        }() : (d.Fotorama.cache[v] = "*", s.on("load", p).on("error", n)), l.state = "", r.src = v
                    }
                }
            })
        }

        function bc(a) {
            te.append(nd.spin().el).appendTo(a)
        }

        function hc() {
            te.detach(), nd && nd.stop()
        }

        function ic() {
            var a = Td.activeFrame[Rc];
            a && !a.data().state && (bc(a), a.on("f:load f:error", function() {
                a.off("f:load f:error"), hc()
            }))
        }

        function jc(a, b) {
            Hb(a, b, function(a, c, f, g, h, i) {
                g || (g = f[h] = _d[h].clone(), i = g.data(), i.data = f, "stage" === b ? (f.html && d('<div class="' + ac + '"></div>').append(f._html ? d(f.html).removeAttr("id").html(f._html) : f.html).appendTo(g), e.captions && f.caption && d(L(ec, L(fc, f.caption))).appendTo(g), f.video && g.addClass(ob).append(qe.clone()), ce = ce.add(g)) : "navDot" === b ? je = je.add(g) : "navThumb" === b && (i.$wrap = g.children(":first"), ke = ke.add(g), f.video && g.append(qe.clone())))
            })
        }

        function lc(a, b, c) {
            return a && a.length && G(a, b, c)
        }

        function mc(a) {
            Hb(a, "stage", function(a, b, c, f, g, h) {
                if (f) {
                    Ce[Rc][x(b)] = f.css(d.extend({
                        left: Dd ? 0 : p(b, we.w, e.margin, rd)
                    }, Dd && l(0))), D(f[0]) && (f.appendTo(be), bd(c.$video));
                    var i = c.fit || e.fit;
                    lc(h.$img, we, i), lc(h.$full, we, i)
                }
            })
        }

        function nc(a, b) {
            if ("thumbs" === yd && !isNaN(a)) {
                var c = -a,
                    e = -a + we.w;
                ke.each(function() {
                    var a = d(this),
                        f = a.data(),
                        g = f.eq,
                        h = {
                            h: Fd
                        },
                        i = "cover";
                    h.w = f.w, f.l + f.w < c || f.l > e || lc(f.$img, h, i) || b && Wb([g], "navThumb", h, i)
                })
            }
        }

        function tc(a, b, c) {
            if (!tc[c]) {
                var f = "nav" === c && zd,
                    g = 0;
                b.append(a.filter(function() {
                    for (var a, b = d(this), c = b.data(), e = 0, f = kd.length; f > e; e++)
                        if (c.data === kd[e]) {
                            a = !0, c.eq = e;
                            break
                        }
                    return a || b.remove() && !1
                }).sort(function(a, b) {
                    return d(a).data().eq - d(b).data().eq
                }).each(function() {
                    if (f) {
                        var a = d(this),
                            b = a.data(),
                            c = Math.round(Fd * b.data.thumbratio) || Ed;
                        b.l = g, b.w = c, a.css({
                            width: c
                        }), g += c + e.thumbmargin
                    }
                })), tc[c] = !0
            }
        }

        function uc(a) {
            return a - De > we.w / 3
        }

        function vc(a) {
            return !(xd || ue + a && ue - ld + a || od)
        }

        function wc() {
            de.toggleClass(ub, vc(0)), ee.toggleClass(ub, vc(1))
        }

        function zc() {
            ye.ok && (ye.prevent = {
                "<": vc(0),
                ">": vc(1)
            })
        }

        function Ac(a) {
            var b, c, d = a.data();
            return zd ? (b = d.l, c = d.w) : (b = a.position().left, c = a.width()), {
                c: b + c / 2,
                min: -b + 10 * e.thumbmargin,
                max: -b + we.w - c - 10 * e.thumbmargin
            }
        }

        function Ec(a) {
            var b = Td.activeFrame[Sd].data();
            T(ne, {
                time: .9 * a,
                pos: b.l,
                width: b.w - 2 * e.thumbborderwidth
            })
        }

        function Gc(a) {
            var b = kd[a.guessIndex][Sd];
            if (b) {
                var c = me.min !== me.max,
                    d = c && Ac(Td.activeFrame[Sd]),
                    e = c && (a.keep && Gc.l ? Gc.l : h((a.coo || we.w / 2) - Ac(b).c, d.min, d.max)),
                    f = c && h(e, me.min, me.max),
                    g = .9 * a.time;
                T(ie, {
                    time: g,
                    pos: f || 0,
                    onEnd: function() {
                        nc(f, !0)
                    }
                }), ad(he, I(f, me.min, me.max)), Gc.l = e
            }
        }

        function Ic() {
            Lc(Sd), Be[Sd].push(Td.activeFrame[Sd].addClass(Mb))
        }

        function Lc(a) {
            for (var b = Be[a]; b.length;) b.shift().removeClass(Mb)
        }

        function Nc(a) {
            var b = Ce[a];
            d.each(qd, function(a, c) {
                delete b[x(c)]
            }), d.each(b, function(a, c) {
                delete b[a], c.detach()
            })
        }

        function Uc(a) {
            rd = sd = ue;
            var b = Td.activeFrame,
                c = b[Rc];
            c && (Lc(Rc), Be[Rc].push(c.addClass(Mb)), a || Td.show.onEnd(!0), t(be, 0, !0), Nc(Rc), mc(qd), Fb(), Gb())
        }

        function Vc(a, b) {
            a && d.extend(we, {
                width: a.width || we.width,
                height: a.height,
                minwidth: a.minwidth,
                maxwidth: a.maxwidth,
                minheight: a.minheight,
                maxheight: a.maxheight,
                ratio: Q(a.ratio)
            }) && !b && d.extend(e, {
                width: we.width,
                height: we.height,
                minwidth: we.minwidth,
                maxwidth: we.maxwidth,
                minheight: we.minheight,
                maxheight: we.maxheight,
                ratio: we.ratio
            })
        }

        function Wc(b, c) {
            a.trigger(ab + ":" + b, [Td, c])
        }

        function Xc() {
            clearTimeout(Yc.t), Qd = 1, e.stopautoplayontouch ? Td.stopAutoplay() : Nd = !0
        }

        function Yc() {
            Yc.t = setTimeout(function() {
                Qd = 0
            }, Kc + Jc)
        }

        function Zc() {
            Nd = !(!od && !Od)
        }

        function $c() {
            if (clearTimeout($c.t), !e.autoplay || Nd) return void(Td.autoplay && (Td.autoplay = !1, Wc("stopautoplay")));
            Td.autoplay || (Td.autoplay = !0, Wc("startautoplay"));
            var a = ue,
                b = Td.activeFrame[Rc].data();
            E(function() {
                return b.state || a !== ue
            }, function() {
                $c.t = setTimeout(function() {
                    Nd || a !== ue || Td.show(xd ? S(!Id) : x(ue + (Id ? -1 : 1)))
                }, e.autoplay)
            })
        }

        function _c() {
            Td.fullScreen && (Td.fullScreen = !1, Dc && kc.cancel(Wd), sc.removeClass(bb), rc.removeClass(bb), a.removeClass(Pb).insertAfter($d), we = d.extend({}, Pd), bd(od, !0, !0), fd("x", !1), Td.resize(), Wb(qd, "stage"), O(Ld, Kd), Wc("fullscreenexit"))
        }

        function ad(a, b) {
            Hd && (a.removeClass(Kb + " " + Lb), b && !od && a.addClass(b.replace(/^|\s/g, " " + Jb + "--")))
        }

        function bd(a, b, c) {
            b && (_d.removeClass(fb), od = !1, r()), a && a !== od && (a.remove(), Wc("unloadvideo")), c && (Zc(), $c())
        }

        function cd(a) {
            _d.toggleClass(ib, a)
        }

        function dd(a) {
            if (!xe.flow) {
                var b = a ? a.pageX : dd.x,
                    c = b && !vc(uc(b)) && e.click;
                dd.p === c || !Dd && e.swipe || !ae.toggleClass(sb, c) || (dd.p = c, dd.x = b)
            }
        }

        function ed(a, b) {
            var c = a.target,
                f = d(c);
            f.hasClass(cc) ? Td.playVideo() : c === pe ? Td[(Td.fullScreen ? "cancel" : "request") + "FullScreen"]() : od ? c === se && bd(od, !0, !0) : b ? cd() : e.click && Td.show({
                index: a.shiftKey || S(uc(a._x)),
                slow: a.altKey,
                user: !0
            })
        }

        function fd(a, b) {
            xe[a] = ze[a] = b
        }

        function gd(a, b) {
            var c = d(this).data().eq;
            Td.show({
                index: c,
                slow: a.altKey,
                user: !0,
                coo: a._x - he.offset().left,
                time: b
            })
        }

        function hd() {
            if (j(), w(), !hd.i) {
                hd.i = !0;
                var a = e.startindex;
                (a || e.hash && c.hash) && (wd = J(a || c.hash.replace(/^#/, ""), kd, 0 === Td.index || a, a)), ue = rd = sd = td = wd = V(wd) || 0
            }
            if (ld) {
                if (id()) return;
                od && bd(od, !0), qd = [], Nc(Rc), Td.show({
                    index: ue,
                    time: 0
                }), Td.resize()
            } else Td.destroy()
        }

        function id() {
            return !id.f === Id ? (id.f = Id, ue = ld - 1 - ue, Td.reverse(), !0) : void 0
        }

        function jd() {
            jd.ok || (jd.ok = !0, Wc("ready"))
        }
        rc = rc || d("html"), sc = sc || d("body");
        var kd, ld, md, nd, od, pd, qd, rd, sd, td, ud, vd, wd, xd, yd, zd, Ad, Bd, Cd, Dd, Ed, Fd, Gd, Hd, Id, Jd, Kd, Ld, Md, Nd, Od, Pd, Qd, Rd, Sd, Td = this,
            Ud = d.now(),
            Vd = ab + Ud,
            Wd = a[0],
            Xd = 1,
            Yd = a.data(),
            Zd = d("<style></style>"),
            $d = d(L(Ob)),
            _d = d(L(cb)),
            ae = d(L(mb)).appendTo(_d),
            be = (ae[0], d(L(pb)).appendTo(ae)),
            ce = d(),
            de = d(L(tb + " " + vb)),
            ee = d(L(tb + " " + wb)),
            fe = de.add(ee).appendTo(ae),
            ge = d(L(yb)),
            he = d(L(xb)).appendTo(ge),
            ie = d(L(zb)).appendTo(he),
            je = d(),
            ke = d(),
            le = be.data(),
            me = ie.data(),
            ne = d(L(_b)).appendTo(ie),
            oe = d(L(Qb)),
            pe = oe[0],
            qe = d(L(cc)),
            re = d(L(dc)).appendTo(ae),
            se = re[0],
            te = d(L(gc)),
            ue = !1,
            ve = {},
            we = {},
            xe = {},
            ye = {},
            ze = {},
            Ae = {},
            Be = {},
            Ce = {},
            De = 0,
            Ee = [];
        _d[Rc] = d(L(nb)), _d[Tc] = d(L(Cb + " " + Eb, L($b))), _d[Sc] = d(L(Cb + " " + Db, L(Zb))), Be[Rc] = [], Be[Tc] = [], Be[Sc] = [], Ce[Rc] = {}, _d.addClass(Bc ? eb : db), Yd.fotorama = this, Td.startAutoplay = function(a) {
            return Td.autoplay ? this : (Nd = Od = !1, s(a || e.autoplay), $c(), this)
        }, Td.stopAutoplay = function() {
            return Td.autoplay && (Nd = Od = !0, $c()), this
        }, Td.show = function(a) {
            var b;
            "object" != typeof a ? (b = a, a = {}) : b = a.index, b = ">" === b ? sd + 1 : "<" === b ? sd - 1 : "<<" === b ? 0 : ">>" === b ? ld - 1 : b, b = isNaN(b) ? J(b, kd, !0) : b, b = "undefined" == typeof b ? ue || 0 : b, Td.activeIndex = ue = V(b), ud = W(ue), vd = Z(ue), qd = [ue, ud, vd], sd = xd ? b : ue;
            var c = Math.abs(td - sd),
                d = u(a.time, function() {
                    return Math.min(Gd * (1 + (c - 1) / 12), 2 * Gd)
                }),
                f = a.overPos;
            a.slow && (d *= 10), Td.activeFrame = pd = kd[ue], bd(od, pd.i !== kd[x(rd)].i), jc(qd, "stage"), mc(Fc ? [sd] : [sd, W(sd), Z(sd)]), fd("go", !0), Wc("show", {
                user: a.user,
                time: d
            });
            var g = Td.show.onEnd = function(b) {
                g.ok || (g.ok = !0, ic(), Wb(qd, "stage"), b || Uc(!0), Wc("showend", {
                    user: a.user
                }), fd("go", !1), zc(), dd(), Zc(), $c())
            };
            if (Dd) {
                var i = pd[Rc],
                    j = ue !== td ? kd[td][Rc] : null;
                U(i, j, ce, {
                    time: d,
                    method: e.transition,
                    onEnd: g
                }, Ee)
            } else T(be, {
                pos: -p(sd, we.w, e.margin, rd),
                overPos: f,
                time: d,
                onEnd: g,
                _001: !0
            });
            if (wc(), yd) {
                Ic();
                var k = C(ue + h(sd - td, -1, 1));
                Gc({
                    time: d,
                    coo: k !== ue && a.coo,
                    guessIndex: "undefined" != typeof a.coo ? k : ue
                }), zd && Ec(d)
            }
            return Md = "undefined" != typeof td && td !== ue, td = ue, e.hash && Md && !Td.eq && F(pd.id || ue + 1), this
        }, Td.requestFullScreen = function() {
            return Bd && !Td.fullScreen && (Kd = xc.scrollTop(), Ld = xc.scrollLeft(), O(0, 0), fd("x", !0), Pd = d.extend({}, we), a.addClass(Pb).appendTo(sc.addClass(bb)), rc.addClass(bb), bd(od, !0, !0), Td.fullScreen = !0, Cd && kc.request(Wd), Td.resize(), Wb(qd, "stage"), ic(), Wc("fullscreenenter")), this
        }, Td.cancelFullScreen = function() {
            return Cd && kc.is() ? kc.cancel(b) : _c(), this
        }, b.addEventListener && b.addEventListener(kc.event, function() {
            !kd || kc.is() || od || _c()
        }, !1), Td.resize = function(a) {
            if (!kd) return this;
            Vc(Td.fullScreen ? {
                width: "100%",
                maxwidth: null,
                minwidth: null,
                height: "100%",
                maxheight: null,
                minheight: null
            } : P(a), Td.fullScreen);
            var b = arguments[1] || 0,
                c = arguments[2],
                d = we.width,
                f = we.height,
                g = we.ratio,
                i = xc.height() - (yd ? he.height() : 0);
            return o(d) && (_d.css({
                width: d,
                minWidth: we.minwidth,
                maxWidth: we.maxwidth
            }), d = we.W = we.w = _d.width(), e.glimpse && (we.w -= Math.round(2 * (n(e.glimpse) / 100 * d || m(e.glimpse) || 0))), be.css({
                width: we.w,
                marginLeft: (we.W - we.w) / 2
            }), f = n(f) / 100 * i || m(f), f = f || g && d / g, f && (d = Math.round(d), f = we.h = Math.round(h(f, n(we.minheight) / 100 * i || m(we.minheight), n(we.maxheight) / 100 * i || m(we.maxheight))), Uc(), ae.addClass(qb).stop().animate({
                width: d,
                height: f
            }, b, function() {
                ae.removeClass(qb)
            }), yd && (he.stop().animate({
                width: d
            }, b), Gc({
                guessIndex: ue,
                time: b,
                keep: !0
            }), zd && tc.nav && Ec(b)), Jd = c || !0, jd())), De = ae.offset().left, this
        }, Td.setOptions = function(a) {
            return d.extend(e, a), hd(), this
        }, Td.shuffle = function() {
            return kd && M(kd) && hd(), this
        }, Td.destroy = function() {
            return Td.cancelFullScreen(), Td.stopAutoplay(), kd = Td.data = null, i(), qd = [], Nc(Rc), this
        }, Td.playVideo = function() {
            var a = Td.activeFrame,
                b = a.video,
                c = ue;
            return "object" == typeof b && a.videoReady && (Cd && Td.fullScreen && Td.cancelFullScreen(), E(function() {
                return !kc.is() || c !== ue
            }, function() {
                c === ue && (a.$video = a.$video || d(d.Fotorama.jst.video(b)), a.$video.appendTo(a[Rc]), _d.addClass(fb), od = a.$video, r(), Wc("loadvideo"))
            })), this
        }, Td.stopVideo = function() {
            return bd(od, !0, !0), this
        }, ae.on("mousemove", dd), xe = X(be, {
            onStart: Xc,
            onMove: function(a, b) {
                ad(ae, b.edge)
            },
            onTouchEnd: Yc,
            onEnd: function(a) {
                ad(ae);
                var b = (Hc && !Rd || a.touch) && e.arrows;
                if (a.moved || b && a.pos !== a.newPos) {
                    var c = q(a.newPos, we.w, e.margin, rd);
                    Td.show({
                        index: c,
                        time: Dd ? Gd : a.time,
                        overPos: a.overPos,
                        user: !0
                    })
                } else a.aborted || ed(a.startEvent, b)
            },
            getPos: function() {
                return -p(sd, we.w, e.margin, rd)
            },
            _001: !0,
            timeLow: 1,
            timeHigh: 1,
            friction: 2,
            select: "." + Nb + ", ." + Nb + " *",
            $wrap: ae
        }), ze = X(ie, {
            onStart: Xc,
            onMove: function(a, b) {
                ad(he, b.edge)
            },
            onTouchEnd: Yc,
            onEnd: function(a) {
                function b() {
                    Gc.l = a.newPos, Zc(), $c(), nc(a.newPos, !0)
                }
                if (a.moved) a.pos !== a.newPos ? (T(ie, {
                    time: a.time,
                    pos: a.newPos,
                    overPos: a.overPos,
                    onEnd: b
                }), nc(a.newPos), Hd && ad(he, I(a.newPos, me.min, me.max))) : b();
                else {
                    var c = a.$target.closest("." + Cb, ie)[0];
                    c && gd.call(c, a.startEvent)
                }
            },
            timeLow: .5,
            timeHigh: 2,
            friction: 5,
            $wrap: he
        }), ye = Y(ae, {
            shift: !0,
            onEnd: function(a, b) {
                Xc(), Yc(), Td.show({
                    index: b,
                    slow: a.altKey
                })
            }
        }), Ae = Y(he, {
            onEnd: function(a, b) {
                Xc(), Yc();
                var c = t(ie) + .25 * b;
                ie.css(k(h(c, me.min, me.max))), Hd && ad(he, I(c, me.min, me.max)), Ae.prevent = {
                    "<": c >= me.max,
                    ">": c <= me.min
                }, clearTimeout(Ae.t), Ae.t = setTimeout(function() {
                    nc(c, !0)
                }, Jc), nc(c)
            }
        }), _d.hover(function() {
            setTimeout(function() {
                Qd || (Rd = !0, cd(!Rd))
            }, 0)
        }, function() {
            Rd && (Rd = !1, cd(!Rd))
        }), K(fe, function(a) {
            R(a), Td.show({
                index: fe.index(this) ? ">" : "<",
                slow: a.altKey,
                user: !0
            })
        }, {
            onStart: function() {
                Xc(), xe.control = !0
            },
            onTouchEnd: Yc,
            tail: xe
        }), d.each("load push pop shift unshift reverse sort splice".split(" "), function(a, b) {
            Td[b] = function() {
                return kd = kd || [], "load" !== b ? Array.prototype[b].apply(kd, arguments) : arguments[0] && "object" == typeof arguments[0] && arguments[0].length && (kd = N(arguments[0])), hd(), Td
            }
        }), hd()
    }, d.fn.fotorama = function(b) {
        return this.each(function() {
            var c = this,
                e = d(this),
                f = e.data(),
                g = f.fotorama;
            g ? g.setOptions(b) : E(function() {
                return !C(c)
            }, function() {
                f.urtext = e.html(), new d.Fotorama(e, d.extend({}, {
                    width: null,
                    minwidth: null,
                    maxwidth: "100%",
                    height: null,
                    minheight: null,
                    maxheight: null,
                    ratio: null,
                    margin: Nc,
                    glimpse: 0,
                    nav: "dots",
                    navposition: "bottom",
                    thumbwidth: Oc,
                    thumbheight: Oc,
                    thumbmargin: Nc,
                    thumbborderwidth: Nc,
                    allowfullscreen: !1,
                    fit: "contain",
                    transition: "slide",
                    transitionduration: Kc,
                    captions: !0,
                    hash: !1,
                    startindex: 0,
                    loop: !1,
                    autoplay: !1,
                    stopautoplayontouch: !0,
                    keyboard: !1,
                    arrows: !0,
                    click: !0,
                    swipe: !0,
                    trackpad: !0,
                    shuffle: !1,
                    direction: "ltr",
                    shadows: !0,
                    spinner: null
                }, a.fotoramaDefaults, b, f))
            })
        })
    }, d.Fotorama.instances = [], d.Fotorama.cache = {}, d.Fotorama.measures = {}, d = d || {}, d.Fotorama = d.Fotorama || {}, d.Fotorama.jst = d.Fotorama.jst || {}, d.Fotorama.jst.style = function(a) {
        var b, c = "";
        return ic.escape, c += ".fotorama" + (null == (b = a.s) ? "" : b) + " .fotorama__nav--thumbs .fotorama__nav__frame{\npadding:" + (null == (b = a.m) ? "" : b) + "px;\nheight:" + (null == (b = a.h) ? "" : b) + "px}\n.fotorama" + (null == (b = a.s) ? "" : b) + " .fotorama__thumb-border{\nheight:" + (null == (b = a.h - a.b * (a.q ? 0 : 2)) ? "" : b) + "px;\nborder-width:" + (null == (b = a.b) ? "" : b) + "px;\nmargin-top:" + (null == (b = a.m) ? "" : b) + "px}"
    }, d.Fotorama.jst.video = function(a) {
        function b() {
            c += d.call(arguments, "")
        }
        var c = "",
            d = (ic.escape, Array.prototype.join);
        return c += '<div class="fotorama__video"><iframe src="', b(("youtube" == a.type ? "http://youtube.com/embed/" + a.id + "?autoplay=1" : "vimeo" == a.type ? "http://player.vimeo.com/video/" + a.id + "?autoplay=1&badge=0" : a.id) + (a.s && "custom" != a.type ? "&" + a.s : "")), c += '" frameborder="0" allowfullscreen></iframe></div>'
    }, d(function() {
        d("." + ab + ':not([data-auto="false"])').fotorama()
    })
}(window, document, location, window.jQuery),
function() {
    var Tinycon = {},
        currentFavicon = null,
        originalFavicon = null,
        faviconImage = null,
        canvas = null,
        options = {},
        r = window.devicePixelRatio || 1,
        size = 16 * r,
        defaults = {
            width: 7,
            height: 9,
            font: 10 * r + "px arial",
            colour: "#ffffff",
            background: "#F03D25",
            fallback: !0,
            crossOrigin: !0,
            abbreviate: !0
        },
        ua = function() {
            var agent = navigator.userAgent.toLowerCase();
            return function(browser) {
                return -1 !== agent.indexOf(browser)
            }
        }(),
        browser = {
            ie: ua("msie"),
            chrome: ua("chrome"),
            webkit: ua("chrome") || ua("safari"),
            safari: ua("safari") && !ua("chrome"),
            mozilla: ua("mozilla") && !ua("chrome") && !ua("safari")
        },
        getFaviconTag = function() {
            for (var links = document.getElementsByTagName("link"), i = 0, len = links.length; len > i; i++)
                if ((links[i].getAttribute("rel") || "").match(/\bicon\b/)) return links[i];
            return !1
        },
        removeFaviconTag = function() {
            for (var links = document.getElementsByTagName("link"), head = document.getElementsByTagName("head")[0], i = 0, len = links.length; len > i; i++) {
                var exists = "undefined" != typeof links[i];
                exists && (links[i].getAttribute("rel") || "").match(/\bicon\b/) && head.removeChild(links[i])
            }
        },
        getCurrentFavicon = function() {
            if (!originalFavicon || !currentFavicon) {
                var tag = getFaviconTag();
                originalFavicon = currentFavicon = tag ? tag.getAttribute("href") : "/favicon.ico"
            }
            return currentFavicon
        },
        getCanvas = function() {
            return canvas || (canvas = document.createElement("canvas"), canvas.width = size, canvas.height = size), canvas
        },
        setFaviconTag = function(url) {
            removeFaviconTag();
            var link = document.createElement("link");
            link.type = "image/x-icon", link.rel = "icon", link.href = url, document.getElementsByTagName("head")[0].appendChild(link)
        },
        drawFavicon = function(label, colour) {
            if (!getCanvas().getContext || browser.ie || browser.safari || "force" === options.fallback) return updateTitle(label);
            var context = getCanvas().getContext("2d"),
                colour = colour || "#000000",
                src = getCurrentFavicon();
            faviconImage = document.createElement("img"), faviconImage.onload = function() {
                context.clearRect(0, 0, size, size), context.drawImage(faviconImage, 0, 0, faviconImage.width, faviconImage.height, 0, 0, size, size), (label + "").length > 0 && drawBubble(context, label, colour), refreshFavicon()
            }, !src.match(/^data/) && options.crossOrigin && (faviconImage.crossOrigin = "anonymous"), faviconImage.src = src
        },
        updateTitle = function(label) {
            if (options.fallback) {
                var originalTitle = document.title;
                "(" === originalTitle[0] && (originalTitle = originalTitle.slice(originalTitle.indexOf(" "))), document.title = (label + "").length > 0 ? "(" + label + ") " + originalTitle : originalTitle
            }
        },
        drawBubble = function(context, label, colour) {
            "number" == typeof label && label > 99 && options.abbreviate && (label = abbreviateNumber(label));
            var len = (label + "").length - 1,
                width = options.width * r + 6 * r * len,
                height = options.height * r,
                top = size - height,
                left = size - width - r,
                bottom = 16 * r,
                right = 16 * r,
                radius = 2 * r;
            context.font = (browser.webkit ? "bold " : "") + options.font, context.fillStyle = options.background, context.strokeStyle = options.background, context.lineWidth = r, context.beginPath(), context.moveTo(left + radius, top), context.quadraticCurveTo(left, top, left, top + radius), context.lineTo(left, bottom - radius), context.quadraticCurveTo(left, bottom, left + radius, bottom), context.lineTo(right - radius, bottom), context.quadraticCurveTo(right, bottom, right, bottom - radius), context.lineTo(right, top + radius), context.quadraticCurveTo(right, top, right - radius, top), context.closePath(), context.fill(), context.beginPath(), context.strokeStyle = "rgba(0,0,0,0.3)", context.moveTo(left + radius / 2, bottom), context.lineTo(right - radius / 2, bottom), context.stroke(), context.fillStyle = options.colour, context.textAlign = "right", context.textBaseline = "top", context.fillText(label, 2 === r ? 29 : 15, browser.mozilla ? 7 * r : 6 * r)
        },
        refreshFavicon = function() {
            getCanvas().getContext && setFaviconTag(getCanvas().toDataURL())
        },
        abbreviateNumber = function(label) {
            for (var metricPrefixes = [
                    ["G", 1e9],
                    ["M", 1e6],
                    ["k", 1e3]
                ], i = 0; i < metricPrefixes.length; ++i)
                if (label >= metricPrefixes[i][1]) {
                    label = round(label / metricPrefixes[i][1]) + metricPrefixes[i][0];
                    break
                }
            return label
        },
        round = function(value, precision) {
            var number = new Number(value);
            return number.toFixed(precision)
        };
    Tinycon.setOptions = function(custom) {
        options = {};
        for (var key in defaults) options[key] = custom.hasOwnProperty(key) ? custom[key] : defaults[key];
        return this
    }, Tinycon.setImage = function(url) {
        return currentFavicon = url, refreshFavicon(), this
    }, Tinycon.setBubble = function(label, colour) {
        return label = label || "", drawFavicon(label, colour), this
    }, Tinycon.reset = function() {
        setFaviconTag(originalFavicon)
    }, Tinycon.setOptions(defaults), window.Tinycon = Tinycon, "function" == typeof define && define.amd && define(Tinycon)
}();