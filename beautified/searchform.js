/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    function SearchFormCtrl($element, $injector, $location, $scope, $timeout, DialogTutorialService, SearchFormService, ToasterService) {
        var searchParamsObj = $scope.searchTrackingPage ? $scope.SearchTrackingParams : SearchFormService.getSearchParams($scope.$root.Self);
        $scope.displayError = {}, $scope.btnDisabled = !1, $scope.isCollapsed = !0, $scope.users = [], $scope.query = searchParamsObj.query ? searchParamsObj.query : "", $scope.ageFrom = "undefined" != typeof searchParamsObj.ageFrom ? searchParamsObj.ageFrom : AgeRange.from, $scope.ageTo = "undefined" != typeof searchParamsObj.ageTo ? searchParamsObj.ageTo : AgeRange.to, $scope.gender = 0 != searchParamsObj.gender ? searchParamsObj.gender : 3, $scope.genderMale = 2 != searchParamsObj.gender, $scope.genderFemale = 2 == searchParamsObj.gender || 3 == searchParamsObj.gender, $scope.genderLooking = 0 != searchParamsObj.genderLooking ? searchParamsObj.genderLooking : 3, $scope.genderLookingMale = 1 == searchParamsObj.genderLooking || 3 == searchParamsObj.genderLooking, $scope.genderLookingFemale = 1 != searchParamsObj.genderLooking, $scope.radiusTo = "undefined" != typeof searchParamsObj.radiusTo ? -1 == searchParamsObj.radiusTo ? 500 : searchParamsObj.radiusTo : 100;
        var selfGeo = SearchFormService.chooseUserLocation($scope.$root.Self);
        $scope.city = searchParamsObj.city ? searchParamsObj.city : selfGeo.city, $scope.latitude = searchParamsObj.latitude ? searchParamsObj.latitude : selfGeo.latitude, $scope.longitude = searchParamsObj.longitude ? searchParamsObj.longitude : selfGeo.longitude, $scope.country = searchParamsObj.country ? searchParamsObj.country : selfGeo.country, $scope.online = "undefined" != typeof searchParamsObj.isOnline ? searchParamsObj.isOnline : !0, $scope["new"] = "undefined" != typeof searchParamsObj["new"] ? searchParamsObj["new"] : !1, $scope.pic = "undefined" != typeof searchParamsObj["userQuality[0]"] ? searchParamsObj["userQuality[0]"] : "pic", $scope.ver = "undefined" != typeof searchParamsObj["userQuality[1]"] ? searchParamsObj["userQuality[1]"] : null, searchParamsObj.flirtInterests ? ($scope.intention_date = $.inArray("date", searchParamsObj.flirtInterests) > -1 ? "date" : null, $scope.intention_frie = $.inArray("frie", searchParamsObj.flirtInterests) > -1 ? "frie" : null, $scope.intention_chat = $.inArray("chat", searchParamsObj.flirtInterests) > -1 ? "chat" : null) : _.each($scope.$root.Self.flirtInterests, function(val) {
            $scope["intention_" + val] = !0
        });
        var searchParams = {},
            userListNavigation = $injector.get("UserListService").navigation;
        userListNavigation.cached_user = [], userListNavigation.loadNextUsers = function() {
            SearchFormService.load(searchParams, {
                inCache: !0,
                service: userListNavigation
            })
        };
        var load = function(reset, scrollDown) {
            if ("undefined" == typeof scrollDown || scrollDown) {
                var query = SearchFormService.load(searchParams, {
                    inCache: !1,
                    service: userListNavigation
                });
                $scope.isLoading = !0, "undefined" == typeof query.then ? $scope.isLoading = !1 : query.then(function() {
                    $scope.isLoading = !1
                })
            } else SearchFormService.restore()
        };
        $scope.reloadList = function(direction) {
            $scope.isLoading || load(!1, direction)
        }, $scope.submitSearchForm = function(reset) {
            return "undefined" != typeof reset && reset === !0 && resetQuery(), SearchFormService.reset(), searchParams = $scope.query ? {
                query: $scope.query
            } : {
                gender: $scope.gender,
                genderLooking: $scope.genderLooking,
                ageFrom: $scope.ageFrom,
                ageTo: $scope.ageTo,
                latitude: $scope.latitude,
                city: $scope.city,
                longitude: $scope.longitude,
                countryCode: $scope.country
            }, 1 == $scope.online && (searchParams.isOnline = $scope.online), 1 == $scope["new"] && (searchParams["new"] = $scope["new"]), $scope.pic && (searchParams["userQuality[0]"] = $scope.pic), $scope.ver && (searchParams["userQuality[1]"] = $scope.ver), $scope.intention_date && (searchParams["interests[0]"] = $scope.intention_date), $scope.intention_frie && (searchParams["interests[1]"] = $scope.intention_frie), $scope.intention_chat && (searchParams["interests[2]"] = $scope.intention_chat), $scope.radiusTo > 0 && $scope.radiusTo < 500 && (searchParams.radiusTo = $scope.radiusTo), $scope.noQuery || load(!0), $scope.$watchCollection(function() {
                return SearchFormService.get()
            }, function(newData) {
                newData.length ? angular.extend($scope, {
                    users: newData
                }) : angular.extend($scope, {
                    users: {}
                })
            }), !1
        }, $scope.resetQueryAndSubmit = function(field, val) {
            ("gender" === field || "genderLooking" === field) && (val = SearchFormService.getGenderFromCheckboxes($scope, field, val)), valHasChanged(field, val) && (resetQuery(), $scope[field] = val, $scope.submitSearchForm())
        }, $scope.setLocation = function(location) {
            $scope.latitude = location.coordinates.latitude, $scope.longitude = location.coordinates.longitude, $scope.city = location.city, $scope.country = location.country, $scope.submitQuery()
        }, $scope.submitQuery = function() {
            return $scope.searchform.query.$valid || "" === $scope.query ? $scope.submitSearchForm() : void 0
        };
        var beginMovingAgeFrom = $scope.ageFrom,
            beginMovingAgeTo = $scope.ageTo;
        $scope.endMoving = function() {
            $scope.ageFrom == $scope.AgeRange.max && $scope.ageTo == $scope.AgeRange.max && ($scope.ageFrom = beginMovingAgeFrom), $scope.ageFrom == $scope.AgeRange.min && $scope.ageTo == $scope.AgeRange.min && ($scope.ageTo = beginMovingAgeTo), beginMovingAgeTo = $scope.ageTo, beginMovingAgeFrom = $scope.ageFrom, resetQuery(), $scope.submitSearchForm()
        }, $scope.addYearsToString = function(value) {
            return void 0 === value ? "" : (value = value.toString(), Translator.get("dynamic.age_years", {
                years: value
            }, value))
        }, $scope.addDistanceToString = function(value) {
            return void 0 === value ? "" : value >= 500 ? Translator.get("equal") : value + " km"
        };
        var valHasChanged = function(field, val) {
            return "undefined" == typeof $scope[field] ? !0 : $scope[field] !== val
        };
        $scope.submitSearchForm(), $scope.handleVerifySelection = function() {
            if ($scope.$root.Self.verified === !0 || 1 === $scope.$root.Self.verified || 4 === $scope.$root.Self.verified) $scope.submitSearchForm(!0);
            else if (2 === $scope.$root.Self.verified) $scope.ver = null, ToasterService.open("error", Translator.get("not_verified"), Translator.get("verify_demand_text_progress"));
            else {
                $scope.ver = null;
                var dialog = $injector.get("DialogService"),
                    modal = dialog.openMessage(Translator.get("not_verified"), Translator.get("verify_demand_text"), {
                        buttons: [{
                            label: Translator.get("verify_now"),
                            cssClass: "btn btn-info",
                            click: function() {
                                $location.path($scope.$root.Ajax.url + "/verify"), modal.close()
                            }
                        }]
                    })
            }
        };
        var resetQuery = function() {
            userListNavigation(), $scope.query = ""
        };
        $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, DialogTutorialService.loadTutorialIfEnabled("tutorial_filter"), $scope.toggleCollapsed = function() {
            $scope.isCollapsed = !$scope.isCollapsed, setSearchResultsPadding()
        };
        var setSearchResultsPadding = function() {
            var paddingTop = 132 + ($scope.isCollapsed ? 0 : 173) + 22;
            $($element).siblings(".search-results").transition({
                paddingTop: paddingTop
            }, 350, "ease")
        };
        $timeout(function() {
            setSearchResultsPadding()
        })
    }

    function SearchTrackingCtrl($scope) {
        $scope.searchTrackingPage = !0, "undefined" != typeof SearchTracking && ($scope.searchTitle = SearchTracking.phrase, $scope.SearchTrackingParams = {
            ageFrom: SearchTracking.params.ageFrom,
            ageTo: SearchTracking.params.ageTo,
            gender: SearchTracking.params.gender,
            genderLooking: SearchTracking.params.genderLooking,
            city: SearchTracking.params.city,
            country: SearchTracking.params.countryCode,
            latitude: SearchTracking.params.latitude,
            longitude: SearchTracking.params.longitude
        })
    }

    function searchForm($window, $document) {
        return {
            controller: "SearchFormCtrl",
            restrict: "E",
            transclude: !1,
            replace: !1,
            templateUrl: "/template/form/searchform.html",
            link: function(scope, elem) {
                scope.isPanelFixed = !1;
                var scrollDocument = function() {
                    var isPanelFixed = $($window).scrollTop() - $(".eyecatcher").outerHeight() > 0;
                    scope.isPanelFixed !== isPanelFixed && (scope.isPanelFixed = isPanelFixed)
                };
                $document.scroll(scope.$apply.bind(scope, scrollDocument)), $document.ready(scrollDocument), elem.bind("$destroy", function() {
                    $(document).unbind("scroll", scrollDocument)
                })
            }
        }
    }

    function SearchFormFctry($resource) {
        return {
            doRequest: $resource(Ajax.api + "/users", {
                cache: !0
            }, {
                get: {
                    method: "GET",
                    params: {
                        query: "@query",
                        gender: "@gender",
                        genderLooking: "@genderLooking",
                        ageFrom: "@ageFrom",
                        ageTo: "@ageTo",
                        "new": "@new",
                        isOnline: "@isOnline",
                        latitude: "@latitude",
                        longitude: "@longitude",
                        countryCode: "@countryCode",
                        radiusTo: "@radiusTo",
                        resultLimit: "@resultLimit",
                        resultPage: "@resultPage"
                    },
                    headers: {
                        "public": !0
                    }
                }
            })
        }
    }

    function SearchFormService(SearchFormFctry, StorageService, storageKey, $rootScope, $collection) {
        var that = this,
            isGuest = !$rootScope.Self || !$rootScope.Self.id,
            storageUserId = isGuest ? "guest" : $rootScope.Self.id,
            users = $collection.getInstance({
                idAttribute: "id"
            }),
            reachedEnd = !1,
            paging = {
                resultLimit: $rootScope.resultLimit,
                resultPage: 1
            };
        this.get = function() {
            return users.all()
        }, this.has = function() {
            return 0 !== users.size()
        }, this.reset = function() {
            users = $collection.getInstance({
                idAttribute: "id"
            }), paging.resultPage = 1, reachedEnd = !1
        }, this.load = function(params, options) {
            if (options = options || {}, reachedEnd === !0) return {};
            var sendParams = {};
            return _.each(params, function(val, key) {
                "city" !== key && (sendParams[key] = val)
            }), angular.extend(sendParams, paging), SearchFormFctry.doRequest.get(sendParams).$promise.then(function(data) {
                return 0 == data.response.result.length ? void(reachedEnd = !0) : (options.inCache || users.addAll(data.response.result, {
                    prepend: !1
                }), options.service.cached_user = data.response.result, reachedEnd = !1, ++paging.resultPage, void(isGuest || params.query || params.resultPage && !(params.resultPage < 2) || that.setSearchParams(params)))
            })
        }, this.getSearchParams = function(user) {
            var oldParams = StorageService.get("search.params");
            if (oldParams) return that.removeSearchParams(), oldParams;
            var params = StorageService.get(storageKey + "." + storageUserId);
            if (params) return params;
            var defaults = angular.copy(SearchSettings);
            return user.length && (defaults.flirtInterests = user.flirtInterests), defaults
        }, this.chooseUserLocation = function(user) {
            var userGeo = {
                city: "",
                latitude: "",
                longitude: "",
                country: ""
            };
            if (void 0 !== user.location && void 0 !== user.coord && void 0 !== user.country) userGeo = {
                city: user.location,
                latitude: user.coord.latitude || "",
                longitude: user.coord.longitude || "",
                country: user.country
            };
            else if (void 0 !== user.city && void 0 !== user.homeLocation) {
                var coords = void 0 !== user.homeLocation.coordinates ? user.homeLocation.coordinates : user.homeLocation;
                userGeo = {
                    city: user.city,
                    latitude: coords.latitude || "",
                    longitude: coords.longitude || "",
                    country: user.homeLocation.country
                }
            }
            return userGeo
        }, this.setSearchParams = function(params) {
            var newParams = angular.extend({
                flirtInterests: []
            }, params);
            newParams.isOnline = void 0 !== params.isOnline ? params.isOnline : !1, newParams["userQuality[0]"] = void 0 !== params["userQuality[0]"] ? params["userQuality[0]"] : !1, _.each(newParams, function(value, key) {
                -1 != key.indexOf("interests[") && (newParams.flirtInterests.push(value), delete newParams[key])
            }), "undefined" == typeof newParams.radiusTo && (newParams.radiusTo = -1), StorageService.set(storageKey + "." + storageUserId, newParams)
        }, this.removeSearchParams = function() {
            StorageService.remove("search.params"), StorageService.remove(storageKey + "." + storageUserId)
        }, this.getGenderFromCheckboxes = function($scope, key, val) {
            return $scope[key + "Male"] ? $scope[key + "Female"] ? 3 : 1 : $scope[key + "Female"] ? 2 : ($scope[key + "Male"] = 2 == val, $scope[key + "Female"] = 1 == val, 1 == val ? 2 : 1)
        }
    }
    angular.module("Lovoo.Form.Search", ["ngResource", "Storage", "uiSlider"]).controller("SearchFormCtrl", SearchFormCtrl).controller("SearchTrackingCtrl", SearchTrackingCtrl), SearchFormCtrl.$inject = ["$element", "$injector", "$location", "$scope", "$timeout", "DialogTutorialService", "SearchFormService", "ToasterService"], SearchTrackingCtrl.$inject = ["$scope"], angular.module("Lovoo.Form.Search").directive("searchform", searchForm), searchForm.$inject = ["$window", "$document"], angular.module("Lovoo.Form.Search").factory("SearchFormFctry", SearchFormFctry).service("SearchFormService", SearchFormService).constant("storageKey", "search.params"), SearchFormFctry.$inject = ["$resource"], SearchFormService.$inject = ["SearchFormFctry", "StorageService", "storageKey", "$rootScope", "$collection"]
});