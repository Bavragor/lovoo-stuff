/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    ! function() {
        "use strict";

        function eyecatcherCtrl($scope, $timeout, $window, EyecatcherActionService, RegisterService, UserActionsFctry) {
            function initializeWindowSize() {
                $scope.limit = $window.innerWidth > 1335 ? 10 : $window.innerWidth > 1230 ? 9 : 8, $scope.users.splice($scope.limit, $scope.users.length - $scope.limit)
            }
            var firstRender = !0,
                visibleWindow = !0;
            $scope.users = [], $scope.limit = 10, $scope.userImgSrc = !1, $scope.animateEyecatcher = !1, $($window).blur(function() {
                visibleWindow = !1
            }), $($window).focus(function() {
                visibleWindow = !0
            }), initializeWindowSize(), $($window).bind("resize", function() {
                initializeWindowSize()
            }), $scope.$watch(function() {
                return EyecatcherActionService.getUsers()
            }, function(newUsers) {
                var tempUsers = [];
                for (var i in newUsers) {
                    var isUserInside = !1;
                    for (var j in $scope.users)
                        if ($scope.users[j].id == newUsers[i].id) {
                            isUserInside = !0;
                            break
                        }
                    if (isUserInside) break;
                    tempUsers.push(newUsers[i])
                }
                firstRender && tempUsers.length ? (firstRender = !1, $scope.animateEyecatcher = !1) : $scope.animateEyecatcher = visibleWindow, $timeout(function() {
                    for (var t = tempUsers.length; t > 0; t--) $scope.users.unshift(tempUsers[t - 1]), $scope.users = $scope.users.slice(0, $scope.limit)
                }, 1)
            }, !0), $scope.$watch(function() {
                return void 0 !== $scope.$root.Self && void 0 !== $scope.$root.Self.id ? $scope.$root.Self.isFlirtstar : !1
            }, function(data) {
                1 == data && ($scope.userImgSrc = $scope.$root.Self.picture)
            }, !0), $scope.openProfileModal = function(user) {
                UserActionsFctry.action(user, "profile")
            }, $scope.addToEyecatcher = function() {
                $scope.$root.isSecurityUser() ? EyecatcherActionService.addToEyecatcher() : RegisterService.showRegisterDialog()
            }
        }
        angular.module("Lovoo.Eyecatcher", ["ngCollection", "Lovoo.User"]).controller("EyecatcherCtrl", eyecatcherCtrl), eyecatcherCtrl.$inject = ["$scope", "$timeout", "$window", "EyecatcherActionService", "RegisterService", "UserActionsFctry"]
    }(),
    function() {
        "use strict";

        function eyecatcher() {
            return {
                restrict: "E",
                transclude: !1,
                replace: !0,
                templateUrl: "/template/eyecatcher/eyecatcher.html",
                controller: "EyecatcherCtrl"
            }
        }

        function userPreview() {
            return {
                restrict: "A",
                replace: !1,
                link: function(scope, ele) {
                    ele.tooltip({
                        placement: "bottom",
                        html: !0,
                        title: '<i class="lo lo-' + (scope.user.isOnline ? "desktop" : "mobile") + ' text-green"></i> ' + minEmoji(scope.user.name) + ", " + scope.user.age + "<br />" + (scope.user.location ? scope.user.location : scope.user.city),
                        template: '<div class="tooltip tooltip-white"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
                    }), ele.click(function() {
                        $(this).tooltip("hide")
                    })
                }
            }
        }
        angular.module("Lovoo.Eyecatcher").constant("EYECATCHER_SETTINGS", {
            type: "top_settings",
            limit: 10,
            reverse: !1,
            page: 1
        }).directive("eyecatcher", eyecatcher).directive("userPreview", userPreview)
    }(),
    function() {
        "use strict";

        function EyecatcherQueryFctry(EYECATCHER_SETTINGS, UserQueryFctry) {
            return {
                get: function(limit, page) {
                    return UserQueryFctry.eyecatcher.get({
                        type: EYECATCHER_SETTINGS.type,
                        resultLimit: limit ? limit : EYECATCHER_SETTINGS.limit,
                        resultPage: page ? page : EYECATCHER_SETTINGS.page
                    })
                },
                set: function(userId) {
                    return UserQueryFctry.eyecatcher.add({
                        userId: userId
                    })
                },
                infoAdd: function(id) {
                    return UserQueryFctry.eyecatcher.add({
                        userId: id
                    })
                },
                buyAdd: function() {
                    return UserQueryFctry.eyecatcher.put()
                }
            }
        }

        function EyecatcherActionService(EYECATCHER_SETTINGS, $collection, $location, $rootScope, $timeout, DialogService, DialogTutorialService, EyecatcherQueryFctry) {
            function request() {
                EyecatcherQueryFctry.get(EYECATCHER_SETTINGS.limit).$promise.then(function(data) {
                    users.removeAll(), users.addAll(data.response.result, EYECATCHER_SETTINGS), $timeout(request, 45e3)
                })
            }
            var users = $collection.getInstance({
                idAttribute: "id"
            });
            this.getUsers = function() {
                return users.all()
            }, this.addToEyecatcher = function() {
                return DialogTutorialService.isTutorialAndEvenTypeEnabled("tutorial_eyecatcher") ? DialogTutorialService.getDialog({
                    type: "tutorial_eyecatcher"
                }) : void EyecatcherQueryFctry.infoAdd($rootScope.Self.id).$promise.then(function(data) {
                    DialogService.openDialog({}, {
                        title: Translator.get("txt.flirtstar_become"),
                        content: Translator.get("dynamic.flirtstar_info", {
                            credits: data.response.credits
                        }),
                        closeLabel: Translator.get("no_thanks"),
                        callbackLabel: Translator.get("txt.flirtstar_become"),
                        callback: function() {
                            EyecatcherQueryFctry.buyAdd().$promise.then(function() {
                                $rootScope.Self.isFlirtstar = !0;
                                var modal = DialogService.openMessage(Translator.get("flirtstar_info"), Translator.get("flirtstar_info_redirect"), {
                                    buttons: [{
                                        label: Translator.get("txt.history_all"),
                                        cssClass: "btn btn-success",
                                        click: function() {
                                            $location.path($rootScope.Ajax.url + "/eyecatcher"), modal.close()
                                        }
                                    }]
                                })
                            })
                        }
                    })
                })
            }, request()
        }
        angular.module("Lovoo.Eyecatcher").factory("EyecatcherQueryFctry", EyecatcherQueryFctry).service("EyecatcherActionService", EyecatcherActionService), EyecatcherQueryFctry.$inject = ["EYECATCHER_SETTINGS", "UserQueryFctry"], EyecatcherActionService.$inject = ["EYECATCHER_SETTINGS", "$collection", "$location", "$rootScope", "$timeout", "DialogService", "DialogTutorialService", "EyecatcherQueryFctry"]
    }()
});