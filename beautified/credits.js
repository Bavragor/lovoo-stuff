/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "packages"], function(angular, packages) {
    ! function(angular) {
        "use strict";

        function CreditsCtrl($routeParams, $scope) {
            $scope.links = [{
                page: "buy",
                url: $scope.$root.Ajax.url + "/credits",
                title: Translator.get("credits_packages"),
                icon: "lo lo-hand-credit"
            }, {
                page: "history",
                url: $scope.$root.Ajax.url + "/credits/history",
                title: Translator.get("credits_overview"),
                icon: "lo lo-balance"
            }], $scope.$root.activePage = $routeParams.page || "buy"
        }

        function CreditsPackagesCtrl($scope, DialogTutorialService) {
            $scope.showPackageDetails = !0, $scope.packages = [], DialogTutorialService.loadTutorialIfEnabled("tutorial_credits")
        }

        function CreditsFreeCtrl($scope, CreditsFreeService) {
            $scope.$watch(function() {
                return CreditsFreeService.list()
            }, function(data) {
                data && ($.each(data, function(key, obj) {
                    var tmp = {};
                    switch (obj.last = obj.last || 0, obj.type) {
                        case "supersonicads":
                            tmp = {
                                noCheck: !0,
                                bgColor: "bg-green",
                                icon: "lo lo-up-2 lo-credit"
                            };
                            break;
                        case "sponsorpay":
                            tmp = {
                                noCheck: !0,
                                bgColor: "bg-green",
                                icon: "lo lo-up-2 lo-credit"
                            };
                            break;
                        case "trialpay":
                            tmp = {
                                noCheck: !0,
                                bgColor: "bg-green",
                                icon: "lo lo-up-2 lo-credit"
                            };
                            break;
                        case "vipweek":
                            obj.last = 0 | $scope.$root.Self.vip, tmp = {
                                btnText: Translator.get(obj.last ? "vip_member" : "become_vip"),
                                btnColor: "btn-warning",
                                icon: "lo lo-up-2 lo-poker",
                                iconColor: "text-orange"
                            };
                            break;
                        case "dailylogin":
                            tmp = {
                                icon: "fa lo-up-3  fa-sign-in",
                                iconColor: "text-blue"
                            };
                            break;
                        case "confirmmail":
                            tmp = {
                                icon: "lo lo-at lo-up-3",
                                iconColor: "text-blue",
                                hide: !!obj.last
                            };
                            break;
                        case "facebookconnect":
                            tmp = {
                                icon: "fa fa-facebook lo-up-1",
                                iconColor: "text-facebook",
                                hide: !!obj.last
                            };
                            break;
                        case "verify":
                            tmp = {
                                icon: "lo lo-up-2 lo-user",
                                iconColor: "text-green",
                                hide: !!obj.last
                            }
                    }
                    tmp.btnText || (tmp.btnText = Translator.get("get_credits")), tmp.btnColor || (tmp.btnColor = "btn-success"), angular.extend(data[key], tmp)
                }), $scope.freecredits = data)
            }), $scope.action = CreditsFreeService.action
        }
        angular.module("Lovoo.Credits", ["ngResource", "Lovoo.Packages"]).controller("CreditsCtrl", CreditsCtrl).controller("CreditsPackagesCtrl", CreditsPackagesCtrl).controller("CreditsFreeCtrl", CreditsFreeCtrl), CreditsCtrl.$inject = ["$routeParams", "$scope"], CreditsPackagesCtrl.$inject = ["$scope", "DialogTutorialService"], CreditsFreeCtrl.$inject = ["$scope", "CreditsFreeService"]
    }(angular),
    function(angular) {
        "use strict";

        function FreeCreditsDirective() {
            return {
                restrict: "E",
                replace: !1,
                transclude: !1,
                templateUrl: "/template/credits/freecredits.html",
                controller: "CreditsFreeCtrl"
            }
        }
        angular.module("Lovoo.Credits").directive("freecredits", FreeCreditsDirective)
    }(angular),
    function(angular, dataLayer) {
        "use strict";

        function CreditsFactory($resource) {
            return {
                free: $resource(Ajax.api + "/self/freecredits", {
                    cache: !0
                }),
                confirm: $resource(Ajax.api + "/self/confirmmail", {
                    cache: !0
                })
            }
        }

        function CreditsFreeService($location, $rootScope, CreditsFactory, DialogService, DialogTutorialService, FacebookService, SettingsService) {
            function encodeSupersonicadsAge(age) {
                return !age || void 0 === age || 13 > age ? 0 : 17 >= age ? 1 : 20 >= age ? 2 : 24 >= age ? 3 : 34 >= age ? 4 : 44 >= age ? 5 : 54 >= age ? 6 : 64 >= age ? 7 : 8
            }
            var freeCreditList;
            CreditsFactory.free.get().$promise.then(function(data) {
                freeCreditList = data.response.freecredits
            }), this.list = function() {
                return freeCreditList
            }, this.action = function(type) {
                switch (type) {
                    case "sponsorpay":
                        return $rootScope.AdsEnabled ? (dataLayer.push({
                            event: "sendEvent",
                            category: "Free Credits",
                            label: "SponsorPay"
                        }), DialogService.openMessage("Sponsorpay", $location.$$protocol + "://iframe.sponsorpay.com/?appid=10505&uid=" + $rootScope.Self.id, {
                            style: "modal-xlg",
                            width: 845
                        })) : DialogTutorialService.openAdblockHint();
                    case "supersonicads":
                        return $rootScope.AdsEnabled ? (dataLayer.push({
                            event: "sendEvent",
                            category: "Free Credits",
                            label: "SupersonicAds"
                        }), DialogService.openMessage("SupersonicAds", $location.$$protocol + "://www.supersonicads.com/delivery/panel.php?applicationKey=2e22a619&applicationUserId=" + $rootScope.Self.id + "&applicationUserAgeGroup=" + encodeSupersonicadsAge($rootScope.Self.age) + "&applicationUserGender=" + $rootScope.Genders[$rootScope.Self.gender].gender, {
                            style: "modal-xlg",
                            width: 845
                        })) : DialogTutorialService.openAdblockHint();
                    case "trialpay":
                        return $rootScope.AdsEnabled ? (dataLayer.push({
                            event: "sendEvent",
                            category: "Free Credits",
                            label: "Trialpay"
                        }), DialogService.openMessage("Trialpay", $location.$$protocol + "://www.trialpay.com/dispatch/5af2e871b44031c5e1bbee1e0b123d24?sid=" + $rootScope.Self.id, {
                            style: "modal-xlg",
                            width: 845
                        })) : DialogTutorialService.openAdblockHint();
                    case "tapjoy":
                        break;
                    case "vipweek":
                        $location.path($rootScope.Ajax.url + "/vip");
                        break;
                    case "dailylogin":
                        break;
                    case "confirmmail":
                        SettingsService.sendConfirmMail();
                        break;
                    case "facebookconnect":
                        FacebookService.connectAccountToLovoo();
                        break;
                    case "facebookfriendinvite":
                        break;
                    case "verify":
                        $location.path($rootScope.Ajax.url + "/verify");
                        break;
                    default:
                        $location.path($rootScope.Ajax.url + type)
                }
            }
        }
        angular.module("Lovoo.Credits").factory("CreditsFactory", CreditsFactory).service("CreditsFreeService", CreditsFreeService), CreditsFactory.$inject = ["$resource"], CreditsFreeService.$inject = ["$location", "$rootScope", "CreditsFactory", "DialogService", "DialogTutorialService", "FacebookService", "SettingsService"]
    }(angular, window.dataLayer)
});