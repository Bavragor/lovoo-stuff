/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "angularAnimate", "angularCookies", "angularRoute", "angularResource", "angularSanitize", "chat", "googleplus", "facebook", "profile", "searchform", "self"], function(angular, angularAnimate, angularCookies, angularRoute, angularResource, angularSanitize, chat, googleplus, facebook, profile, searchform, self) {
    var Ajax = {
        url:          "https://www.lovoo.com",
        api:          "https://www.lovoo.com/api_web.php",
        imgUrl:       "https://img.lovoo.com",
        imgUrlStatic: "/bundles/website/images/"
    };

    function runDialogModule($templateCache) {
        renderDialogTemplates($templateCache)
    }

    function DialogService($injector, $modal, $rootScope, RoutingService) {
        var globalDefaults = {
                backdrop: "static",
                keyboard: !0,
                backdropClick: !0,
                dialogFade: !0,
                templateUrl: "dialog/advanced-message.html"
            },
            globalOptions = {
                title: Translator.get("LOVOO"),
                content: Translator.get("lovoo_slogan"),
                closeLabel: Translator.get("Close"),
                cancelLabel: Translator.get("cancel"),
                callbackLabel: Translator.get("ok")
            };
        this.openDialog = function(customDefaults, customOptions) {
            function modalController($scope, $modalInstance) {
                $scope.dialogOptions = tempOptions, $scope.dialogOptions.close = function(result) {
                    $modalInstance.dismiss(result)
                }, $scope.dialogOptions.callback = function(data) {
                    $modalInstance.close(), customOptions.callback(data)
                }
            }
            var tempDefaults = {},
                tempOptions = {};
            if (angular.extend(tempDefaults, globalDefaults, customDefaults), angular.extend(tempOptions, globalOptions, customOptions), tempDefaults.controller) {
                var tempScope = $rootScope.$new();
                angular.extend(tempScope, tempOptions), tempDefaults.scope = tempScope
            } else tempDefaults.controller = modalController;
            return $modal.open(tempDefaults)
        }, this.openMessage = function(title, message, options) {
            function isUrl(s) {
                var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                return regexp.test(s)
            }

            function MessageCtrl($scope, $modalInstance, $sce, $timeout) {
                var defaultButtons = [{
                    label: Translator.get("Close"),
                    cssClass: "btn btn-link-close",
                    click: function() {
                        $modalInstance.close()
                    }
                }];
                if (tempOptions.buttons && tempOptions.defaultBtn ? tempOptions.buttons.unshift(defaultButtons[0]) : !tempOptions.buttons && tempOptions.defaultBtn && (tempOptions.buttons = defaultButtons), tempOptions.id && ($scope.modalId = tempOptions.id), $scope.model = {
                        headerText: title,
                        bodyText: message,
                        buttons: tempOptions.buttons,
                        close: function() {
                            $modalInstance.close()
                        }
                    }, tempOptions.close && $timeout(function() {
                        $modalInstance.close()
                    }, tempOptions.duration), isUrl(message)) {
                    var url = $sce.trustAsResourceUrl(message);
                    angular.extend($scope.model, {
                        iframe: {
                            url: url,
                            width: tempOptions.width,
                            height: tempOptions.height
                        },
                        style: tempOptions.style,
                        bodyText: tempOptions.bodyText || ""
                    })
                }
            }
            var isOpened, dialogOptions, tempOptions = {
                close: !1,
                duration: 2e3,
                height: "700",
                width: "720",
                style: {
                    width: "762px"
                },
                defaultBtn: !0
            };
            return angular.extend(tempOptions, options || {}), tempOptions.id && $("#" + tempOptions.id + ".modal-dialog").length && (isOpened = !0), dialogOptions = {
                backdrop: tempOptions.backdrop || !0,
                dialogFade: !0,
                controller: MessageCtrl,
                windowClass: tempOptions.style.length ? tempOptions.style.split(" ") : "",
                templateUrl: "dialog/simple-message.html"
            }, isOpened ? $modal : $modal.open(dialogOptions)
        }, this.showProfileDialog = function(userScope, additionalOptions) {
            function profileModalController($scope, $modalInstance, selfLocation) {
                var currentJson = angular.toJson(userScope),
                    current = JSON.parse(currentJson),
                    goBack = !0,
                    profileService = $injector.get("LoadUserService"),
                    areKeysBlocked = !1,
                    keyDownEvent = function(event) {
                        if (!areKeysBlocked) {
                            var direction, activeModal, maxz;
                            if (37 === event.which) direction = -1;
                            else {
                                if (39 !== event.which) return;
                                direction = 1
                            }
                            areKeysBlocked = !0, $(".modal.in, .fancybox-overlay").each(function() {
                                var z = parseInt($(this).css("z-index"), 10);
                                (!activeModal || z > maxz) && (activeModal = this, maxz = z)
                            }), !profileService.isLoading && activeModal && $(activeModal).hasClass("profile-dialog") && ($("body").unbind("keydown.profileDialog keyup.profileDialog"), additionalOptions.func(current, direction, $modalInstance))
                        }
                    };
                window.onpopstate = function() {
                    $scope.$close(), selfLocation.isHistoryStopped = !1, window.onpopstate = function() {}
                }, $scope.closeProfile = function(closeModal) {
                    closeModal && $scope.$close(), window.onpopstate = function() {}, $("body").unbind("keydown.profileDialog keyup.profileDialog"), selfLocation.goBack(), $("body").trigger("path-change", {
                        url: window.location.pathname
                    })
                }, $scope.dialogOptions = tempOptions, $.isFunction(additionalOptions.func) && (additionalOptions.navigate = function(direction) {
                    profileService.isLoading || additionalOptions.func(current, direction, $modalInstance)
                }, $("body").unbind("keydown.profileDialog keyup.profileDialog").bind({
                    "keydown.profileDialog": keyDownEvent,
                    "keyup.profileDialog": function() {
                        areKeysBlocked = !1
                    }
                })), additionalOptions.user = current, additionalOptions.userAction = $injector.get("UserActionsFctry").action, angular.extend($scope, {
                    user: current,
                    backdrop: !0,
                    additionalOptions: additionalOptions
                }), $modalInstance.opened.then(function() {
                    profileService.clear(), RoutingService.ignoreNextRootScopeRoutingUpdate(), selfLocation.replacePath($scope.$root.Ajax.url + "/profile/" + current.id), $injector.get("DialogTutorialService").loadTutorialIfEnabled("tutorial_online")
                }), $modalInstance.result.then(function(data) {
                    goBack = !!data
                }), $modalInstance.result["finally"](function() {
                    profileService.clear(), goBack && $scope.closeProfile(!1)
                })
            }
            var tempDefaults = {},
                tempOptions = globalOptions;
            return angular.extend(tempDefaults, globalDefaults, {
                backdrop: !0,
                windowClass: "profile-dialog",
                size: "lg",
                templateUrl: "/template/dialog/profile.html"
            }), additionalOptions = void 0 !== additionalOptions ? additionalOptions : {}, $.isFunction(additionalOptions.func) && additionalOptions.func(userScope), tempDefaults.controller || (tempDefaults.controller = profileModalController), $modal.open(tempDefaults)
        }
    }

    function ExceptionService($injector, DialogService) {
        function colorizeButtonByActionTarget(actionTarget) {
            switch (actionTarget) {
                case "":
                    return "btn btn-danger";
                case "vip":
                    return "btn btn-warning";
                case "credits":
                case "credits_free":
                    return "btn btn-success";
                default:
                    return "btn btn-info"
            }
        }

        function renderMultiLineException(msgData) {
            if (msgData) {
                for (var tmpText = [], i = 0; i < msgData.length; ++i) {
                    if (!msgData[i].messages) return;
                    for (var d = 0; d < msgData[i].messages.length; ++d) tmpText.push(msgData[i].messages[d])
                }
                tmpText.length && (exceptionData.text = tmpText.join("<br />"))
            }
        }

        function exception(code) {
            switch (code) {
                case 2403:
                case 2324:
                    break;
                case 2202:
                    DialogService.openDialog({
                        windowClass: "modal-xlg",
                        templateUrl: "/template/exception/buy-credits.html"
                    }, {
                        data: exceptionData
                    });
                    break;
                case 2404:
                    var PackageService = $injector.get("PackageService");
                    PackageService.openOffer(exceptionData);
                    break;
                case 2406:
                default:
                    var modal, actionButtons = [],
                        useDefaultBtn = !0;
                    exceptionData.infoAction && actionButtons.push({
                        label: exceptionData.infoAction.title,
                        cssClass: colorizeButtonByActionTarget(exceptionData.infoAction.target) + " pull-left",
                        click: function() {
                            window.location.href = Ajax.url + "/" + exceptionData.infoAction.target
                        }
                    }), _.each(exceptionData.actions, function(action) {
                        actionButtons.push({
                            label: action.title,
                            cssClass: colorizeButtonByActionTarget(action.target),
                            click: function() {
                                window.location.href = Ajax.url + "/" + action.target
                            }
                        })
                    }), exceptionData.closeTitle && (useDefaultBtn = !1, actionButtons.push({
                        label: exceptionData.closeTitle,
                        cssClass: colorizeButtonByActionTarget(""),
                        click: function() {
                            modal.close()
                        }
                    })), modal = DialogService.openMessage(exceptionData.title, exceptionData.text, {
                        defaultBtn: useDefaultBtn,
                        buttons: actionButtons
                    })
            }
        }
        var exceptionData;
        this.handle = function(code, data) {
            exceptionData = data.dialog || data, renderMultiLineException(data.statusData), exception(code)
        }
    }

    function PackageService(DialogService) {
        this.openOffer = function(offerData) {
            var modal = DialogService.openDialog({
                size: "lg",
                templateUrl: "/template/exception/offer.html",
                controller: function($scope) {
                    $scope.data = offerData, $scope.modal = modal, $scope.purchase = {
                        step: 1
                    }
                }
            })
        }
    }

    function DialogButtonsDirective() {
        return {
            restrict: "E",
            templateUrl: "/template/dialog/dialog-buttons.html",
            scope: {
                options: "="
            }
        }
    }

    function renderDialogTemplates($templateCache) {
        $templateCache.put("template/modal/window.html", '<div tabindex="-1" role="dialog" class="modal fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)"><dialog-buttons options="$parent.additionalOptions"></dialog-buttons><div class="modal-dialog" ng-class="{\'modal-sm\': size == \'sm\', \'modal-lg\': size == \'lg\'}"><div class="modal-content overflow-visible" modal-transclude></div></div></div>'), $templateCache.put("dialog/advanced-message.html", '<div class="modal-header"><span class="h4"><strong>{{dialogOptions.title}}</strong></span></div><div class="modal-body" ng-bind-html="dialogOptions.content"></div><div class="modal-footer"><button class="btn btn-link-close" ng-click="dialogOptions.close()">{{dialogOptions.closeLabel}}</button><button class="btn btn-primary" ng-click="dialogOptions.callback()">{{dialogOptions.callbackLabel}}</button></div>'), $templateCache.put("dialog/simple-message.html", '<div class="modal-header"><button ng-click="$close()" style="z-index:1;" class="absolute-right absolute-top close" data-dismiss="modal" aria-hidden="true">&times;</button><b>{{model.headerText}}</b></div><div class="modal-body"><span ng-bind-html="model.bodyText"></span><iframe ng-init="iframe = model.iframe" width="{{iframe.width}}" height="{{iframe.height}}" ng-src="{{iframe.url}}" ng-show="iframe" frameBorder="0">Your browser does not support iframes.</iframe></div><div class="modal-footer"><button ng-repeat="button in model.buttons" ng-click="button.click()" class="{{button.cssClass}}" ng-bind-html="button.label"></button></div>')
    }

    function BatchRequestFactory($injector, $q, $resource) {
        var $resourceMinErr = angular.$$minErr("$resource"),
            BatchRequest = {
                headers: {},
                post: function(default_data, actions) {
                    var Resource = {},
                        request_data = {},
                        default_actions = {
                            get: {
                                method: "GET"
                            },
                            post: {
                                method: "POST"
                            },
                            "delete": {
                                method: "DELETE"
                            }
                        };
                    return actions = angular.extend({}, default_actions, actions), $.each(actions, function(name, action) {
                        /^(POST|PUT|PATCH)$/i.test(action.method);
                        Resource[name] = function(a1, a2, a3) {
                            function handleBatchException(rejection) {
                                if (!rejection.show) return rejection;
                                if (Lovoo.exceptionCodes(rejection.statusCode)) {
                                    var _header = Translator.get("action_interrupted"),
                                        _content = rejection.statusMessage.replace("_", " "),
                                        dialog = $injector.get("DialogService");
                                    dialog.openMessage(_header, _content, {
                                        id: rejection.statusCode
                                    })
                                } else {
                                    var exception = $injector.get("ExceptionService");
                                    exception.handle(rejection.statusCode, rejection)
                                }
                                return $q.reject(rejection)
                            }

                            function do_request() {
                                var request = $resource(Ajax.api + "/batch", {
                                    responseType: "json",
                                    cache: !1
                                }, {
                                    post: {
                                        method: "POST",
                                        isArray: !0,
                                        headers: BatchRequest.headers,
                                        params: {
                                            batch: "@batch"
                                        },
                                        ignoreLoadingBar: default_data.ignoreLoadingBar || !1,
                                        transformResponse: function(data) {
                                            var severalBatchResponse, resultsCollection = [];
                                            return data = JSON.parse(data), _.each(data, function(value) {
                                                severalBatchResponse = JSON.parse(value.body), severalBatchResponse.exception && handleBatchException(severalBatchResponse), resultsCollection.push(severalBatchResponse)
                                            }), [resultsCollection]
                                        }
                                    }
                                });
                                return request.post(request_data)
                            }
                            var data, success, error, queries = {},
                                batch_data = [];
                            switch (arguments.length) {
                                case 2:
                                    if (!angular.isFunction(a2)) {
                                        queries = a1, data = a2;
                                        break
                                    }
                                    if (angular.isFunction(a1)) {
                                        success = a1, error = a2;
                                        break
                                    }
                                    success = a2, error = a3;
                                case 1:
                                    angular.isFunction(a1) ? success = a1 : queries = a1;
                                    break;
                                case 0:
                                    break;
                                default:
                                    throw $resourceMinErr("badargs", "Expected up to 4 arguments [params, data, success, error], got {0} arguments", arguments.length)
                            }
                            return $.each(queries, function(key, value) {
                                var params = value.params,
                                    ret = [];
                                if ("id" != key) {
                                    for (var d in params) {
                                        var req = new RegExp(":" + d, "g");
                                        value.path && (value.path = value.path.replace(req, params[d])), ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(params[d]))
                                    }
                                    batch_data.push({
                                        method: value.method || action.method || "GET",
                                        relative_url: (default_data.path ? default_data.path : "") + (value.path ? (default_data.path ? "/" : "") + value.path : ""),
                                        body: ret.join("&")
                                    })
                                }
                            }), angular.extend(request_data, {
                                batch: JSON.stringify(batch_data)
                            }), _.size(default_data.headers) && angular.extend({}, BatchRequest.headers, default_data.headers), do_request()
                        }
                    }), Resource
                }
            };
        return BatchRequest.post
    }

    function RegisterFormCtrl(DATE_FORMATS, REGISTERFORM_FIELDS, $filter, $scope, $timeout, $window, AuthService, FacebookService, RegisterService, RegisterFormStorage, vcRecaptchaService) {
        function validateStep(step) {
            var nextStepEnabled = !0;
            return _.each(REGISTERFORM_FIELDS, function(field, fieldName) {
                field.step === step && ($scope.displayFieldError(fieldName), field.validateByForm && $scope.registerForm[fieldName].$invalid ? nextStepEnabled = !1 : field.required && !$scope[fieldName] && (nextStepEnabled = !1))
            }), nextStepEnabled
        }
        var minDate, preSelectedItems, extendedParams = {};
        minDate = RegisterService.birthdayRestrictions("to"), minDate.setDate(minDate.getDate() + 1), $scope.staticId = RegisterFormStorage.add($scope), $scope.displayError = {}, $scope.btnDisabled = !1, $scope.step = 0, $scope.name = "", $scope.gender = null, $scope.genderLooking = null, $scope.registerGenders = {}, $.each(Genders, function(key, value) {
            "3" !== key && ($scope.registerGenders[key] = value)
        }), $scope.registerGenderLookingGenders = Genders, $scope.dateFormat = DATE_FORMATS[Translator.locale] ? DATE_FORMATS[Translator.locale] : DATE_FORMATS["default"], $scope.birthday = $filter("date")(RegisterService.birthdayRestrictions("from"), $scope.dateFormat), $scope.dateOptions = {
            formatYear: "yyyy",
            startingDay: 1
        }, $scope.dateRestriction = {
            min: minDate,
            max: RegisterService.birthdayRestrictions("min")
        }, $scope.city = "", $scope.latitude = "", $scope.longitude = "", $scope.country = "", $scope.email = "", $scope.password = "", $scope.repeatPassword = "", $scope.addCaptcha = registerHasCaptcha, $scope.br = !1, preSelectedItems = $scope.$parent.dialogOptions ? $scope.$parent.dialogOptions.formParams || {} : {}, angular.extend($scope, preSelectedItems), _.size($scope.presetErrors) > 0 && ($scope.validationErrors = $scope.presetErrors), $scope.birthday || ($scope.birthday = RegisterService.birthdayRestrictions("from")), $scope.switchRegisterToLogin = AuthService.showLoginDialog, $scope.facebookRegister = FacebookService.tryLoadUserOrConnectOnLogin, $scope.open = function($event) {
            $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
        }, $scope.continueRegistration = function() {
            validateStep($scope.step) && $scope.switchToStep(1)
        }, $scope.switchToStep = function(step) {
            RegisterFormStorage.switchAllToFirstStep(), $scope.step = step
        }, $scope.doRegister = function() {
            if (!$scope.br) return void $scope.displayFieldError("br");
            $scope.btnDisabled = !0;
            var registerParams = {
                name: $scope.name,
                email: $scope.email,
                password: $scope.password,
                gender: $scope.gender,
                genderLooking: $scope.$root.websiteVersion < 3 ? $scope.genderLooking : 0,
                country: $scope.country,
                hasCaptcha: $scope.addCaptcha,
                city: $scope.city,
                latitude: $scope.latitude,
                longitude: $scope.longitude,
                birthday: $filter("transformToDateString")($scope.birthday, $scope.dateFormat)
            };
            $scope.facebookId ? extendedParams = {
                facebookId: $scope.facebookId,
                facebookPictureUrl: $scope.facebookPictureUrl
            } : $scope.googleId && (extendedParams = {
                googleId: $scope.googleId,
                googlePictureUrl: $scope.googlePictureUrl,
                token: $scope.token
            }), angular.extend(registerParams, extendedParams), $scope.addCaptcha && $.each(vcRecaptchaService.data(), function(key, val) {
                registerParams["recaptcha_" + key + "_field"] = val
            }), RegisterService.submitRegistration(registerParams).$promise.then(function(data) {
                var response = data.response.result,
                    query = "",
                    returnUrl = "";
                $scope.btnDisabled = !1, response.addCaptcha && ($scope.addCaptcha = response.addCaptcha, vcRecaptchaService.reload(!0)), _.size(response.errors) > 0 ? ($scope.validationErrors = response.errors, $scope.password = "", $scope.repeatPassword = "", $scope.displayError = {}) : ($scope.presetParams && $scope.presetParams.affPId && (query = "?" + $.param({
                    affPId: $scope.presetParams.affPId
                })), returnUrl = "prod" === window.ENVIRONMENT ? "https://www.lovoo.com/welcome/registration" : $scope.$root.Ajax.url + "/welcome/registration", $window.location.href = $scope.redirectUrl || returnUrl + query)
            })
        }, $scope.checkButton = function(value, field) {
            $scope[field] = value
        }, $scope.setLocation = function(location) {
            $scope.latitude = location.coordinates.latitude, $scope.longitude = location.coordinates.longitude, $scope.city = location.city, $scope.country = location.country
        }, $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, $scope.$watch(function() {
            return FacebookService.getTransformedFacebookUser()
        }, function(facebookUser) {
            void 0 !== typeof facebookUser && facebookUser && ($scope.name = facebookUser.name, $scope.email = facebookUser.email, $scope.gender = facebookUser.gender, $scope.genderLooking = facebookUser.genderLooking, $scope.facebookId = facebookUser.id, $scope.facebookPictureUrl = "https://graph.facebook.com/" + facebookUser.id + "/picture?width=1000", $scope.birthday = facebookUser.birthday ? facebookUser.birthday : $scope.birthday, $scope.city = facebookUser.city, $scope.latitude = facebookUser.latitude, $scope.longitude = facebookUser.longitude, $scope.usedFacebook = !0, $scope.usedGooglePlus = !0)
        }), $scope.presetParams && (_.each($scope.presetParams, function(value, key) {
            if ("birthday" === key && value) {
                var splitDate = value.split("-");
                $scope[key] = $filter("date")(new Date(parseInt(splitDate[0]), parseInt(splitDate[1]) - 1, parseInt(splitDate[2])))
            } else $scope[key] = value
        }), $scope.presetParams.birthday && $timeout($scope.continueRegistration))
    }

    function RegisterForm() {
        return {
            restrict: "E",
            replace: !1,
            controller: "RegisterFormCtrl",
            templateUrl: "/template/form/registerform.html",
            scope: {
                redirectUrl: "@?",
                presetParams: "=",
                presetErrors: "="
            }
        }
    }

    function RegisterFctry($resource) {
        return {
            register: $resource(Ajax.url + "/register", {}, {
                post: {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "X-Requested-With": "XMLHttpRequest",
                        "public": !0
                    },
                    transformRequest: function(data) {
                        return void 0 !== data ? $.param(data) : data
                    }
                }
            })
        }
    }

    function RegisterFormStorage() {
        var registerForms = [];
        return {
            add: function($scope) {
                return registerForms.push($scope), registerForms.length
            },
            count: function() {
                return registerForms.length
            },
            switchAllToFirstStep: function() {
                _.each(registerForms, function($scope) {
                    $scope.step = 0
                })
            }
        }
    }

    function RegisterService($rootScope, RegisterFctry, DialogService) {
        this.submitRegistration = function(params) {
            return RegisterFctry.register.post(params)
        }, this.showRegisterDialog = function(params) {
            if (params = params || {}, !$rootScope.isSecurityUser()) {
                var modal = DialogService.openDialog({
                    size: "sm",
                    templateUrl: "/template/dialog/register.html",
                    backdrop: !0
                }, {
                    formParams: params
                });
                modal.opened.then(function() {
                    dataLayer.push({
                        event: "sendVirtualPageView",
                        vpv: "/registration/open-dialog"
                    })
                })
            }
        }, this.oppositeGender = function(gender) {
            return 1 == gender ? 2 : 1
        }, this.birthdayRestrictions = function(type) {
            var date = new Date,
                fullYear = date.getFullYear() - BirthdayAgeRange[type];
            return date.setFullYear(fullYear), date
        }
    }

    function UserDialogProfileCtrl($scope, LoadUserService, ToasterService, UserActionsFctry) {
        var parent = $scope.$parent,
            batches = ["gallery", "details"];
        $scope.$root.Self.admin && nv || $scope.$root.Self.id == parent.user.id || batches.push("visit"), LoadUserService.load(parent.user, batches), $scope.$watchCollection(function() {
            return LoadUserService.get()
        }, function(data) {
            _.size(data) && (parent.user.details = data.details, parent.user.images = data.gallery || [])
        }), $scope.userAction = function(user, userAction, options) {
            $scope.$root.Self.id === user.id && ToasterService.open("error", Translator.get("action_interrupted"), Translator.get("self-dealing")), UserActionsFctry.action(user, userAction, options)
        }
    }

    function userListCtrl($scope, UserListService, UserActionsFctry, RegisterService, NotificationService) {
        var notifyCounter, notifyList = {
            visits: "v",
            hotties: "h",
            kiss: "k",
            hit: "mh",
            wantyou: "mv"
        };
        UserListService.resetList(), UserListService.enableLoading(), $scope.action = $scope.$root.Self.length <= 0 ? RegisterService.showRegisterDialog : UserActionsFctry.action;
        var cols = $scope.columns ? $scope.columns : $scope.$root.userListColumns;
        switch ($scope.c_columns = 12 / cols, $scope.columns || $scope.$root.$watch("userListColumns", function(newData, oldData) {
            newData != oldData && ($scope.c_columns = 12 / $scope.$root.userListColumns)
        }), UserListService.page_limit = $scope.limit, UserListService.navigation.openNext = function(index, users, curr_user, modal) {
            users[index] && !angular.equals(users[index], curr_user) && modal.result["finally"](function() {
                UserActionsFctry.action(users[index], "profile", {
                    crypt: users[index].crypt,
                    list: $scope.category,
                    func: $scope.navigate
                })
            }), modal.close(!1)
        }, $scope.navigate = UserListService.navigation, $scope.$watch(function() {
            return $scope.$parent.filter
        }, function(data) {
            void 0 !== data && load({
                type: data,
                resultPage: 1
            })
        }), $scope.isSelf = function(user) {
            return $scope.$root.Self.id == user.id
        }, $scope.type) {
            case "parentRequest":
            case "search":
                $scope.users = {}, $scope.isLoading = !0;
                var users, isLoading, oldUsers, oldIsLoading, reloadList;
                $scope.$watchCollection("[$parent.users , $parent.isLoading, $parent.reloadList]", function(data, old) {
                    users = data[0], isLoading = data[1], reloadList = data[2], UserListService.navigation.cached = users, users !== oldUsers && angular.extend($scope, {
                        users: users
                    }), isLoading !== oldIsLoading && angular.extend($scope, {
                        isLoading: isLoading
                    }), oldUsers = users, oldIsLoading = isLoading, "function" == typeof reloadList && void 0 === $scope.reloadList && ($scope.reloadList = $scope.$parent.reloadList)
                });
                break;
            default:
                $scope.isLoading = !0, $scope.$watch(function() {
                    return UserListService.get()
                }, function(data) {
                    void 0 !== data && angular.extend($scope, {
                        users: data
                    })
                }, !0), notifyList[$scope.category] && NotificationService.subscribe([notifyList[$scope.category]], function() {
                    var key = notifyList[$scope.category],
                        noti = this;
                    "number" == typeof notifyCounter && noti[key] > notifyCounter && load({
                        clear: !0
                    }), noti[key] !== notifyCounter && (notifyCounter = noti[key])
                });
                var load = function(options) {
                    var query = UserListService.getList($scope.category, $scope.limit, options);
                    $scope.isLoading = !0, "undefined" == typeof query.then ? $scope.isLoading = !1 : query.then(function() {
                        $scope.isLoading = !1
                    })
                };
                $scope.reloadList = function() {
                    $scope.isLoading || load({})
                }, load({
                    clear: !0
                })
        }
    }

    function ServerGeneratedCtrl($scope, FacebookService, RegisterService) {
        $scope._contentChanged = !1, $scope.facebookRegister = FacebookService.tryLoadUserOrConnectOnLogin;
        var getViewScope = function() {
            return angular.element("#ngContent").scope()
        };
        $scope.$watch(function() {
            return getViewScope()
        }, function(newVal, oldVal) {
            newVal && newVal.$id && ($scope._contentChanged = !0), newVal || ($scope._contentChanged = !1)
        }), $scope.showRegisterDialog = function() {
            $scope.$root.isSecurityUser() ? window.location.href = $scope.$root.Ajax.url + "/?user_id=" + $scope.userParams.userId : RegisterService.showRegisterDialog()
        }
    }

    function UserKissCtrl($scope, ToasterService, userQuery) {
        var parent = $scope.$parent;
        $scope.user = parent.user, $scope.sendKiss = function(type) {
            userQuery.single.post({
                userId: parent.user.id,
                category: "kiss",
                type: type
            }).$promise.then(function(data) {
                ToasterService.open("success", Translator.get("success.title_kiss"), Translator.get("success.info_kiss", {
                    user: parent.user.name
                }, 1)), $scope.$close()
            })
        }, userQuery.kiss.types({
            userId: parent.user.id
        }).$promise.then(function(data) {
            $scope.kisses = data.response
        })
    }

    function UserReportCtrl($scope, UserReportService, ToasterService) {
        function decodeHtml(value) {
            var dummie = document.createElement("span");
            return dummie.innerHTML = value, dummie.firstChild.data
        }
        var parent = $scope.$parent;
        $scope.text = parent.text ? decodeHtml(parent.text) : "", $scope.referenceObject = parent.referenceObject || null, $scope.referenceType = parent.referenceType || null, $scope.btnDisabled = !1, UserReportService.loadTypes({
            referenceType: $scope.referenceType
        }), $scope.$watch(function() {
            return UserReportService.getTypes()
        }, function(types) {
            void 0 !== types && types && ($scope.reportTypes = types, $scope.type = "user" === $scope.referenceType ? "fake" : "unfit")
        }), $scope.submitReportForm = function() {
            var params = {
                category: "report",
                text: $scope.text,
                type: $scope.type
            };
            $scope.btnDisabled = !0, $scope.$root.isSecurityUser() && ("user" === $scope.referenceType ? UserReportService.submitReportUserForm(angular.extend(params, {
                userId: $scope.referenceObject.id
            })).then(function() {
                $scope.btnDisabled = !1, $scope.$close(), ToasterService.open("success", Translator.get("success.title_report"), Translator.get("success.info_report", {
                    user: $scope.referenceObject.name
                }))
            }) : "photo" === $scope.referenceType && $scope.referenceObject.submitReportForm(angular.extend(params, {
                photoId: $scope.referenceObject.id
            })).then(function() {
                $scope.btnDisabled = !1, $scope.$close(), ToasterService.open("success", Translator.get("report_post"), Translator.get("success.info_report_photo"))
            }))
        }, $scope.checkButton = function(value, field) {
            $scope[field] = value
        }
    }

    function ProfileImage(ImageService) {
        function templateFunction(ele, attr) {
            return attr.element ? "<" + attr.element + "/>" : '<div class="user-image"></div>'
        }

        function linkFunction($scope, ele, attr) {
            var crypt, attributes = {
                category: $scope.category,
                size: $scope.size,
                reload: void 0 !== attr.reload
            };
            attr.$observe("id", function(value) {
                void 0 !== value && value ? angular.extend(attributes, {
                    source: angular.extend(attributes.source || {}, {
                        pictureId: value
                    })
                }) : attributes.source && attributes.source.pictureId && delete attributes.source.pictureId
            }), attr.$observe("gender", function(value) {
                void 0 !== value && angular.extend(attributes, {
                    source: angular.extend(attributes.source || {}, {
                        gender: value
                    })
                })
            }), $scope.$watch(function() {
                return attributes
            }, function(options) {
                var user = $scope.$parent.user;
                user && user.name && (attr.$set("title", void 0 !== $scope.overwriteTitle ? $scope.overwriteTitle : user.name), attr.$set("alt", void 0 !== $scope.overwriteTitle ? $scope.overwriteTitle : user.name), crypt = user.crypt ? user.crypt : !1, attributes.source || angular.extend(attributes, {
                    source: $scope.$parent.user
                })), angular.extend(attributes, {
                    size: crypt ? "l" : $scope.size,
                    category: crypt ? "thumb" : $scope.category
                });
                var setBackgroundImage = options && "true" !== attr.hide && !attr.element,
                    setSrc = options && attr.element,
                    image = new Image;
                if (image.onload = function() {
                        setBackgroundImage && (ele.css({
                            background: "url(" + image.src + ") center no-repeat"
                        }), ele.css({
                            backgroundSize: "cover"
                        })), setSrc && attr.$set("src", image.src), image = null
                    }, image.onerror = function() {
                        return "xl" === attributes.size ? ($scope.size = attributes.size = "l", void(image.src = ImageService.createProfileImageUrl(options) + getReloadParam(options))) : (setBackgroundImage && (ele.css({
                            background: "url(" + ImageService.getDefaultProfileImageUrl(options) + getReloadParam(options) + ") center no-repeat"
                        }), ele.css({
                            backgroundSize: "cover"
                        })), setSrc && attr.$set("src", ImageService.getDefaultProfileImageUrl(options) + getReloadParam(options)), void(image = null))
                    }, image.src = ImageService.createProfileImageUrl(options) + getReloadParam(options), !(ele.hasClass("img-xs") || ele.hasClass("img-md") || ele.hasClass("img-xxs") || ele.hasClass("img-sm") || ele.hasClass("img-lg"))) {
                    var styles = $scope.imgsizing ? $scope.imgsizing.split(";") : [],
                        width = styles.length ? styles[0] : $scope.imgsizing,
                        height = styles[1] ? styles[1] : width;
                    ele.css({
                        width: width || "100%",
                        height: height || "100%"
                    })
                }
                $(ele).addClass("user-userpic")
            }, !0)
        }

        function getReloadParam(options) {
            return options.reload ? "?" + Math.floor(1e8 * Math.random()) : ""
        }
        return {
            restrict: "E",
            replace: !0,
            template: templateFunction,
            scope: {
                category: "@",
                size: "@",
                id: "@",
                imgsizing: "@",
                overwriteTitle: "@"
            },
            link: linkFunction
        }
    }

    function profileBox() {
        return {
            restrict: "E",
            replace: !1,
            transclude: !1,
            controller: "SelfProfileBoxCtrl",
            templateUrl: "/template/widget/profile-box.html"
        }
    }

    function userList(TemplateService) {
        return {
            restrict: "E",
            replace: !0,
            transclude: !0,
            scope: {
                type: "@",
                category: "@",
                limit: "@",
                mode: "@",
                imgsizing: "@",
                columns: "@"
            },
            controller: "userListCtrl",
            link: function(scope, element, attrs) {
                var src = "list/" + (attrs.mode || "simple-list") + ".html";
                TemplateService.loadTemplate(src, element, scope), scope.itemAction = function(action, params) {
                    angular.isFunction(scope.$parent.onItemAction) && scope.$parent.onItemAction()(action, params)
                }
            }
        }
    }

    function scrollRefresh($window, $timeout, $interval) {
        return function(scope, ele, attr) {
            function handler() {
                var containerBottom = ele.offset().top + ele.height(),
                    scrollUnitHeight = children[0].clientHeight,
                    realScrollTop = $window.scrollTop() + $window.height(),
                    reloadPass = containerBottom - scrollUnitHeight * (scrollDistance + 1),
                    shouldScroll = realScrollTop >= reloadPass;
                shouldScroll && scrollEnabled ? scope.$root.$$phase ? scope.$eval(attr.scrollRefresh) : scope.$apply(attr.scrollRefresh) : shouldScroll && (checkWhenEnabled = !0)
            }
            var scrollDistance = 0,
                scrollEnabled = !0,
                checkWhenEnabled = !1,
                func = attr.scrollRefresh.replace(/\(\)/i, "");
            if ($.isFunction(scope[func])) {
                $window = angular.element($window), attr.scrollRefreshDistance && scope.$watch(function() {
                    return attr.scrollRefreshDistance
                }, function(data) {
                    scrollDistance = parseInt(data, 10)
                }), ele.parent().attr("scroll-refresh-disabled") && scope.$watch(function() {
                    return ele.parent().attr("scroll-refresh-disabled")
                }, function(data) {
                    scrollEnabled = !data, scrollEnabled && checkWhenEnabled && (checkWhenEnabled = !1, handler())
                });
                var children = $(".thumbnail", ele),
                    intval = $interval(function() {
                        children.length && ($interval.cancel(intval), $window.unbind("scroll").bind("scroll", handler), scope.$on("$destroy", function() {
                            $window.unbind("scroll")
                        }), $timeout(function() {
                            attr.scrollRefreshImmediateCheck ? scope.$eval(attr.scrollRefreshImmediateCheck) && handler() : handler()
                        }, 0)), children = $(".thumbnail", ele)
                    }, 500)
            }
        }
    }

    function profileBackgroundImage(ImageService) {
        return {
            restrict: "A",
            scope: {
                target: "=",
                onLoaded: "="
            },
            link: function(scope, ele, attr) {
                function render(data) {
                    var image = new Image,
                        position = attr.position ? attr.position : "center center",
                        url = ImageService.createProfileImageUrl({
                            source: {
                                pictureId: data.picture,
                                gender: data.gender
                            },
                            category: "image",
                            size: "m"
                        });
                    image.onload = function() {
                        attr.$set("style", "background: url(" + url + ") " + position + "; display: block;"), scope.onLoaded && scope.onLoaded()
                    }, image.src = url
                }
                scope.$watchCollection(function() {
                    return scope.target || scope.$parent.user
                }, function(data) {
                    void 0 !== data && render(data)
                })
            }
        }
    }

    function userHover() {
        return {
            restrict: "E",
            replace: !1,
            transclude: !1,
            scope: {
                user: "=",
                actionTime: "@"
            },
            templateUrl: "/template/list/user-hover.html"
        }
    }

    function onlineStatus() {
        return {
            restrict: "E",
            replace: !1,
            transclude: !1,
            scope: {
                user: "=",
                iconClass: "@",
                showDescription: "="
            },
            template: '<span ng-if="user.lastOnlineTime >= 0"><i ng-if="user && user.isOnline" class="lo lo-desktop text-success" ng-class="iconClass"></i><i ng-if="user && !user.isOnline && user.isMobile" class="lo lo-mobile text-success" ng-class="iconClass"></i><i ng-if="user && !user.isOnline && !user.isMobile" class="lo lo-mobile text-gray" ng-class="iconClass"></i><span ng-if="showDescription" ng-switch="(user.id == $root.Self.id || !!user.isOnline)"><span ng-switch-when="true">{{"only_online_users"|translate}}</span><span ng-switch-when="false">{{"last_online"|translate }}: {{user.lastOnlineTime*1000|date_diff_extended|translate}}</span></span></span>'
        }
    }

    function iGender() {
        return {
            restrict: "E",
            replace: !0,
            transclude: !1,
            scope: {
                user: "=",
                iconClass: "@"
            },
            template: '<span><i ng-if="user && user.gender == 1" class="lo lo-male" ng-class="iconClass" title="{{\'Sex\'|translate}}"></i><i ng-if="user && user.gender == 2" class="lo lo-female" ng-class="iconClass" title="{{\'Sex\'|translate}}"></i></span>'
        }
    }

    function svgVerified() {
            return {
                restrict: "E",
                templateUrl: "/template/profile/svg-verified.html",
                replace: !0,
                link: function(scope, element, attributes) {
                    var svgSize = (attributes.size || 25) + "px";
                    element.attr({
                        height: svgSize,
                        width: svgSize
                    })
                }
            }
        }! function(window) {
            "use strict";
            window.ENVIRONMENT = "prod"
        }(window), angular.module("Lovoo.Templates", ["/template/chat/list.html", "/template/chat/messages.html", "/template/chat/requests.html", "/template/credits/freecredits.html", "/template/dialog/block-user.html", "/template/dialog/chat-request.html", "/template/dialog/conversations.html", "/template/dialog/dialog-buttons.html", "/template/dialog/edit-profile.html", "/template/dialog/edit-whazzup.html", "/template/dialog/kisses.html", "/template/dialog/login.html", "/template/dialog/match-hit.html", "/template/dialog/photo-upload.html", "/template/dialog/post-modal.html", "/template/dialog/profile.html", "/template/dialog/pw-forget.html", "/template/dialog/register.html", "/template/dialog/report-form.html", "/template/dialog/tutorial.html", "/template/dialog/unlock-user.html", "/template/dialog/v3-nologin-info.html", "/template/dialog/verify-upload.html", "/template/dialog/vip-benefits.html", "/template/directives/tab-list.html", "/template/directives/validation-errors.html", "/template/error.html", "/template/exception/buy-credits.html", "/template/exception/offer.html", "/template/eyecatcher/eyecatcher.html", "/template/feed/advanced-feed.html", "/template/feed/feed-page.html", "/template/feed/follow-button.html", "/template/feed/hashtag-checklist.html", "/template/feed/post/photo.html", "/template/feed/post/profile-details.html", "/template/feed/post/profile-picture.html", "/template/feed/post/widget-hashtag.html", "/template/feed/read-more-text.html", "/template/feed/show-more-posts.html", "/template/feed/simple-feed.html", "/template/flirtmatch.html", "/template/footer/footer-sidebar.html", "/template/footer/footer.html", "/template/form/account-settings/change-birthday.html", "/template/form/account-settings/change-email.html", "/template/form/account-settings/change-name.html", "/template/form/account-settings/change-origin.html", "/template/form/account-settings/change-password.html", "/template/form/comments.html", "/template/form/interests.html", "/template/form/location.html", "/template/form/login-form.html", "/template/form/login-nintynine-form.html", "/template/form/registerform.html", "/template/form/searchfilter.html", "/template/form/searchform.html", "/template/form/vip-settings.html", "/template/helpcenter/faq.html", "/template/helpcenter/imprint.html", "/template/helpcenter/supportform.html", "/template/itemlist.html", "/template/list/advanced-list.html", "/template/list/intro.html", "/template/list/pictures.html", "/template/list/simple-list.html", "/template/list/user-hover.html", "/template/listitem/credits.html", "/template/listitem/friend.html", "/template/listitem/kiss.html", "/template/listitem/pictures.html", "/template/packages/credits-default.html", "/template/packages/packages.html", "/template/packages/vip-daily.html", "/template/packages/vip-default.html", "/template/packages/vip-weekly.html", "/template/page/challenge.html", "/template/page/contacts.html", "/template/page/credits.html", "/template/page/eyecatcher.html", "/template/page/fans.html", "/template/page/flirtmatch.html", "/template/page/hashtag.html", "/template/page/helpcenter.html", "/template/page/index-old.html", "/template/page/index.html", "/template/page/post.html", "/template/page/profile-guest.html", "/template/page/profile-old.html", "/template/page/profile.html", "/template/page/search-tracking.html", "/template/page/search.html", "/template/page/self-gallery.html", "/template/page/self-page.html", "/template/page/settings.html", "/template/page/settings/about.html", "/template/page/settings/account-delete.html", "/template/page/settings/account-linked.html", "/template/page/settings/account.html", "/template/page/settings/community-blocked.html", "/template/page/settings/community-chat-blocker.html", "/template/page/settings/community.html", "/template/page/settings/notifications.html", "/template/page/settings/private.html", "/template/page/settings/promo.html", "/template/page/settings/support.html", "/template/page/verify.html", "/template/page/vip.html", "/template/profile/edit-interview.html", "/template/profile/header-content.html", "/template/profile/profile-edit-details.html", "/template/profile/self_details.html", "/template/profile/svg-verified.html", "/template/profile/user_details.html", "/template/testview/dialog_test.html", "/template/testview/icons.html", "/template/testview/tutorial_test.html", "/template/topmenu/svg-brand.html", "/template/topmenu/topmenu-logged-in.html", "/template/topmenu/topmenu-logged-out.html", "/template/topmenu/topmenu-mobile.html", "/template/topmenu/topmenu.html", "/template/unique-page.html", "/template/verify/verify_photo.html", "/template/verify/verify_sms.html", "/template/widget/detail-count.html", "/template/widget/first-steps.html", "/template/widget/profile-box.html", "/template/widget/purchase.html"]),
        angular.module("/template/chat/list.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/chat/list.html", '<div class=chat-list-header><h5 class="section-title space-before-sm space-left-sm">{{"chat_my_conversations" | translate}}</h5><div class="absolute-top absolute-right" ng-class="startOnOpen ? \'visible-xs-block\' : \'hidden\'"><button class="cursor-pointer text-white btn btn-link" ng-click=convSingle.reportUser() tooltip="{{\'report_profile\'|translate}}" data-toggle=tooltip title="{{\'report_profile\'|translate}}" tooltip-placement=bottom tooltip-append-to-body=true><i class="fa fa-lg fa-warning"></i></button> <button ng-click="sidebarOpen = !sidebarOpen;" class="btn btn-link sidebar-btn"><i class="fa fa-lg fa-angle-left text-white"></i></button></div></div><div class="chat-list-filter bg-white padding"><select class="form-control space-before-sm" ng-model=convList.orderFilter ng-options="c.name|translate for c in convList.filter"></select><div style="position: absolute; bottom: -1px; width: 100%" class=chat-list-tabs ng-controller="ChatTabCtrl as convTab"><ul class="nav nav-tabs"><li ng-repeat="link in convTab.links" ng-class="{\'active\': link.page == type}"><a class="cursor-pointer text-gray-light" ng-click="$parent.$parent.type = link.page" data-toggle=tab>{{link.title | translate}} <span class="badge badge-up bg-pink" ng-if=convTab.counts[link.badge]>{{convTab.counts[link.badge]}}</span></a></li></ul></div></div><div class=chat-list chat-list-refresh=next() scroll-pane=convList.conversations><div class=sidebar-chat-inner><div class="media-list media-list-devided media-list-hover conversation-list"><div ng-cloak class="media chat padding-md hover-base" ng-init="user = conv.user" ng-repeat="conv in (convList.filteredList = (convList.conversations | orderBy:[\'pinned\', (convList.orderFilter.value || \'lastMessage.time\')]:true))" ng-class="{active: active == conv.id, online: user.isOnline}"><a href={{$root.Ajax.url}}/profile/{{user.id}} class=pull-left ng-click="convList.action(user, \'profile\')" eat-click><profile-image class="media-object img-xs img-circle cursor-pointer" size=s ng-class="{\'user-vip\': user.vip}"></profile-image></a><div class="media-body cursor-pointer" ng-click="convList.single(conv.id, $index); $parent.active=conv.id"><online-status user=conv.user></online-status><strong class="h5 space-left-sm text-normal"><span ng-bind-html=user.name has-emoji></span>, {{user.age}} <span class="pull-right hover-hide small">{{conv.lastMessage.time*1000 | date_diff | translate}}</span></strong> <span class="pull-right lo lo-close fa-lg space-before-sm cursor-pointer hover-show" ng-click="convList.remove(conv.id, convList.filteredList[($last ? $index-1 : $index + 1)].id);$event.stopPropagation();" ng-if="$parent.type!=\'requests\'"></span><br><div class="h6 second-line nowrap text-overflow"><div ng-switch=conv.lastMessage.direction><i ng-switch-when=1 ng-class="{true: \'text-pink fa-envelope\', false: \'text-gray fa-envelope-o\'}[!!conv.countNewMessages]" class=fa></i> <i ng-switch-when=2 ng-class="{true: \'text-success\', false: \'text-gray\'}[$parent.conv.readconfirm == 2]" class="lo lo-check"></i><div style="display: inline-block" ng-class="{true: \'text-bold\', false: \'text-gray\'}[!!conv.countNewMessages]" class="quote space-left-sm" has-emoji ng-bind-html=conv.lastMessage.content></div></div></div></div></div><div class="media chat padding-md text-center" ng-if=!convList.conversations.length ng-switch=convList.loaded><span ng-switch-when=false>{{"is_loading"|translate}} <i class="fa fa-spinner fa-spin space-left-sm"></i></span> <span ng-switch-when=true>{{"nothing_found"|translate}}</span></div></div></div></div>')
        }]), angular.module("/template/chat/messages.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/chat/messages.html", '<div class=modal-content-main><div class=modal-content-main-header><button ng-click="sidebarOpen = !sidebarOpen;" class="btn btn-link sidebar-btn" ng-class="startOnOpen ? \'visible-xs-block\': \'hidden\'"><i class="fa fa-lg fa-th-list"></i></button></div><include-template name=chat/requests></include-template><div ng-if="type == \'chat\' || accepted"><div class=modal-tab-pane ng-if=convSingle.messages.length ng-file-drop=convSingle.onFileSelect($files) ng-file-drag-over-class=convSingle.dragAttachment($event) ng-file-drop-available="dropSupported=true"><div class=conversation-top-panel><div class="size-xs pull-left space-right-sm"><a href={{$root.Ajax.url}}/profile/{{user.id}} ng-click="convList.action(user, \'profile\')" eat-click><profile-image id={{user.picture}} gender={{user.gender}} size=s category=thumb class="img-circle size-xs cursor-pointer"></profile-image></a></div><div class=space-left-sm><span class="h3 text-normal"><online-status user=convSingle.conversation.user></online-status><span ng-bind-html=user.name has-emoji></span>, {{user.age}}</span><br><div class="h6 space-before-sm hidden-xs" has-emoji ng-bind=user.whazzup style="text-overflow:ellipsis; overflow: hidden; white-space:nowrap; margin-top: 5px"></div><a class="cursor-pointer absolute-right absolute-top space-right hidden-xs" ng-click=convSingle.reportUser()><small>{{\'report_profile\'|translate}}</small></a></div></div><div class=dragged-open><div class="middle text-center"><i class="fa fa-5x" ng-class="{true: \'fa-upload text-gray-dark\', false: \'lo lo-close\'}[convSingle.fileError]"></i><div ng-hide=convSingle.fileError class="dragged-error-message text-bold hidden-xs">{{"only_images_allowed"|translate}}</div></div></div><form role=form class=conversation-bottom-panel ng-submit=convSingle.send(convSingle.conversation.user.id)><div class=image-preview ng-show="convSingle.message.attachments != null"><div class="sel-file padding-sm row" ng-repeat="f in convSingle.message.attachments"><div class="col-xs-2 space-left-sm"><img class="" ng-show=convSingle.message.dataUrls ng-src="{{convSingle.message.dataUrls[$index]}}"></div><div class=col-xs-8><div><strong>{{"file_name"|translate}}:</strong> {{f.name}}</div><div><strong>{{"file_size"|translate}}:</strong> {{convSingle.fileSize(f.size)}}</div></div><div class="absolute-right cursor-pointer"><i ng-click=convSingle.removeAttachment() class="lo lo-close"></i></div></div><hr class="hr-none"></div><div ng-class="{\'convSingle.submitOnEnter\': \'input-group\'}"><div ng-if=!convSingle.submitOnEnter><textarea msd-elastic focus required type=text class="form-control border-none" style="width: 100%" ng-model=convSingle.message.text max-rows=2 rows=1 placeholder="{{\'your_message\'|translate}}"></textarea></div><div ng-if=convSingle.submitOnEnter><input focus required ng-model=convSingle.message.text type=text placeholder="{{\'your_message\'|translate}}" class="form-control border-none"></div></div><hr class="hr-none"><div class="row flexbox-vertical-center padding" style="margin: 0"><div class="col-xs-2 col-md-3"><div class=file-upload><input type=file multiple=multiple accept=image/* ng-file-select="convSingle.onFileSelect($files)"><label class=text-normal><i class="fa fa-lg fa-paperclip space-right-sm text-gray"></i> <span class="hidden-xs hidden-sm">{{"choose_picture"|translate}}</span></label></div></div><div class="col-xs-6 col-sm-5 col-md-4 col-xs-offset-3 col-sm-offset-4 col-md-offset-2 text-center"><label class="text-normal cursor-pointer"><span class="">{{\'submit_enter\'|translate}}</span> <input type=checkbox ng-click=convSingle.toggleSubmit() ng-checked="convSingle.submitOnEnter"></label></div><div ng-show=!convSingle.submitOnEnter class="col-xs-1 col-md-3 text-right"><button type=submit ng-disabled=convSingle.sendInProgress class="btn btn-xs btn-danger"><span class="hidden-xs hidden-sm">{{"send_msg"|translate}}</span> <span class="visible-xs-inline visible-sm-inline"><i class="fa fa-share"></i></span></button></div></div></form><div class=conversation stick-to-bottom=convSingle.conversation scroll-pane=convSingle.messages.all() flexible-bottom><div class=conversation-inner ng-show=convSingle.messages.length><ul class="media-list conversation-panel"><li class="media padding-md chat-item" ng-show="!(convSingle.complete || convSingle.messages.length < 14)"><div class=text-center><button class="btn btn-xs btn-primary" ng-click=convList.single(convSingle.conversation.id) ng-disabled=!($parent.convSingle.loaded)><i class="fa fa-spinner fa-spin space-right-sm" ng-show=!($parent.convSingle.loaded)></i> {{"chat_load_older"|translate}}</button></div></li><li class="media padding-md chat-item" ng-repeat="message in convSingle.messages.all() track by message.id" ng-class="{2: \'self\'}[message.direction]" ng-init="user=(message.direction==1 ? $parent.user : $root.Self)"><a ng-click="message.direction==1 ? convList.action(user, \'profile\') : \'\'" class="user-image size-xxs text-center" ng-class="{1: \'space-right-sm\', 2: \'space-left-sm\'}[message.direction]"><profile-image size=s category=thumb class="img-xxs img-circle user-image"></profile-image></a><div class="media-body overflow-visible"><div has-emoji class="chat-message animated fadeIn animation-duration-05"><div style="white-space: pre-wrap" has-emoji class="text-left white-box padding" ng-bind-html=message.content></div><span class="absolute cursor-pointer" tooltip="{{\'delete_msg.hint\' | translate}}" data-toggle=tooltip tooltip-placement=top tooltip-append-to-body=true ng-click="convSingle.removeMessage(convSingle.conversation.id, message.id)"><i class="lo lo-close small opacity05"></i></span><div class="thumbnail animated fadeIn animation-delay-2 animation-duration-02" ng-if=message.attachments ng-switch=!!message.attachments.picture><a ng-switch-when=true class=fancybox style="display: block" ng-href={{$root.Ajax.imgUrl}}/attachments/{{message.attachments.picture.id}}/image_l.jpg><img ng-show=!isLoading check-attachment ng-src="{{$root.Ajax.imgUrl}}/attachments/{{message.attachments.picture.id}}/{{message.availableType || \'thumb_l\'}}.jpg"><loading-bar ng-show=isLoading></loading-bar></a> <a ng-switch-when=false ng-cloak target=_blank style="display: block" class="padding space-left space-right space-before text-center no-underline" ng-init="location = convSingle.attachmentLocation(message.attachments.coordinates)" ng-href="https://maps.google.com/maps?q={{message.attachments.coordinates.latitude}},{{message.attachments.coordinates.longitude}}&z=17"><i class="fa fa-map-marker fa-lg fa-5x text-purple"></i><h4 class="section-title space-before-sm">{{location.$$state.value}}</h4></a></div><div style="position: absolute; top: 100%; right: 0; width: 100%"><small style="white-space: nowrap" class="pull-left text-smaller chat-message-time">{{message.time*1000 | date_diff | translate}} {{message.time*1000 | date:"HH:mm"}}</small> <small style="white-space: nowrap" class="pull-right text-smaller" ng-if="$last && message.direction == 2"><span ng-show=convSingle.conversation.readconfirm><span class="lo lo-check space-after-sm text-green" style="position: relative; top: 1px"></span> {{"readconfirm_is"|translate}}</span></small></div></div><br></div></li></ul></div></div></div><div class="modal-tab-pane text-center padding" ng-if=!convSingle.messages.length ng-init="listScope = $parent.convList" ng-switch="loaded || (!listScope.conversations.length && listScope.loaded)"><div ng-switch-default class="space-before row h2 text-gray-light text-normal"><i class="fa fa-spinner fa-spin space-left-sm space-left-sm"></i> {{"is_loading"|translate}}</div><div ng-switch-when=true class="space-before row"><div class="col-xs-offset-2 col-xs-8"><p><i class="fa fa-comments fa-5x text-gray-light"></i></p><p><strong>{{"no_conversations.title"|translate}}</strong></p><p>{{"no_conversations.text"|translate}}</p><p><a href={{$root.Ajax.url}}/search target="{{ startOnOpen ? \'main\' : \'\' }}" class="btn btn-danger" ng-click=dialogOptions.close();>{{"finde_new_people_now"|translate}}</a></p></div></div></div></div></div>')
        }]), angular.module("/template/chat/requests.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/chat/requests.html", '<div ng-if="(type == \'requests\' && !accepted)"><div class="modal-tab-pane modal-chatrequest" ng-if=convSingle.messages.length><div class="row row-nopadding white-box" horizontal-scroll-pane-hidden scroll-pane=convSingle.conversation.lastMessage.content><div class="col-xs-5 bg-cover"><div style="max-height: 350px; overflow: hidden"><a href={{$root.Ajax.url}}/profile/{{user.id}} eat-click><profile-image gender={{user.gender}} id={{user.picture}} element=img category=image size=s imgsizing=100%;auto class="img-fullwidth cursor-pointer" ng-click="convSingle.user_action(user,\'profile\')"></profile-image></a><div class="absolute-top absolute-right"><svg-verified ng-if=user.verified class="inline-block text-blue"></svg-verified>&nbsp;<div ng-show=user.vip class="inline-block circle circle-xxs bg-orange text-white"><i class="lo lo-poker lo-down-1"></i></div></div></div><div class=padding-lg><p class=text-center><button class="btn btn-success btn-width-md" ng-click="$parent.$parent.accepted=true;"><span class="space-left space-right hidden-xs">{{"accept"|translate}}</span> <i class="fa fa-lg fa-check visible-xs-inline-block"></i></button> <button class="btn btn-danger visible-xs-inline-block"><i class="lo fa-lg lo-close"></i></button></p><small class=hidden-xs>{{ "chat.userNotYourType"|translate:{name: user.name} }} {{ "chat.reject_request"|translate }} <a ng-click="convSingle.reject(convSingle.conversation.id, convList.filteredList, convList.listIndex);$event.stopPropagation();" class="cursor-pointer text-gray-dark lowercase">{{"reject"|translate}}</a></small></div></div><div class="col-sm-7 col-sm-7-important nose-left"><div class=padding-lg><a class="cursor-pointer pull-right space-left-sm" ng-click=convSingle.reportUser()><small>{{\'report_profile\'|translate}}</small></a><p class="h3 font-light"><online-status user=user></online-status>{{user.name}}, {{user.age}}</p><p class=space-after has-emoji ng-bind-html=convSingle.conversation.lastMessage.content></p></div></div></div></div><div class="modal-tab-pane text-center padding" ng-if=!convSingle.messages.length ng-init="listScope = $parent.convList" ng-switch="loaded || (!listScope.conversations.length && listScope.loaded)"><div ng-switch-default class="space-before row h2 text-gray-light text-normal"><i class="fa fa-spinner fa-spin space-left-sm space-left-sm"></i> {{"is_loading"|translate}}</div><div ng-switch-when=true class="space-before row"><div class=col-xs-2></div><div class=col-xs-8><p><i class="fa fa-comments-hi fa-5x text-gray-light"></i></p><p><strong>{{"no_chat_requests.title"|translate}}</strong></p><p>{{"no_chat_requests.text"|translate}}</p><p><a href="{{$root.Ajax.url}}/" target="{{ startOnOpen ? \'main\' : \'\' }}" class="btn btn-danger" ng-click=dialogOptions.close();>{{"match.play_now"|translate}}</a></p></div><div class=col-xs-2></div></div></div></div>')
        }]), angular.module("/template/credits/freecredits.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/credits/freecredits.html", '<table class="table white-box vertical-center"><tr ng-repeat="freecredit in freecredits" ng-if=!freecredit.hide><td class=size-sm ng-class=freecredit.bgColor><span class="fa-stack fa-lg" ng-class="freecredit.iconColor || \'text-white\'"><i class="fa fa-stack-1x fa-lg" ng-class=freecredit.icon></i></span></td><td><strong>{{freecredit.title}}</strong><br><small>{{freecredit.message}}</small></td><td class=text-gray>{{freecredit.restrictionTitle}} {{ "dynamic.confirm_credits"|translate:{credits: (freecredit.credits|formatNumber)}:freecredit.credits }}</td><td class="text-center vertical-center" ng-if=freecredit.noCheck ng-class="\'single-line-lg\'"><button class="btn btn-width-md" ng-class=freecredit.btnColor ng-click=action(freecredit.type)><span class="space-before-sm space-after-sm" ng-bind-html=freecredit.btnText></span></button></td><td class="text-center vertical-center" ng-if=!freecredit.noCheck ng-switch=freecredit.last ng-class="{\'single-line-lg\' : !freecredit.last}"><button ng-switch-when=0 class="btn btn-width-md" ng-class=freecredit.btnColor ng-click=action(freecredit.type)><span class="space-before-sm space-after-sm" ng-bind-html=freecredit.btnText></span></button> <i ng-switch-default class="lo lo-check fa-2x text-green space-right-sm lo-up-2"></i></td></tr></table>')
        }]), angular.module("/template/dialog/block-user.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/block-user.html", '<div class=modal-header><span class=h4><strong>{{ ::\'block_user\' | translate }}</strong></span></div><div class="modal-body row"><div class="col-sm-2 col-xs-3"><profile-image id="{{ dialogOptions.user.picture }}" category=thumb size=s class="img-circle img-md space-left-sm space-before-xs" ng-class="{\'user-vip\': !!$root.Self.vip}"></profile-image></div><div class="col-sm-10 col-xs-9"><div class="space-before-sm space-left" ng-bind-html="::\'txt.block_info\' | translate:{\'name\': dialogOptions.user.name}"></div></div></div><div class=modal-footer><button class="btn btn-link-close" ng-click=dialogOptions.close()>{{dialogOptions.cancelLabel}}</button> <button class="btn btn-primary" ng-click=dialogOptions.callback()>{{ ::\'block_do\' | translate }}</button></div>')
        }]), angular.module("/template/dialog/chat-request.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/chat-request.html", '<div class=modal-header><button class=close ng-click=$close()>&times;</button><h4 class=modal-title>{{::\'chat_request\'|translate}}</h4></div><form role=form name=request novalidate><div class=modal-body><div class=space-after ng-bind-html="\'chat_request_user\'|translate:{user: user.name}" has-emoji></div><validation-errors></validation-errors><textarea name=message ng-model=message class=form-control rows=2 placeholder="{{::\'your_message\'| translate}}" ng-required=true ng-blur="displayFieldError(\'message\')" ng-maxlength=1200></textarea><div ng-show="request.message.$error.required && displayError.message" class="text-danger form-error"><small>{{::"no_empty_msg" | translate}}</small></div><div ng-show="request.message.$error.maxlength  && displayError.message" class="text-danger form-error"><small>{{"form_error_maxlength" | translate:{count:1200} }}</small></div><div class="space-before full-width" ng-if=addCaptcha vc-recaptcha tabindex=3 ng-model=captcha theme=clean lang={{$root.Translator.locale}} key=$root.CaptchaConfig.key></div></div><div class=modal-footer><small class="pull-left text-left"><strong><i class="fa fa-star text-orange"></i> {{::"topchat" | translate}}</strong><br>{{::"vip_advantages.topchat_text" | translate}}</small><div class="btn-group space-before-sm"><label class="btn btn-default" ng-class="{\'active\': pinned}"><input type=checkbox name=pinned ng-click=confirmTopchat($event) ng-checked="pinned"> <i class="fa fa-star fa-inactive text-gray"></i> <i class="fa fa-star fa-active text-orange"></i></label><button class="btn btn-primary" ng-disabled="request.$invalid || btnDisabled" ng-click=sendRequest()>{{::"send_msg"|translate}}</button></div></div></form>')
        }]), angular.module("/template/dialog/conversations.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/conversations.html", '<div><conversation-list></conversation-list><conversation-single></conversation-single><div class="absolute-right absolute-top"><button ng-click=$close(); type=button data-dismiss=modal aria-hidden=true class=close>Ã—</button></div></div>')
        }]), angular.module("/template/dialog/dialog-buttons.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/dialog-buttons.html", '<div ng-if=options.user class="white-box modal-sticky-header"><div class=modal-body><div class=row><div class=col-xs-1><profile-image id="{{ options.user.picture }}" imgsizing=50 size=s category=thumb background ng-class="{\'user-vip\': !!options.user.vip}"></profile-image><svg-verified ng-if="options.user.verified === 1 || options.user.verified  === true" class="absolute-right absolute-top user-verificated text-blue"></svg-verified></div><div class=col-xs-5><div class=space-left has-emoji><h5 class="inbox-heading text-bold text-overflow"><online-status user=options.user></online-status>{{ options.user.name }}, {{ options.user.age }}</h5><h5 ng-if=options.user.whazzup class="inbox-heading text-light quote text-overflow">{{ options.user.whazzup }}</h5></div></div><div class="col-xs-6 pull-right text-right"><div ng-click="options.userAction(options.user, \'like\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{ ::\'You want\'|translate }}" data-toggle=tooltip title="{{ ::\'You want\'|translate }}" tooltip-placement=bottom tooltip-append-to-body=true><i class="fa fa-heart fa-lg"></i></div><div ng-click="options.userAction(options.user, \'chat\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{ ::\'send_msg\'|translate }}" data-toggle=tooltip title="{{ ::\'send_msg\'|translate }}" tooltip-placement=bottom tooltip-append-to-body=true><i class="fa fa-envelope fa-lg"></i></div><div ng-click="options.userAction(options.user, \'kiss\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{ ::\'send_kiss\'|translate }}" data-toggle=tooltip title="{{ ::\'send_kiss\'|translate }}" tooltip-placement=bottom tooltip-append-to-body=true><i class="lo lo-lips fa-lg"></i></div><div ng-click="options.userAction(options.user, \'fan\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{ ::\'hotlist_add\'|translate }}" data-toggle=tooltip title="{{ ::\'hotlist_add\'|translate }}" tooltip-placement=bottom tooltip-append-to-body=true><i class="fa fa-star fa-lg"></i></div></div></div></div></div>')
        }]), angular.module("/template/dialog/edit-profile.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/edit-profile.html", '<div class=row-main ng-click=$close()><div class="col-sm-12 col-md-6" ng-style="{padding: $root.BS.greaterSm ? \'0 16px\' : \'\'}" ng-class="{\'space-after-md\': !$root.BS.greaterSm}"><div class=bg-white ng-click=$event.stopPropagation()><h5 class="relative bg-gray text-white no-space-after padding text-bold">{{Â ::\'Details\' | translateÂ }}</h5><div class="padding space-before"><profile-edit-details user=dialogOptions.user></profile-edit-details></div></div></div><div class="col-sm-12 col-md-6" ng-style="{padding: $root.BS.greaterSm ? \'0 16px\' : \'\'}"><div class=bg-white ng-click=$event.stopPropagation()><h5 class="relative bg-gray text-white no-space-after padding text-bold">{{Â ::\'details.me\' | translateÂ }}</h5><profile-edit-interview user=dialogOptions.user></profile-edit-interview></div></div></div>')
        }]), angular.module("/template/dialog/edit-whazzup.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/edit-whazzup.html", '<div class=modal-header><button class=close ng-click=$close();>&times;</button><p class="h4 modal-title">{{ ::\'change_stat\' | translate }}</p></div><div class=modal-body><p>{{ ::\'custom.whazzup\' | translate }}</p><form ng-submit=doSubmit(whazzupForm) name=whazzupForm id=form><div class=form-group><textarea type=text rows=2 class=form-control name=whazzup placeholder="{{ ::\'edit_status\' | translate }}" ng-blur="displayFieldError(\'whazzup\')" ng-required=true ng-trim=false maxlength="{{ WHAZZUP_MAXLENGTH }}" ng-model=whazzup>\n            </textarea><span class="pull-right text-smaller text-muted space-after-sm space-before-sm">({{ \'letters_to_go\' | translate:{letters: WHAZZUP_MAXLENGTH - whazzup.length} }})</span><div class="text-danger form-error" ng-show="whazzupForm.whazzup.$error.required && displayError.whazzup"><small>{{ ::\'form_error_required\' | translate }}</small></div><div class="text-danger form-error" ng-show="whazzupForm.whazzup.$error.maxlength&& displayError.whazzup"><small>{{ ::\'form_error_maxlength\' | translate:{count:WHAZZUP_MAXLENGTH} }}</small></div><div class=clearfix></div></div><div class=form-group><button class="btn btn-primary btn-block" type=submit ng-disabled="whazzupForm.$invalid || btnDisabled">{{ ::\'send\' | translate }}</button></div></form></div>')
        }]), angular.module("/template/dialog/kisses.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/kisses.html", '<div class=modal-header><button class=close ng-click=$close()>&times;</button><h3 class="modal-title text-center">{{"which_kiss"|translate}}</h3></div><div class="modal-body text-center"><h4 class=space-after-sm>{{"current_roll"|translate}} {{ "dynamic.confirm_credits"|translate:{credits: ($root.Self.credits|formatNumber)}:$root.Self.credits }}</h4><div class=row><div class="col-xs-8 col-sm-offset-2"><div class="row space-before"><div class="col-xs-4 space-before text-right text-overflow"><br>{{user.name}}<br>{{ "dynamic.age_years"|translate:{years: user.age}:user.age }}</div><div class="col-xs-4 text-center"><div class="thumbnail padding-lg"><profile-image class="img-responsive user-userpic img-circle" imgsizing=120 category=thumb size=m></profile-image></div><svg-verified ng-if=user.verified class="absolute-right absolute-bottom user-verificated text-blue"></svg-verified></div><div class="col-xs-4 space-before text-left" ng-if="user.location || user.city"><br>{{"from"|translate}} <span ng-if=user.location>{{user.location}}</span> <span ng-if="!user.location && user.city">{{user.city}}</span></div><div class="col-xs-4 space-before text-left" ng-if="!user.location && !user.city"><br>{{"City"|translate}}:<br><span>{{\'not_specified\'|translate}}</span></div></div></div></div><div class="split-line opacity05"><hr class=opacity03><h4 class=split-text><i class="lo lo-lips fa-lg opacity05"></i></h4><hr class=opacity03></div><div ng-if=kisses><div class="row space-after"><div class=col-xs-4 ng-repeat="kiss in kisses.kissTypesSelf | limitTo:3"><div class="size-lg center-block"><span class="absolute-top text-gray ng-binding" title="{{\'dynamic.got_yet\'|translate:{count: (kisses.kissTypesUser[$index].count|formatNumber)}:(kisses.kissTypesUser[$index].count || 0) }}"><i class="lo lo-lips lo-down-2 text-top"></i> {{kisses.kissTypesUser[$index].count ? kisses.kissTypesUser[$index].count : 0}}</span> <img ng-src={{$root.Ajax.imgUrlStatic}}kisses/{{kiss.kisstype}}.png></div><p>{{kiss.title}}</p><button ng-click=sendKiss(kiss.kisstype); class="btn btn-primary">{{ "dynamic.confirm_credits"|translate:{credits: (kiss.credits|formatNumber)}:kiss.credits }}</button></div></div><div class="row space-after"><div class=col-xs-4 ng-repeat="kiss in kisses.kissTypesSelf | skipTo:3" ng-init="$index = $index+3"><div class="size-lg center-block"><span class="absolute-top text-gray ng-binding" title="{{\'dynamic.got_yet\'|translate:{count: (kisses.kissTypesUser[$index].count|formatNumber)}:(kisses.kissTypesUser[$index].count || 0) }}"><i class="lo lo-lips lo-down-2 text-top"></i> {{kisses.kissTypesUser[$index].count ? kisses.kissTypesUser[$index].count : 0}}</span> <img ng-src={{$root.Ajax.imgUrlStatic}}kisses/{{kiss.kisstype}}.png></div><p>{{kiss.title}}</p><button ng-click=sendKiss(kiss.kisstype); class="btn btn-primary">{{ "dynamic.confirm_credits"|translate:{credits: (kiss.credits|formatNumber)}:kiss.credits }}</button></div></div></div><loading-bar ng-show=!kisses></loading-bar></div>')
        }]), angular.module("/template/dialog/login.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/login.html", '<div class="form-signin padding"><div class="modal-header modal-white"><button ng-click=$close() class=close data-dismiss=modal aria-hidden=true>Ã—</button><div class="modal-title ng-binding h2"><p>{{ ::\'login_now\' | translate }}</p><p class=h6>{{ ::\'ru_new\' | translate }} <a class="cursor-pointer underline text-blue" ng-click=$close();switchLoginToRegister()>{{ ::\'register_now\' | translate }}</a></p></div></div><div class=modal-body><login-form></login-form></div></div>')
        }]), angular.module("/template/dialog/match-hit.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/match-hit.html", '<div class=space-before><div class="match-view clearfix"><profile-image id={{$root.Self.picture}} imgsizing=50%;auto element=img size=xl></profile-image><a href={{$root.Ajax.url}}/profile/{{user.id}} ng-click="$root.action(user, \'profile\')" eat-click><profile-image id={{user.picture}} imgsizing=50%;auto element=img size=xl></profile-image></a><div class="absolute-top circle-md bg-pink border-white icon-center"><i class="lo lo-down-1 padding-md fa-3x lo-match text-white"></i></div></div><div class="text-center modal-body"><h2 class="text-light section-title space-after-sm">{{"match.hit.title" | translate}}</h2><p ng-bind-html="\'match.hit.write\' | translate:{\'name\': user.name, \'location\': user.location ? user.location : user.city, \'age\': user.age}"></p><button class="btn btn-danger space-before" ng-click="$close();action(user, \'chat\');">{{"send_msg"|translate}}</button></div><a href="" class="absolute-bottom absolute-left text-gray-dark" ng-click=$close();>{{"match.hit.play_more" | translate}}</a></div>')
        }]), angular.module("/template/dialog/photo-upload.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/photo-upload.html", '<div class=modal-header><button class=close ng-click=$close()>&times;</button><h3 class=modal-title>{{ "upload_pic"|translate }}</h3></div><div class=modal-body><div ng-if=error class="alert alert-danger" bs-alert ng-bind-html=error></div><input type=hidden name=backUrl ng-model="backUrl"><div class="form-group text-center space-after"><file-upload-preview imgsizing=300 watch-input=#profile-picture-upload></file-upload-preview></div><p class=text-center ng-if="uploadProgress > 0"></p><p class=text-center ng-if="uploadProgress > 0"><i class="fa fa-spinner fa-spin space-right-sm"></i>{{uploadProgress}} %</p><div ng-if=$root.Self.picture class=form-group><button class="btn btn-success btn-block text-center" type=button ng-click=doUpload(true) ng-hide="uploadProgress > 0 || error"><i class="fa fa-star space-right-sm"></i> {{"as_new_profile_pic"|translate}}</button></div><div class=form-group><button class="btn btn-info btn-block" type=button ng-click=doUpload() ng-hide="uploadProgress > 0 || error"><i class="fa fa-upload space-right-sm"></i> {{"upload_to_gallery"|translate}}</button></div><div class=form-group><button class="btn btn-danger btn-block" type=button ng-click=$close()><i class="fa fa-times space-right-sm"></i> {{"no_thanks"|translate}}</button></div></div>')
        }]), angular.module("/template/dialog/post-modal.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/post-modal.html", '<div ng-switch=$root.BS.xs class=full-height><div ng-switch-when=false><div class="modal-header modal-white padding-lg clearfix"><div class=pull-left><a ng-href="{{ ::$root.Ajax.url }}/profile/{{ ::post.owner.id }}" class="cursor-pointer pull-left relative close-all-modals"><profile-image id="{{ post.owner.picture }}" size=s class="circle img-sm space-right-sm"></profile-image></a><div class=pull-left><h4 class="gray-dark-links space-after-xs space-before-xs text-bold"><a ng-href="{{ ::$root.Ajax.url }}/profile/{{ ::post.owner.id }}" class="space-right-xs close-all-modals"><span has-emoji>{{ post.owner.name }}, {{ post.owner.age }}</span></a><svg-verified ng-if="post.owner.verified === 1" class="space-right-xs user-verificated text-blue" size=12></svg-verified><online-status user=post.owner></online-status></h4><p ng-if="post.owner.location ||Â post.owner.city" class=no-space-after>{{ ::\'post.from_city\' | translate: {city: post.owner.location || post.owner.city} }}</p></div></div><div class="pull-right space-before-sm text-center"><follow-button type=user value=post.owner class="btn btn-default-inverted-gender border-radius btn-border-2"></follow-button></div><ul class="pull-right no-space-after space-right-sm text-center list-unstyled"><li ng-if="post.owner.postCount !== null" class="pull-left padding-horizontal-xl" ng-style="{borderRight: post.owner.isSelf ? \'1px solid #D2D2D2\' : \'\'}"><p class="h4 space-after-xs space-before-xs text-bold">{{ post.owner.postCount }}</p><p class=no-space-after>{{ ::\'post.posts\' | translate:{}: count: post.owner.postCount }}</p></li><li ng-if=post.owner.isSelf class="pull-left padding-horizontal-xl" style="border-right: 1px solid #D2D2D2"><p class="h4 space-after-xs space-before-xs text-bold">{{ post.owner.counts.fls || 0 }}</p><p class=no-space-after>{{ ::\'feed.follow_you\' | translate }}</p></li><li ng-if=post.owner.isSelf class="pull-left padding-horizontal-xl"><p class="h4 space-after-xs space-before-xs text-bold">{{ post.owner.counts.fld || 0 }}</p><p class=no-space-after>{{ ::\'feed.you_follow\' | translate }}</p></li></ul></div><hr class="no-space-after no-space-before"><div class="modal-body padding-none"><div class="row no-space display-flex"><div class="col-md-7 padding-lg"><img class="full-width space-after-sm" ng-src="{{Â post.getImage({width: 410}).url }}"><p class="gender-links text-center" ng-bind-html=post.getCaptionWithLinks() has-emoji></p><p class="text-gray clearfix no-space-after"><small class=pull-left ng-if=post.location.city><i class="fa fa-map-marker"></i> <span class=space-left-xs>{{ ::post.location.city }}</span></small> <small class=pull-right><i class="fa fa-clock-o"></i> <span class=space-left-xs>{{ ::post.getFormattedTime() }}</span></small></p><hr><div class=clearfix><div class=row><div class=col-md-4><div class="toggle-like pull-left cursor-pointer relative clearfix" ng-click=post.toggleLike() ng-class="post.hasLiked ? \'like\' : \'unlike\'"><i class="pull-left fa fa-heart-o text-gray"></i> <i class="fa fa-heart text-purple absolute"></i></div><small ng-class="{\'text-gray\': !post.likeCount}" class="pull-left space-left-xs">{{ \'post.likes\' | translate: {count: post.likeCount}: post.likeCount }}</small></div><div class="col-md-4 text-center"><i class="fa fa-share-square-o cursor-pointer"></i><div class="relative dropup" style="display: inline-block"><a class="cursor-pointer dropdown-toggle text-gray-dark text-center" data-toggle=dropdown><i class="space-left-sm space-right-sm fa fa-lg fa-ellipsis-h"></i></a><ul class="dropdown-menu dropdown-menu-top text-center"><li class="dropdown text-center" role=presentation><a class=cursor-pointer ng-click=post.openReportFormModal() role=menuitem>{{ ::\'report_profile\' | translate }}</a></li><li class="dropdown text-center" role=presentation><a class=cursor-pointer ng-click=post.owner.block() role=menuitem>{{ ::\'block_user\' | translate }}</a></li></ul></div></div><div class="col-md-4 text-right"><small ng-if=post.replies class=show ng-class="{\'text-gray\': !post.replies.allCount}">{{ \'post.replies\' | translate: {count: post.replies.allCount}: post.replies.allCount }}</small></div></div></div></div><div class="col-md-5 padding-none bg-gray-light"><loading-bar class=space-before-md ng-show="!post.replies || !post.topReplies"></loading-bar><p ng-show="post.replies && !post.replies.posts.length && post.topReplies && !post.topReplies.posts.length" class="padding-horizontal-lg text-center space-before-md no-space-after">{{ ::\'post.no_reply\' | translate }}</p><div ng-if="post.topReplies && post.topReplies.posts.length"><h5 class="padding-horizontal-lg space-before-md space-after-md">{{ ::\'post.top_reply\' | translate }} ({{ \'post.likes\' | translate: {count: post.likeCount}: post.likeCount }})</h5><div ng-repeat="reply in post.topReplies.posts"><div class="padding-horizontal-lg clearfix"><a ng-click=reply.openPostModal() class="pull-left relative space-right-sm space-after-sm cursor-pointer img-sm bg-centered bg-cover" style="border: 2px solid #FFF; z-index: 1; background-image: url({{ ::reply.getImage({width: 60}).urlÂ }})"></a><div class="relative clearfix"><h6><span class=space-right-xs>{{ ::reply.owner.name }}, {{ ::reply.owner.age }}</span><svg-verified ng-if="reply.owner.verified === 1" class="space-right-xs user-verificated text-blue" size=12></svg-verified><online-status user=reply.owner></online-status></h6><small class=gender-links ng-bind-html=reply.getCaptionWithLinks() has-emoji></small> <small class="absolute-top-0 absolute-right-0 text-gray"><i class="fa fa-clock-o"></i> <span class=space-left-xs>{{ ::reply.getFormattedTime() }}</span></small></div></div><hr class="white hr-lg"></div></div><div ng-if="post.replies && post.replies.posts.length"><h5 class="padding-horizontal-lg space-after-md" ng-class="{\'space-before-md\': post.topReplies && !post.topReplies.posts.length }">{{ ::\'post.all_replies\' | translate }} ({{ post.replies.allCount }})</h5><div ng-repeat="reply in post.replies.posts"><div class="padding-horizontal-lg clearfix"><a ng-click=reply.openPostModal() class="pull-left relative space-right-sm space-after-sm cursor-pointer img-sm bg-centered bg-cover" style="border: 2px solid #FFF; z-index: 1; background-image: url({{ ::reply.getImage({width: 60}).urlÂ }})"></a><div class="relative clearfix"><h6><span class=space-right-xs>{{ ::reply.owner.name }}, {{ ::reply.owner.age }}</span><svg-verified ng-if="reply.owner.verified === 1" class="space-right-xs user-verificated text-blue" size=12></svg-verified><online-status user=reply.owner></online-status></h6><small class=gender-links ng-bind-html=reply.getCaptionWithLinks() has-emoji></small> <small class="absolute-top-0 absolute-right-0 text-gray"><i class="fa fa-clock-o"></i> <span class=space-left-xs>{{ ::reply.getFormattedTime() }}</span></small></div></div><hr ng-if=!$last class="white hr-lg"></div></div><hr class="white hr-lg"><div class=padding-horizontal-lg><p class=text-center>{{Â ::\'post.try_app\' | translate }}</p><div class="row space-after-lg"><div class=col-md-6><a class="cursor-pointer show padding-sm"><img class=full-width ng-src="{{ $root.Ajax.imgUrlStatic }}appstore.png"></a></div><div class=col-md-6><a class="cursor-pointer show padding-sm"><img class=full-width ng-src="{{ $root.Ajax.imgUrlStatic }}googleplay.png"></a></div></div></div></div></div></div></div><div ng-switch-when=true class=full-height><div class="bg-cover full-height overflow-scroll" style="background: url({{ ::post.getImage({width: 410}).url }}) center center fixed"><div class="modal-header modal-white clearfix"><button class="pull-left fa fa-2x fa-angle-left close" ng-click=$close()></button><p ng-show=post.replies class="text-center space-before-xs no-space-after">{{ \'post.replies\' | translate: {count: post.replies.allCount}: post.replies.allCount }}</p></div><div ng-show="!post.replies || !post.topReplies" class="padding-lg text-white bg-black-transparent"><loading-bar></loading-bar></div><p ng-show="post.replies && !post.replies.allCount && post.topReplies && !post.topReplies.allCount" class="padding-lg text-center text-white no-space-after bg-black-transparent">{{ ::\'post.no_reply\' | translate }}</p><div class=text-white ng-if="post.topReplies && post.topReplies.posts.length"><h5 class="padding-lg no-space bg-black-transparent text-spacing">{{ ::\'post.top_reply\' | translate }} ({{ \'post.likes\' | translate: {count: post.likeCount}: post.likeCount }})</h5><div ng-repeat="reply in post.topReplies.posts"><div class="padding-lg text-white clearfix bg-black-transparent" style="margin-bottom: 1px"><a ng-click=reply.openPostModal() class="pull-left relative space-right-sm space-after-sm cursor-pointer img-lg bg-centered bg-cover" style="border: 1px solid #FFF; z-index: 1; background-image: url({{ ::reply.getImage({width: 60}).urlÂ }})"></a><div class="relative clearfix"><small><span class=space-right-xs>{{ ::reply.owner.name }}, {{ ::reply.owner.age }}</span><svg-verified ng-if="reply.owner.verified === 1" class="space-right-xs user-verificated text-blue" size=12></svg-verified><online-status user=reply.owner></online-status></small><p class=gender-links ng-bind-html=reply.getCaptionWithLinks() has-emoji></p><p class="absolute-top-0 absolute-right-0 text-gray"><i class="fa fa-clock-o"></i> <span class=space-left-xs>{{ ::reply.getFormattedTime() }}</span></p></div></div></div></div><div class=text-white ng-if="post.replies && post.replies.posts.length"><h5 class="padding-lg no-space bg-black-transparent" style="letter-spacing: 1.55px">{{ ::\'post.all_replies\' | translate }} ({{ post.replies.allCount }})</h5><div ng-repeat="reply in post.replies.posts"><div class="padding-lg text-white clearfix bg-black-transparent" style="margin-bottom: 1px"><a ng-click=reply.openPostModal() class="pull-left relative space-right-sm space-after-sm cursor-pointer img-lg bg-centered bg-cover" style="border: 1px solid #FFF; z-index: 1; background-image: url({{ ::reply.getImage({width: 60}).urlÂ }})"></a><div class="relative clearfix"><small><span class=space-right-xs>{{ ::reply.owner.name }}, {{ ::reply.owner.age }}</span><svg-verified ng-if="reply.owner.verified === 1" class="space-right-xs user-verificated text-blue" size=12></svg-verified><online-status user=reply.owner></online-status></small><p class=gender-links ng-bind-html=reply.getCaptionWithLinks() has-emoji></p><p class="absolute-top-0 absolute-right-0 text-gray"><i class="fa fa-clock-o"></i> <span class=space-left-xs>{{ ::reply.getFormattedTime() }}</span></p></div></div></div></div></div></div></div>');

        }]), angular.module("/template/dialog/profile.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/profile.html", '<div id=profileContainer sticky-header ng-controller=UserDialogProfileCtrl><div class=space-after-sm><div class="modal-body white-box"><button type=button data-dismiss=modal aria-hidden=true style=z-index:1 class="close cursor-pointer absolute-right" ng-click=closeProfile(true)>Ã—</button><div class=row><div class=col-xs-4><div class=relative><profile-image imgsizing=230 size=s category=image background ng-class="{\'user-vip\': !!user.vip}"></profile-image><svg-verified ng-if="user.verified === 1 || user.verified  === true" class="absolute-right absolute-top user-verificated text-blue"></svg-verified></div></div><div class=col-xs-8><div class=space-left has-emoji><h4><i-gender user=user icon-class="lo-down-2 left-indent fa-fw"></i-gender><span class="text-bold text-overflow">{{user.name}}, {{user.age}}</span></h4><h3 ng-if=user.whazzup class="inbox-heading break-word text-light quote">{{user.whazzup}}</h3><p class=space-before-sm><online-status show-description=1 icon-class="left-indent fa-fw" user=user></online-status><span ng-if="user.location || user.city"><br><i class="left-indent fa fa-fw fa-map-marker"></i> <span ng-if=user.location>{{user.location}} <span ng-if="$root.Self && $root.Self.id && user.distance > 0">({{\'distance\'|translate}}: {{user.distance}} km)</span></span> <span ng-if="!user.location && user.city">{{user.city}}</span></span></p></div></div><div class="absolute-bottom absolute-right"><div ng-click="userAction(user, \'like\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'You want\'|translate}}" data-toggle=tooltip data-original-title="{{\'You want\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="fa fa-heart fa-lg"></i></div><div ng-click="userAction(user, \'chat\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'send_msg\'|translate}}" data-toggle=tooltip data-original-title="{{\'send_msg\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="fa fa-envelope fa-lg"></i></div><div ng-click="userAction(user, \'kiss\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'send_kiss\'|translate}}" data-toggle=tooltip data-original-title="{{\'send_kiss\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="lo lo-lips fa-lg"></i></div><div ng-click="userAction(user, \'fan\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'hotlist_add\'|translate}}" data-toggle=tooltip data-original-title="{{\'hotlist_add\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="fa fa-star fa-lg"></i></div></div></div></div></div><div class=overflow-visible><div class="modal-body white-box tab-content overflow-visible"><h4 class=section-title><ul class=list-inline ng-init="action=\'gallery\'"><li ng-class="{\'gallery\': \'active\'}[action]"><a ng-click="action=\'gallery\';" class=cursor-pointer data-toggle=tab><i class="fa fa-picture-o fa-lg"></i> {{"menu_gallery"|translate}} <span class=text-normal>({{user.counts.p}})</span></a></li><li ng-class="{\'flirt\': \'active\'}[action]"><a ng-click="action=\'flirt\';" class=cursor-pointer data-toggle=tab><i class="fa fa-file-text-o fa-lg"></i> {{"Details"|translate}} <span class=text-normal>({{user.counts.details*100|number:0}}%)</span></a></li><li class="pull-right dropdown dropdown-compact"><a class="cursor-pointer dropdown-toggle" data-toggle=dropdown><i class="fa fa-fw fa-cog"></i></a><ul class=dropdown-menu><li class=dropdown role=presentation><a class=cursor-pointer ng-click="userAction(user, \'block\')" role=menuitem>{{"block_do"|translate}}</a></li><li class=dropdown role=presentation><a class=cursor-pointer ng-click="userAction(user, \'report\', {referenceType: \'user\'})" role=menuitem>{{"report_profile"|translate}}</a></li></ul></li></ul></h4><div ng-switch=action><div ng-switch-default id=gallery class="active tab-pane"><div ng-if="$root.Self.picture && user.images" class="masonryContainer opacity0 thumbnail-box"><div class="masonry-item thumbnail thumbnail-slidehover" ng-repeat="image in user.images track by $index" masonize><g-image width="{{user.images.length == 1  ? 710 : 350}}" category=image size=m id={{image.id}}></g-image><div class=caption><i class="fa fa-upload space-right-sm"></i>{{"uploaded"|translate}} {{image.time*1000 | date_diff | translate}}</div></div></div><loading-bar ng-show=!user.images></loading-bar><div ng-if="$root.Self.picture && user.images.length <= 0" class="space-before-sm space-after"><hr class="hr-none padding-negative space-after"><span class=h5>{{\'no_pic_name\'|translate:{\'user\':user.name} }}</span> <span class=h5>{{\'discover_more_name\'|translate:{\'user\':user.name} }}</span><br><a class="cursor-pointer h5" ng-click="userAction(user, \'chat\')">{{\'ask_now\'|translate}}</a></div><div ng-if=!$root.Self.picture class=space-before-sm><div class=row><div class=col-xs-3><div class="thumbnail thumbnail-slidehover"><profile-image gender={{$root.Self.gender}} element=img></profile-image><img ng-src={{uploadImage}}><div class="absolute-bottom absolute-left absolute-right text-center"><div class="btn btn-primary btn-file btn-block"><photo-upload-button watch-id=profile-picture-upload></photo-upload-button></div></div></div></div><div class=col-xs-9><strong>{{\'self.upload.picture\'|translate}}</strong><p class=space-before-sm>{{\'self.upload.picture.info\'|translate:{\'username\': user.name} }}</p></div></div></div></div><div ng-switch-when=flirt id=interview class=tab-pane><div class="padding-negative bg-white padding"><hr class="hr-none padding-negative space-after"><p class=space-after><strong>{{\'details.me\'|translate}}</strong></p><div ng-if=user.flirtInterests.length class=space-after-sm><p>{{::"this_user_looks_for"|translate}}</p><span ng-repeat="userFlirtInterest in user.flirtInterests">{{"intensions."+userFlirtInterest|translate}}<br></span></div><div ng-hide="user.details.me.length <= 0" class=space-after><div class=row><dl class="dl-horizontal text-left"><div class="col-xs-6 space-after-sm" ng-repeat="detail in user.details.me track by $index"><dt class=text-overflow><span class=text-normal title={{detail.item}}>{{detail.item}}</span></dt><dd class=text-overflow>{{detail.label}}</dd></div></dl></div></div><div ng-hide="user.details.flirt.length <= 0" class=space-after><hr class="hr-none padding-negative space-after"><p class="space-after space-before-sm"><strong>{{\'details.flirt\'|translate}}</strong></p><div class=row><div class="col-xs-6 space-after-sm" ng-repeat="detail in user.details.flirt track by $index">{{detail.item}}<br><strong>{{detail.label}}</strong></div></div></div><div ng-show="user.details.me.length <= 0 && user.details.flirt.length <= 0" class=space-after><span class=h5>{{\'no_entry_name\'|translate:{\'user\':user.name} }}</span> <span class=h5>{{\'discover_more_name\'|translate:{\'user\':user.name} }}</span><br><a class="cursor-pointer h5" ng-click="userAction(user, \'chat\')">{{\'ask_now\'|translate}}</a></div></div></div></div></div></div></div>')
        }]), angular.module("/template/dialog/pw-forget.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/pw-forget.html", '<div class=modal-header><button class=close ng-click=$close()>&times;</button><p class="h4 modal-title">{{ ::\'pw_forget\' | translate }}</p></div><div class=modal-body><p>{{ ::\'pw_forget_info\' | translate }}</p><form ng-submit=doReset() name=pwForgetForm id=form><div class=form-group><input type=email class=form-control name=email placeholder="{{ \'your_email\' | translate }}" ng-blur="displayFieldError(\'email\')" ng-required=true ng-model="email"><div class="text-danger form-error" ng-show="pwForgetForm.email.$error.required && displayError.email"><small>{{ ::\'form_error_required\' | translate }}</small></div><div class="text-danger form-error" ng-show="pwForgetForm.email.$error.email && displayError.email"><small>{{ ::\'form_error_email\' | translate }}</small></div></div><div class=form-group><button class="btn btn-primary btn-block" type=submit ng-disabled="pwForgetForm.$invalid || btnDisabled">{{ ::\'send\' | translate }}</button></div></form></div>')
        }]), angular.module("/template/dialog/register.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/register.html", '<div id=modalRegister class="modal-register padding-sm"><div class="modal-header modal-white"><button class="close absolute-top absolute-right" ng-click=$close()>Ã—</button><div class="modal-title h2"><span class=inbox-heading>{{ "register_free" | translate }}</span></div><p class=help-block>{{"one_step_away"|translate}}</p></div><div class=modal-body><register-form></register-form></div></div>')
        }]), angular.module("/template/dialog/report-form.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/report-form.html", '<div class=modal-header><p ng-if="referenceType == \'user\'" class="modal-title h4">{{ ::\'report_profile\' | translate }}</p><p ng-if="referenceType == \'photo\'" class="modal-title h4">{{ ::\'report_post\' | translate }}</p></div><div class="modal-body row space-right-sm"><div class="col-sm-2 col-xs-3"><profile-image ng-if="referenceType == \'user\'" id="{{ referenceObject.picture }}" category=thumb size=s class="img-md img-circle space-left-sm space-before-xs" ng-class="{\'user-vip\': !!referenceObject.vip}"></profile-image><img ng-if="referenceType == \'photo\'" width=84 class="space-left-sm space-before-xs" ng-src="{{ referenceObject.getImage({width: 84}).url }}"></div><div class="col-sm-10 col-xs-9"><div ng-if="referenceType == \'user\'"><div class="space-before-sm space-left">{{ ::\'report.user.description.1\' | translate:{name: referenceObject.name} }}</div><div class="space-before-sm space-left">{{ ::\'report.user.description.2\' | translate }}</div></div><div ng-if="referenceType == \'photo\'"><div class="space-before-sm space-left">{{ ::\'report.post.description.1\' | translate }}</div><div class="space-before-sm space-left">{{ ::\'report.post.description.2\' | translate }}</div></div><div class="space-before-sm space-left">{{ \'report.confirmation\' | translate }}</div><form class="space-before space-left" role=form class=form method=post id=reportform name=reportform ng-submit=submitReportForm()><div class=form-group><p ng-repeat="(key, label) in reportTypes"><input id={{key}} type=radio name=type ng-checked="type == key" ng-click="checkButton(key, \'type\')" ng-val="type"><label for={{key}} class="text-normal space-left-sm">{{label}}</label></p></div><div class=form-group><textarea id=text class=form-control name=text rows=4 placeholder="{{ ::\'optional_message\' | translate }}" ng-model=text ng-maxlength=360 maxlength=360 ng-blur="displayFieldError(\'text\')" ng-bind=text>\n                </textarea><small>{{ \'letters_to_go\' | translate:{letters: 360 - (text.length || 0) } }}</small></div><div class=form-group><button type=submit ng-disabled="reportform.$invalid || btnDisabled" class="btn btn-primary pull-right">{{ ::\'report_now\' | translate }}</button> <button class="btn btn-link-close pull-right" type=button ng-click=$close()>{{ ::\'Close\' | translate }}</button></div></form></div></div>')
        }]), angular.module("/template/dialog/tutorial.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/tutorial.html", '<div class="modal-body row"><button class=close style=margin-right:5px ng-click=$close()>&times;</button><div class="col-xs-3 padding space-before"><img class=img-fullwidth ng-src="{{$root.Ajax.imgUrlStatic }}{{tutorialData.picture}}.png"></div><div class=col-xs-9><div class="space-left space-right"><p class="h1 text-light" ng-bind-html=tutorialData.title></p><p ng-bind-html=tutorialData.text></p><div class="row space-before"><div ng-repeat="action in tutorialData.actions" class=col-xs-6 ng-switch=tutorialData.tutorialType><button ng-switch-when=vip ng-click=execute(action.target) class="btn btn-block" ng-class="{ \'btn-danger\': action.target == \'next\', \'btn-warning\': action.target != \'next\'}">{{action.title}}</button> <button ng-switch-when=credits ng-click=execute(action.target) class="btn btn-block" ng-class="{ \'btn-danger\': action.target == \'next\', \'btn-success\': action.target != \'next\'}">{{action.title}}</button> <button ng-switch-default ng-click=execute(action.target) class="btn btn-block" ng-class="{ \'btn-danger\': action.target == \'next\', \'btn-primary\': action.target != \'next\'}">{{action.title}}</button></div></div><div ng-if=!tutorialData.hideUnsubscriber class=space-before-sm><label class="text-normal pull-left space-before-sm space-right"><input type=checkbox name=state ng-change=setState(state) ng-model=state ng-true-value=0 ng-false-value="1"> {{"not_ask_again"|translate}}</label></div></div></div></div>')
        }]), angular.module("/template/dialog/unlock-user.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/unlock-user.html", "<div class=modal-header><button class=close ng-click=$close()>&times;</button><h4 class=modal-title>{{\"unlock_entry_header\"|translate}}</h4></div><div class=modal-body>{{ 'txt.confirm_unlock_user'|translate:{'credits': unlockCredits} }}</div><div class=modal-footer><label class=\"text-normal pull-left space-before-sm space-right\"><input ng-model=generalConfirmation type=checkbox ng-change=setGeneralConfirmation(generalConfirmation)> {{'not_ask_again'|translate}}</label><button class=\"btn btn-warning\" ng-click=vip()>{{'become_vip'|translate}}</button> <button class=\"btn btn-primary\" ng-click=doUnlock()>{{'unlock_now' | translate}}</button></div>")
        }]), angular.module("/template/dialog/v3-nologin-info.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/v3-nologin-info.html", '<div class=v3-info><div class=modal-header><button class=close ng-click=$close()>&times;</button><h3 class=modal-title>{{ ::\'v3.loginInfo.title\' | translate }}</h3></div><div class=modal-body><div ng-bind-html="::\'v3.loginInfo.subtitle\' | translate"></div><div class=row><div class="col-xs-6 space-before"><a href="https://itunes.apple.com/de/app/lovoo-neue-kostenlose-chat/id445338486?mt=8"><img ng-src="{{ $root.Ajax.imgUrlStatic }}/v3/appstore.png" style="width: 100%"></a></div><div class="col-xs-6 space-before"><a href="https://play.google.com/store/apps/details?id=net.lovoo.android&amp;hl=de"><img ng-src="{{ $root.Ajax.imgUrlStatic }}/v3/googleplay.png" style="width: 100%"></a></div></div></div></div>')
        }]), angular.module("/template/dialog/verify-upload.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/verify-upload.html", '<div class=modal-header><button class=close ng-click=unsetPreviewAndClose()>&times;</button><h4 class=modal-title>{{ "verify.step.second"|translate }}</h4></div><div class=modal-body><div ng-if=error class="alert alert-danger" bs-alert ng-bind-html=error></div><div class=form-group><p ng-bind-html=verify.text></p></div><div class="form-group text-center space-after"><file-upload-preview imgsizing=300 watch-input=#verify-picture-upload></file-upload-preview><p class=text-center ng-if=uploadProgress><i class="fa fa-spinner fa-spin space-before-sm space-right-sm"></i>{{uploadProgress}}</p></div><div ng-show=!hasPreview class=form-group id=formUserUserPictureUpload><input type=hidden name=backUrl ng-model="backUrl"><div class="btn btn-primary btn-file btn-block"><photo-upload-button hide-dialog watch-id=verify-picture-upload></photo-upload-button></div></div><div ng-show=hasPreview class=form-group><button class="btn btn-block btn-success" type=button ng-click=doUpload() ng-disabled=btnDisabled><i class="fa fa-upload space-right-sm"></i> {{ "upload_pic"|translate }}</button></div><div class=form-group><button class="btn btn-danger btn-block" type=button ng-click=unsetPreviewAndClose()><i class="fa fa-times space-right-sm"></i> {{"no_thanks"|translate}}</button></div></div>')
        }]), angular.module("/template/dialog/vip-benefits.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/dialog/vip-benefits.html", '<div class="modal-body text-center" ng-switch=showDefaultImage><div ng-switch-when=true class="relative text-center"><div class="absolute full-width space-before-sm"><div class="row text-bold uppercase"><div class="col-xs-6 text-black"><span class=bg-white style="padding: 2px 10px">{{ ::\'vip_benefit.before\' | translate }}</span></div><div class="col-xs-6 text-white"><span class=bg-orange style="padding: 2px 10px">{{ ::\'vip_benefit.after\' | translate }}</span></div></div></div><img class=full-width ng-src="{{ ::$root.Ajax.imgUrlStatic }}dialog/vip-benefits.jpg"></div><h5 class="text-bold uppercase space-before">{{ ::\'vip_benefit.headline\' | translate }}</h5><p class=space-after>{{ ::\'vip_benefit.text\' | translate }}</p><a ng-switch-when=true ng-href="{{ ::$root.Ajax.url }}/" ng-click=$close() class="btn btn-success space-after-sm">{{ ::\'vip_benefit.btn\' | translate }}</a><div ng-switch-when=false class=row><button class="close absolute-top-0 absolute-right space-before-xs" ng-click=$close()>&times;</button><div class="col-xs-4 ng-animate" ng-repeat="user in users track by user.id"><div class="thumbnail thumbnail-user thumbnail-slidehover thumbnail-square thumbnail-square" ng-class="{\'user-vip\': !!user.vip}"><div thumbnail-cover class="absolute full-width full-height bg-white opacity08" style="z-index: 100" index=$index></div><a href="{{ ::$root.Ajax.url }}/?user_id={{ user.id }}" ng-click="action(user, \'profile\', {crypt: user.crypt, list: $parent.category, func: navigate})" ng-class="{self: isSelf(user)}" eat-click><profile-image class="cursor-pointer img-fullwidth" category=thumb size=l background reload></profile-image><svg-verified ng-if=user.verified class="absolute-right absolute-top user-verificated text-blue"></svg-verified></a><user-hover user=user action-time="{{ user.actionTime }}"></user-hover></div></div></div></div>')
        }]), angular.module("/template/directives/tab-list.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/directives/tab-list.html", '<a ng-repeat="link in links track by $index" ng-hide=link.isHidden ng-href="{{link.isActive ? \'\' : link.url}}" ng-class="{active: link.isActive}"><i ng-class=link.icon class=fa-lg></i> {{ link.title }} <small><span class="label label-default bg-pink" ng-show=counts[link.badge].new>{{ counts[link.badge].count }}</span></small></a>')
        }]), angular.module("/template/directives/validation-errors.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/directives/validation-errors.html", '<div class="alert alert-danger alert-dismissable" ng-show=$parent.validationErrors><p ng-repeat="error in $parent.validationErrors"><strong><small class=capitalize ng-bind-html=error.label></small>: <small ng-bind-html=error.message></small></strong></p></div>')
        }]), angular.module("/template/error.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/error.html", '<div ng-class=$root.routing.containerClass><div class="panel panel-default white-box text-center padding"><p class="h3 text-orange">{{::\'error_occured\'|translate}}</p><p class="h4 space-before-sm space-after-sm"><i class="text-blue fa fa-fw fa-info-circle"></i> <span ng-bind-html="\'faq_info\'|translate:{\'url\': $root.Ajax.url+\'/faq\'}"></span></p><a class="btn btn-info" href-reload="{{$root.Ajax.url}}/">{{::\'Back\'|translate }}</a></div></div>')
        }]), angular.module("/template/exception/buy-credits.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/exception/buy-credits.html", "<div class=form-signin style=\"background-color: #f6f6f6\"><div class=modal-header><button class=close ng-click=$close()>&times;</button><h4 class=modal-title>{{::'buy_credits' | translate}}</h4></div><div class=modal-body><p>{{::'pos_modal.label' | translate}}</p><package-list package-type=2 package-color=green></package-list><div class=modal-footer><a href-reload={{$root.Ajax.url}}/credits/free class=\"btn btn-info\"><i class=\"lo lo-credit lo-down-2 space-right-sm\"></i> {{::'pos_modal.free_credits' | translate}}</a> <button ng-click=$close(); class=\"btn btn-danger\">{{::'cancel' | translate}}</button></div></div></div>")
        }]), angular.module("/template/exception/offer.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/exception/offer.html", '<div class=modal-body><button class=close ng-click=modal.close()>&times;</button> <span class="h2 text-normal">{{data.title}}</span><div class="space-before-sm space-after-sm"><img ng-if="data.picture == \'dialog/match_booster\'" class=img-fullwidth ng-src="{{$root.Ajax.imgUrlStatic }}{{data.picture}}/for-gender-looking-{{$root.Self.genderLooking == 1 ? 1 : 2 }}.jpg"> <img ng-if="data.picture != \'dialog/match_booster\'" class=img-fullwidth ng-src="{{$root.Ajax.imgUrlStatic }}{{data.picture}}/l.jpg"></div><div>{{data.text}} {{data.infoAction.title}}.</div><div class="row space-before-sm"><div class=col-xs-6 ng-init="action = data.actions[0]"><button ng-click="purchase.step=2" ng-disabled class="btn btn-info btn-block">{{action.title}}</button></div><div class=col-xs-6><a href-reload={{$root.Ajax.url}}/vip class="btn btn-warning btn-block">{{\'become_vip\'|translate}}</a></div></div><div class=space-before-sm ng-init="action = data.actions[0]"><div ng-init="package=action.package; packageColor=\'info-blue\'" class="list-group text-left" ng-show="purchase.step==2"><purchase package-type=2></purchase></div></div></div>')
        }]), angular.module("/template/eyecatcher/eyecatcher.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/eyecatcher/eyecatcher.html", '<div ng-if=$root.routing.hasEyeCatcher class=eyecatcher ng-class="{\'eyecatcher-animation\': animateEyecatcher}"><div ng-class="\'container-static-\' + $root.routing.container" class=relative><span class=eyecatcher-space ng-if=$root.isSecurityUser()></span> <a ng-click=addToEyecatcher() class="add-to-eyecatcher thumbnail cursor-pointer" ng-if=$root.isSecurityUser() ng-switch=!!userImgSrc><profile-image ng-switch-when=true id={{$root.Self.picture}} class="img-circle user-userpic" ng-class="{\'user-vip\': !!$root.Self.vip}" size=m></profile-image><div ng-switch-default class=ec_default></div><div class="caption cursor-pointer">{{"txt.flirtstar_set"|translate}}</div></a><div class=eyecatcher-holder ng-repeat="user in users | limitTo: limit"><a href={{$root.Ajax.url}}/profile/{{user.id}} ng-click=openProfileModal(user) class="thumbnail userpic cursor-pointer" user-preview><profile-image class="img-circle img-sm" size=s linked=true ng-class="{\'user-vip\': user.vip}"></profile-image><svg-verified ng-if=user.verified class="text-blue user-verificated"></svg-verified></a></div><div class="text-right eyecatcher-link" ng-if=$root.isSecurityUser()><a href={{$root.Ajax.url}}/eyecatcher class="h6 text-gray-dark">{{ "all_results"|translate }}</a></div></div></div>')
        }]), angular.module("/template/feed/advanced-feed.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/advanced-feed.html", '<div class=row-main ng-if=feedOptions.feed ng-switch="feedOptions.feed.posts.length > 0"><div ng-switch-when=true ng-if=!feedOptions.isHidden class=masonryContainer><div class="masonry-item invisible" ng-style="{width: $root.BS.xs ? \'100%\' : \'50%\' }" style="padding: 0 16px; box-sizing: border-box" ng-repeat="post in feedOptions.feed.posts" masonize-feed><ng-include src="\'/template/feed/post/\' + post._type + \'.html\'"></ng-include></div></div><div ng-switch-when=false class=col-xs-12><div class="padding bg-white text-center" ng-switch=type><span ng-switch-when=hashtag>{{ ::\'hashtag.hashtag_has_no_posts\' | translate }}</span> <span ng-switch-when=challenge>{{ ::\'challenge.challenge_has_no_posts\' | translate }}</span></div></div></div>')
        }]), angular.module("/template/feed/feed-page.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/feed-page.html", '<div class="row-main space-before feed-page"><div class="col-sm-12 col-md-2-5 col-main relative space-after"><div class=feed-page-sidebar><div class=bg-white><div ng-click="setType($event, \'timeline\')"><div class=cursor-pointer><input type=radio ng-model=type name=search-options-type ng-change=feedOptions.getFeed(true) value="timeline"> <span class="space-left-xs cursor-pointer">{{ ::\'feed.i_follow\' | translate }}</span></div></div><div ng-click="setType($event, \'pictures\')"><div class=cursor-pointer><input type=radio ng-model=type name=search-options-type ng-change=feedOptions.getFeed(true) value="pictures"> <span class="space-left-xs cursor-pointer">{{ ::\'feed.pictures\' | translate }}</span></div><div ng-class="{inactive: type != \'pictures\'}" class=inner><label class=collapsed data-toggle=collapse data-target=#collapse-sidebar-trending-hashtags><i class="fa fa-caret-down sidebar-icon"></i>{{ ::\'feed.trending_hashtags\' | translate }} <span class=text-gray-light ng-if="(trendingHashtags | filter: {ngModel: true}).length && type == \'pictures\'">({{ \'feed.active_count\' | translate: {count: (trendingHashtags | filter: {ngModel: true}).length} }})</span></label><ul hashtag-checklist=trendingHashtags feed-options=feedOptions class="list-unstyled collapse" id=collapse-sidebar-trending-hashtags></ul><label class=collapsed data-toggle=collapse data-target=#collapse-sidebar-distance-pictures><i class="fa fa-caret-down sidebar-icon"></i>{{ ::\'feed.radius\' | translate }}</label><ul class="list-unstyled collapse" id=collapse-sidebar-distance-pictures collapse-refresh><li class=space-right-sm><location-field callback=setLocation preset-location=presetLocation></location-field><slider style="left: -4px" class=slider-default floor=1 ceiling="{{ maxDistance }}" step=1 move-end=onChangeSlider ng-model=distance translate=convertDistanceToString></slider></li></ul></div></div><div ng-click="setType($event, \'people\')"><div class=cursor-pointer><input type=radio ng-model=type name=search-options-type ng-change=feedOptions.getFeed(true) value="people"> <span class="space-left-xs cursor-pointer">{{ ::\'feed.people\' | translate }}</span></div><div ng-class="{inactive: type != \'people\'}" class=inner><label class=collapsed data-toggle=collapse data-target=#collapse-sidebar-age><i class="fa fa-caret-down sidebar-icon"></i>{{ ::\'feed.age\' | translate }}</label><ul class="list-unstyled collapse" id=collapse-sidebar-age collapse-refresh><li class=space-right-sm><slider style="left: -4px" class=slider-default floor="{{ $root.AgeRange.min }}" ceiling="{{ $root.AgeRange.max }}" step=1 move-end=onChangeSlider ng-model-low=ageFrom ng-model-high=ageTo translate=convertAgeToString></slider></li></ul><label class=collapsed data-toggle=collapse data-target=#collapse-sidebar-genders><i class="fa fa-caret-down sidebar-icon"></i>{{ ::\'feed.gender\' | translate }}</label><ul class="list-unstyled collapse" id=collapse-sidebar-genders><li ng-repeat="gender in genders"><label><span class=sidebar-icon><input ng-change=feedOptions.getFeed(true) ng-model=gender.ngModel type="checkbox"></span>{{ ::gender.name }}</label></li></ul><label class=collapsed data-toggle=collapse data-target=#collapse-sidebar-distance-people><i class="fa fa-caret-down sidebar-icon"></i>{{ ::\'feed.radius\' | translate }}</label><ul class="list-unstyled collapse" id=collapse-sidebar-distance-people collapse-refresh><li class=space-right-sm><location-field callback=setLocation preset-location=presetLocation></location-field><slider style="left: -4px" class=slider-default floor=1 ceiling="{{ maxDistance }}" step=1 move-end=onChangeSlider ng-model=distance translate=convertDistanceToString></slider></li></ul></div></div></div><div class=space-before><footer-sidebar></footer-sidebar></div></div></div><div class="col-sm-12 col-md-7 col-main"><feed type=hashtag feed-options=feedOptions></feed><loading-bar class=space-before ng-if=feedOptions.isLoadingFeed></loading-bar></div><div class="col-sm-12 col-md-2-5 col-main relative"><a ng-if="!$root.BS.xs && !$root.BS.sm" class=cursor-pointer style="bottom: 25px; display: none; position: fixed; z-index: 1" scroll-top ignore-footer><img ng-src="{{ ::$root.Ajax.imgUrlStatic }}/btn-to-top.png"></a></div></div>')
        }]), angular.module("/template/feed/follow-button.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/follow-button.html", "<button ng-class=\"{'btn-is-loading': isLoading}\" class=btn-follow ng-click=clickButton()><span><i ng-class=\"hasFollowed ? 'active' : 'inactive'\" class=\"space-left-xs space-right-xs relative\"></i> {{ actionName }}</span></button>")
        }]), angular.module("/template/feed/hashtag-checklist.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/hashtag-checklist.html", '<li ng-repeat="hashtag in hashtags"><label class="overflow-hidden nowrap"><span class=sidebar-icon><input ng-change=feedOptions.getFeed(true) ng-model=hashtag.ngModel type="checkbox"></span>#{{ ::hashtag.hashtag }}</label></li><li class=feed-add-hashtag><label class="overflow-hidden nowrap"><i ng-click=addHashtag() class="fa fa-plus sidebar-icon"></i> <input class=full-width ng-enter=addHashtag() ng-model=newHashtagName placeholder="{{ ::\'feed.add_hashtag\' | translate }}" type="text"></label></li>')
        }]), angular.module("/template/feed/post/photo.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/post/photo.html", '<div class="bg-white space-after text-center clearfix"><div ng-if=post.isMostPopular class="bg-gender padding text-white text-spacing">{{ ::\'feed.most_popular\' | translate }}</div><div class=relative><div class="feed-post-hover absolute full-width full-height"><div class="text-vertical-center text-white"><i class="space-left-sm space-right-sm cursor-pointer fa fa-lg fa-share-square-o"></i> <i class="space-left-sm space-right-sm cursor-pointer fa fa-lg fa-search-plus" ng-click=post.openPostModal()></i><div class="relative dropup" style="display: inline-block"><a class="cursor-pointer dropdown-toggle text-white show" data-toggle=dropdown><i class="space-left-sm space-right-sm fa fa-lg fa-ellipsis-h"></i></a><ul class="dropdown-menu dropdown-menu-top text-center"><li class="dropdown text-center" role=presentation><a class=cursor-pointer ng-click=post.openReportFormModal() role=menuitem>{{ ::"report_profile" | translate }}</a></li><li class="dropdown text-center" role=presentation><a class=cursor-pointer ng-click=post.owner.block() role=menuitem>{{ ::\'block_user\' | translate }}</a></li></ul></div></div></div><img class=full-width feed-image post="post"></div><a ng-href="{{ ::$root.Ajax.url }}/profile/{{ ::post.owner.id }}" class="cursor-pointer relative img-sm inline-block" style="margin-top: -32px; z-index: 1"><profile-image id="{{ post.owner.picture }}" category=thumb size=s class="img-circle img-sm inline-block" ng-class="post.owner.vip ? \'user-vip\' : \'user-non-vip\'"></profile-image><svg-verified ng-if="post.owner.verified == 1" class="user-verificated text-blue absolute" size=14 style="right: -4px; bottom: 4px"></svg-verified></a><div class="space-left-sm space-right-sm"><div ng-switch=post._type class="text-gray gray-links"><small has-emoji ng-bind-html=post.getPostTypeDescription()></small></div><p class=gender-links ng-bind-html=post.getCaptionWithLinks() has-emoji></p><p class="text-gray clearfix no-space-after"><small class=pull-left ng-if=post.location.city><i class="fa fa-map-marker"></i> <span class=space-left-xs>{{ ::post.location.city }}</span></small> <small class=pull-right><i class="fa fa-clock-o"></i> <span class=space-left-xs>{{ ::post.getFormattedTime() }}</span></small></p></div><hr><div class="space-left-sm space-right-sm space-after-sm relative clearfix"><div class="toggle-like pull-left cursor-pointer clearfix" ng-click=post.toggleLike() ng-class="post.hasLiked ? \'like\' : \'unlike\'"><i class="pull-left fa fa-heart-o text-gray"></i> <i class="fa fa-heart text-purple absolute"></i></div><small ng-class="{\'text-gray\': post.likeCount == 0}" class="pull-left space-left-xs">{{ \'post.likes\' | translate: {count: post.likeCount}: post.likeCount }}</small> <span ng-click="((post.replyCount > 0 && $root.BS.xs) ||Â !$root.BS.xs) ? post.openPostModal() : \'\'" ng-class="{\'cursor-pointer\': ((post.replyCount > 0 && $root.BS.xs) ||Â !$root.BS.xs)}"><small class=pull-right ng-class="{\'text-gray\': post.replyCount == 0}">{{ \'post.replies\' | translate: {count: post.replyCount}: post.replyCount }}</small></span></div></div>');

        }]), angular.module("/template/feed/post/profile-details.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/post/profile-details.html", '<div class="bg-white space-after profile-details hover-base"><h5 class="relative bg-gender text-white padding-lg text-bold no-space-after"><i ng-if=post.owner.isSelf class="cursor-pointer lo lo-pencil hover-show pull-right" ng-click=post.openEditProfileModal()></i><div ng-if=!post.owner.isSelf class=pull-right><a class="cursor-pointer dropdown-toggle text-white" data-toggle=dropdown><i class="cursor-pointer fa fa-ellipsis-h"></i></a><ul class="dropdown-menu nose-top-right-xs box-shadow-default space-before-xs"><li class=dropdown role=presentation><a class=cursor-pointer ng-click=post.owner.report() role=menuitem>{{ ::\'report_profile\' | translate }}</a></li><li class=dropdown role=presentation><a class=cursor-pointer ng-click=post.owner.block() role=menuitem>{{ ::\'block_user\' | translate }}</a></li></ul></div>{{ post.owner.name }}, {{ post.owner.age }}</h5><div class=padding><div ng-if=post.owner.location><i class="fa-fw lo lo-pin lo-down-2 text-gray space-right-xs"></i>{{ ::\'from\' | translate }} {{ post.owner.location }}</div><online-status show-description=1 icon-class="fa-fw space-right-xs" user=post.owner class="show space-before-xs"></online-status><div ng-switch=!!post.owner.verified class=space-before-xs><div ng-switch-when=true><svg-verified class="text-blue fa-fw lo-down-2 space-right-xs" size=15 user=post.owner></svg-verified>{{ ::\'verified\' | translate }}</div><div ng-switch-when=false><svg-verified class="text-blue-light fa-fw lo-down-3 space-right-xs" size=15 user=post.owner></svg-verified><span ng-switch=post.owner.isSelf><a ng-switch-when=true href={{$root.Ajax.url}}/verify>{{ ::\'not_verified\' | translate }}</a><span ng-switch-when=false>{{ ::\'not_verified\' | translate }}</span></span></div></div></div><div ng-if=!post.owner.isSelf class="padding clearfix"><button ng-click=post.owner.chat() class="pull-left btn btn-default-inverted-gender btn-radius btn-border-2 border-radius"><i class="fa fa-fw fa-comment-o space-left-xs space-right-xs"></i>{{ ::\'profile.details.chat\' | translate }}</button><follow-button type=user value=post.owner class="pull-right btn btn-default-inverted-gender btn-radius btn-border-2 border-radius"></follow-button></div><tabset justified=true masonize-update-height=ul class=space-before-sm><tab heading="{{ ::\'kat.flirt\' | translate }}"><div class=padding><p ng-if=post.owner.freetext ng-bind-html=post.owner.freetext></p><p ng-if=!post.owner.freetext ng-switch=post.owner.isSelf><span ng-switch-when=true>{{ ::\'profile.details.no_freetext_self\' | translate: {name: post.owner.name} }} <a ng-click=post.openEditProfileModal() class=cursor-pointer>{{ ::\'complete_profile\' | translate }}</a></span> <span ng-switch-when=false>{{ ::\'profile.details.no_freetext\' | translate: {name: post.owner.name} }} {{ ::\'discover_more_name\' | translate: {user: post.owner.name} }} <a ng-click=post.owner.chat() class=cursor-pointer>{{ ::\'ask_now\' | translate }}</a></span></p><div ng-if=post.owner.interests.length><div class="text-gray text-spacing space-after-xs">{{ ::\'interests\' | translate }}</div><div class=clearfix><span ng-repeat="hashtag in post.owner.interests" ng-class="{\'no-space-after\': $last}" class="gender-links space-right-xs space-after-xs pull-left"><a ng-href="{{ ::$root.Ajax.url }}/hashtag/{{ hashtag }}">#{{ hashtag }}</a></span> <span ng-if=!post.owner.interests>{{ ::\'nothing_found\' | translate }}</span></div></div></div></tab><tab heading="{{ ::\'details.me\' | translate }}"><div class="row no-space media-list-devided"><div class="col-xs-12 media padding" ng-show=post.owner.homeLocation><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-map-marker"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ ::\'profile.details.title.hometown\' | translate }}</span><div class=text-semi-bold>{{ post.owner.homeLocation.location }}</div></div></div></div><div class="col-xs-12 media padding" ng-show="post.owner.genderLooking != undefined && post.owner.isSelf"><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw lo lo-venus-mars"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ ::\'genderLooking\' | translate }}</span><div class=text-semi-bold>{{ $root.Genders[post.owner.genderLooking].genderGroupTranslated }} ({{ post.owner.ageRange.from }} - {{ post.owner.ageRange.to}})</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.flirt.rel><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-circle-o-notch"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ ::post.owner.details.flirt.rel.item }}</span><div class=text-semi-bold>{{ post.owner.details.flirt.rel.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show="post.owner.details.me.size || post.owner.details.me.fig"><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-user"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ ::\'profile.details.title.look\' | translate }}</span><div class=text-semi-bold>{{ post.owner.details.me.size.label }}, {{ post.owner.details.me.fig.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.me.child><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-child"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ ::post.owner.details.me.child.item }}</span><div class=text-semi-bold>{{ post.owner.details.me.child.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.me.smoke><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-magic"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ post.owner.details.me.smoke.item }}</span><div class=text-semi-bold>{{ post.owner.details.me.smoke.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.me.religion><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-warning"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ post.owner.details.me.religion.item }}</span><div class=text-semi-bold>{{ post.owner.details.me.religion.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.me.life><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-home"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ post.owner.details.me.life.item }}</span><div class=text-semi-bold>{{ post.owner.details.me.life.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.me.educ><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-graduation-cap"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ post.owner.details.me.educ.item }}</span><div class=text-semi-bold>{{ post.owner.details.me.educ.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.details.me.job><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-briefcase"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ post.owner.details.me.job.item }}</span><div class=text-semi-bold>{{ post.owner.details.me.job.label }}</div></div></div></div><div class="col-xs-12 media padding" ng-show=post.owner.languages.length><div class=row><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw fa-globe"></i></div><div class="col-xs-10 text-overflow"><span class=text-gray>{{ ::\'profile.details.title.languages\' | translate }}</span><div class=text-semi-bold><span ng-repeat="language in post.owner.languages">{{ $root.Languages[language] }} <span ng-hide=$last>,&nbsp;</span></span></div></div></div></div></div></tab></tabset></div>')
        }]), angular.module("/template/feed/post/profile-picture.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/post/profile-picture.html", '<div class="bg-white space-after text-center clearfix profile-pictures"><div class=relative><img class=full-width feed-image post="post"></div><div ng-show="post.pictures.length || post.owner.id == $root.Self.id" class=space-before><div class="text-gray text-spacing space-after-xs" ng-bind="::\'profile.more_profile_pictures\' | translate"></div><div class="row padding display-flex"><div class=col-xs-4 ng-repeat="picture in post.pictures | limitTo: 3"><div class="bg-centered bg-cover full-width" style="height: 97px" ng-style="{backgroundImage: \'url(\' + post.getImageFromImageArray({width: 92, category: \'thumb\', images: picture}).url + \')\'}"></div></div><div class="col-xs-4 empty flexbox-vertical-fill" ng-style="{height: (!post.pictures.length ? \'108px\' : \'inherit\')}" ng-if="post.owner.id == $root.Self.id && post.pictures.length < 3" ng-repeat="placeholder in [] | range: (3 - post.pictures.length)"><div class="full-width relative text-gender"><div class="cursor-pointer text-center upload-picture flexbox-vertical-center border-gender"><div><i class="fa fa-lg lo lo-add"></i> <span class=show>{{ ::\'upload_pic\' | translate }}</span></div></div></div></div></div></div></div>')
        }]), angular.module("/template/feed/post/widget-hashtag.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/post/widget-hashtag.html", '<div class="bg-white space-after text-center clearfix"><div class="bg-gender padding text-white text-spacing">{{ ::\'feed.widget_hashtag_headline\' | translate }}</div><div class="bg-cover bg-centered bottom-shadow full-width space-after-sm relative" style="height: 170px" ng-style="{backgroundImage: \'url(\' + post.getImage({width: 336}).url + \')\'}"><div class="white-links inline-block padding-sm space-before-lg" style="background-color: rgba(0, 0, 0, .5)"><a ng-href="{{ ::$root.Ajax.url }}/hashtag/{{ ::post.hashtag }}" class="show text-spacing relative" style="border: 1px solid #FFF; padding: 10px 20px; z-index: 1">#{{ ::post.hashtag }}</a></div><div class="absolute-bottom absolute-right text-spacing text-white">{{ ::\'hashtag.pictures\' | translate: {posts: post.allCount}: post.allCount }}</div></div><div class="space-left-sm space-right-sm space-after-sm"><p class="space-after-xs text-gray small">{{ ::\'feed.widget_hashtag_description\' | translateÂ }}</p><div class=row style="height: 103px"><div ng-repeat="hashtagPost in post.hashtagPosts" class="col-xs-4 full-height padding-sm"><a class="bg-centered bg-cover cursor-pointer full-height show" ng-click=hashtagPost.openPostModal() ng-style="{backgroundImage: \'url(\' + hashtagPost.getImage({width: 90}).url + \')\'}"></a></div></div></div><follow-button type=hashtag value=post.hashtag class="btn btn-default-inverted-gender btn-border-2 border-radius space-after-md text-spacing text-normal"></follow-button></div>')
        }]), angular.module("/template/feed/read-more-text.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/read-more-text.html", '<p>{{ texts[0] }} <span ng-if="texts.length == 2"><span ng-hide=showFullText>...</span> <span ng-show=showFullText>{{ texts[1] }}</span> <a class=cursor-pointer ng-click=toggleText()><span ng-hide=showFullText>{{::"feed.read_more"|translate }}</span> <span ng-show=showFullText>{{::"feed.read_less"|translate }}</span></a></span></p>')
        }]), angular.module("/template/feed/show-more-posts.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/show-more-posts.html", '<div class="thumbnail-square relative"><div class="absolute full-width full-height bg-purple bg-hover-dark text-white"><p class="text-center text-vertical-center padding">{{ ::\'feed.show_all_post\' | translate }}</p></div></div>')
        }]), angular.module("/template/feed/simple-feed.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/feed/simple-feed.html", '<div ng-switch="feedOptions.feed.posts.length > 0"><div ng-switch-when=true ng-switch=$root.BS.xs><div ng-switch-when=false><div class=row><div ng-repeat="post in feedOptions.feed.posts" class="col-sm-3 col-md-2 space-after-sm"><img post-id="{{ ::post.id }}" class=cursor-pinter width=100% ng-src="{{ ::post.getImageUrl({size: \'l\', category: \'thumb\'}) }}"></div><div class="col-sm-3 col-md-2 space-after-sm"><show-more-posts></show-more-posts></div></div></div><div ng-switch-when=true class=masonryContainer><div class="masonry-item space-after-sm" style="padding: 0 5px; box-sizing: border-box; width: 50%" ng-repeat="post in feed" masonize masonize-gitter=0><img width=100% ng-src="{{ ::post.getImageUrl({size: \'l\', category: \'thumb\'}) }}"></div><div class="masonry-item space-after-sm" style="padding: 0 5px; box-sizing: border-box; width: 50%" masonize masonize-gitter=0><show-more-posts></show-more-posts></div></div></div><div ng-switch-when=false ng-class="{\'text-center\': $root.BS.xs}" ng-switch=type><span ng-switch-when=hashtag>{{ ::\'hashtag.hashtag_has_no_posts\' | translate }}</span> <span ng-switch-when=challenge>{{ ::\'challenge.challenge_has_no_posts\' | translate }}</span></div></div>')
        }]), angular.module("/template/flirtmatch.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/flirtmatch.html", '<div class="panel match panel-default white-box"><div ng-switch=gameStatus><div ng-switch-when=0 class="space-before space-left space-after"><loading-bar class=space-left-sm></loading-bar></div><div ng-switch-when=1 ng-cloak><div class=row><div class="col-xs-6 match-user-image-bg cursor-pointer" profile-background-image on-loaded=imageLoaded show-profile><svg-verified ng-if=user.verified class="absolute-right absolute-top text-blue"></svg-verified><div class="match-panel text-center padding-lg"><div class="btn-group match-icon-hover"><div class="text-white fa-3x circle-md cursor-pointer circle-inner space-right no" ng-class="{\'match-icon-pop\': popAnimation == \'no\'}" ng-click="vote(user, 0)"><i class="lo lo-close padding-md"></i></div><div class="text-white fa-3x circle-md cursor-pointer circle-inner space-left yes" ng-class="{\'match-icon-pop\': popAnimation == \'yes\'}" ng-click="vote(user, 1)"><i class="fa fa-heart padding-md lo-down-05"></i></div></div></div></div><div class="col-xs-6 padding-lg modal-users-sidebar nose-left"><div class="space-left-sm space-right-sm"><h2 ng-if="user.whazzup != \'\'" class="media-heading space-after text-light quote break-word" has-emoji>{{user.whazzup}}</h2><p><span>{{ user.age }} {{ ::\'years\' | translate }}</span> <span ng-if=user.location>{{::"from"|translate}} {{user.location}}</span> <span ng-if="!user.location && user.city">{{::"from"|translate}} {{user.city}}</span></p><div ng-if="user.flirtInterests.length && $root.Self.vip"><p>{{::"this_user_looks_for"|translate}}</p><span ng-repeat="userFlirtInterest in user.flirtInterests">{{"intensions."+userFlirtInterest|translate}}<br></span></div><p class=space-before><a href={{::$root.Ajax.url}}/profile/{{user.id}} class="underline text-blue cursor-pointer" show-profile eat-click>{{::"show_profile"|translate}}</a></p><div ng-if=!$root.Self.vip class="absolute-bottom text-center space-after-sm"><dfp-banner slot-id=div-gpt-ad-1404396289262-1></dfp-banner></div><div ng-show="$root.Self.vip && users.length > 1" class=absolute-bottom><p>{{ ::"next_match" |translate }}:</p><span class="pull-left space-right-sm user-userpic" ng-repeat="user in users" ng-show=!$first><profile-image id={{user.picture}} size=m class=invisible ng-if="$index < 4" category=image></profile-image><profile-image id={{user.picture}} size=s class="img-circle img-xs"></profile-image></span></div></div></div></div><div id=flirtmatch_nothing ng-hide=user>{{::"nothing_found"|translate}}</div></div><div ng-switch-when=2324 class="space-before space-left space-after row"><div class=col-xs-2><profile-image id={{::$root.Self.picture}} category=thumb size=m class=img-md ng-class="{\'user-vip\': !!self.vip}"></profile-image></div><div class=col-xs-10><p>{{"upload.picture"|translate}}</p><div class="btn btn-primary btn-file"><photo-upload-button watch-id=profile-picture-upload></photo-upload-button></div></div></div><div ng-switch-default class="space-before space-left space-after" ng-cloak>{{::"nothing_found"|translate}}</div></div></div>')
        }]), angular.module("/template/footer/footer-sidebar.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/footer/footer-sidebar.html", "<div class=\"text-tiny text-gray-dark gray-dark-links clearfix\"><p class=no-space-after><a target=_blank class=cursor-pointer href=\"http://static.lovoo.net/agb/{{ switchLang }}\">{{ ::'br_short' | translate }}</a> | <a target=_blank class=cursor-pointer href=\"http://static.lovoo.net/legal/{{ switchLang }}\">{{ ::'data_security' | translate }}</a> | <a target=_blank href=\"{{ $root.Ajax.url }}/imprint\">{{ ::'Imprint' | translate }}</a></p><p>{{ ::'footer_sidebar.copyright' | translate: {year: currentYear} }}</p></div>")
        }]), angular.module("/template/footer/footer.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/footer/footer.html", '<div ng-hide="$root.routing.footer == \'hidden\'"><footer ng-if="style == \'extensive\'" class=responsive-footer><div ng-class=$root.routing.containerClass><div class="row space-before space-after"><div class="col-xs-12 col-sm-2 col-md-4"><section class=row><div class="col-xs-6 col-sm-12 col-md-6"><h5><i class="fa fa-fw fa-apple left-indent hidden-xs pull-left"></i> {{ ::\'lovoo_iphone\' | translate }}</h5><a target=_blank href="http://itunes.apple.com/{{ locale }}/app/lovoo/id445338486?mt=8" title="App Store">{{ ::\'footer.download_now\' | translate }}</a><p class=no-space-after>&nbsp;</p><h5>{{ ::\'lovoo_ipad\' | translate }}</h5><a target=_blank href="http://itunes.apple.com/{{ locale }}/app/lovoo/id445338486?mt=8" title="App Store">{{ ::\'footer.download_now\' | translate }}</a><p class="hidden-md hidden-lg no-space-after">&nbsp;</p></div><div class="col-xs-6 col-sm-12 col-md-6"><h5><i class="fa fa-fw fa-android left-indent hidden-xs pull-left"></i> {{ ::\'lovoo_android\' | translate }}</h5><a target=_blank href="https://market.android.com/details?id=net.lovoo.android" title="Google Play">{{ ::\'footer.download_now\' | translate }}</a></div></section></div><div class="col-xs-12 col-sm-7 col-md-6"><section class=row><div class="col-xs-12 col-sm-4 col-md-4"><hr class="visible-xs rainbow-top"><h5>{{ ::\'support_n_help\' | translate }}</h5><a target=_blank href="{{ ::$root.getHelpcenterLink() }}" class=cursor-pointer>{{ ::\'footer.link_faq\' | translate }}</a><br><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/leitfaden-tipps-zur-sicherheit/">{{ ::\'security_hints_new\' | translate }}</a><br><span ng-if="switchLang == \'de\'"><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/unsere-bilder-guideline-im-lovoo-network/">{{ ::\'footer.pic_guideline\' | translate }}</a><br><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/guidelines-fuer-den-umgang-bei-lovoo/">{{ ::\'footer.netiquette\' | translate }}</a><br></span> <span ng-if="switchLang != \'de\'"><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/picture-guidelines/">{{ ::\'footer.pic_guideline\' | translate }}</a><br><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/netiquette/">{{ ::\'footer.netiquette\' | translate }}</a><br></span> <a target=_blank href="http://inside.lovoo.{{ switchDomain }}/category/testberichte/">{{ ::\'test_n_experience_new\' | translate }}</a></div><div class="col-xs-6 col-sm-4 col-md-4"><hr class="visible-xs rainbow-top"><h5>{{ ::\'about_brand\' | translate:{brand: \'LOVOO\'} }}</h5><a target=_blank href="http://inside.lovoo.{{ switchDomain }}">{{ ::\'Inside LOVOO\' | translate }}</a><br><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/jobs/">{{ ::\'career_n_jobs_new\' | translate }}</a><br><a target=_blank href="{{ ::$root.Ajax.url }}/imprint">{{ ::\'Imprint\' | translate }}</a><br><a target=_blank href="http://static.lovoo.net/agb/{{ switchLang }}">{{ ::\'br_short\' | translate }}</a><br><a target=_blank href="http://static.lovoo.net/legal/{{ switchLang }}">{{ ::\'data_security\' | translate }}</a></div><div class="col-xs-6 col-sm-4 col-md-4"><hr class="visible-xs rainbow-top"><h5>{{ ::\'footer.business\' | translate }}</h5><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/business/affiliate/">{{ ::\'affiliate_new\' | translate }}</a><br><a target=_blank href="http://inside.lovoo.{{ switchDomain }}/business/boost/">{{ ::\'App Promotion\' | translate }}</a><br><a target=_blank href=http://lovoo.com/talents>{{ ::\'talents\' | translate }}</a><br></div></section></div><div class="col-xs-12 col-sm-3 col-md-2"><section class=row><div class=col-xs-12><hr class="visible-xs rainbow-top"><h5 class=text-overflow>{{ ::\'love_it\' | translate }}</h5><h5><div class=fb-like data-href=https://facebook.com/lovoo data-layout=button_count data-action=like data-show-faces=true data-share=false></div></h5><h5 style="overflow: hidden"><div class=g-plusone data-size=medium data-annotation=bubble></div><script type=text/javascript>window.___gcfg = {lang: Translator.languageCodes[Translator.locale]};\n                                (function () {\n                                    var po = document.createElement(\'script\');\n                                    po.type = \'text/javascript\';\n                                    po.async = true;\n                                    po.src = \'https://apis.google.com/js/client:plusone.js\';\n                                    var s = document.getElementsByTagName(\'script\')[0];\n                                    s.parentNode.insertBefore(po, s);\n                                })();</script></h5><h5><a href=https://twitter.com/lovoo class=twitter-follow-button data-show-count=false data-lang="{{ $root.locale }}">{{ ::\'Follow us\' | translate }}</a><script>!function (d, s, id) {\n                                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? \'http\' : \'https\';\n                                    if (!d.getElementById(id)) {\n                                        js = d.createElement(s);\n                                        js.id = id;\n                                        js.src = p + \'://platform.twitter.com/widgets.js\';\n                                        fjs.parentNode.insertBefore(js, fjs);\n                                    }\n                                }(document, \'script\', \'twitter-wjs\');</script></h5></div></section></div></div></div></footer><footer ng-if="style == \'compact\'" class=compact-footer><div ng-class=$root.routing.containerClass class=clearfix><span class="pull-left links"><a target=_blank href="http://static.lovoo.net/legal/{{ switchLang }}">{{ ::\'data_security\' | translate }}</a><span class="space-left-sm space-right-sm">|</span> <a target=_blank href="http://static.lovoo.net/agb/{{ switchLang }}">{{ ::\'br_short\' | translate }}</a><span class="space-left-sm space-right-sm">|</span> <a target=_blank href="{{ ::$root.Ajax.url }}/imprint">{{ ::\'Imprint\' | translate }}</a></span> <span class="pull-right brand">{{ ::\'footer.company\' | translate }} {{ currentYear }}</span></div></footer><footer class=brand-footer><div ng-class=$root.routing.containerClass><div class=row><div class="col-xs-12 text-center"><h6 ng-if="style == \'extensive\'" class="space-before-sm space-after-sm">&copy; LOVOO {{ currentYear }}. {{ ::\'footer.rights_reserved\' | translate }}</h6><a target=_blank href=http://instagram.com/lovooapp title="{{ ::\'footer.social_button_title\' | translate: {social: \'Instagram\'} }}"><i class="lo lo-instagram fa-2x space-right"></i></a> <a target=_blank href=http://www.youtube.com/lovootv title="{{ ::\'footer.social_button_title\' | translate: {social: \'Youtube\'} }}"><i class="lo lo-youtube fa-2x space-right"></i></a> <a target=_blank href=http://www.pinterest.com/lovoo title="{{ ::\'footer.social_button_title\' | translate: {social: \'Pinterest\'} }}"><i class="lo lo-pinterest fa-2x space-right"></i></a> <a target=_blank href=https://twitter.com/lovoo title="{{ ::\'footer.social_button_title\' | translate: {social: \'Twitter\'} }}"><i class="lo lo-twitter fa-2x space-right"></i></a> <a target=_blank href=http://www.facebook.com/lovoo title="{{ ::\'footer.social_button_title\' | translate: {social: \'Facebook\'} }}"><i class="lo lo-facebook fa-2x space-right"></i></a> <a rel=publisher target=_blank href=https://plus.google.com/+LovooTV title="{{ ::\'footer.social_button_title\' | translate: {social: \'Google+\'} }}"><i class="lo lo-googleplus fa-2x"></i></a></div></div></div></footer></div>')
        }]), angular.module("/template/form/account-settings/change-birthday.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/account-settings/change-birthday.html", '<div class="white-box padding space-after" ng-controller=SettingsChangeBirthdayCtrl><form role=form name=changeBirthdayForm id=changeBirthdayForm novalidate><h4 class="space-after clearfix"><strong>{{ "change_birthday"|translate}}</strong><div class=pull-right><button type=submit ng-click=save() class="btn btn-sm btn-success" ng-disabled="changeEmailForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button> <button ng-click="$parent.$parent.edit=!$parent.edit" class="btn btn-sm btn-danger">{{ "cancel" | translate }}</button></div></h4><div class=row><div class=form-group><div class=form-group><div ng-class="{true: \'col-xs-3\', false: \'col-xs-2\'}[$index==1]" ng-repeat="dateSelect in dateSelectGroup"><p>{{ dateSelect.key|translate }}</p><select class=form-control name=birthday ng-model=birthday[dateSelect.key] ng-options="translateBirthdayOptions(dateSelect.key, value) for value in dateSelect.values"></select></div></div></div></div></form></div>')
        }]), angular.module("/template/form/account-settings/change-email.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/account-settings/change-email.html", '<div class="white-box padding space-after" ng-controller=SettingsChangeGeneralCtrl><form role=form name=changeEmailForm id=changeEmailForm novalidate handle-autofill><h4 class="space-after clearfix"><strong>{{ "change_email"|translate}}</strong><div class=pull-right><button type=submit ng-click=saveEmail() class="btn btn-sm btn-success" ng-disabled="changeEmailForm.$invalid || btnDisabled.email" class="btn btn-success">{{ "save" | translate }}</button> <button ng-click="$parent.$parent.edit=!$parent.edit" class="btn btn-sm btn-danger">{{ "cancel" | translate }}</button></div></h4><div class=row><div class=col-xs-7><div class=form-group><input id=email class=form-control type=email name=email placeholder="{{ \'your_email\' | translate }}" ng-model=email ng-blur="displayFieldError(\'email\')" ng-required=true autocomplete="off"><div class="text-danger form-error" ng-show=changeEmailForm.email.$error.required><small>{{"form_error_required" | translate}}</small></div><div class="text-danger form-error" ng-show=changeEmailForm.email.$error.email><small>{{"form_error_email" | translate}}</small></div></div></div></div></form></div>')
        }]), angular.module("/template/form/account-settings/change-name.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/account-settings/change-name.html", '<div class="white-box padding space-after" ng-controller=SettingsChangeGeneralCtrl><form id=changeNameForm role=form name=changeNameForm novalidate handle-autofill><h4 class="space-after clearfix"><strong>{{ "change_name"|translate}}</strong><div class=pull-right><button type=submit ng-click=saveName() class="btn btn-sm btn-success" ng-disabled="changeNameForm.$invalid || btnDisabled.name" class="btn btn-success">{{ "save" | translate }}</button> <button ng-click="$parent.$parent.edit=!$parent.edit" class="btn btn-sm btn-danger">{{ "cancel" | translate }}</button></div></h4><div class=row><div class=col-xs-7><div class=form-group><input id=name class=form-control type=text name=name placeholder="{{ \'your_name\'| translate }}" ng-model=name ng-required=true ng-blur="displayFieldError(\'name\')" ng-minlength=3 ng-maxlength=100><div ng-show="changeNameForm.name.$error.required && displayError.name" class="text-danger form-error"><small>{{"form_error_required" | translate}}</small></div><div ng-show="changeNameForm.name.$error.$error.minlength  && displayError.name" class="text-danger form-error"><small>{{"form_error_minlength" | translate:{count:3} }}</small></div><div ng-show="changeNameForm.name.$error.maxlength  && displayError.name" class="text-danger form-error"><small>{{"form_error_maxlength" | translate:{count:100} }}</small></div></div></div></div></form></div>')
        }]), angular.module("/template/form/account-settings/change-origin.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/account-settings/change-origin.html", '<div class="white-box padding space-after" ng-controller=SettingsChangeOriginCtrl><form id=changeOriginForm role=form name=changeOriginForm novalidate handle-autofill><h4 class="space-after clearfix"><strong>{{ "change_origin"|translate}}</strong><div class=pull-right><button type=submit ng-click=save() class="btn btn-sm btn-success" ng-disabled="changeOriginForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button> <button ng-click="$parent.$parent.edit=!$parent.edit" class="btn btn-sm btn-danger">{{ "cancel" | translate }}</button></div></h4><div class=row><div class=col-xs-7><div class="form-group space-after"><label class=space-after-sm>{{"my_origin"|translate}}: <span ng-if=$root.Self.homeLocation.location>{{ $root.Self.homeLocation.location }}</span></label><location-field callback=setLocation></location-field></div></div></div><div class=row><div class=col-xs-12><div class=form-group><label>{{"my_languages"|translate}}:</label><ul class="content-columns-4 list-unstyled"><li ng-repeat="(lang,title) in $root.Languages"><label class=h5><input type=checkbox ng-value=lang name=languages[] ng-click=toggleLanguageSelection(lang) ng-checked="languages.indexOf(lang) != -1"> {{ title }}</label></li></ul></div></div></div></form></div>')
        }]), angular.module("/template/form/account-settings/change-password.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/account-settings/change-password.html", '<div class="white-box padding space-after" ng-controller=SettingsChangePasswordCtrl><form role=form name=changePasswordForm id=changePasswordForm novalidate handle-autofill><h4 class="space-after clearfix"><strong>{{ \'my_password\'|translate}}</strong><div class=pull-right><button type=submit ng-click=savePassword() class="btn btn-sm btn-success" ng-disabled="changePasswordForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button> <button ng-click="$parent.$parent.edit=!$parent.edit" class="btn btn-sm btn-danger">{{ "cancel" | translate }}</button></div></h4><div class=row><div class=col-xs-7><div class=form-group ng-show="$root.Self.pwf != 2"><input type=password class=form-control name=passwordOld placeholder="{{ \'current_password\'|translate }}" ng-model=passwordOld ng-blur="displayFieldError(\'passwordOld\')" ng-required="$root.Self.pwf != 2 ? true : false"><div ng-show="changePasswordForm.passwordOld.$error.required && displayError.passwordOld" class="text-danger form-error"><small>{{"form_error_required" | translate}}</small></div></div><div class=form-group><input id=password class=form-control type=password name=password placeholder="{{ \'new_password\' | translate }}" ng-model=password ng-blur="displayFieldError(\'password\')" ng-required=true ng-minlength="5"><div ng-show="changePasswordForm.password.$error.required && displayError.password" class="text-danger form-error"><small>{{"form_error_required" | translate}}</small></div><div ng-show="changePasswordForm.password.$error.minlength && displayError.password" class="text-danger form-error"><small>{{"form_error_minlength" | translate:{count:5} }}</small></div></div><div class=form-group><input id=repeatPassword class=form-control type=password name=repeatPassword placeholder="{{ \'repeat_password\' | translate }}" ng-model=repeatPassword ng-blur="displayFieldError(\'repeatPassword\')" repeat-check="#password"><div ng-show="changePasswordForm.repeatPassword.$error.repeatcheck && displayError.repeatPassword" class="text-danger form-error"><small>{{"form_error_repeat" | translate}}</small></div></div></div></div></form></div>');

        }]), angular.module("/template/form/comments.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/comments.html", '<li class="media padding-md"><a class="pull-left cursor-pointer space-right-sm" ng-click=showProfileDialog(user)><profile-image category=thumb id={{user.picture}} size=s imgsizing=60 class="media-object img-xs img-circle"></profile-image></a><div class=media-body><form role=form class="form-inline space-before-sm" ng-submit=submitCommentForm() name=commentForm><div class=input-group><input type=text placeholder="{{\'your_comment\'|translate}}" class=form-control ng-model=commentText name=commentText ng-minlength=1 required ng-maxlength=300><div class="text-danger form-error" ng-show=commentForm.commentText.$error.REQUIRED><small>{{"form_error_required" | translate}}</small></div><span class=input-group-btn><button type=button alt="{{\'your_comment\'|translate}}" class="btn btn-primary" ng-click=submitCommentForm() class="btn btn-primary pull-right"><i class="fa fa-share"></i></button></span></div></form></div></li>')
        }]), angular.module("/template/form/interests.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/interests.html", '<div class="white-box padding space-after" ng-controller=SettingsInterestsCtrl><form id=interestsForm role=form name=interestsForm novalidate><h4 class="space-after clearfix"><strong>{{ "interests"|translate}}</strong><div class=pull-right><button ng-click=save() class="btn btn-sm btn-success" ng-disabled="interestsForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button></div></h4><div class=row><div class=col-xs-6><div class=form-group><label>{{"im_searching"|translate}}:</label><ul class=list-unstyled><li ng-repeat="item in intentionsList | orderBy:-translated:false"><label class=text-normal><input type=checkbox name=interests ng-model="interests[item.key]"> {{ item.translated }}</label></li></ul></div></div></div></form></div>')
        }]), angular.module("/template/form/location.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/location.html", '<div class="input-group merged"><input name=city placeholder="{{ \'City\'| translate }}" class="form-control input-group-addon-white" suggest=cities on-selected=setLocation ng-enter=setLocation ng-model="locationTitle"> <span class="input-group-addon input-group-addon-white"><a class="cursor-pointer text-gender" ng-hide=geoLocationIsLoading ng-click=requestGeolocation()><i class="fa fa-location-arrow"></i></a> <i class="fa fa-spinner fa-spin" ng-show=geoLocationIsLoading></i></span></div>')
        }]), angular.module("/template/form/login-form.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/login-form.html", '<form id=form name=loginForm autocomplete=on novalidate handle-autofill><validation-errors></validation-errors><div class=form-group><input class=form-control name=authEmail type=email placeholder="{{\'your_email\'|translate}}" ng-model=authEmail ng-blur="displayFieldError(\'authEmail\')" ng-required="true"><div class="text-danger form-error" ng-show="loginForm.authEmail.$error.required && displayError.authEmail"><small>{{"form_error_required" | translate}}</small></div><div class="text-danger form-error" ng-show="loginForm.authEmail.$error.email && displayError.authEmail"><small>{{"form_error_email" | translate}}</small></div></div><div class=form-group><div class=row><div class="col-sm-8 col-xs-12"><input class="form-control password" type=password name=authPassword placeholder="{{ \'your_password\'| translate }}" ng-model=authPassword ng-blur="displayFieldError(\'authPassword\')" ng-required="true"><div ng-show="loginForm.authPassword.$error.required && displayError.authPassword" class="text-danger form-error"><small>{{"form_error_required" | translate}}</small></div></div><div class="col-sm-4 col-xs-12"><div class="visible-xs space-after-sm"></div><button type=submit ng-click=doLogin() ng-disabled="loginForm.$invalid || btnDisabled" class="btn btn-info btn-block">{{ "Login"|translate }}</button></div></div></div><div class="row space-before"><div class=col-sm-6><label class="checkbox-inline text-normal space-right" for=pwr><input id=pwr type=checkbox ng-model="authPwRemember"> {{ "pw_remember"|translate }}</label></div><div class="col-sm-6 text-right"><a ng-click="$close(); openPasswordForgetDialog(authEmail)" class="static-control cursor-pointer underline text-blue">{{ "pw_forget"|translate }}</a></div></div></form><div class=split-line><hr><h4 class=split-text>{{"or"|translate}}</h4><hr></div><div class=form-group><button ng-click=facebookRegister() class="btn btn-block btn-primary btn-lg btn-facebook text-left"><i class="fa fa-fw fa-facebook space-right-sm"></i><span class="h3 text-normal">{{ "facebook_start" | translate }}</span></button><googleplus-signin class=btn-block></googleplus-signin><span class=help-block>* {{ "facebook_post_info" | translate }}</span></div>')
        }]), angular.module("/template/form/login-nintynine-form.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/login-nintynine-form.html", '<div class=form-group><button ng-click=facebookRegister() class="btn btn-block btn-primary btn-lg btn-facebook text-left"><i class="fa fa-fw fa-facebook space-right-sm"></i><span class="h3 text-normal">{{ "facebook_start" | translate }}</span></button><googleplus-signin class=btn-block></googleplus-signin><span class=help-block>* {{ "facebook_post_info" | translate }}</span></div><div class=split-line><hr><h4 class=split-text>{{"or"|translate}}</h4><hr></div><form id=form id=loginForm name=loginForm autocomplete=on novalidate handle-autofill><validation-errors></validation-errors><div class=form-group><input class=form-control name=authEmail type=email placeholder="{{\'your_email\'|translate}}" ng-model=authEmail ng-blur="displayFieldError(\'authEmail\')" ng-required="true"><div class="text-danger form-error" ng-show="loginForm.authEmail.$error.required && displayError.authEmail"><small>{{"form_error_required" | translate}}</small></div><div class="text-danger form-error" ng-show="loginForm.authEmail.$error.email && displayError.authEmail"><small>{{"form_error_email" | translate}}</small></div></div><div class=form-group><div class=row><div class=col-xs-12><input class="form-control password" type=password name=authPassword placeholder="{{ \'your_password\'| translate }}" ng-model=authPassword ng-blur="displayFieldError(\'authPassword\')" ng-required="true"><div ng-show="loginForm.authPassword.$error.required && displayError.authPassword" class="text-danger form-error"><small>{{"form_error_required" | translate}}</small></div></div><div class="col-xs-12 space-before-sm"><label class="checkbox-inline text-normal space-right" for=pwr><input id=pwr type=checkbox ng-model="authPwRemember"> {{ "pw_remember"|translate }}</label></div></div><div class="text-right space-before-sm"><button type=submit ng-click=doLogin() ng-disabled="loginForm.$invalid || btnDisabled" class="btn btn-nn no-space-after" ng-class="isVip ? \'btn-green\' : \'btn-orange\'">{{ isVip ? ("login_now"|translate) : ("become_vip"|translate) }}</button></div></div><div class="row space-before space-after-sm"><div class="col-xs-12 text-center"><a ng-click=openPasswordForgetDialog(authEmail) class="static-control cursor-pointer underline text-black text-uppercase">{{ "pw_forget"|translate }}</a></div></div></form>')
        }]), angular.module("/template/form/registerform.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/registerform.html", '<div ng-if="usedFacebook!=true"><div class=form-group><button ng-click=facebookRegister() class="btn btn-block btn-primary btn-lg btn-facebook text-left dowrap"><i class="fa fa-fw fa-facebook"></i> <span class="h3 text-normal">{{ ::\'facebook_start\' | translate }}</span></button></div></div><div ng-if="usedGooglePlus!=true"><div class=form-group><googleplus-signin class=btn-block></googleplus-signin></div><div class=split-line><hr><h4 class="split-text uppercase">{{ ::\'or\' | translate }}</h4><hr></div></div><validation-errors></validation-errors><form id=registerForm-{{::staticId}} name=registerForm novalidate handle-autofill><div ng-show="step==0"><div class=form-group><input id=name-{{::staticId}} class=form-control type=text name=name placeholder="{{::\'your_name\'| translate}}" ng-model=name ng-required=true ng-blur="displayFieldError(\'name\')" ng-minlength=3 ng-maxlength="100"><div ng-show="registerForm.name.$error.required && displayError.name" class="text-danger form-error"><small>{{ ::\'form_error_required\' | translate }}</small></div><div ng-show="registerForm.name.$error.minlength  && displayError.name" class="text-danger form-error"><small>{{ ::\'form_error_minlength\' | translate: {count: 3} }}</small></div><div ng-show="registerForm.name.$error.maxlength  && displayError.name" class="text-danger form-error"><small>{{ ::\'form_error_maxlength\' | translate: {count: 100} }}</small></div></div><div class=form-group><location-field callback=setLocation preset-location=location></location-field></div><div class=form-group><p class=input-group><input type=text name=birthday class="form-control input-group-addon-white bg-white cursor-pointer" datepicker-options=dateOptions datepicker-popup="{{ dateFormat }}" datepicker-append-to-body=true datepicker-mode="\'year\'" min-date=dateRestriction.min max-date=dateRestriction.max ng-model=birthday ng-click="opened=true" ng-required=true readonly=readonly is-open=opened show-button-bar="false"> <span class="input-group-addon input-group-addon-white"><button type=button class="border-none bg-white" ng-click=open($event) ng-style="{border: $root.websiteVersion < 3 ? \'0\' : \'\'}"><i class="fa fa-calendar"></i></button></span></p><div ng-show="registerForm.birthday.$error.required && !birthday" class="text-danger form-error"><small>{{ ::\'form_error_required\' | translate }}</small></div><div ng-show=registerForm.birthday.$error.date class="text-danger form-error"><small>{{ ::\'form_error_format\' | translate: {format: dateFormat } }}</small></div></div><div class=form-group ng-class="{\'space-after-lg\': $root.websiteVersion >= 3}"><label ng-if="$root.websiteVersion < 3">{{ ::\'Sex\' | translate }}</label><div class="row space-after-md"><div class="col-sm-4 col-xs-6" ng-repeat="(genderVal, genderArr) in registerGenders"><label class="radio-inline text-normal space-right" for=g_{{::genderVal}}-{{::staticId}}><input id=g_{{::genderVal}}-{{::staticId}} type=radio name=gender ng-checked="gender == genderVal" ng-click="checkButton(genderVal, \'gender\')" ng-val="genderVal"> {{ ::genderArr.genderTranslated }}</label></div><div class="col-sm-4 col-xs-6">&nbsp;</div></div><div ng-show="!gender && displayError.gender" class="text-danger form-error"><small>{{ ::\'form_error_required\' | translate }}</small></div></div><div ng-if="$root.websiteVersion < 3" class=form-group ng-class="{\'space-after-lg\': $root.websiteVersion < 3}"><label>{{::"im_searching" | translate}}</label><div class=row><div class=col-xs-4 ng-repeat="(val, genderLookingArr) in registerGenderLookingGenders"><label class="radio-inline text-normal space-right" for=gL_{{::val}}-{{::staticId}}><input id=gL_{{::val}}-{{::staticId}} type=radio name=genderLooking ng-checked="genderLooking==val" ng-click="checkButton(val, \'genderLooking\')" ng-val="val"> {{::genderLookingArr.genderGroupTranslated}}</label></div></div><div ng-show="!genderLooking && displayError.genderLooking" class="text-danger form-error"><small>{{ ::\'form_error_required\' | translate }}</small></div></div><div class="clearfix space-after-md"><small class="pull-left text-gray gray-dark-links" ng-bind-html="::\'v3.already_user\' | translate"></small><div class="form-horizontal pull-right"><button ng-click=continueRegistration() data-placement=right class="btn btn-success">{{ ::\'go_on\' | translate }}</button></div></div></div><div ng-show="step==1"><div class=form-group><input id=email-{{::staticId}} class=form-control type=email name=email placeholder="{{::\'your_email\' | translate}}" ng-model=email ng-blur="displayFieldError(\'email\')" ng-required="true"><div class="text-danger form-error" ng-show="registerForm.email.$error.required && displayError.email"><small>{{ ::\'form_error_required\' | translate }}</small></div><div class="text-danger form-error" ng-show="registerForm.email.$error.email && displayError.email"><small>{{ ::\'form_error_email\' | translate }}</small></div></div><div class=form-group><input id=password-{{::staticId}} class=form-control type=password name=password placeholder="{{::\'your_password\' | translate}}" ng-model=password ng-blur="displayFieldError(\'password\')" ng-required=true ng-minlength="5"><div ng-show="registerForm.password.$error.required && displayError.password" class="text-danger form-error"><small>{{ ::\'form_error_required\' | translate }}</small></div><div ng-show="registerForm.password.$error.minlength && displayError.password" class="text-danger form-error"><small>{{ ::\'form_error_minlength\' | translate: {count: 5} }}</small></div></div><div class=form-group><input id=repeatPassword-{{::staticId}} class=form-control type=password name=repeatPassword placeholder="{{::\'repeat_password\' | translate}}" ng-model=repeatPassword ng-required=true ng-blur="displayFieldError(\'repeatPassword\')" repeat-check="#password-{{::staticId}}"><div ng-show="registerForm.repeatPassword.$error.repeatcheck && displayError.repeatPassword" class="text-danger form-error"><small>{{ ::\'form_error_repeat\' | translate }}</small></div></div><div class="form-group overflow-hidden" ng-if="step==1" ng-show=addCaptcha><div vc-recaptcha tabindex=3 ng-model=model.captcha theme=clean lang={{::$root.Translator.locale}} key=$root.CaptchaConfig.key></div></div><div class=form-group><div class="checkbox checkbox-styled space-after-lg"><label for=br-{{::staticId}}><input id=br-{{::staticId}} name=br type=checkbox ng-model=br ng-blur="displayFieldError(\'br\')"> <span ng-bind-html="\'br_confirm\' | translate:{url: \'http://static.lovoo.net/agb/\' + $root.staticDomains(), legal: \'http://static.lovoo.net/legal/\' + $root.staticDomains()}"></span><div class=ui-styling></div><small ng-show="displayError.br && true != br" class="text-danger form-error">{{ ::\'form_error_accecpt_agb\' | translate }}</small></label></div></div><div class=form-horizontal><div class=row><div class=col-xs-4><small class="form-control-static space-before-xs"><a class="cursor-pointer underline text-blue" ng-click=switchToStep(0)>{{ ::\'Back\' | translate }}</a></small></div><div class="col-xs-8 text-right"><button ng-click=doRegister() ng-disabled="registerForm.$invalid || btnDisabled" data-placement=right class="btn dowrap btn-success">{{ ::\'register_free\' | translate }}</button></div></div></div></div></form>')
        }]), angular.module("/template/form/searchfilter.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/searchfilter.html", '<div class="white-box padding space-after" ng-controller=SettingsSearchFilterCtrl><form id=searchfilterForm role=form name=searchfilterForm novalidate><h4 class="space-after clearfix"><strong>{{ "searchSettings"|translate}}</strong><div class=pull-right><button ng-click=save() class="btn btn-sm btn-success" ng-disabled="searchfilterForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button></div></h4><div class=form-group><label class=space-after-sm>{{"im_searching"|translate}}:</label><div class=row><div class=col-xs-2 ng-repeat="(val, genderArr) in $root.Genders"><label class=text-normal><input type=radio name=gender ng-model=gender ng-checked="gender == val" ng-click="checkButton(val, \'gender\')" ng-value="val"> {{ genderArr.genderGroupTranslated }}</label></div></div></div><div class=form-group><label class=space-after-sm>{{"at_age_of"|translate}}</label><div class=row><div class=col-xs-6><slider class=slider-default floor={{AgeRange.min}} ceiling={{AgeRange.max}} step=1 ng-model-low=ageFrom ng-model-high=ageTo translate=addYearsToString></slider></div></div></div></form></div>')
        }]), angular.module("/template/form/searchform.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/searchform.html", '<div class="panel-search full-width" ng-class="isPanelFixed ? \'panel-fixed \' + $root.routing.containerClass : \'absolute\'"><div class="overflow-visible panel panel-default padding no-space-after"><form name=searchform ng-submit=submitQuery()><div class=row><div class=col-xs-3><div class="form-group space-after-sm"><p>{{ "im_searching"|translate}}</p><div class=checkbox-inline><label class="text-normal space-right-sm cursor-pointer" for=gs-1><input id=gs-1 type=checkbox value=1 ng-model=genderMale ng-change="resetQueryAndSubmit(\'gender\', 1)" ng-checked="genderMale"> {{Genders[1].genderGroupTranslated}}</label></div><div class=checkbox-inline><label class="text-normal cursor-pointer" for=gs-2><input id=gs-2 type=checkbox value=1 ng-model=genderFemale ng-change="resetQueryAndSubmit(\'gender\', 2)" ng-checked="genderFemale"> {{Genders[2].genderGroupTranslated}}</label></div></div></div><div class=col-xs-3><div class=form-group><p>{{"users_from"|translate}} <span ng-if=city>{{city}}</span></p><location-field callback=setLocation></location-field></div></div><div class=col-xs-3><div class="form-group no-space-after space-left-sm space-right-sm"><p>{{"distance_from"|translate:{\'city\': city} }}</p><slider class=slider-default floor=1 ceiling=500 step=1 ng-model=radiusTo move-begin=beginMoving move-end=endMoving translate=addDistanceToString></slider></div></div><div class=col-xs-3><div class="form-group no-space-after space-left-sm space-right-sm"><p>{{"at_age_of"|translate}}</p><slider class=slider-default floor={{AgeRange.min}} ceiling={{AgeRange.max}} step=1 ng-model-low=ageFrom ng-model-high=ageTo move-begin=beginMoving move-end=endMoving translate=addYearsToString></slider></div></div></div><div collapse=isCollapsed class=panel-search-more><div class="space-before row"><div class=col-xs-3><div class=form-group><p>{{ "searchform.who_looking_for"|translate}}</p><div class="form-group space-after-sm"><div class=checkbox-inline><label class="text-normal cursor-pointer space-right-sm" for=gl-1><input id=gl-1 type=checkbox value=1 ng-model=genderLookingMale ng-change="resetQueryAndSubmit(\'genderLooking\', 1)" ng-checked="genderLookingMale"> {{Genders[1].genderGroupTranslated}}</label></div><div class=checkbox-inline><label class="text-normal cursor-pointer" for=gl-2><input id=gl-2 type=checkbox value=1 ng-model=genderLookingFemale ng-change="resetQueryAndSubmit(\'genderLooking\', 2)" ng-checked="genderLookingFemale"> {{Genders[2].genderGroupTranslated}}</label></div></div></div></div><div class=col-xs-3><div class=form-group><p>{{"activity_n_quality"|translate }}</p><div class=checkbox><label class="text-normal space-right-sm" for=isNew><input id=isNew type=checkbox ng-model=new ng-checked=new ng-change="submitSearchForm(true)"> {{ \'only_new_users\'|translate }}</label></div><div class=checkbox><label class="text-normal space-right-sm" for=isOnline><input id=isOnline type=checkbox ng-model=online ng-checked=online ng-change="submitSearchForm(true)"> {{ \'only_online_users\'|translate }}</label></div><div class=checkbox><label class="text-normal space-right-sm" for=hasPic><input id=hasPic type=checkbox ng-model=pic ng-checked=pic ng-true-value="\'pic\'" ng-change="submitSearchForm(true)"> {{ \'only_pic_users\'|translate }}</label></div><div ng-show=$root.Self.id class=checkbox><label class="text-normal space-right-sm" for=isVerified><input id=isVerified type=checkbox ng-model=ver ng-checked=ver ng-true-value="\'ver\'" ng-change="handleVerifySelection()"> {{ \'only_verified_users\'|translate }}</label></div></div></div><div class=col-xs-3><div class="form-group space-left-sm"><p>{{"what_looking_for"|translate }}</p><div class=checkbox><label class="text-normal space-right-sm" for=intentionS_{{Intentions.date.key}}><input id=intentionS_{{Intentions.date.key}} type=checkbox ng-model=intention_date ng-checked=intention_date ng-true-value="\'date\'" ng-change="submitSearchForm(true)"> {{Intentions.date.translated}}</label></div><div class=checkbox><label class="text-normal space-right-sm" for=intentionS_{{Intentions.frie.key}}><input id=intentionS_{{Intentions.frie.key}} type=checkbox ng-model=intention_frie ng-checked=intention_frie ng-true-value="\'frie\'" ng-change="submitSearchForm(true)"> {{Intentions.frie.translated}}</label></div><div class=checkbox><label class="text-normal space-right-sm" for=intentionS_{{Intentions.chat.key}}><input id=intentionS_{{Intentions.chat.key}} type=checkbox ng-model=intention_chat ng-checked=intention_chat ng-true-value="\'chat\'" ng-change="submitSearchForm(true)"> {{Intentions.chat.translated}}</label></div></div></div><div class=col-xs-3><div class="form-group space-left-sm space-right-sm"><p>{{"name_search"|translate}}</p><div class=input-group><input class=form-control type=text name=query ng-model=query placeholder="{{ \'enter_name\'| translate }}" ng-blur="displayFieldError(\'query\')" ng-minlength=3 ng-enter=submitQuery() ng-maxlength="100"> <span class=input-group-btn><button name=add type=button ng-click=submitQuery() class="btn btn-info"><i class="fa fa-search"></i></button></span></div><div ng-show="searchform.query.$error.minlength && displayError.query" class="text-danger form-error"><small>{{"form_error_minlength" | translate:{count:3} }}</small></div><div ng-show="searchform.query.$error.maxlength && displayError.query" class="text-danger form-error"><small>{{"form_error_maxlength" | translate:{count:100} }}</small></div></div></div></div><div class=clearfix></div></div><div class=text-center><a ng-click=toggleCollapsed() type=button class=cursor-pointer><i class="fa fa-cog fa-fw"></i> <span ng-if=isCollapsed>{{ "further_options"|translate }}</span> <span ng-if=!isCollapsed>{{ "less_options"|translate }}</span></a></div></form></div></div>')
        }]), angular.module("/template/form/vip-settings.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/form/vip-settings.html", '<form name=vipform ng-submit=saveVipSettings() class="white-box padding space-after"><div class=pull-right><span class="text-success space-right-sm" ng-if=saved>{{ "successful"|translate }}</span> <button type=submit class="btn btn-success">{{ "save"|translate }}</button></div><div><label class=text-normal for=ghost><input type=checkbox name=ghost id=ghost ng-model=settings.ghost> {{ "vip_advantages.ghost_title"|translate }}</label></div><div><label class=text-normal for=public><input type=checkbox name=public id=public ng-model=settings.public> {{ "enable_vip_branding"|translate }}</label></div></form>')
        }]), angular.module("/template/helpcenter/faq.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/helpcenter/faq.html", '<div id=faq-wrapper><div class="h3 white-box padding space-after"><a title="{{\'lovoo_slogan\'|translate}}" href-reload="{{$root.Ajax.url}}/"><img src="{{$root.Ajax.imgUrlStatic}}lovoo_logo.png"></a> {{"FAQ"|translate}}</div><div class="h4 white-box padding space-after"><a href-reload={{imprintUrl}}><i class="fa fa-fw fa-info-circle"></i> {{"Lovoo Info"|translate}}</a></div><div class="white-box padding space-after text-warning" ng-show=faqData.submitFeedback><i class="fa fa-exclamation-circle"></i> {{faqData.submitFeedback}} <button ng-type=button class=close data-dismiss=modal aria-hidden=true>&times;</button></div><div class="panel panel-faq" ng-repeat="(idx, category) in faqData.translatedFaqs"><div class="panel-heading h3" ng-bind-html=category.about></div><ul class=list-group><li id={{faq.options.anchor}} ng-class="{active: totalIndex == currentShown }" class=list-group-item ng-repeat="faq in category.faqs" ng-init="totalIndex=increase();isAnchoredGoThereAndOpen(faq.options.anchor,totalIndex)"><p ng-click=toggleAnswer(totalIndex) class=list-group-item-heading><strong ng-bind-html=faq.question></strong></p><p ng-show="totalIndex == currentShown" class=list-group-item-text ng-bind-html=faq.answer|unsafe></p></li></ul></div></div>')
        }]), angular.module("/template/helpcenter/imprint.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/helpcenter/imprint.html", '<div id=imprint-wrapper class="panel panel-info"><div class=panel-heading><h1 class=inbox-heading>{{"Imprint"|translate}}</h1></div><div class=panel-body><p><strong>Anschrift / Address</strong></p><p>LOVOO GmbH</p><p>Prager StraÃŸe 10</p><p>01069 Dresden</p><p>Kontakt / Contact: <a href=mailto:support@lovoo.com>support@lovoo.com</a></p><hr><p><strong>GeschÃ¤ftsfÃ¼hrer / CEO</strong></p><p>Alexander Friede, Benjamin Bak, BjÃ¶rn Bak</p><p>USt-ID: DE282583522</p><p>Amtsgericht Dresden</p><p>HRB 31175</p><hr><p><strong>GeschÃ¤ftliche Anfragen / Business</strong></p><p><a class="btn btn-primary btn space-right" target=_blank href={{supportformUrl}}>Kontakt / Contact</a> <a target=_blank class="cursor-pointer space-right" title="{{\'Lovoo on XING\'|translate}}" href=https://www.xing.com/companies/lovoogmbh><img alt=xing ng-src={{$root.Ajax.imgUrlStatic}}icons/logo-xing.png width=43 height="50"></a> <a target=_blank class=cursor-pointer title="{{\'Lovoo on LinkedIn\'|translate}}" href="https://www.linkedin.com/company/lovoo-gmbh?trk=company_name"><img ng-src={{$root.Ajax.imgUrlStatic}}icons/logo-linkedin.png alt=linkedin width=50 height="50"></a></p><hr><p><strong>Fragen rund um deine Daten / Questions about your data</strong></p><p>Datenschutzbeauftragte der LOVOO GmbH / LOVOO Data Security Manager: <a href="mailto:datenschutz@lovoo.com?subject=Datenschutz%20%7C%20Data%20Security">datenschutz@lovoo.com</a></p></div></div>')
        }]), angular.module("/template/helpcenter/supportform.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/helpcenter/supportform.html", '<div class=navbar-padding><div class="h3 white-box padding space-after" ng-switch=app><div ng-switch-when=voome><a title="{{\'lovoo_slogan\'|translate}}" href-reload="{{$root.Ajax.url}}/"><img ng-src={{$root.Ajax.imgUrlStatic}}logo_voo.png height="60px;"></a> {{"support_n_help"|translate}}</div><div ng-switch-default><a title="{{\'lovoo_slogan\'|translate}}" href-reload="{{$root.Ajax.url}}/"><img ng-src="{{$root.Ajax.imgUrlStatic}}lovoo_logo.png"></a> {{"support_n_help"|translate}}</div></div><hr class="sm space-after"><div class="white-box padding space-after" ng-show=submittedSuccessful><p ng-bind-html="\'thanks_report\'|translate : {url: supportUrl}"></p><p>{{::\'support_overview\'|translate}}:</p><p class=space-before-sm><strong>{{::\'Email\'|translate}}</strong><br>{{email}}</p><p class=space-before-sm><strong>{{::\'Subject\'|translate}}</strong><br>{{subjectChoices[subject]}}</p><p class="space-before-sm break-word no-space-after"><strong>{{::\'your_message\' | translate}}</strong><br>{{description}}</p></div><form ng-show=!submittedSuccessful role=form class=form method=post id=supportform name=supportform ng-submit=trySubmit()><div class=row><div class=col-sm-6><div class=form-group><input id=email name=email class=form-control type=email placeholder="{{\'your_email\'|translate}}" ng-model=email ng-required=true ng-blur="displayFieldError(\'email\')"><div class="text-danger form-error" ng-show="supportform.email.$error.required  && displayError.email"><strong><small>{{ \'form_error_required\'|translate }}</small></strong></div><div class="text-danger form-error" ng-show="supportform.email.$error.email && displayError.email"><strong><small>{{ \'form_error_email\'|translate }}</small></strong></div></div></div><div class=col-sm-6><div class=form-group><div class="btn-block btn-group"><button type=button class="btn btn-block btn-default dropdown-toggle" data-toggle=dropdown ng-blur="displayFieldError(\'subject\')">{{ displaySubject }} <span class=caret></span></button><ul class=dropdown-menu role=menu><li ng-if="key != \'oth\'" ng-click="selectSubject(key, label)" class=cursor-pointer ng-class="{active: subject ==  key }" ng-repeat="(key, label) in subjectChoices"><a>{{ label }}</a></li><li ng-click="selectSubject(\'oth\', subjectChoices.oth)" class=cursor-pointer ng-class="{active: subject ==  \'oth\' }"><a>{{ subjectChoices.oth }}</a></li></ul></div><input type=hidden ng-required=true name=subject ng-model=subject><div class="text-danger form-error" ng-show="supportform.subject.$error.required  && displayError.subject"><strong><small>{{ \'form_error_required\'|translate }}</small></strong></div></div></div></div><div class=form-group><textarea id=description class=form-control name=description rows=6 placeholder="{{ \'your_description\' | translate }}" ng-model=description ng-required=true required="" ng-trim=false maxlength={{SUPPORTFORM_MESSAGE_MAXLENGTH}} ng-blur="displayFieldError(\'description\')">\n                {{ description }}\n            </textarea><div ng-show="(!supportform.description.$error.maxlength && !supportform.description.$error.required) || !displayError.description" class="pull-left text-smaller text-gray-dark text-muted space-after-sm space-before-sm">({{"letters_to_go"|translate:{\'letters\': SUPPORTFORM_MESSAGE_MAXLENGTH - (description.length + (lineBreaks.length || 0)) } }})</div><div class="text-danger form-error" ng-show="supportform.description.$error.required  && displayError.description"><strong><small>{{ \'form_error_required\'|translate }}</small></strong></div><div class="text-danger form-error" ng-show="supportform.description.$error.maxlength  && displayError.description"><strong><small>{{"form_error_maxlength" | translate:{count:SUPPORTFORM_MESSAGE_MAXLENGTH} }}</small></strong></div></div><input type=hidden name=user_id ng-model=user_id> <input type=hidden name=device_id ng-model=device_id><div class=form-group><button type=submit ng-disabled=supportform.$invalid class="btn btn-primary pull-right">{{ "send"|translate }}</button><div ng-show=$root.supportFormAttachmentEnabled><form method=POST action={{uploadAction}} enctype=multipart/form-data><div class="btn btn-default btn-file pull-right space-right-sm"><i class="fa fa-lg fa-paperclip text-gray"></i> <input type=file accept=image/* ng-file-select="onFileSelectAttachment($files)"></div></form></div><div class=clearfix></div></div></form><div ng-show="$root.supportFormAttachmentEnabled && uploads.length > 0"><div class=row ng-repeat="u in uploads"><div class=col-xs-6><div class="white-box padding space-after-sm"><div class=row><div class=col-xs-6><img width=100% ng-src="{{u.dataUrl}}"></div><div class=col-xs-6><span ng-show=!submittedSuccessful class="pull-right space-left-sm cursor-pointer"><i ng-click=removeAttachment($index) class="lo lo-close"></i></span><p class=break-all><strong>{{::"file_name"|translate}}:</strong> {{::u.details.name}}</p><p><strong>{{::"file_size"|translate}}:</strong> {{::u.details.size|formatBytes}}</p></div></div></div></div></div></div></div>')
        }]), angular.module("/template/itemlist.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/itemlist.html", '<ul class="{{ className }} list-group"><li class=list-group-item ng-repeat="item in items track by $index|orderBy:sort"><list-item-type item=item></list-item-type></li><li class="list-group-item text-center" ng-show=!items.length><i class="fa fa-spin fa-spinner fa-2x text-muted"></i></li></ul>')
        }]), angular.module("/template/list/advanced-list.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/list/advanced-list.html", '<div class="white-box padding" scroll-refresh=reloadList() scroll-refresh-distance=1 ng-init="active = $parent.activePage"><div class="row thumbnail-box"><div class="col-xs-{{ c_columns }}" ng-repeat="ele in users" ng-init="user = ele.user" ng-hide=ele.listHide><div class="thumbnail thumbnail-square thumbnail-slidehover thumbnail-fullshaded" ng-class="{\'user-vip\': !!user.vip}"><a href={{$root.Ajax.url}}/profile/{{user.id}} ng-click="action(user, \'profile\', {crypt: user.crypt, list: $parent.category})" eat-click><profile-image class="cursor-pointer img-fullwidth" category=thumb size=xl background></profile-image><svg-verified ng-if=user.verified class="absolute-right absolute-top user-verificated text-blue"></svg-verified><span ng-if="category == \'kiss\'" title="{{ele.type.title}} {{\'of\' |translate}} {{user.name}}" class="absolute-top absolute-left kisstype" style=top:5px ng-class="ele.type.kisstype"></a><div ng-if="category == \'blocks\'" class="absolute-top absolute-left list-action cursor-pointer no-underline circle hover-base bg-gray-light bg-inverted" title="{{\'block_undo\'|translate}}" ng-click="action(ele, \'unblock\')"><i class="lo lo-close hover-hide"></i> <i class="lo lo-close text-white hover-show"></i></div><div ng-if="category == \'friends\'" class="absolute-top absolute-left list-action cursor-pointer no-underline circle hover-base bg-gray-light bg-inverted" title="{{\'friendshipend\'|translate}}" ng-click="action(ele, \'friend_remove\')"><i class="lo lo-close hover-hide"></i> <i class="lo lo-close text-white hover-show"></i></div><div ng-if="category == \'kiss\'" ng-show="ele.flag == 0" class="cursor-pointer no-underline text-center circle kissback bg-gray-light bg-inverted" tooltip="{{\'kiss_back\'|translate}}" ng-click="action(ele, \'kiss_back\')"><i class="lo lo-kissback text-white"></i></div><div ng-if="category == \'favorites\'" class="absolute-top absolute-left list-action cursor-pointer no-underline circle hover-base bg-gray-light bg-inverted" title="{{\'hotlist_delete\'|translate}}" ng-click="action(ele, \'fan_remove\')"><i class="lo lo-close hover-hide"></i> <i class="lo lo-close text-white hover-show"></i></div><user-hover user=user action-time={{ele.time}}></user-hover></div></div><loading-bar ng-show=isLoading></loading-bar><div ng-show="!isLoading && !users.length" class="padding space-before-sm h4 text-center">{{"nothing_found"|translate}}</div></div></div>');

        }]), angular.module("/template/list/intro.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/list/intro.html", '<div ng-if="!$root.isSecurityUser() || ($root.Self.picture && items.length > 0)" class="masonryContainer thumbnail-box"><div ng-if="items.length == 0" class=text-center>{{::"profile.user_has_no_moments"|translate}}</div><div ng-if="items.length > 0" class=masonry-item style="padding: 0 5px; width: 50%; box-sizing: border-box" ng-repeat="item in items track by $index|orderBy:sort" masonize masonize-gitter=0><div class=thumbnail><g-image width=100% category="{{ category || \'thumb\' }}" size="{{ size || \'m\' }}" id={{item.id}}></g-image></div></div></div>')
        }]), angular.module("/template/list/pictures.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/list/pictures.html", '<div ng-if="!$root.isSecurityUser() || ($root.Self.picture && items.length > 0)" class=row><div class="{{ className }}" ng-repeat="item in items | limitTo: !$root.isSecurityUser() ? 5: items.length"><div class=thumbnail><g-image id="{{ item.id }}" category="{{ category || \'image\' }}" size="{{ size || \'s\' }}" class="img-photo img-fullwidth img-responsive img-fullwidth cursor-pointer"></g-image></div></div><div ng-if=!$root.isSecurityUser() class=col-xs-2><div class="thumbnail-square relative"><div class="absolute full-width full-height bg-purple bg-hover-dark text-white"><p class="text-center text-vertical-center padding">{{ ::\'feed.show_all_post\' | translate }}</p></div></div></div></div><div ng-if="$root.isSecurityUser() && $root.Self.picture && items.length <= 0"><span class=h5>{{\'no_pic_name\'|translate:{\'user\': user.name} }}</span> <span class=h5>{{\'discover_more_name\'|translate:{\'user\': user.name} }}</span><br><a class="cursor-pointer h5" ng-click="action(user, \'chat\')">{{\'ask_now\'|translate}}</a></div><div ng-if="$root.isSecurityUser() && !$root.Self.picture"><div class=row><div class=col-xs-3><div class="thumbnail thumbnail-slidehover"><profile-image gender={{$root.Self.gender}} element=img></profile-image><img ng-src={{uploadImage}}><div class="absolute-bottom absolute-left absolute-right text-center"><div class="btn btn-primary btn-file btn-block"><photo-upload-button watch-id=profile-picture-upload></photo-upload-button></div></div></div></div><div class=col-xs-9><strong>{{\'self.upload.picture\'|translate}}</strong><p class=space-before-sm>{{\'self.upload.picture.info\'|translate:{\'username\': user.name} }}</p></div></div></div>')
        }]), angular.module("/template/list/simple-list.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/list/simple-list.html", '<div class="white-box padding" scroll-refresh=reloadList() scroll-refresh-distance=2><div class="row thumbnail-box"><div class="col-xs-{{c_columns}} ng-animate" ng-repeat="user in users track by user.id"><div class="thumbnail thumbnail-user thumbnail-slidehover thumbnail-square thumbnail-square" ng-class="{\'user-vip\': !!user.vip}"><a href={{$root.Ajax.url}}/profile/{{user.id}} ng-click="action(user, \'profile\', {crypt: user.crypt, list: $parent.category, func: navigate})" ng-class="{self: isSelf(user)}" eat-click><profile-image class="cursor-pointer img-fullwidth" category=thumb size=xl background></profile-image><svg-verified ng-if=user.verified class="absolute-right absolute-top user-verificated text-blue"></svg-verified></a><user-hover user=user action-time={{user.actionTime}}></user-hover></div></div></div><loading-bar ng-show=isLoading></loading-bar><div ng-show="!isLoading && !users.length" class="padding space-before-sm h4 text-center">{{"nothing_found"|translate}}</div></div>')
        }]), angular.module("/template/list/user-hover.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/list/user-hover.html", '<div class=caption><div class="h6 text-left text-overflow" ng-switch=user.crypt><span ng-switch-when=1>{{"unlock_user"|translate:{user: user.name} }}?</span><div ng-switch-default><online-status user=user></online-status><span has-emoji>{{user.name}}, {{user.age}} {{\'years\'|translate}}</span><br><div style=height:2px></div><span ng-if=actionTime>{{actionTime * 1000 | date_diff | translate}}, {{actionTime*1000 | date: "HH:mm"}}</span> <span ng-if=!actionTime><div ng-if=user.location>{{user.location}}</div><div ng-if="!user.location && user.city">{{user.city}}</div></span></div></div></div>')
        }]), angular.module("/template/listitem/credits.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/listitem/credits.html", '<div class=row><div class=col-xs-3 ng-switch="item.type == \'freecredits\' ? item.subtype: item.type"><i ng-switch-when=kiss class="pull-left h1 fa fa-2x fa-fw lo lo-lips" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"></i> <i ng-switch-when=topchat class="pull-left h1 fa fa-2x fa-fw lo lo-user-comment-star" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"></i> <i ng-switch-when=topflirt class="pull-left h1 fa fa-2x fa-fw fa fa-star" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"></i> <i ng-switch-when=unlockuser class="pull-left h1 fa fa-2x fa-fw lo lo-user-cursor" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"></i> <i ng-switch-default class="pull-left h1 fa fa-2x fa-fw lo lo-heart-circle" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"></i> <i ng-switch-when=verify class="pull-left h1 fa fa-2x fa-fw lo lo-at" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"></i> <span class="pull-left space-left-sm" ng-if=item.user ng-switch=item.user.del><profile-image ng-switch-when=1 gender={{item.user.gender}} ng-click="action(item.user, \'profile\')" size=s class="media-object img-xs img-circle cursor-pointer"></profile-image><profile-image ng-switch-default ng-click="action(item.user, \'profile\')" id={{item.user.picture}} size=s class="media-object img-xs img-circle cursor-pointer"></profile-image></span></div><div class=col-xs-3><h4 class=single-line>{{item.title}}</h4></div><div class=col-xs-3><h3 class="text-normal text-right space-right text-green single-line" ng-class="{out: \'text-pink\', in: \'text-green\'}[item.change]"><span ng-if="item.credits >= 0">+{{ "dynamic.confirm_credits"|translate:{credits: (item.credits|formatNumber)}:item.credits }}</span> <span ng-if="item.credits < 0">{{ "dynamic.confirm_credits"|translate:{credits: (item.credits|formatNumber)}:item.credits }}</span></h3></div><div class=col-xs-3><h5 class="single-line pull-right">{{item.time*1000 | date_diff | translate}}, {{item.time*1000 | date: "HH:mm"}}</h5></div></div>')
        }]), angular.module("/template/listitem/friend.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/listitem/friend.html", '<a class=pull-left href=#><profile-image category="" id="{{ item.picture }}" size=s imgsizing=32 class="media-object cursor-pointer img-fullwidth"></profile-image></a><div class=media-body><h4 class=list-group-user-heading>{{ item.name }} ({{item.age}})</h4><p ng-if=item.location>{{ item.location }}</p><p ng-if="!item.location && item.city">{{ item.city }}</p></div>')
        }]), angular.module("/template/listitem/kiss.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/listitem/kiss.html", '<div class="operation_icon relative"><kisstype-image type="{{ item.type.kisstype }}"></kisstype-image><span class="{{ item.change }}"></span></div><a href="#{{ item.user.id }}" class=face><profile-image category="" id="{{ item.user.picture }}" size=s imgsizing=32 class="user-pic cursor-pointer img-fullwidth"></profile-image></a> <a class=name href="#{{ item.user.id }}">{{ item.user.name }}, {{ item.user.age }} <span class=online_status></span></a> <span class=city>{{ item.user.city || item.user.location }}</span> <span class="status top60">{{ item.time*1000 | date:\'dd.MM.yyyy HH:mm:ss\' }}</span> <input ng-show="item.flag === 0" class="button kiss back absolute push_animated" type=button style=top:45px;right:20px;width:145px;paddingRight:3px value="{{ \'kiss_back\'|translate }}">')
        }]), angular.module("/template/listitem/pictures.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/listitem/pictures.html", '<g-image id="{{ item.id }}" category="{{category || \'image\'}}" size="{{size || \'s\'}}" class="img-photo img-fullwidth img-responsive img-fullwidth"></g-image>')
        }]), angular.module("/template/packages/credits-default.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/packages/credits-default.html", '<div ng-class="getPackageColor(pckg, packageColor)" class=white-box><div ng-if=pckg.bestPrice class="h3 text-white padding" style="text-transform: uppercase">{{ "credits.very_popular" | translate }}</div><div class="bg-white padding-lg"><i class="absolute-right space-right-sm fa fa-lg fa-info-circle text-success" data-toggle=popover></i><div class=table><div text-fill width-only max-font-size=50 inner-tag=h1 class="table-cell text-fill"><h1 ng-class="{\'text-normal\': !($first || $index == 1)}">{{ pckg.credits }}<br>{{ "menu_credits" | translate }}</h1></div></div><hr class="hr-none"><p><span class=text-gray ng-bind="\'only_price\' | translate: {price: pckg.localePrice, currency: pckg.currency }"></span></p><p><button ng-class=getButtonColor(packageColor) class="btn btn-block dowrap" ng-click=$parent.selectPackage(pckg)><i class="fa fa-shopping-cart space-right-sm"></i> {{"now_buy"|translate}}</button></p></div></div>')
        }]), angular.module("/template/packages/packages.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/packages/packages.html", '<div><div class="white-box padding space-after"><strong>{{ "stepbystep.title" | translate }}</strong><hr class=hr-md><div class="full-justify wizard"><span class="nowrap inline-block bg-white" ng-repeat-start="step in [1,2,3]"><span class="inline-block circle circle-xs bg-inverted" ng-class="{true: \'text-gray-dark bg-gray\', false: \'text-gray-light bg-gray-light\'}[purchaseStep == step]"><h3 ng-class="{true: \'text-bold\', false: \'text-light text-gray-light\'}[purchaseStep == step]">{{ step }}</h3><i ng-if="purchaseStep > step" class="lo lo-check"></i></span> <span ng-click=clickWizardStep(step) ng-class="{true: \'text-bold\', false: \'text-gray-light\'}[purchaseStep == step]"><span class=space-left ng-switch=step ng-class="{true: \'cursor-pointer\', false: \'\'}[purchaseStep > step && purchaseStep != 3]"><span ng-switch-when=1>{{ "choose_package" | translate }}</span> <span ng-switch-when=2>{{ "paymentmethod" | translate }}</span> <span ng-switch-when=3>{{ "confirmation" | translate }}</span></span></span></span> <span ng-repeat-end></span><div class=dotted-line></div></div></div><div ng-switch=purchaseStep><div ng-switch-when=1><div class="row flexbox-vertical-bottom packages space-after"><div ng-if=packages.length ng-repeat="pckg in packages" class="col-xs-3 text-center"><package></package></div><loading-bar class="text-center full-width" ng-if=!packages.length></loading-bar></div><span>{{"purchase.methods"|translate}}</span> <span ng-repeat="method in methods" class=space-right ng-class="{\'space-left-sm\': $first}"><img style="height: 20px" ng-src="{{$root.Ajax.imgUrlStatic}}/payment/{{method.key}}-t.png"></span></div><div ng-switch-when=2 class=list-group><purchase package-type="{{ packageType }}"></purchase></div><div ng-switch-when=3 class=list-group><div class=panel-default><div class="panel-heading text-white" ng-class="\'bg-\' + packageColor"><span class=h3>{{ "purchase.step3.title" | translate }}</span></div><div class=panel-body><p>{{ "purchase.step3.content" | translate }}</p></div><div class=panel-footer><a href-reload={{$root.Ajax.url}}/search class="btn btn-primary pull-right">{{ "purchase.step3.btn2" | translate }}</a> <a ng-show="packageType != 1" ng-click="$parent.purchaseStep = 1" class=btn ng-class=getButtonColor(packageColor)>{{ "purchase.step3.btn1" | translate }}</a><div class=clearfix></div></div></div></div><div ng-switch-when=4 class=list-group><div class=panel-default><div class="panel-heading text-white" ng-class="\'bg-\' + packageColor"><span class=h3>{{ "purchase.step4.title" | translate }}</span></div><div class=panel-body><p>{{ "purchase.step4.content" | translate }}</p></div><div class=panel-footer><a href-reload={{$root.Ajax.url}}/search class="btn btn-primary pull-right">{{ "purchase.step3.btn2" | translate }}</a> <a ng-show="packageType != 1" ng-click="$parent.purchaseStep = 1" class=btn ng-class=getButtonColor(packageColor)>{{ "purchase.step3.btn1" | translate }}</a><div class=clearfix></div></div></div></div></div></div>')
        }]), angular.module("/template/packages/vip-daily.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/packages/vip-daily.html", '<div ng-class="getPackageColor(pckg, packageColor)" class=white-box style="overflow: visible"><div ng-if=pckg.bestPrice class="h3 text-white padding" style="text-transform: uppercase">{{ \'vip.topseller\' | translate }}</div><div class="bg-white padding-lg relative"><h3 ng-if="pckg.saving > 0" class=flag style="line-height: 49px">-{{Math.round(pckg.saving*100)}}%</h3><h3 ng-if="pckg.saving == 0" class="flag flexbox-vertical-center flexbox-horizontal-center" style="font-size: 16px; line-height: 18px; text-transform: uppercase"><span ng-bind-html="\'vip.beginner\' | translate" class=relative style="top: -3px"></span></h3><div class="table no-space-after"><div text-fill max-font-size=65 inner-tag=h1 width-only class="table-cell text-fill" style="padding-top: 30px"><h1 style="line-height: 0.9" ng-bind-html=pckg.monthsHtml></h1></div></div><p style="margin-bottom: 22px"><small ng-bind="\'price_per_week\' | translate: {price: pckg.localeWeeklyPrice, currency: pckg.currency}"></small></p><p><button ng-class=getButtonColor(packageColor) class="btn btn-block dowrap" ng-click=selectPackage(pckg) ng-bind-html="\'now_buy_for\' | translate: {price: pckg.localePrice, currency: pckg.currency }"></button></p></div></div>')
        }]), angular.module("/template/packages/vip-default.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/packages/vip-default.html", '<div ng-class="getPackageColor(pckg, packageColor)" class=white-box><div ng-if=pckg.bestPrice class="h3 text-white padding">{{ "recommendation" | translate }}</div><div class="bg-white padding-lg"><strong class=h3>{{ pckg.idTitle }}</strong><hr><strong ng-if="pckg.saving == 0" ng-bind-html="\'vip.piece_of_happiness\'|translate"></strong> <strong ng-if="pckg.saving > 0" ng-bind-html="\'save_percent\'|translate:{\'percent\':Math.round(pckg.saving*100)}"></strong><div class=table><div text-fill max-font-size=100 inner-tag=h1 class="table-cell text-fill"><huge-price price=pckg.price></huge-price></div></div><p><button ng-class=getButtonColor(packageColor) class="btn btn-block dowrap" ng-click=selectPackage(pckg) ng-bind-html="\'now_buy\'|translate"></button></p><small ng-bind-html="\'vip.debit_every_months\' | translate: {months: pckg.months}:pckg.months"></small><br><small ng-bind-html="\'price_per_week\' | translate: {price: pckg.localeWeeklyPrice, currency: pckg.currency}"></small></div></div>')
        }]), angular.module("/template/packages/vip-weekly.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/packages/vip-weekly.html", '<div ng-class="getPackageColor(pckg, packageColor)" class=white-box><div ng-if=pckg.bestPrice class="h3 text-white padding" style="text-transform: uppercase">{{ \'vip.topseller\' | translate }}</div><div class="bg-white padding-lg"><strong class=h3>{{ pckg.idTitle }}</strong><hr><span ng-if="pckg.saving == 0" ng-bind-html="\'vip.piece_of_happiness\'|translate"></span> <span ng-if="pckg.saving > 0" ng-bind-html="\'save_percent\'|translate:{\'percent\':Math.round(pckg.saving*100)}"></span><div class="table no-space-after"><div text-fill max-font-size=100 inner-tag=h1 class="table-cell text-fill"><huge-price price=pckg.weeklyPrice></huge-price></div></div><h2 style="margin-top: 0">{{ \'per_week\' | translate }}</h2><p><button ng-class=getButtonColor(packageColor) class="btn btn-block dowrap" ng-click=selectPackage(pckg) ng-bind-html="\'now_buy_for\' | translate: {price: pckg.localePrice, currency: pckg.currency }"></button></p></div></div>')
        }]), angular.module("/template/page/challenge.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/challenge.html", '<div ng-click="tryApprouting(\'start\')"><loading-bar class=space-before ng-if=isLoadingChallenge></loading-bar><div ng-if=!isLoadingChallenge><div ng-class="$root.BS.xs ? \'\' : $root.routing.containerClass" class="header-background-cover bottom-shadow" cover-src="{{ coverSrc }}"><div class="flexbox-vertical-center flexbox-horizontal-center text-white"><div class="absolute-left absolute-bottom text-left" ng-switch=daysLeft.status><h3 ng-if=!$root.BS.xs class="space-after-sm space-left-xs">{{ ::\'challenge.details\' | translate }}</h3><p class=space-after-xs><i class="lo lo-watch space-right-sm"></i> <span ng-switch-when=upcoming>{{ "challenge.days_left_until_start"|translate:{days: daysLeft.days}:daysLeft.days }}</span> <span ng-switch-when=in-progress>{{ "challenge.days_left_until_end"|translate:{days: daysLeft.days}:daysLeft.days }}</span> <span ng-switch-default>{{ ::"challenge.challenge_ended"|translate }}</span></p><p ng-if="!$root.BS.xs && feedOptions.feed"><i class="fa fa-camera space-right-sm"></i> {{ "challenge.post"|translate:{posts:feedOptions.feed.allCount}:feedOptions.feed.allCount }}</p></div><div class=text-center><div class="circle inline-block bg-cover bg-centered" ng-class="{\'space-after-sm\': !$root.BS.xs}" ng-style="{ backgroundImage: \'url(\' + userPicture + \')\' }"></div><p ng-if=!$root.BS.xs class="space-before no-space-after"><button class="btn btn-default-inverted-white border-radius btn-border-2">{{ ::\'challenge.join_now\' | translate }}</button></p></div><div class="absolute-right absolute-bottom text-right"><p ng-if="$root.BS.xs && feedOptions.feed"><i class="fa fa-camera space-right-sm"></i> {{ "challenge.post"|translate:{posts:feedOptions.feed.allCount}:feedOptions.feed.allCount }}</p><div ng-if=!$root.BS.xs><h3 class="space-after-sm space-left-xs">{{ ::\'challenge.hashtags\' | translate }}</h3><p ng-repeat="hashtag in hashtags | limitTo: 3" class=space-after-xs>#{{ ::hashtag }}</p></div></div></div></div><div class="{{ $root.routing.containerClass }}" ng-class="$root.BS.xs ? \'space-before-sm\' : \'space-before bg-white\'"><div ng-class="$root.BS.xs ? \'\' : \'padding row space-after\'"><div ng-class="$root.BS.xs ? \'\' : \'col-xs-12\'"><h1 class="uppercase space-after-sm">{{ title }}</h1><p class="text-gray space-after">{{ subTitle }}</p><read-more-text chars="{{ $root.BS.xs ? 180 : 1000 }}" text="{{ description }}"></read-more-text><div ng-if=$root.BS.xs class="text-center space-before space-after-sm"><button class="btn btn-purple">{{ ::\'challenge.join_now\' | translate }}</button></div><div ng-if=!$root.BS.xs><h4 class="space-before-lg uppercase text-light">{{ ::\'feed.newest_posts\' | translate }}</h4><feed type=challenge feed-options=feedOptions></feed></div><tabset ng-if=$root.BS.xs class="feed-tabs feed-tabs-12"><tab heading="{{ ::\'feed.newest_posts\' | translate }}"><feed type=challenge feed-options=feedOptions></feed></tab></tabset><loading-bar class="space-before-sm space-after-sm" ng-if=feedOptions.isLoadingFeed></loading-bar><div ng-class="$root.BS.xs ? \'text-center\' : \'\'" class="space-before space-after-xs"><button class="btn btn-purple">{{ ::\'challenge.join_now\' | translate }}</button></div></div></div></div></div></div>')
        }]), angular.module("/template/page/contacts.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/contacts.html", "<div ng-class=$root.routing.containerClass><h5 class=section-title><tab-list links=links></tab-list><div class=pull-right><i columns-switcher></i></div></h5><div ng-switch=activePage><div ng-switch-when=favorites><user-list type=list mode=advanced-list category=favorites></user-list></div><div ng-switch-when=friends><user-list type=list mode=advanced-list category=friends></user-list></div></div></div>")
        }]), angular.module("/template/page/credits.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/credits.html", '<div ng-class=$root.routing.containerClass><h5 ng-if=$root.isSecurityUser() class=section-title><tab-list links=links></tab-list></h5><div ng-switch=activePage><div ng-switch-default><div ng-controller="CreditsPackagesCtrl as CreditsCtrl"><package-list package-type=2 package-color=green></package-list><div ng-if="showPackageDetails && $root.isSecurityUser()" class=space-before><br><h5 class=text-bold>{{"freecredits_title"|translate}}</h5><freecredits></freecredits></div></div></div><div ng-switch-when=history><div class="white-box padding space-after"><span class="fa-stack fa-2x space-right text-gray"><i class="lo lo-hand-credit fa-stack-2x"></i></span> <span class=h4>{{ "dynamic.confirm_credits"|translate:{credits: ($root.Self.credits|formatNumber)}:$root.Self.credits }}</span> {{"current_amount"|translate}} <a href={{$root.Ajax.url}}/credits class="btn btn-success pull-right space-before-sm">{{"buy_credits"|translate}}</a></div><item-list type=credits service-params=userParams sort=-time></item-list></div></div></div>')
        }]), angular.module("/template/page/eyecatcher.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/eyecatcher.html", "<div ng-class=$root.routing.containerClass><user-list columns=4 category=eyecatcher_global></user-list></div>")
        }]), angular.module("/template/page/fans.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/fans.html", "<div ng-class=$root.routing.containerClass><h5 class=section-title><tab-list links=links></tab-list><div class=pull-right><i columns-switcher></i></div></h5><div ng-switch=activePage><div ng-switch-when=fans><user-list type=list category=hotties></user-list></div><div ng-switch-when=favorites><user-list type=list mode=advanced-list category=favorites></user-list></div></div></div>")
        }]), angular.module("/template/page/flirtmatch.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/flirtmatch.html", "<div ng-class=$root.routing.containerClass><h5 class=section-title><tab-list links=links></tab-list><div class=pull-right ng-if=\"$root.activePage != 'vote'\"><i columns-switcher></i></div></h5><div ng-switch=$root.activePage><div ng-switch-when=you-want><user-list type=list category=youwant></user-list></div><div ng-switch-when=want-you><user-list type=list category=wantyou></user-list></div><div ng-switch-when=match><user-list type=list category=hit></user-list></div><div ng-switch-when=vote><flirtmatch></flirtmatch></div></div></div>")
        }]), angular.module("/template/page/hashtag.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/hashtag.html", '<div ng-click="tryApprouting(\'start\')"><div ng-show=feedOptions.feed><div ng-if=hashtagInfo class=bg-black><div class="relative overflow-hidden" ng-class="$root.BS.greaterSm ? $root.routing.containerClass : \'\'"><div ng-class="{\'row-main\': $root.BS.greaterSm}"><div ng-if=$root.BS.greaterSm class="col-md-2-5 col-main relative"><div ng-repeat="post in feedOptions.feed.posts | limitTo: 12" ng-if="$index % 2 == 0"><div class="absolute bg-centered bg-cover" ng-style="{backgroundImage: \'url(\' + post.getImage({width: thumbResolution}).url + \')\', width: thumbWidth, height: thumbWidth, top: $index % 4 ? thumbWidth + thumbBorder : 0, right: (Math.floor($index / 4) * (thumbWidth + thumbBorder)) - thumbPadding}"></div></div><div class="absolute-top-0 bottom-shadow" style="width: 300px" ng-style="{right: -thumbPadding, height: thumbWidth * 2 + thumbBorder, background: \'url(\' + $root.Ajax.imgUrlStatic + \'/gradient-left-lg.png) top left repeat-y\'}"></div></div><div ng-class="{\'col-md-7 col-main\': $root.BS.greaterSm}"><div class="header-background-cover bottom-shadow" cover-src="{{ hashtagInfo.coverSrc }}"><div class="flexbox-vertical-center flexbox-horizontal-center text-white" ng-style="{height: thumbWidth * 2 + thumbBorder}"><div class="relative inline-block" style="background-color: rgba(0, 0, 0, .5)"><h4 style="border: 1px solid white; padding: 10px 20px"><p class=text-spacing style="margin-bottom: 18px">#{{ hashtag }}</p><p class=no-space-after><follow-button type=hashtag value=hashtag class="btn btn-default-inverted-white border-radius text-spacing text-normal"></follow-button></p></h4></div><p class="absolute-bottom absolute-left-0 full-width text-center text-spacing">{{ \'hashtag.pictures\' | translate: {posts: hashtagInfo.allCount}: hashtagInfo.allCount }}</p></div></div></div><div ng-if=$root.BS.greaterSm class="col-md-2-5 col-main ralative"><div ng-repeat="post in feedOptions.feed.posts | limitTo: 12" ng-if="$index % 2 == 1"><div class="absolute bg-centered bg-cover" ng-style="{backgroundImage: \'url(\' + post.getImage({width: thumbResolution}).url + \')\', width: thumbWidth, height: thumbWidth, top: ($index - 1) % 4 ? thumbWidth + thumbBorder : 0, left: (Math.floor(($index - 1) / 4) * (thumbWidth + thumbBorder)) - thumbPadding}"></div></div><div class="absolute-top-0 bottom-shadow" style="width: 300px" ng-style="{left: -thumbPadding, height: thumbWidth * 2 + thumbBorder, background: \'url(\' + $root.Ajax.imgUrlStatic + \'/gradient-right-lg.png) top right repeat-y\'}"></div></div></div></div></div><div class="{{ $root.routing.containerClass }}" ng-class="$root.BS.xs ? \'space-before-sm\' : \'space-before\'"><div ng-if=!$root.BS.xs class="row-main space-after"><div class="col-md-2-5 col-main"></div><div class="col-md-7 col-main"><feed type=hashtag feed-options=feedOptions></feed></div><div class="col-md-2-5 col-main"></div></div><div ng-if=$root.BS.xs><div class="row text-center space-before space-after-sm"><button class="btn btn-purple"><i class="fa fa-camera space-right-sm"></i>{{ ::\'hashtag.upload_picture\' | translate }}</button></div><tabset class="feed-tabs feed-tabs-12"><tab heading="{{ ::\'feed.newest_posts\' | translate }}"><feed type=hashtag feed-options=feedOptions></feed></tab></tabset><div class="row text-center space-before space-after"><button class="btn btn-purple"><i class="fa fa-camera space-right-sm"></i>{{ ::\'hashtag.upload_picture\' | translate }}</button></div></div></div></div><loading-bar class=space-before ng-if=feedOptions.isLoadingFeed></loading-bar></div>')
        }]), angular.module("/template/page/helpcenter.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/helpcenter.html", '<div ng-class=$root.routing.containerClass><div class=content-secondary><div id=settings-menu class=white-box><ul class="panel-group media-list media-list-devided"><li class="media padding-md"><div class=media-body><h4 data-toggle=collapse href=#apps data-parent=#settings-menu class=cursor-pointer>{{ ::"lovoo_mobile" | translate }}</h4><div id=apps class="h4 list-unstyled collapse"><h5><a target=_blank href="http://itunes.apple.com/de/app/lovoo/id445338486?mt=8">{{ ::"lovoo_iphone" | translate }}</a></h5><h5><a target=_blank href="http://itunes.apple.com/de/app/lovoo/id445338486?mt=8">{{ ::"lovoo_ipad" | translate }}</a></h5><h5><a target=_blank href="https://market.android.com/details?id=net.lovoo.android">{{ ::"lovoo_android" | translate }}</a></h5></div></div></li><li class="media padding-md"><div class=media-body><h4 data-toggle=collapse href=#helpcenter data-parent=#settings-menu class=cursor-pointer>{{ ::"support_n_help" | translate }}</h4><div id=helpcenter class="h4 list-unstyled collapse"><h5><a target=_blank ng-href="{{ $root.Ajax.url + \'/faq\' }}">{{ ::"FAQ" | translate }}</a></h5><h5><a href="http://inside.lovoo.net/leitfaden-tipps-zur-sicherheit/" target=_blank>{{ ::"security_hints_new" | translate }}</a></h5></div></div></li><li class="media padding-md"><div class=media-body><div class=media-body><h4 data-toggle=collapse href=#general data-parent=#settings-menu class=cursor-pointer>{{ ::"general_hints"|translate }}</h4><div id=general class="h4 list-unstyled collapse"><h5><a class=cursor-pointer target=_blank href="http://static.lovoo.net/legal/{{switchLang }}">{{ ::"data_security" | translate }}</a></h5><h5><a class=cursor-pointer target=_blank href="http://static.lovoo.net/agb/{{ switchLang }}">{{ ::"br_short" | translate }}</a></h5></div></div></div></li><li class="media padding-md"><div class=media-body><h4><a href="http://inside.lovoo.net/news/" target=_blank>{{ ::"press_n_newsroom" | translate }}</a></h4></div></li><li class="media padding-md"><div class=media-body><h4 data-toggle=collapse href=#inside data-parent=#settings-menu class=cursor-pointer>{{ ::"inside_lovoo" | translate:{\'brand\':\'LOVOO\'} }}</h4><div id=inside class="h4 list-unstyled collapse"><h5><a href=http://inside.lovoo.net target=_blank>{{ ::"About us" | translate }} <i class="fa fa-link-external"></i></a></h5><h5><a href="http://inside.lovoo.net/jobs/" target=_blank>{{ ::"career_n_jobs" | translate }}</a></h5><h5><a href="http://inside.lovoo.net/category/testberichte/" target=_blank>{{ ::"test_n_experience" | translate }}</a></h5><h5><a href="http://inside.lovoo.net/business/affiliate/" target=_blank>{{ ::"affiliate_new" | translate }}</a></h5><h5><a href="http://inside.lovoo.net/business/boost/" target=_blank>{{ ::"App Promotion" | translate }}</a></h5><h5><a href=http://inside.lovoo.net/blog target=_blank>{{ ::"LOVOO Blog" | translate }}</a></h5><h5><a href=http://inside.lovoo.net/business/developer-api target=_blank>{{ ::"Developer / Api" | translate }}</a></h5><h5><a href=http://www.linkedin.com/pub/tobias-b%C3%B6rner/4a/3b4/758 target=_blank>{{ ::"business_request" | translate }}</a></h5></div></div></li></ul></div></div><div class=content-main><imprint user-id={{$root.Self.id}}></imprint></div></div>')
        }]), angular.module("/template/page/index-old.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/index-old.html", '<first-steps></first-steps><flirtmatch-page></flirtmatch-page><div class="content-wide space-after"><h5 class=section-title>{{"new_at_lovoo"|translate}}</h5><user-list type=list columns=6 category=start limit=6 scroll-refresh-disabled=true></user-list></div>')
        }]), angular.module("/template/page/index.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/index.html", "<first-steps></first-steps><profile-header user=$root.Self></profile-header><div ng-class=$root.routing.containerClass><feed-page></feed-page></div>")
        }]), angular.module("/template/page/post.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/post.html", '<div ng-click="tryApprouting(\'start\')"><loading-bar class=space-before ng-if=isLoading></loading-bar><div ng-if=!isLoading ng-switch=$root.BS.xs><div ng-switch-when=true><div class="header-background-cover bottom-shadow" cover-src="{{ coverSrc }}"><div></div></div><div ng-class=$root.routing.containerClass class=space-before-sm><section class=media><profile-image category=thumb imgsizing=60 size=s class="pull-left img-circle size-sm" ng-class="{\'user-vip\': !!user.vip}"></profile-image><div class=media-body><p><span class=text-bold>{{ ::user.name }}</span>, {{ ::user.age }}</p><p ng-if=user.location>{{ ::\'post.from_city\' | translate: {city: user.location} }}</p><p ng-if="!user.location && user.city">{{ ::\'post.from_city\' | translate: {city: user.city} }}</p></div></section><p class=space-left-sm><span class=quote>{{ ::post.caption }}</span></p><tabset class="feed-tabs feed-tabs-12"><tab heading="{{ ::\'post.similar_posts\' | translate }}"><feed type=similar feed=similarPosts></feed></tab></tabset><div class="row text-center space-before space-after-sm"><button class="btn btn-purple"><i class="fa fa-camera space-right-sm"></i>{{::"hashtag.upload_picture"|translate}}</button></div></div></div><div ng-switch-when=false ng-class=$root.routing.containerClass class="bg-white space-after"><div class=row style="height: 527px; padding-bottom: 5px"><div class="col-xs-3 flexbox-vertical-center flexbox-horizontal-center full-height"><div class=full-width><profile-image category=thumb imgsizing=60 size=s class="inline-block img-circle size-sm" ng-class="{\'user-vip\': !!user.vip}"></profile-image><h4 class="text-bold space-after-xs">{{ ::user.name }}<span class=text-normal>, {{ ::user.age }}</span></h4><p ng-if=user.location>{{ ::\'post.from_city\' | translate: {city: user.location} }}</p><p ng-if="!user.location && user.city">{{ ::\'post.from_city\' | translate: {city: user.city} }}</p><follow-button type=user value=user class="btn btn-inverted-purple btn-border-2 space-after"></follow-button><p class=no-space-after><span class=quote>{{ ::post.caption }}</span></p></div></div><div class="col-xs-6 full-height bg-cover bottom-shadow" ng-style="{background: \'url(\' + coverSrc + \') center\'}"><div class="absolute-left absolute-bottom"><div class="flexbox-vertical-center text-white"><span class=cursor-pointer ng-click=post.toggleLike()><i class="fa fa-2x fa-heart{{ post.hasLiked ? \'\' : \'-o\' }}"></i></span> <span class=space-left-sm>{{ post.likeCount | formatNumber }}</span></div></div></div><div class=col-xs-3><div class=row><div ng-if="similarPosts.length > 0" class=col-xs-6 style="padding-right: 0"><div class=bg-cover style="height: 128px" ng-style="{background: \'url(\' + similarPosts[0].imageUrl + \') center\'}"></div></div><div ng-if="similarPosts.length > 1" class=col-xs-6><div class=bg-cover style="height: 128px" ng-style="{background: \'url(\' + similarPosts[1].imageUrl + \') center\'}"></div></div></div><div class="row space-before-xs space-after-xs"><div ng-if="similarPosts.length > 2" class=col-xs-12><div class=bg-cover style="height: 256px" ng-style="{background: \'url(\' + similarPosts[2].imageUrl + \') center\'}"></div></div></div><div class=row><div ng-if="similarPosts.length > 3" class=col-xs-6 style="padding-right: 0"><div class=bg-cover style="height: 128px" ng-style="{background: \'url(\' + similarPosts[3].imageUrl + \') center\'}"></div></div><div ng-if="similarPosts.length > 3" class=col-xs-6 style="height: 128px"><div class="full-width full-height bg-purple bg-hover-dark text-white"><p class="text-center text-vertical-center padding">{{ ::\'feed.show_all_post\' | translate }}</p></div></div></div></div></div></div></div></div>');

        }]), angular.module("/template/page/profile-guest.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/profile-guest.html", '<div ng-if=user ng-click="tryApprouting(\'profile\', {u: user.id})"><div ng-class="$root.BS.xs ? \'\' : $root.routing.containerClass" class="header-background-cover dark-bg bottom-shadow" cover-src="{{ coverSrc }}"><div class="flexbox-vertical-center flexbox-horizontal-center text-white"><div ng-if=!$root.BS.xs class="absolute-left absolute-bottom text-left"><h3 class="space-after-sm space-left-xs">{{ ::user.name }}<span class=text-normal>, {{ ::user.age }}</span></h3><p class=space-after-xs><profile-location user=user></profile-location></p><p ng-if=!user.verified class=space-after-xs><svg-verified style="height: 14px; left: -3px; top: 2px; margin-right: -1px" class="text-blue relative"></svg-verified>{{ ::\'verified\' | translate }}</p><p><online-status show-description=1 icon-class=fa-fw user=user></online-status></p></div><div class=text-center><profile-image class="circle inline-block space-after-sm" overwrite-title="{{ ::user.name }}" size=s id="{{ ::user.picture }}" imgsizing=150 category=image gender="{{ ::user.gender }}"></profile-image><div ng-if=$root.BS.xs><h4 class="no-space-before space-after-xs"><b>{{ ::user.name }},</b> {{ ::user.age }}</h4><profile-location user=user></profile-location></div><p ng-if=!$root.BS.xs class="space-before no-space-after"><follow-button type=user value=user class="btn btn-default-inverted-white btn-radius btn-border-2"></follow-button></p></div><div ng-if="!$root.BS.xs && hashtags.length" class="absolute-right absolute-bottom text-right"><h3 class="space-after-sm space-left-xs">{{ ::\'profile.most_used\' | translate }}</h3><p ng-repeat="hashtag in hashtags | limitTo: 3" class=space-after-xs>#{{ ::hashtag }}</p></div></div></div><div class="{{ $root.routing.containerClass }}" ng-class="$root.BS.xs ? \'space-before-sm\' : \'space-before bg-white\'"><div ng-if=!$root.BS.xs class="padding row space-after"><div class=col-xs-12><h4 class="uppercase text-light space-after">{{ ::\'posts\' | translate }}</h4><self-gallery ng-if=isSelf></self-gallery><item-list ng-if=!isSelf class-name=col-xs-2 template=list/pictures type=pictures category=thumb size=l service-params="{\'userId\': user.id, resultLimit: 1}"></item-list></div><div class="col-xs-12 space-before"><h4 ng-if=$rootScope.isSecurityUser() class="uppercase text-light space-after">{{ ::\'profile.profile_information\' | translate }} ({{ user.counts.details * 100 | number: 0 }}%)</h4><div class=row><div ng-if=freeText class=col-xs-6><h5 class=text-bold>{{ ::\'about_me\' | translate }}</h5><p class=space-after-xs>{{ ::freeText }}</p></div><div ng-if=hashtags.length class=col-xs-6><h5 class=text-bold>{{ ::\'interests\' | translate }}</h5><p class=space-after-xs style="font-size: 150%"><span ng-repeat="hashtag in hashtags | limitTo: 20" class=space-after-xs ng-class="{\'space-right-xs\': !$last}">#{{ ::hashtag }}</span></p></div></div></div></div><div ng-if=$root.BS.xs><p>{{ user.whazzup }}</p><div class="row text-center space-before space-after-sm"><button class="btn btn-purple">{{::"profile.meet_now"|translate}}</button></div><tabset class="feed-tabs feed-tabs-6"><tab heading="{{ ::\'profile.moments\' | translate }}"><item-list template=list/intro type=pictures category=thumb size=s service-params="{\'userId\': user.id}"></item-list></tab><tab heading="{{ ::\'profile.profile_information\' | translate }} ({{ user.counts.details * 100 | number: 0 }}%)"><include-template ng-if=!isSelf name=profile/user_details></include-template></tab></tabset><div class="row text-center space-before space-after"><button class="btn btn-purple">{{ ::"profile.meet_now" | translate }}</button></div></div></div></div><loading-bar ng-if=!user class=space-before-lg></loading-bar>')
        }]), angular.module("/template/page/profile-old.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/profile-old.html", '<div ng-click=showRegisterDialog() ng-if=user ng-class=$root.routing.containerClass class=space-before><div class="relative section-title"><h1 class="h5 section-title text-overflow" has-emoji ng-bind-html="user.name + \', \' + user.age"></h1><ul ng-if=!isSelf class="list-inline absolute-right-0 absolute-top-0"><li class="dropdown dropdown-compact pull-right"><a class="cursor-pointer dropdown-toggle" data-toggle=dropdown><i class="fa fa-fw fa-cog"></i></a><ul class="dropdown-menu box-shadow-default space-before-xs"><li class=dropdown role=presentation><a class=cursor-pointer ng-click="userAction(user, \'block\')" role=menuitem>{{"block_do"|translate}}</a></li><li class=dropdown role=presentation><a class=cursor-pointer ng-click="userAction(user, \'report\', {referenceType: \'user\'})" role=menuitem>{{"report_profile"|translate}}</a></li></ul></li></ul></div><div class="panel panel-default padding overflow-visible"><table class=table><tr><td><profile-image overwrite-title="{{ user.name }}" size=s id="{{ user.picture }}" imgsizing=260 category=image background gender="{{ user.gender }}"></profile-image></td><td class="relative full-width"><div class=space-left><h2 ng-show="user.whazzup != \'\'" class="space-after h3"><span class="quote text-light break-word" has-emoji>{{ user.whazzup }}</span></h2><online-status show-description=1 icon-class="left-indent fa-fw" user=user></online-status><br><profile-location icon-class=left-indent user=user></profile-location></div><div ng-if=isSelf class="no-underline h3 text-light absolute-bottom absolute-left"><a href={{$root.Ajax.url}}/self/kisses><i class="lo lo-lips fa-lg"></i> {{ "profile.kisses" | translate: {kisses: (user.counts.k || 0)}: (user.counts.k || 0) }}</a> <a class=space-left-lg href={{$root.Ajax.url}}/self/visits><i class="lo lo-user-cursor fa-lg"></i> {{ "profile.visits" | translate: {visits: (user.counts.v || 0)}: (user.counts.v || 0) }}</a> <a class=space-left-lg href={{$root.Ajax.url}}/fans><i class="fa fa-star fa-lg"></i> {{ "profile.fans" | translate: {fans: (user.counts.h || 0)}: (user.counts.h || 0) }}</a></div><div ng-if=!isSelf class="absolute-bottom absolute-right"><div ng-click="userAction(user, \'like\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'You want\'|translate}}" data-toggle=tooltip data-original-title="{{\'You want\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="fa fa-heart fa-lg"></i></div><div ng-click="userAction(user, \'chat\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'send_msg\'|translate}}" data-toggle=tooltip data-original-title="{{\'send_msg\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="fa fa-envelope fa-lg"></i></div><div ng-click="userAction(user, \'kiss\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'send_kiss\'|translate}}" data-toggle=tooltip data-original-title="{{\'send_kiss\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="lo lo-lips fa-lg"></i></div><div ng-click="userAction(user, \'fan\')" class="cursor-pointer circle size-xs bg-blue text-white space-right-sm inline-block" tooltip="{{\'hotlist_add\'|translate}}" data-toggle=tooltip data-original-title="{{\'hotlist_add\'|translate}}" data-placement=top data-container=body tooltip-append-to-body=true><i class="fa fa-star fa-lg"></i></div></div></td></tr></table></div><div class="panel panel-default padding overflow-visible"><div class=relative><h3 class="font-size-normal space-after">{{"menu_gallery"|translate}} <span class=text-normal>({{ user.counts.p || 0 }})</span></h3><div id=gallery class=navbar-anchor-scroll></div><self-gallery ng-if=isSelf></self-gallery><item-list ng-if=!isSelf class-name=col-xs-2 template=list/pictures type=pictures category=thumb size=l service-params="{\'userId\': user.id}"></item-list><div class=clearfix></div></div><include-template ng-if=isSelf name=profile/self_details></include-template><include-template ng-if=!isSelf name=profile/user_details></include-template></div><loading-bar ng-if=!user class=space-before-lg></loading-bar></div>')
        }]), angular.module("/template/page/profile.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/profile.html", '<div ng-click="tryApprouting(\'profile\', {u: user.id})"><div ng-if=user><profile-header user=user></profile-header><div class="{{ $root.routing.containerClass }}" ng-class="$root.BS.xs ? \'space-before-sm\' : \'space-before\'"><div ng-if=!$root.BS.xs class=space-after ng-switch=!!$root.isSecurityUser()><div ng-switch-when=true class=row-main><div class="col-sm-12 col-md-2-5 col-main"></div><div class="col-sm-12 col-md-7 col-main"><feed feed-options=feedOptions></feed></div></div><div ng-switch-when=false><div class="panel panel-default padding overflow-visible"><table class=table><tr><td class="relative full-width"><div class=space-left><h2 ng-show="user.whazzup != \'\'" class="space-after h3"><span class="quote text-light break-word" has-emoji>{{ user.whazzup }}</span></h2><online-status show-description=1 icon-class="left-indent fa-fw" user=user></online-status><br><profile-location icon-class=left-indent user=user></profile-location></div></td></tr></table></div><div class="panel panel-default padding overflow-visible"><div class=relative><h3 class="font-size-normal space-after">{{"menu_gallery"|translate}} <span class=text-normal>({{ user.counts.p || 0 }})</span></h3><div id=gallery class=navbar-anchor-scroll></div><self-gallery ng-if=user.isSelf></self-gallery><item-list ng-if=!user.isSelf class-name=col-xs-2 template=list/pictures type=pictures category=thumb size=l service-params="{\'userId\': user.id}"></item-list><div class=clearfix></div></div><include-template ng-if=user.isSelf name=profile/self_details></include-template><include-template ng-if=!user.isSelf name=profile/user_details></include-template></div></div></div><div ng-if=$root.BS.xs><p>{{ user.whazzup }}</p><div class="row text-center space-before space-after-sm"><button class="btn btn-purple">{{::"profile.meet_now"|translate}}</button></div><tabset class="feed-tabs feed-tabs-6"><tab heading="{{ ::\'profile.moments\' | translate }}"><item-list template=list/intro type=pictures category=thumb size=s service-params="{\'userId\': user.id}"></item-list></tab><tab heading="{{ ::\'profile.profile_information\' | translate }} ({{ user.counts.details * 100 | number: 0 }}%)"><include-template ng-if=!isSelf name=profile/user_details></include-template></tab></tabset><div class="row text-center space-before space-after"><button class="btn btn-purple">{{ ::"profile.meet_now" | translate }}</button></div></div></div></div><loading-bar ng-if=!user class=space-before-lg></loading-bar></div>')
        }]), angular.module("/template/page/search-tracking.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/search-tracking.html", "<div ng-class=$root.routing.containerClass class=relative ng-controller=SearchTrackingCtrl><searchform></searchform><div class=\"search-results space-after\"><h1>{{ searchTitle|translate:{'city': city} }}</h1><user-list type=search></user-list></div></div>")
        }]), angular.module("/template/page/search.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/search.html", '<div ng-class=$root.routing.containerClass class=relative><searchform></searchform><div class="search-results space-after"><user-list ng-if=users type=search columns=4></user-list></div></div>')
        }]), angular.module("/template/page/self-gallery.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/self-gallery.html", '<div class="absolute-top-0 absolute-right-0"><div class="btn btn-primary btn-file btn-sm min-width-150"><photo-upload-button watch-id=profile-picture-upload></photo-upload-button></div></div><div ng-if=!$root.Self.picture><p class=space-before-sm>{{\'self.upload.picture\'|translate}}</p></div><div class=row><div ng-repeat="image in images.all() track by image.id" ng-cloak class=col-xs-2><div class="thumbnail thumbnail-slidehover thumbnail-fullshaded"><g-image id="{{ image.id }}" category=thumb size=l class="img-photo img-fullwidth img-responsive img-fullwidth cursor-pointer"></g-image><div class="absolute-top absolute-left cursor-pointer no-underline user-verificated circle hover-base bg-gray-light bg-inverted" title="{{\'delete_pic\'|translate}}" ng-click=removeImage(image)><i class="lo lo-close hover-hide"></i> <i class="lo lo-close text-white hover-show"></i></div><div ng-click="$root.Self.picture != image.id ? setFirst(image.id) : null" class="absolute-top absolute-right user-verificated cursor-pointer no-underline circle hover-base" ng-class="{\'true\': \'bg-green\', \'false\': \'bg-gray-light  bg-inverted\'}[$root.Self.picture === image.id]"><i class="fa fa-star hover-hide" title={{&quot;pic_for_profile&quot;|translate}}></i> <i class="fa fa-star text-white hover-show" title={{&quot;pic_for_profile&quot;|translate}}></i></div></div></div></div>')
        }]), angular.module("/template/page/self-page.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/self-page.html", "<div ng-class=$root.routing.containerClass ng-controller=SelfPagesCtrl><h5 class=section-title><tab-list links=links></tab-list><div class=pull-right><i columns-switcher></i></div></h5><div ng-switch=$root.activePage><div ng-switch-when=visits><user-list type=list category=visits limit=12></user-list></div><div ng-switch-when=visited><user-list type=list category=visited limit=12></user-list></div><div ng-switch-when=kisses><user-list type=list category=kiss limit=12 mode=advanced-list></user-list></div></div></div>")
        }]), angular.module("/template/page/settings.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings.html", '<div ng-class=$root.routing.containerClass><div class=row-wide><div class="content-secondary space-after"><h5 class="section-title text-overflow"><a class=active ng-href={{$root.Ajax.url}}/self>{{ $root.Self.name }}, {{ $root.Self.age }}</a></h5><div id=settings-menu class=white-box><ul class="media-list media-list-devided media-list-hover"><li class="media padding-md cursor-pointer" ng-repeat="section in sections" ng-class="{\'active\': section.url==page}" ng-if=!section.condition ng-click="action($index, section.url)"><div class=pull-left><h5 class=text-gray><i class="fa fa-lg fa-fw" ng-class=section.icon></i></h5></div><div class=media-body><h5 ng-class="{\'font-weight-bold\': section.url==page}"><span class=text-black>{{ section.title|translate }}</span></h5><div ng-if=section.sub class="space-left-sm list-unstyled submenu" ng-show="page==section.url"><h6 ng-repeat="sub in section.sub" ng-switch="section.url==page && sub.url==subpage" ng-if=!sub.condition><strong ng-switch-when=true>{{ sub.title|translate }}</strong> <a class="text-black cursor-pointer" ng-click="action($index, section.url ,sub.url)" ng-switch-default>{{ sub.title|translate }}</a></h6></div></div></li></ul></div></div><div class=content-main><h5 class=section-title><i class="lo lo-user-circle fa-lg"></i> {{ \'menu_settings\'|translate }} - {{SettingsPage| translate}}<div class=pull-right ng-if="subpage == \'blocked\'"><i columns-switcher></i></div></h5><settings page="{{ page }}" subpage="{{ subpage }}"></settings></div></div></div>')
        }]), angular.module("/template/page/settings/about.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/about.html", "<div ng-init=\"$parent.SettingsPage='About us'\"><imprint></imprint></div>")
        }]), angular.module("/template/page/settings/account-delete.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/account-delete.html", '<div ng-controller=SettingsAccountDeleteCtrl><div class="white-box padding space-after"><div class=row><div class=col-xs-5 ng-bind-html="\'account_delete_regrett\'|translate"></div><div class="col-xs-6 col-sm-offset-1"><a href="{{$root.Ajax.url}}/" class="btn btn-success btn-block space-before-sm">{{"prefer_continuing"|translate}}</a></div></div></div><div class="white-box padding"><form role=form name=deleteAcctForm id=delete_form novalidate handle-autofill><div class=form-group><div class=row><div class=col-xs-6><div class=checkbox ng-repeat="(key,label) in $root.DeleteReasons|limitObj:3"><label class="text-normal space-right-sm" for=del_{{key}}><input id=del_{{key}} name=delReasons[] type=checkbox ng-model="delReasons[key]"> {{ label }}</label></div></div><div class=col-xs-6><div class=checkbox ng-repeat="(key,label) in $root.DeleteReasons|skipTo:3"><label class="text-normal space-right-sm" for=delete_{{key}}><input id=delete_{{key}} name=delReasons[] type=checkbox ng-model="delReasons[key]"> {{ label }}</label></div></div></div></div><div class=form-group><textarea rows=2 class=form-control name=delText placeholder="{{ \'account_delete_more\'|translate }}" ng-blur="displayFieldError(\'delText\')" ng-maxlength=160 maxlength=161 ng-model=delText>\n                </textarea><div class="text-danger form-error" ng-show="deleteAcctForm.delText.$error.maxlength && displayError.delText"><small>{{"form_error_maxlength" | translate:{count:160} }}</small></div></div><div class=form-group><div class=row><div class=col-sm-6><input class="form-control password" type=password name=delPassword placeholder="{{ \'your_password\'| translate }}" ng-model=delPassword ng-blur="displayFieldError(\'delPassword\')" ng-required=true ng-minlength="5"><div ng-show="deleteAcctForm.delPassword.$error.required && displayError.delPassword" class="text-danger form-error"><small>{{"form_error_required" | translate}}</small></div><div ng-show="deleteAcctForm.delPassword.$error.minlength && displayError.delPassword" class="text-danger form-error"><small>{{"form_error_minlength" | translate:{count:5} }}</small></div></div><div class=col-sm-6><button type=submit ng-click=showConfirmationDialog2() ng-disabled="deleteAcctForm.$invalid || btnDisabled" class="btn btn-danger btn-block">{{ "delete_account"|translate }}</button></div></div></div></form></div></div>')
        }]), angular.module("/template/page/settings/account-linked.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/account-linked.html", '<div class="white-box padding" ng-controller=SettingsLinkedAccountsCtrl><div class=row><p class=col-xs-12>{{ "facebook_post_info"|translate }}</p><div class="col-xs-8 space-after"><button ng-click="$root.Self.fb ? facebookDisconnect() : facebookConnect()" class="btn btn-primary btn-lg btn-facebook text-left"><i class="fa fa-fw fa-facebook pull-left space-right-sm"></i> <span class="h3 text-normal">{{ $root.Self.fb ? ("facebook_disconnect"|translate) : ("facebook_connect"|translate) }}</span></button></div><div class=col-xs-8><button ng-click="$root.Self.socialNetworks.google ? googlePlus.disconnect() : googlePlus.connect()" class="btn btn-primary btn-lg btn-google-plus text-left hidden"><i class="fa fa-fw fa-google-plus pull-left space-right-sm"></i> <span class="h3 text-normal">{{ $root.Self.socialNetworks.google ? ("googleplus_disconnect"|translate) : ("googleplus_connect"|translate) }}</span></button></div></div></div>')
        }]), angular.module("/template/page/settings/account.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/account.html", '<div ng-controller=SettingsAccountMainCtrl><div class="white-box padding space-after" ng-if=!edit><button class="btn btn-primary pull-right" ng-click="$parent.edit=!$parent.edit">{{"edit" | translate}}</button><p ng-repeat="item in formFields" ng-if=!edit class=text-overflow ng-show=!item.condition><strong>{{ item.label }}</strong><br><span ng-if="item.key!=\'languages\' && item.key!=\'city\'">{{$root.Self[item.key]}}</span> <span ng-if="item.key==\'city\'">{{$root.Self.homeLocation.location ? $root.Self.homeLocation.location : "not_specified"|translate}}</span> <span ng-if="item.key==\'password\'">{{$root.Self.pwf != 2 ? \'*********\' : \'not_specified\'|translate}}</span> <span ng-if="item.key==\'languages\'">{{$root.Self[item.key]|languages}}</span> <span ng-if="item.key==\'user_id\'">{{$root.Self.id}}</span></p></div><div ng-if=edit><include-template name=form/account-settings/change-name></include-template><include-template name=form/account-settings/change-email></include-template><include-template name=form/account-settings/change-birthday></include-template><include-template name=form/account-settings/change-origin></include-template><include-template name=form/account-settings/change-password></include-template></div></div>')
        }]), angular.module("/template/page/settings/community-blocked.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/community-blocked.html", "<div ng-init=\"$parent.SettingsPage='blocked_members'\"><user-list type=list category=blocks limit=12 mode=advanced-list></user-list></div>")
        }]), angular.module("/template/page/settings/community-chat-blocker.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/community-chat-blocker.html", '<div class="white-box padding" ng-controller=SettingsChatBlockerCtrl><form ng-if=filter id=chatblockerForm role=form name=chatblockerForm><h4 class="space-after clearfix"><strong>{{ "who_can_not_write_me"|translate}}</strong><div class=pull-right><button ng-click=saveFilterSettings() class="btn btn-sm btn-success" ng-disabled="chatblockerForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button></div></h4><div class="row space-before"><ul class="col-xs-10 list-unstyled"><li ng-repeat="(setting,value) in filter"><label class=text-normal><input type=checkbox ng-change=handleClickInformation(setting) ng-model="filter[setting]"> {{ \'chat_blocker_\'+ setting|translate }}</label></li></ul></div><div class=text-normal>{{ "dynamic.requests_blocked"|translate:{requests:filteredMsg}:filteredMsg }}</div></form><div ng-if="!filter && !nothingFound">{{"is_loading"|translate}}<i class="fa fa-spinner fa-spin space-left-sm"></i></div><div ng-if="!filter && nothingFound">{{"nothing_found"|translate}}"|translate}}</div></div>')
        }]), angular.module("/template/page/settings/community.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/community.html", "<div ng-controller=SettingsCommunityMainCtrl><include-template name=form/searchfilter></include-template><include-template name=form/interests></include-template></div>")
        }]), angular.module("/template/page/settings/notifications.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/notifications.html", '<div ng-controller=SettingsNotificationsCtrl><div class="white-box padding space-after"><p class=space-after-sm><strong>{{ "desktop_notifications"|translate }}</strong></p><div class=space-after-sm><button ng-if=!isGranted ng-click=setDesktopNotifications() class="btn btn-primary btn-primary">{{"activate"|translate}}</button> <button ng-if=isGranted ng-click=setDesktopNotifications() class="btn btn-primary btn-primary">{{"disable"|translate}}</button></div></div><div ng-if=isGranted class="white-box padding space-after"><form id=pushSettingsForm role=form name=pushSettingsForm novalidate><h4 class="space-after clearfix"><strong>{{ "further_options"|translate}}</strong><div class=pull-right><button ng-click=save() class="btn btn-sm btn-success" ng-disabled="pushSettingsForm.$invalid || btnDisabled" class="btn btn-success">{{ "save" | translate }}</button></div></h4><div class=form-group><label class=space-after-sm>{{"desktop_notification_enable"|translate}}</label><div class=row><div class=col-xs-6 ng-repeat="(key, item) in pushOptions"><label class=text-normal><input type=checkbox name=key ng-model="pushSettings[key]"> {{ item.label |translate }}</label></div></div></div></form></div></div>')
        }]), angular.module("/template/page/settings/private.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/private.html", "<div ng-init=\"$parent.SettingsPage='vip_settings'|translate\"><vip-settings></vip-settings></div>")
        }]), angular.module("/template/page/settings/promo.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/promo.html", '<div class="panel panel-warning"><div class=panel-heading><h1>{{ "promo_invited"|translate }}</h1></div><div class=panel-body><form class=form-inline role=form><div class="form-group col-xs-2"><input type=text class=form-control placeholder=000></div><div class="form-group col-xs-3"><input type=text class=form-control placeholder=0000></div><div class="form-group col-xs-2"><input type=text class=form-control placeholder=000></div><div class="form-group col-xs-3"><input type=button class="btn btn-primary" value="{{ \'Check code\'|translate }}"></div></form></div><div class=panel-footer><input type=button class="btn btn-warning" value="{{ \'Redeem now\'|translate }}"></div></div>')
        }]), angular.module("/template/page/settings/support.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/settings/support.html", "<div ng-init=\"$parent.SettingsPage='support_n_help';\"><faq user-id={{$root.Self.id}}></faq></div>")
        }]), angular.module("/template/page/verify.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/verify.html", '<div class=content-main ng-init="list=1"><h5 class=section-title><i class="lo lo-user fa-lg"></i> {{"verify_now"|translate}}</h5><div class="white-box padding"><h4><strong>{{"hello_user"|translate:{name: $root.Self.name} }}!</strong></h4><p>{{"verify.text.1"|translate:{credits:verifyFreecredits} }}</p><button ng-if="!$root.Self.verified && !verifyUploadEnabled" class="btn btn-info pull-right" ng-click=scrollToVerification()>{{"verify_now"|translate}}</button> <button ng-if="!$root.Self.verified && verifyUploadEnabled" class="btn btn-info pull-right" ng-click=openUploadDialog()>{{"verify_now"|translate}}</button></div><h5 class="section-title space-before">{{"verify.title.2"|translate}}</h5><ul class=list-group><li class=list-group-item ng-repeat="adv in advantages"><i class="pull-left text-green fa fa-2x fa-fw lo-down-3" ng-class=adv.icon></i> <strong class="p list-group-item-heading" ng-bind-html=adv.title></strong> <i class="lo fa-2x lo-check pull-right text-success single-line"></i><p class=list-group-item-text><small ng-bind-html=adv.text></small></p></li></ul><include-template name=verify/verify_photo></include-template><include-template name=verify/verify_sms></include-template></div>')
        }]), angular.module("/template/page/vip.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/page/vip.html", '<div ng-class=$root.routing.containerClass><div ng-show=!isVip class=space-after><h5 class=section-title><i class="lo lo-poker-circle-o fa-lg"></i>{{ "become_vip" | translate }}</h5><package-list package-type=1 package-color=orange></package-list></div><br><div ng-if=isVip><h4><i class="lo lo-poker-circle-o fa-lg"></i> {{ "You are VIP member" | translate }}</h4><vip-settings></vip-settings><div ng-controller=VipPurchaseCtrl><div ng-if=displayCancelLink class="white-box padding space-after"><p>{{\'abo_info\' | translate}}</p><p ng-if=!cancelled>{{\'abo_extends\' | translate:{\'date\': endDate } }}</p><p ng-if=cancelled>{{\'abo_cancelled\' | translate:{\'date\': endDate } }}</p><p>{{\'chosen_payment\' | translate }}: <strong class=uppercase>{{type}}</strong></p><a ng-if=!cancelled class=cursor-pointer ng-click=confirmVipCancel()>{{\'cancel_abo\'|translate}}</a></div></div></div><div ng-if=showPackageDetails><table class="table white-box vertical-center"><tr><td colspan=2><div class="padding text-bold">{{ "vip.advantages"|translate }}</div></td><td><strong class="nowrap padding">{{ "vip.without_membership"|translate }}</strong></td><td><strong class="nowrap padding">{{ "vip.with_membership"|translate }}</strong></td></tr><tr ng-repeat="item in packageDetails"><td><i class="inline-block text-orange fa fa-2x fa-fw" ng-class=item.icon></i></td><td><strong>{{ item.title|translate }}</strong><br>{{ item.text|translate }}</td><td class=text-center><i ng-class="item.without ? \'lo-check\' : \'lo-close\'" class="lo fa-2x text-gray"></i></td><td class=text-center><i ng-class="item.with ? \'lo-check\' : \'lo-close\'" class="lo fa-2x text-orange"></i></td></tr><tr><td colspan=3><div class=padding><strong>{{ "have_questions"|translate }}</strong><br>{{ "vip.questions_about_vip"|translate }}</div></td><td class=text-center><a class="btn btn-warning" href="{{ ::$root.getHelpcenterLink() }}/sections/200292271-VIP" target=_blank ng-bind-html="\'to_faq\'|translate"></a></td></tr></table></div></div>')
        }]), angular.module("/template/profile/edit-interview.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/profile/edit-interview.html", '<div><p class="padding text-gray no-space-after">{{ ::\'profile_edit_interview.that_is_you\' | translate }}</p><accordion close-others=false><accordion-group is-open=true><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-pin"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::\'profile.details.title.hometown\' | translate }}</span><div class=text-semi-bold>{{ user.homeLocation.location }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormLocation class=row ng-submit=editInterview.updateLocation(FormLocation)><div class="col-xs-offset-2 col-xs-10"><location-field callback=editInterview.saveLocationToProfile preset-location=editInterview.location></location-field><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-venus-mars"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::\'genderLooking\' | translate }}</span><div class=text-semi-bold>{{ $root.Genders[user.genderLooking].genderGroupTranslated }} ({{ editInterview.age.from }} - {{ editInterview.age.to }})</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormSexualityAndAge class=row ng-submit=editInterview.updateSexualityAndAgeRange(FormSexualityAndAge)><div class="col-xs-10 col-xs-offset-2"><div class=space-after-sm><p>{{ ::\'genderLooking\' | translate }}</p><label class=text-normal ng-class="{\'space-right-sm\': !$last}" ng-repeat="(val, genderArr) in ::$root.Genders | limitObj: 2"><input type=radio name=genderLooking ng-model=user.genderLooking ng-checked="user.genderLooking == val" ng-click="checkButton(val, \'gender\')" ng-value="val"> {{ genderArr.genderGroupTranslated }}</label></div><div collapse-dom-refresh><p>{{ \'at_age_of\' | translate }} ({{ editInterview.age.from }} - {{ editInterview.age.to }})</p><slider class=slider-default step=1 name=ageRange floor="{{ $root.AgeRange.min }}" ceiling="{{ $root.AgeRange.max }}" ng-model-low=editInterview.age.from ng-model-high=editInterview.age.to move-end=editInterview.age.endMoving translate=editInterview.age.convertString></slider></div><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-married"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.flirt.status.title }}</span><div class=text-semi-bold>{{ user.details.flirt.status.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormRelationship class=row ng-submit=editInterview.updateDetails(FormRelationship)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="status in ::editInterview.detailOptions.flirt.status.opts | skipTo: 1" for="{{ status.id }}"><input id="{{ status.id }}" type=radio name=flirtStatus ng-model=user.details.flirt.status.id value="{{ status.id }}"> {{ status.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-user"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::\'profile.details.title.look\' | translate }}</span><div class=text-semi-bold ng-show=user.details.me.size>{{ user.details.me.size.label }} <span ng-show=user.details.me.fig>, {{ user.details.me.fig.label }}</span></div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormAppearance class=row ng-submit=editInterview.updateDetails(FormAppearance)><div class="col-xs-10 col-xs-offset-2"><div collapse-dom-refresh><p>{{ ::editInterview.detailOptions.me.size.title }}</p><slider class=slider-default step=1 name=size floor="{{ editInterview.size.min }}" ceiling="{{ editInterview.size.max }}" ng-model=editInterview.size.current translate=editInterview.size.convertString></slider></div><div class=row><p>{{ ::editInterview.detailOptions.me.fig.title }}</p><div class=col-xs-6><label class="show text-normal" ng-repeat="figure in ::editInterview.detailOptions.me.fig.opts | limitTo: 4 | skipTo: 1" for="{{ figure.id }}"><input id="{{ figure.id }}" type=radio name=figure value="{{ figure.id }}" ng-model="user.details.me.fig.id"> {{ figure.name }}</label></div><div class=col-xs-6><label class="show text-normal" ng-repeat="figure in ::editInterview.detailOptions.me.fig.opts | skipTo: 4" for="{{ figure.id }}"><input id="{{ figure.id }}" type=radio name=figure value="{{ figure.id }}" ng-model="user.details.me.fig.id"> {{ figure.name }}</label></div></div><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-children"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.me.child.title }}</span><div class=text-semi-bold>{{ user.details.me.child.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormChildren class=row ng-submit=editInterview.updateDetails(FormChildren)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="children in ::editInterview.detailOptions.me.child.opts | skipTo: 1" for="{{ children.id }}"><input id="{{ children.id }}" type=radio name=children value="{{ children.id }}" ng-model="user.details.me.child.id"> {{ children.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-smoke"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.me.smoke.title }}</span><div class=text-semi-bold>{{ user.details.me.smoke.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormSmoke class=row ng-submit=editInterview.updateDetails(FormSmoke)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="smoke in ::editInterview.detailOptions.me.smoke.opts | skipTo: 1" for="{{ smoke.id }}"><input id="{{ smoke.id }}" type=radio name=smoke value="{{ smoke.id }}" ng-model="user.details.me.smoke.id"> {{ smoke.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-religion"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.me.religion.title }}</span><div class=text-semi-bold>{{ user.details.me.religion.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormReligion class=row ng-submit=editInterview.updateDetails(FormReligion)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="religion in ::editInterview.detailOptions.me.religion.opts | skipTo: 1" for="{{ religion.id }}"><input id="{{ religion.id }}" type=radio name=religion value="{{ religion.id }}" ng-model="user.details.me.religion.id"> {{ religion.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-home"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.me.life.title }}</span><div class=text-semi-bold>{{ user.details.me.life.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormLife class=row ng-submit=editInterview.updateDetails(FormLife)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="life in ::editInterview.detailOptions.me.life.opts | skipTo: 1" for="{{ life.id }}"><input id="{{ life.id }}" type=radio name=life value="{{ life.id }}" ng-model="user.details.me.life.id"> {{ life.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-education"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.me.educ.title }}</span><div class=text-semi-bold>{{ user.details.me.educ.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormEducation class=row ng-submit=editInterview.updateDetails(FormEducation)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="education in ::editInterview.detailOptions.me.educ.opts | skipTo: 1" for="{{ education.id }}"><input id="{{ education.id }}" type=radio name=education value="{{ education.id }}" ng-model="user.details.me.educ.id"> {{ education.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm lo lo-briefcase"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::editInterview.detailOptions.me.job.title }}</span><div class=text-semi-bold>{{ user.details.me.job.label }}</div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormJob class=row ng-submit=editInterview.updateDetails(FormJob)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="job in ::editInterview.detailOptions.me.job.opts | skipTo: 1" for="{{ job.id }}"><input id="{{ job.id }}" type=radio name=job value="{{ job.id }}" ng-model="user.details.me.job.id"> {{ job.name }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group><accordion-group><accordion-heading><div class="row font-size-normal hover-base"><div class="col-xs-2 text-overflow text-center"><i class="fa fa-2x fa-fw space-before-sm fa-globe"></i></div><div class="col-xs-9 text-overflow"><span class=title>{{ ::\'profile.details.title.languages\' | translate }}</span><div class=text-semi-bold><span ng-repeat="language in user.languages">{{ $root.Languages[language] }} <span ng-hide=$last>,&nbsp;</span></span></div></div><div class="col-xs-1 text-right space-before-sm text-gray"><i class="fa fa-fw lo lo-pencil hover-show"></i> <i class="fa fa-fw fa-angle-up show-on-active"></i></div></div></accordion-heading><form name=FormLanguages class=row ng-submit=editInterview.updateLanguages(FormLanguages)><div class="col-xs-10 col-xs-offset-2"><label class="show text-normal" ng-repeat="language in ::editInterview.transformedLanguages | orderBy: \'value\'" for="{{ language.key }}"><input type=checkbox id="{{ language.key }}" ng-value=language.key name=languages[] ng-click=editInterview.toggleLanguageSelection(language.key) ng-checked="user.languages.indexOf(language.key) != -1"> {{ ::language.value }}</label><input type=submit class="btn btn-xs btn-primary pull-right text-normal font-size-normal space-before-sm" value="{{ \'profile_edit_details.save\' | translate }}"></div></form></accordion-group></accordion></div>');

        }]), angular.module("/template/profile/header-content.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/profile/header-content.html", '<div class="feed-header-cover dark-bg bottom-shadow"><div ng-if=user ng-class="$root.BS.xs ? \'\' : $root.routing.containerClass" class="flexbox-vertical-center flexbox-horizontal-center"><div class="text-center full-width text-white"><div class="relative inline-block" ng-style="{paddingTop: $root.BS.xs ? \'50px\' : 0}"><a ng-if=user.isSelf href="{{ ::$root.Ajax.url }}/self"><profile-image class="img-circle space-after-sm" overwrite-title="{{ ::user.name }}" size=s id="{{ ::user.picture }}" imgsizing=100 category=image gender="{{ ::user.gender }}"></profile-image></a> <a ng-if=!user.isSelf href="{{ ::$root.Ajax.url }}/profile/{{ user.id }}"><profile-image class="img-circle space-after-sm" overwrite-title="{{ ::user.name }}" size=s id="{{ ::user.picture }}" imgsizing=100 category=image gender="{{ ::user.gender }}"></profile-image></a><svg-verified ng-if=user.verified class="absolute-right absolute-bottom text-blue" size=20></svg-verified></div><div ng-if=user.isSelf class="space-before-sm no-space-after row display-flex flexbox" ng-class="{\'no-space\': $root.BS.xs}"><div class="col-xs-5-5 text-right"><p ng-switch=user.isSelf><span ng-switch-when=true>{{ \'profile_header.follow_you.self\' | translate: {count: user.counts.fls || 0} }}</span> <span ng-switch-when=false>{{ \'profile_header.follow_you.foreign\' | translate: {count: user.counts.fls || 0} }}</span> <span ng-class="{\'space-left-sm space-right-sm\': !$root.BS.xs}" class=hidden-xs>|</span> <span class=visible-xs-block></span> <span ng-switch-when=true>{{ \'profile_header.you_follow.self\' | translate: {count: user.counts.fld || 0} }}</span> <span ng-switch-when=false>{{ \'profile_header.you_follow.foreign\' | translate: {count: user.counts.fld || 0} }}</span></p><p>{{ \'profile_header.visitors\' | translate: {count: user.counts.v || 0} }}</p></div><div class="col-xs-1 flexbox-vertical-fill flexbox-horizontal-center"><div class=vertical-divider></div></div><div class="col-xs-5-5 text-left"><p ng-switch=user.isSelf><span ng-switch-when=true>{{ ::\'profile_header.your_interests.self\' | translate }}</span> <span ng-switch-when=false>{{ ::\'profile_header.your_interests.foreign\' | translate: {user: user.name} }}</span></p><p ng-if=user.interests.length class=clearfix><span ng-repeat="hashtag in user.interests | limitTo: 4" ng-class="{\'no-space-after\': $last}" class="white-links space-after-xs pull-left" ng-class="{\'space-right-xs\': !$last}"><a ng-href="{{ ::$root.Ajax.url }}/hashtag/{{ hashtag }}">#{{ hashtag }}</a></span></p><p ng-if="user.interests && !user.interests.length">{{ ::\'profile_header.no_interests_found\' | translate }}</p></div></div><div ng-if=!user.isSelf><p ng-if=profile_header.visitors.length>{{ \'profile_header.visitors\' | translate: {count: user.counts.v} }}</p><p ng-if=user.interests.length><span>{{ ::\'profile_header.your_interests.self\' | translate }}</span></p><p ng-if=user.interests.length class="clearfix text-center"><span ng-repeat="hashtag in user.interests | limitTo: 4" ng-class="{\'no-space-after\': $last}" class="white-links space-after-xs pull-left" ng-class="{\'space-right-xs\': !$last}"><a ng-href="{{ ::$root.Ajax.url }}/hashtag/{{ hashtag }}">#{{ hashtag }}</a></span></p></div></div></div></div>')
        }]), angular.module("/template/profile/profile-edit-details.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/profile/profile-edit-details.html", '<div ng-switch=editDetails.isLoading><loading-bar ng-switch-when=true></loading-bar><div ng-switch-when=false><textarea ng-model=editDetails.freetext maxlength="{{ ::editDetails.freetextMaxLength }}" class="btn-default-inverted-text-gray-dark full-width padding-sm space-before-sm space-after-sm font-size-normal" rows=5 placeholder="{{ ::\'profile_edit_details.freetext_placeholder\' | translate }}"></textarea><p class=clearfix><button ng-click=user.setFreetext(editDetails.freetext) class="btn btn-xs btn-primary pull-right text-normal font-size-normal">{{ ::\'profile_edit_details.save\' | translate }}</button></p><p class="text-gray text-spacing">{{ ::\'profile_edit_details.interested_in\' | translate }}</p><p class=clearfix><button ng-repeat="hashtag in user.interests" ng-click=editDetails.unlikeHashtag(hashtag) class="btn btn-xs btn-default-inverted-text-gray-dark space-right-xs space-after-xs pull-left text-normal font-size-normal">{{ ::hashtag }} <i class="fa fa-times-circle"></i></button></p><label class="text-gender no-space-after overflow-hidden nowrap full-width relative"><i ng-click=editDetails.likeHashtag(editDetails.newHashtag) class="absolute fa fa-lg lo lo-add cursor-pointer" style="top: 6px"></i> <span class=show style="padding-left: 24px"><input class="full-width input-gender text-normal font-size-normal" ng-enter=editDetails.likeHashtag(editDetails.newHashtag) ng-model=editDetails.newHashtag placeholder="{{ ::\'feed.add_hashtag\' | translate }}" type=text style="font-size: 22px"></span></label><hr><p class="text-gray text-spacing">{{ ::\'profile_edit_details.trending_hashtags\' | translate }}</p><p class="clearfix no-space-after"><button ng-repeat="hashtag in editDetails.trendingHashtags" ng-click=editDetails.likeHashtag(hashtag) class="btn btn-xs btn-default-inverted-text-gray-dark space-right-xs space-after-xs pull-left text-normal font-size-normal">{{ ::hashtag }} <i class="fa fa-plus-circle text-gender"></i></button></p></div></div>')
        }]), angular.module("/template/profile/self_details.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/profile/self_details.html", '<div class=relative><hr class="padding-negative space-after"><h3 class="font-size-normal space-after">{{"Details"|translate}} <span class=text-normal>({{user.counts.details*100|number:0}}%)</span></h3><div class=row><div class="col-xs-6 space-after" ng-repeat="(key, val) in userDetailOpts.me"><div class=row><div class="col-xs-4 text-overflow">{{val.title}}</div><div class="col-xs-8 text-overflow"><span ng-show=!isEditMode class=text-bold>{{ user.details.me[key].label || \'not_specified\'|translate }}</span> <span ng-show=isEditMode><select ng-model=saveData[key] class=form-control><option value="{{ opt.id }}" ng-selected="opt.id == user.details.me[key].id" ng-repeat="opt in val.opts">{{ opt.name }}</option></select></span></div></div></div></div><div class="absolute-top absolute-right-0"><div ng-if=!isEditMode ng-click=toggleEditMode(true) class="btn btn-sm btn-primary min-width-150"><i class="space-right-sm fa fa-pencil"></i> {{ "edit"|translate }}</div><div ng-if=isEditMode><div ng-click=saveProfile() class="btn btn-sm btn-success space-right-xs">{{ "save"|translate }}</div><div ng-click=toggleEditMode(false) class="btn btn-sm btn-danger">{{ "cancel"|translate }}</div></div></div></div><div class=relative><hr class="space-after"><h3 class="font-size-normal space-after">{{\'details.flirt\'|translate}}</h3><div class=row><div class="col-xs-6 space-after" ng-repeat="(key, val) in userDetailOpts.flirt"><p class=space-after-sm>{{val.title}}</p><p class=no-space-after><span ng-show=!isEditMode class=text-bold>{{ user.details.flirt[key].label || \'not_specified\'|translate }}</span> <span ng-show=isEditMode><select ng-model=saveData[key] class=form-control><option value="{{ opt.id }}" ng-selected="opt.id == user.details.flirt[key].id" ng-repeat="opt in val.opts">{{ opt.name }}</option></select></span></p></div></div><div class="absolute-top absolute-right"><div ng-if=!isEditMode ng-click=toggleEditMode(true) class="btn btn-sm btn-primary min-width-150"><i class="space-right-sm fa fa-pencil"></i> {{ "edit"|translate }}</div><div ng-if=isEditMode><div ng-click=saveProfile() class="btn btn-sm btn-success space-right-xs">{{ "save"|translate }}</div><div ng-click=toggleEditMode(false) class="btn btn-sm btn-danger">{{ "cancel"|translate }}</div></div></div></div>')
        }]), angular.module("/template/profile/svg-verified.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/profile/svg-verified.html", '<svg version=1.1 xmlns=http://www.w3.org/2000/svg xml:space=preserve xmlns:xml=http://www.w3.org/XML/1998/namespace viewbox="0 0 140 140"><g id=Verified_Background><title has-emoji ng-bind-html="::(\'is_verified\' | translate: {user: user.name})"></title><path fill=currentColor d="M130,79.357V12.845c0,0-33.262-12.599-60.006-12.599S10,12.845,10,12.845V80h0.007 c0,0-0.005,0.058-0.005,0.126c0,32.975,55.46,59.667,60,59.667s60.008-26.75,60.008-59.725C130.01,79.836,130,79.59,130,79.357z"></g><g id=Verified_Checkmark><polygon fill=#FFFFFF points="99.749,34.251 64.394,69.607 50.251,55.464 36.109,69.606 50.251,83.749 50.251,83.749 64.394,97.891 78.535,83.749 113.891,48.394"></g></svg>')
        }]), angular.module("/template/profile/user_details.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/profile/user_details.html", '<div ng-if=user.details.me><hr class="padding-negative space-after"><h3 class="font-size-normal space-after">{{"Details"|translate}} <span class=text-normal>({{user.counts.details*100|number:0}}%)</span></h3><div class=row><div class="col-xs-12 col-sm-6 space-after" ng-repeat="detail in user.details.me"><div class=row><div class="col-xs-12 col-sm-5 text-overflow" ng-class="$root.BS.xs ? \'space-after-sm\' : \'\'">{{detail.item}}</div><div class="col-xs-12 col-sm-7 text-overflow text-bold">{{detail.label}}</div></div></div></div></div><div ng-if=user.details.flirt><hr class="space-after"><h3 class="font-size-normal space-after">{{\'details.flirt\'|translate}}</h3><div class=row><div class="col-xs-12 col-sm-6 space-after-sm" ng-repeat="detail in user.details.flirt"><p class=space-after-sm>{{detail.item}}</p><p><strong>{{detail.label}}</strong></p></div></div></div><div ng-if="!$root.isSecurityUser() && user.details"><div class=row><div class="col-xs-12 col-sm-6 space-after-sm" ng-repeat="detail in user.details"><p class=space-after-sm>{{detail.item}}</p><p><strong>{{detail.label}}</strong></p></div></div></div><div ng-if="!user.details && !user.details.me && !user.details.flirt"><hr class="hr-none padding-negative space-after"><h3 class="font-size-normal space-after-sm">{{"Details"|translate}}</h3><span class=h5>{{\'no_entry_name\'|translate:{\'user\':user.name} }}</span> <span class=h5>{{\'discover_more_name\'|translate:{\'user\':user.name} }}</span><br><a class="cursor-pointer h5" ng-click="userAction(user, \'chat\')">{{\'ask_now\'|translate}}</a></div>')
        }]), angular.module("/template/testview/dialog_test.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/testview/dialog_test.html", '<div ng-class=$root.routing.containerClass ng-controller=DialogTestCtrl><div class="white-box padding space-before"><p>Teste FB + Redirecting</p><div class=row><div class=col-xs-4><button ng-click=facebookRegister() class="btn btn-primary btn-block btn-lg btn-facebook"><i class="fa fa-facebook space-right-sm"></i><span class="h3 text-normal">FB Button</span></button> <span class=help-block>* {{ "facebook_post_info" | translate }}</span></div><div class=col-xs-4><button class="btn btn-block btn-lg btn-info" href-reload="{{$root.Ajax.url }}/?direct_login=1">Direkt FB</button></div><div class=col-xs-4><button class="btn btn-block btn-lg btn-success" ng-click=locationCheck()>$location test <small>(s.console)</small></button></div></div></div><div class="white-box padding space-before"><div class="h5 space-after"><span>Lade direkt Dialog</span><div class=pull-right><span class="space-left-sm label label-info">Default</span> <span class="space-left-sm label label-danger">Offer</span> <span class="space-left-sm label label-success">Credits Paket</span> <span class="space-left-sm label label-warning">Vip Paket</span></div></div><small>Dialoge kommen von Test-Api /misc/dialog?id=[id] und sind nicht alle vollstÃ¤ndig</small><hr><div class="row space-before" ng-show=dialogTypes><div class=col-xs-12><ul class="content-columns-3 list-unstyled"><li ng-repeat="(key, dialog) in dialogTypes" class=space-after-sm><button class="btn btn-sm btn-block" title={{dialog.type}} ng-class="{\'btn-danger\':dialog.type==\'offer\', \'btn-success\':dialog.type==\'credits\', \'btn-warning\':dialog.type==\'vip\', \'btn-info\':dialog.type==\'default\'}" ng-click=load(key)>{{key}}</button></li></ul></div></div></div></div>')
        }]), angular.module("/template/testview/icons.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/testview/icons.html", '<div class=space-before ng-class=$root.routing.containerClass><h1 class=space-before>{{ title }} <small><span>Farbenshema:</span> <span ng-repeat="color in colorTheme" class=space-right-sm style="display: inline-block; height: 20px; width: 60px; background: rgb({{ color[0] }}, {{ color[1] }}, {{ color[2] }})"></span></small></h1><div class="white-box padding space-before"><h3 class="section-title space-after">Meta Daten</h3><div class=row><div class="col-xs-4 space-after-sm" ng-repeat="(key, pref) in preferences[0]"><div class=row><div class="col-xs-5 text-bold">{{ key }}:</div><div class=col-xs-7>{{ pref }}</div></div></div><div class="col-xs-4 space-after-sm" ng-repeat="(key, pref) in preferences[1]"><div class=row><div class="col-xs-5 text-bold">{{ key }}:</div><div class=col-xs-7><div ng-repeat="(oKey, oValue) in pref track by $index"><span class=text-bold>{{ oKey }}:</span> {{ oValue }}</div></div></div></div></div></div><div class="white-box padding space-before space-after"><h3 class="section-title space-after">Icons</h3><div class=row><div class=col-xs-5><div class="row space-after-sm"><label for=search class=col-xs-4>Suche:</label><input type=text id=search class=col-xs-4 name=search ng-model=search autocomplete="false"></div><div class="row space-after-sm"><label for=iconSize class=col-xs-4>SchriftgrÃ¶ÃŸe:</label><input type=text id=iconSize class=col-xs-4 name=iconFontSize ng-model="iconFontSize"></div><div class=row><label for=iconClasses class=col-xs-4>Icon-Klassen:</label><input type=text id=iconClasses class=col-xs-4 ng-model=newIconClass ng-enter="pushNewClass()"></div></div><div class="col-xs-6 col-xs-offset-1"><div ng-show=iconClasses.length><label>Icon Klassen:</label><ul class=list-inline><li style="padding: 0 !important" ng-class="{\'space-right-xs\': !$last}" ng-repeat="iconClass in iconClasses track by $index"><span class="label label-info text-normal">{{ iconClass }} <a href=# class="space-left-sm text-white no-underline" ng-click=removeIconClass($index)>x</a></span></li></ul></div></div><div class="col-xs-12 space-after-sm space-before-sm"><hr class="devider"></div></div><div class=row><div class=col-xs-6><div ng-repeat="icon in iconColumns[0] | filter: search" ng-style="{\'font-size\': iconFontSize + \'px\'}"><i class="lo {{ \'lo-\' + icon.name }}" ng-class=iconClasses></i> <span>lo-{{ icon.name }}</span></div></div><div class=col-xs-6><div ng-repeat="icon in iconColumns[1] | filter: search" ng-style="{\'font-size\': iconFontSize + \'px\'}"><i class="lo {{ \'lo-\' + icon.name }}" ng-class=iconClasses></i> <span>lo-{{ icon.name }}</span></div></div></div></div></div>')
        }]), angular.module("/template/testview/tutorial_test.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/testview/tutorial_test.html", '<div ng-class=$root.routing.containerClass ng-controller=TutorialTestCtrl><div class="white-box padding"><p class=h5>Teste Funktionen: [nÃ¤chstes], [ausschalten], [einmalige Anzeige]<br><small>(ggf. zurÃ¼cksetzen, wenn alles durch, oder ausgeschalten)</small></p><br><div class=row><div class=col-xs-12><ul class="content-columns-2 list-unstyled"><li><button class="btn btn-primary btn-sm btn-block" ng-click=load()>Lade nÃ¤chsten Dialog</button></li><li><button class="btn btn-danger btn-sm btn-block" ng-click=reset()>ZurÃ¼cksetzen</button></li></ul></div></div></div><div class="white-box padding space-before"><p class=h5>Lade direkt Tutorial Dialog</p><br><div class=row ng-show=tutorialTypes><div class=col-xs-12><ul class="content-columns-4 list-unstyled"><li ng-repeat="tutorialType in tutorialTypes" class=space-after-sm><button class="btn btn-warning btn-sm btn-block" ng-click=load(tutorialType)>{{tutorialType}}</button></li><li class=space-after-sm title="Augeschalten weil Text auf neuen NewsFeed verweist"><button class="btn btn-warning btn-sm btn-block" ng-disabled=true>tutorial_match</button></li><li class=space-after-sm title="Augeschalten weil Feature noch nicht drin"><button class="btn btn-warning btn-sm btn-block" ng-disabled=true>tutorial_adblocker</button></li><li class=space-after-sm title="Augeschalten weil Feature noch nicht drin"><button class="btn btn-warning btn-sm btn-block" ng-disabled=true>tutorial_like</button></li></ul></div></div></div></div>')
        }]), angular.module("/template/topmenu/svg-brand.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/topmenu/svg-brand.html", '<svg xmlns=http://www.w3.org/2000/svg version=1.1 viewbox="135 90 960 440" width=164 height=62 xml:space=preserve xmlns:xml=http://www.w3.org/XML/1998/namespace><g class=svg-title><polygon id=letter-1 fill=#808080 points="524.8,265.6 524.8,183 515.1,183 515.1,275.3 573.2,275.3 573.2,265.6"></polygon><path id=letter-2 fill=#808080 d="M637.3 180.7c-26.1 0-47.7 21.6-47.7 47.7s21.6 47.7 47.7 47.7c26.1 0 47.7-21.6 47.7-47.7C684.2 202.3 663.3 180.7 637.3 180.7zM637.3 266.4c-20.9 0-38-17.1-38-38s17.1-38 38-38 38 17.1 38 38S658.1 266.4 637.3 266.4z"></path><polygon id=letter-3 fill=#808080 points="798.1,183 757.2,260.4 717,183 705.8,183 754.2,275.3 760.9,275.3 809.3,183"></polygon><path id=leter-4 fill=#808080 d="M865.2 180.7c-26.1 0-47.7 21.6-47.7 47.7s21.6 47.7 47.7 47.7 47.7-21.6 47.7-47.7C912.1 202.3 890.5 180.7 865.2 180.7zM865.2 266.4c-20.9 0-38-17.1-38-38s17.1-38 38-38c20.9 0 38 17.1 38 38C902.4 249.2 885.3 266.4 865.2 266.4z"></path><path id=letter-5 fill=#808080 d="M994.7 180.7c-26.1 0-47.7 21.6-47.7 47.7s21.6 47.7 47.7 47.7c26.1 0 47.7-21.6 47.7-47.7S1020.8 180.7 994.7 180.7zM994.7 266.4c-20.9 0-38-17.1-38-38s17.1-38 38-38c20.9 0 38 17.1 38 38S1015.6 266.4 994.7 266.4z"></path></g><g class=svg-heart-bg><path marker-start=none marker-end=none fill=white d="M301.4 81c-29.5 0-53.1 9.8-72.8 21 -21.6-13.8-45.9-20.3-70.8-20.3C81 81 19.4 142.7 19.4 219.4c0 38 15.7 73.4 43.3 99.7L230.5 487c0 0 154.1-153.4 171.1-171.1 26.2-27.5 39.3-58.4 39.3-97C439.1 142.7 377.4 81 301.4 81z"></path></g><g class=svg-heart><path marker-start=none marker-end=none fill=#E63296 d="M231.9 290.9c17.7-17.7 29.5-43.9 29.5-70.8 0-56.4-46.6-103.6-103.6-103.6s-103 46.6-103 103c0 29.5 12.5 55.1 31.5 74.1l70.8 70.8 70.8-70.8C230.5 292.9 231.9 290.9 231.9 290.9L231.9 290.9z"></path><path marker-start=none marker-end=none fill=#32AADC d="M374.8 290.9c17.7-17.7 29.5-43.9 29.5-70.8 0-56.4-46.6-103.6-103.6-103.6S197.1 163 197.1 220.1c0 29.5 12.5 55.1 31.5 74.1l70.8 70.8 71.5-70.8C372.8 292.9 374.8 290.9 374.8 290.9L374.8 290.9z"></path><path marker-start=none marker-end=none fill=#FAAA3C d="M303.3 361.7c17.7-17.7 29.5-43.9 29.5-70.8 0-56.4-46.6-103.6-103.6-103.6s-103 46.6-103 103.6c0 29.5 12.5 55.1 31.5 74.1l70.8 70.8 70.8-72.8C301.4 363.7 303.3 361.7 303.3 361.7L303.3 361.7z"></path><path marker-start=none marker-end=none fill=#B450A0 d="M231.9 290.9L231.9 290.9c17.7-17.7 29.5-43.9 29.5-70.8 0-9.8-0.7-19.7-3.3-28.2 -9.2-2.6-17.7-3.3-28.2-3.3 -9.8 0-19.7 0.7-27.5 3.3 -2.6 9.2-3.3 17.7-3.3 28.2 0 29.5 12.5 55.1 31.5 74.1l0 0C230.5 292.9 231.9 290.9 231.9 290.9z"></path><path marker-start=none marker-end=none fill=#FA643C d="M197.1 219.4c0-9.8 0.7-19.7 3.3-28.2 -43.3 13.1-74.8 52.5-74.8 99.7 0 27.5 10.5 53.1 29.5 70.8l2.6 2.6 70.8-70.8C209.6 275.8 197.1 248.9 197.1 219.4z"></path><path marker-start=none marker-end=none fill=#5ABE0A d="M256.8 191.9c2.6 9.2 3.3 17.7 3.3 28.2 0 27.5-10.5 53.1-29.5 70.8l0 0c0 0-1.3 1.3-2.6 2.6l0 0 0 0 70.8 70.8 3.3-3.3c17-17.7 27.5-43.3 27.5-70.2C331.5 243 300.7 203.7 256.8 191.9z"></path><path marker-start=none marker-end=none fill=#8C3CA0 d="M229.9 187.3c9.8 0 19.7 0.7 28.2 3.3 -5.9-17.7-14.4-33.4-28.2-46.6 -13.1 13.1-23 28.2-28.2 46.6C210.2 189.2 219.4 187.3 229.9 187.3z"></path></g></svg>')
        }]), angular.module("/template/topmenu/topmenu-logged-in.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/topmenu/topmenu-logged-in.html", '<div class="collapse navbar-logged-in navbar-collapse navbar-ex1-collapse" ng-switch=navigation><ul ng-switch-when=classic class="nav nav-primary navbar-nav"><li ng-repeat="item in ::loggedIn.mainmenu track by $index" class="{{ item.listCss }}" ng-class="{active: isActive(item.url)}"><a class=cursor-pointer ng-if="item.id !== \'chats\'" ng-click="checkForOfferwallBeforeRouting(item.url.indexOf(\'?\') != -1 ? item.url.substring(0, item.url.indexOf(\'?\')) : item.url)"><i class="fa-fw text-normal fa-lg h3" ng-class=item.icon></i> <span class=hidden-xs>{{ item.title }}</span></a> <a ng-if="item.id == \'chats\'" ng-click=loggedIn.openChatDialog() class=cursor-pointer><i class="fa-fw text-normal fa-lg h3" ng-class=item.icon></i> <span class=hidden-xs>{{ item.title }}</span> <span ng-if=loggedIn.count.messages class="badge badge-up bg-pink">{{ loggedIn.count.messages }}</span></a></li></ul><ul class="nav navbar-nav navbar-right usermenu"><li ng-if="!$root.BS.xs && !$root.Self.vip && navigation == \'classic\'"><div><a class="btn btn-xs btn-warning" ng-href="{{ ::$root.Ajax.url + \'/vip\' }}" style="margin-top: 8px"><i class="lo lo-poker-circle-o fa-lg text-white relative" style="top: 2px"></i> <span class=visible-lg-inline>{{ ::\'become_vip\' | translate }}</span> <i class="fa fa-plus fa-fw text-white"></i></a></div></li><li ng-if="!$root.BS.xs && navigation == \'classic\'"><div><a ng-href="{{ ::$root.Ajax.url + \'/credits\' }}" class="btn btn-xs btn-success" style="margin-top: 8px"><i class="fa-fw text-normal fa-lg lo lo-credit-circle text-white relative" style="top: 2px"></i> <span class=visible-lg-inline>{{ \'dynamic.confirm_credits\' | translate: {\'credits\': ($root.Self.credits | formatNumber) }: $root.Self.credits }}</span> <i class="fa fa-fw fa-plus text-white"></i></a></div></li><li ng-if="navigation == \'modern\'"><a class="cursor-pointer small navbar-modern-button" ng-class="{\'active\': $root.activePage == \'feed\'}" ng-href="{{ ::$root.Ajax.url + \'/\' }}"><span>{{ ::\'topmenu.feed\' | translate }}</span></a></li><li ng-if="navigation == \'modern\'"><a class="cursor-pointer small tooltip-next small navbar-modern-button" ng-class="{\'active\': $root.activePage == \'match\'}"><span>{{ ::\'topmenu.match\' | translate }}</span></a><div class="nose-top absolute bg-white padding space-before-sm text-center small" style="width: 200px; margin-left: -100px; left: 50%">{{ ::\'topmenu.match_is_coming_back\' | translate }}</div></li><li class="dropdown cursor-pointer"><a class="dropdown-toggle topmenu" data-toggle=dropdown><i class="fa fa-bell fa-lg"></i> <span ng-show=loggedIn.count.info class="badge badge-up" ng-class="navigation == \'modern\' ? \'bg-gender\' : \'bg-pink\'">{{ loggedIn.count.info }}</span></a><ul class=dropdown-menu><li ng-if="$root.websiteVersion < 3.0" ng-repeat="entry in loggedIn.notifications track by $index" ng-switch=entry.type><a ng-switch-when=v ng-href="{{ ::$root.Ajax.url + \'/self/visits\' }}"><i class="lo lo-user-cursor fa-lg"></i> {{ entry.alert }}</a> <a ng-switch-when=mh ng-href="{{ ::$root.Ajax.url + \'/flirtmatch/match\' }}"><i class="lo lo-cards-users-check fa-lg"></i> {{ entry.alert }}</a> <a ng-switch-when=mv ng-href="{{ ::$root.Ajax.url + \'/flirtmatch/want-you\' }}"><i class="lo lo-cards-question fa-lg"></i> {{ entry.alert }}</a> <a ng-switch-when=k ng-href="{{ ::$root.Ajax.url + \'/self/kisses\' }}"><i class="lo lo-lips fa-lg"></i> {{ entry.alert }}</a> <a ng-switch-when=h ng-href="{{ ::$root.Ajax.url + \'/fans\' }}"><i class="lo lo-user-star fa-lg"></i> {{ entry.alert }}</a></li><li ng-if="$root.websiteVersion >= 3.0" ng-repeat="entry in loggedIn.notifications track by $index" ng-switch=entry.type><a ng-switch-when=visit ng-href="{{ ::$root.Ajax.url + \'/self/visits\' }}"><i class="lo lo-user-cursor fa-lg"></i> {{ \'notifications.news.visit\' | translate: {count: entry.count}: entry.count }}</a> <a ng-switch-when=photo_like ng-href="{{ ::$root.Ajax.url + \'/profile/\'+ entry.actor.id }}"><i class="fa fa-heart k fa-lg space-right-sm"></i> {{ \'notifications.news.photo_like\' | translate: {count: entry.count}: entry.count }}</a> <a ng-switch-when=follower ng-href="{{ ::$root.Ajax.url + \'/profile/\'+ entry.actor.id }}"><i class="fa fa-thumbs-o-up fa-lg space-right-sm"></i> {{ \'notifications.news.follower\' | translate: {count: entry.count}: entry.count }}</a> <a ng-switch-when=reply ng-href="{{ ::$root.Ajax.url + \'/profile/\'+ entry.actor.id }}"><i class="fa fa-reply fa-lg space-right-sm"></i> {{ \'notifications.news.reply\' | translate: {count: entry.count}: entry.count }}</a></li><li ng-show=!loggedIn.notifications.length class="padding-sm text-center text-muted"><i class="fa fa-lg fa-frown-o space-right-sm"></i> {{ ::\'no_notifications\' | translate }}</li></ul></li><li ng-if="navigation == \'modern\'"><a ng-click=loggedIn.openChatDialog() class="topmenu cursor-pointer"><i class="fa fa-comment fa-lg"></i> <span ng-if=loggedIn.count.messages class="badge badge-up" ng-class="navigation == \'modern\' ? \'bg-gender\' : \'bg-pink\'">{{ loggedIn.count.messages }}</span></a></li><li class="dropdown cursor-pointer"><a class="dropdown-toggle topmenu cursor-pointer" data-toggle=dropdown ng-switch=navigation><profile-image ng-switch-when=classic id="{{ $root.Self.picture }}" category=thumb size=s imgsizing=32 classname=userimg class="image-responsive img-circle" ng-class="{\'user-vip\': !!$root.Self.vip}" style="margin-top: -4px"></profile-image><i ng-switch-when=modern class="fa fa-gear fa-lg"></i></a><ul class=dropdown-menu><li><a class=cursor-pointer ng-click="checkForOfferwallBeforeRouting($root.Ajax.url + \'/self\')"><i class="lo lo-user-square-o fa-fw fa-lg space-right-sm"></i> {{ ::\'menu_profile\' | translate }}</a></li><li><a ng-href="{{ $root.Ajax.url + \'/credits\' }}"><i class="lo lo-credit-circle fa-fw fa-lg space-right-sm"></i> {{ ::\'menu_credits\' | translate }}</a></li><li ng-if="(!$root.Self.smsVerified || $root.Self.verified == false || $root.Self.verified == 0 || $root.Self.verified == 2) && $root.websiteVersion < 3.0"><a class=cursor-pointer ng-click="checkForOfferwallBeforeRouting($root.Ajax.url + \'/verify\')"><i class="lo lo-user fa-fw fa-lg space-right-sm"></i> {{ ::\'verify_now\' | translate }}</a></li><li><a ng-href="{{ $root.Ajax.url + \'/vip\' }}"><i class="lo lo-poker-circle-o fa-fw fa-lg space-right-sm"></i> {{ ::\'menu_vip\' | translate }}</a></li><li ng-if="$root.websiteVersion >= 3.0"><a ng-href="{{ $root.getHelpcenterLink() }}"><i class="fa fa-life-ring fa-fw fa-lg space-right-sm"></i> {{ ::\'menu_faq\' | translate }}</a></li><li><a class=cursor-pointer ng-click="checkForOfferwallBeforeRouting($root.Ajax.url + \'/settings/account\')"><i class="fa fa-fw fa-lg fa-gear space-right-sm"></i> {{ ::\'menu_settings\' | translate }}</a></li><li><a target=_blank class=cursor-pointer href="{{ $root.Ajax.url }}/imprint"><i class="fa fa-fw fa-lg fa-file-text-o space-right-sm"></i> {{ ::\'Imprint\' | translate }}</a></li><li><a ng-click=loggedIn.doLogout() target=_self><i class="fa fa-fw fa-lg fa-sign-out space-right-sm"></i> {{ ::\'logout_title\' | translate }}</a></li></ul></li></ul></div>')
        }]), angular.module("/template/topmenu/topmenu-logged-out.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/topmenu/topmenu-logged-out.html", '<div class="collapse navbar-collapse navbar-ex1-collapse"><button ng-click=onLogin() class="btn navbar-btn navbar-right pull-right" ng-class="topmenuClass == \'navbar-modern-dark-xs\' ? \'btn-success\' : $root.websiteVersion < 3 ? \'btn-default-inverted\' : \'btn-default-inverted-gray-dark\'">{{ ::\'login_submit\' | translate }}</button><ul class="nav navbar-nav navbar-right space-right-lg capitalize hidden-xs"><li class=dropdown><a class="dropdown-toggle cursor-pointer" data-toggle=dropdown><img class="lo-up-1 space-right-sm" ng-src="{{ ::$root.Ajax.imgUrlStatic }}flags/{{ $root.Translator.locale }}.png" style="margin-top: -3px"> {{ $root.Languages[$root.Translator.locale] }} <i class="fa fa-chevron-down space-left-sm"></i></a><ul class=dropdown-menu role=menu><li ng-repeat="(key, value) in ::$root.Languages"><a class=cursor-pointer ng-click=changeLanguageToLocale(key) target=_self title="{{ value }}"><img class="lo-up-1 space-right-sm" ng-src="{{ ::$root.Ajax.imgUrlStatic }}flags/{{ key }}.png"> {{ value }}</a></li></ul></li></ul><p class="navbar-text navbar-right space-right-lg hidden-xs">{{ ::\'get_the_app\' | translate }} <a class=space-left-sm target=_blank href="http://itunes.apple.com/{{ ::$root.Translator.locale }}/app/lovoo/id445338486?mt=8" title="App Store"><i class="fa fa-lg fa-apple text-gray"></i></a> <a class=space-left-sm target=_blank href="https://market.android.com/details?id=net.lovoo.android" title="Google Play"><i class="fa fa-lg fa-android text-gray"></i></a></p></div>')
        }]), angular.module("/template/topmenu/topmenu-mobile.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/topmenu/topmenu-mobile.html", '<header class="center-block text-center space-after navbar-padding"><a ng-if="application!=\'voome\'" class="brand-logo no-underline flexbox-vertical-center" href-reload="{{ $root.Ajax.url }}/" style="display: block"><svg-image class=header-logo height=60 width=60 source=lovoo_logo alt=.png></svg-image><svg-image class=header-logo-type height=30 width=150 source=lovoo_logo_type alt=.png></svg-image></a> <a ng-if="application==\'voome\'" class="brand-logo no-underline flexbox-vertical-center" href-reload="https://voo.me/" style="display: block"><svg-image class=header-logo height=60 width=60 source=logo_voo alt=.png></svg-image></a></header>')
        }]), angular.module("/template/topmenu/topmenu.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/topmenu/topmenu.html", '<div ng-class="{\'navbar-padding\': !isTransparent}"><nav class="navbar navbar-default navbar-fixed-top navbar-lovoo transition" ng-class=topmenuClass><div ng-class=$root.routing.containerClass><a class=cursor-pointer ng-href="{{ ::$root.Ajax.url }}" ng-switch=brandLogo ng-click="checkForOfferwallBeforeRouting($root.Ajax.url + \'/\')" eat-click><svg-brand ng-switch-when=classic class="navbar-brand svg-brand"></svg-brand><img ng-switch-when=small-white class=pull-left style="height: 35px; margin-top: 8px" ng-src="{{ ::$root.Ajax.imgUrlStatic }}lovoo-logo-type-white.svg"> <img ng-switch-when=small-grey class=pull-left style="height: 35px; margin-top: 8px" ng-src="{{ ::$root.Ajax.imgUrlStatic }}lovoo-logo-type-grey.svg"></a><div ng-switch=$root.isSecurityUser()><topmenu-logged-in ng-switch-when=true></topmenu-logged-in><include-template ng-switch-when=false name=topmenu/topmenu-logged-out></include-template></div></div></nav></div>')
        }]), angular.module("/template/unique-page.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/unique-page.html", "<include-template ng-if=\"$root.uniquePageData.type == 'challenge'\" ng-controller=ChallengeCtrl name=\"{{ 'page/' + $root.uniquePageData.type }}\"></include-template><include-template ng-if=\"$root.uniquePageData.type == 'hashtag'\" ng-controller=HashtagCtrl name=\"{{ 'page/' + $root.uniquePageData.type }}\"></include-template><div ng-if=\"$root.uniquePageData.type == 'profile'\" ng-switch=$root.uniquePageData.template><include-template ng-switch-when=page/profile-old ng-controller=ProfilePageCtrl name=page/profile-old></include-template><include-template ng-switch-when=page/profile-guest ng-controller=ProfilePageCtrl name=page/profile-guest></include-template><include-template ng-switch-when=page/profile ng-controller=ProfileFeedPageCtrl name=page/profile></include-template></div>");

        }]), angular.module("/template/verify/verify_photo.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/verify/verify_photo.html", '<div id=verifyAnchor ng-controller=VerifyPhotoCtrl class=space-after><div ng-show=!$root.Self.verified class="white-box padding space-before"><h4><strong>{{"verify.step.first"|translate}}</strong></h4><div class=row><div class=col-sm-8><h5 class=space-before>{{"verify.step.fb"|translate}}</h5><button ng-disabled="facebookVerifyFailed || errors || verifyUploadEnabled" ng-click=connectFacebook() class="btn btn-block btn-primary btn-lg text-left btn-facebook"><i class="fa fa-facebook pull-left space-left-sm space-right-sm"></i> {{"verify_facebook"|translate}}</button> <small ng-if=facebookVerifyFailed><strong>{{ "verify_facebook_failed" | translate }}</strong></small><h5 class=space-before>{{"verify.step.vip"|translate}}</h5><button ng-disabled="hasBoughtPackage.vip || errors || verifyUploadEnabled" ng-click=becomeVip(); class="btn btn-block btn-warning btn-lg text-left"><i class="lo lo-poker fa-lg lo-down-2 space-left-sm space-right-sm"></i> {{"become_vip"|translate}}</button><h5 class=space-before>{{"verify.step.credits"|translate}}</h5><button ng-disabled="hasBoughtPackage.credits || errors || verifyUploadEnabled" ng-click=buyCredits(); class="btn btn-block btn-primary btn-lg text-left btn-info"><i class="lo lo-credit fa-lg space-left-sm space-right-sm lo-down-1"></i> {{"buy_credits"|translate}}</button><h4 class=space-before><strong>{{"verify.step.second"|translate}}</strong></h4><h5 ng-if=errors class=text-danger><strong>{{errors[0].message}}</strong></h5><div ng-if=errors ng-switch=errors[0].code><button ng-switch-when=2323 ng-click=sendConfirmMail() class="btn btn-block btn-info btn-lg text-left"><i class="fa fa-envelope lo-down-3 pull-left space-left-sm space-right-sm"></i> {{"resend_confirmation_mail"|translate}}</button><div ng-switch-when=2324 class="btn btn-file btn-block btn-info btn-lg text-left"><photo-upload-button watch-id=profile-picture-upload></photo-upload-button></div></div><button ng-disabled="errors || !verifyUploadEnabled" ng-click=openUploadDialog() class="btn btn-block btn-success btn-lg text-left space-before"><i class="fa fa-upload pull-left lo-right-2 lo-down-2 space-left-sm space-right-sm"></i> {{"verify_now"|translate}}</button></div></div></div><div ng-show=$root.Self.verified class="white-box padding space-before"><h5 class=section-title>{{"verify_profile"|translate}}</h5><p ng-show="$root.Self.verified == 1 || $root.Self.verified  == true" class=space-after>{{"verified"|translate }} <i class="lo lo-check lo-up-1 space-left-sm text-success single-line"></i></p><p ng-show="$root.Self.verified == 2" class=space-after>{{"outstanding"|translate }}</p></div></div>')
        }]), angular.module("/template/verify/verify_sms.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/verify/verify_sms.html", '<div id=verifyAnchorSMS ng-controller=VerifySmsCtrl><div ng-show="!$root.Self.smsVerified && (featureSmsVerification==VERIFY_SMS_ENABLED || featureSmsVerification==VERIFY_SMS_SENT)" class="white-box padding space-before"><h5 class=section-title>{{"verify.sms"|translate}}</h5><p class=space-after ng-show="featureSmsVerification==VERIFY_SMS_ENABLED">{{"verify.text.sms"|translate }}</p><p class=space-after ng-show="featureSmsVerification==VERIFY_SMS_SENT">{{"verify_sms_info"|translate }}</p><div class="row form-group"><validation-errors></validation-errors><form ng-show="featureSmsVerification==VERIFY_SMS_ENABLED" id=verifySMSForm name=verifySMSForm novalidate><div class=col-xs-4><select class=form-control ng-model=country name=country ng-options="ctr.prefix as ctr.prefix + \' (\' + ctr.title +\')\' for (key,ctr) in countries"></select></div><div class=col-xs-6><input type=number class=form-control ng-model=phoneNumber name=phoneNumber placeholder="{{\'your_phone_number\'|translate}}" ng-blur="displayFieldError(\'phoneNumber\')" ng-required="true"> <small ng-show="verifySMSForm.phoneNumber.$error.required && displayError.phoneNumber" class="text-danger form-error" ng-bind-html="\'form_error_required\' | translate"></small> <small ng-show="verifySMSForm.phoneNumber.$error.number && displayError.phoneNumber" class="text-danger form-error" ng-bind-html="\'form_error_number\' | translate"></small></div><div class=col-xs-2><button ng-click=submitPhoneNumber() ng-disabled="verifySMSForm.$invalid || btnDisabled" class="btn btn-info btn-block">{{"send"|translate}}</button></div></form><form ng-show="featureSmsVerification==VERIFY_SMS_SENT" name=verifyCodeForm id=verifyCodeForm ng-submit=submitSmsCode()><div class=col-xs-8><input type=text class=form-control name=verifyCode placeholder="{{ \'your_verify_code\'|translate }}" ng-blur="displayFieldError(\'verifyCode\')" ng-required=true ng-model="verifyCode"><div class="text-danger form-error" ng-show="verifyCodeForm.verifyCode.$error.required && displayError.verifyCode"><small>{{"form_error_required" | translate}}</small></div></div><div class=col-xs-4><button class="btn btn-primary btn-block" type=submit ng-disabled="verifyCodeForm.$invalid || btnDisabled">{{ "send"|translate }}</button></div></form></div><p class=space-before-sm ng-show="featureSmsVerification==VERIFY_SMS_SENT">{{"sms_not_received"|translate }}<br>{{"sms_resend"|translate}}: <i ng-click=displayPhoneNumberForm() class="fa fa-pencil-square-o lo-down-2 text-normal text-info cursor-pointer"></i></p></div><div ng-show=$root.Self.smsVerified class="white-box padding space-before"><h5 class=section-title>{{"verify.sms"|translate}}</h5><p class=space-after>{{"user_is_sms_verified"|translate }} <i class="lo lo-check lo-up-1 space-left-sm text-success single-line"></i></p></div></div>')
        }]), angular.module("/template/widget/detail-count.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/widget/detail-count.html", '<a class=cursor-pointer ng-href={{$root.Ajax.url}}/self><div class="progresspanel panel panel-default white-box padding-sm padding-horizontal"><div class="space-before-sm space-after-sm"><div class=h5>{{"complete_profile"|translate}}</div><div id=pointerWidth class=pppointer><strong>{{detailsCount}}%</strong></div><div class=pull-left>0%</div><div class=pull-right>100%</div><div class=clearfix></div><div class=ppslider><div id=pointerProgress class=ppposition></div></div></div></div></a>')
        }]), angular.module("/template/widget/first-steps.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/widget/first-steps.html", '<div class=modal-header><button class=close ng-click="dismiss(\'close\')">&times;</button><h5 class=section-title>{{"fstep.welcome"|translate}}</h5></div><div class=modal-body><div class="row space-after"><div class=col-xs-1><svg-image class=img-responsive width=67 height=65 source=lovoo_logo alt=.png></svg-image></div><div class=col-xs-11><h4 class="text-bold media-heading">{{"hello_user"|translate:{\'name\': $root.Self.name} }},</h4><p>{{"fstep.description"|translate}}</p></div></div><div><div class=list-group-item><div class=row><div class="col-xs-1 h4 text-left" ng-class="{\'text-success\': $root.Self.confirmed, \'text-gray\': !$root.Self.confirmed}"><i class="lo lo-at h2 text-normal single-line space-left-sm"></i></div><div class=col-xs-4><h4 class=single-line>{{"fstep.title.email"|translate}}</h4></div><div class=col-xs-3 ng-switch=$root.Self.confirmed><h4 ng-switch-when=0 class="single-line text-pink">+{{"dynamic.confirm_credits"|translate:{credits:help.email.credits}:help.email.credits}}</h4><h4 ng-switch-when=1 class="single-line text-overflow">{{"credits_earned"|translate:{\'credits\': help.email.credits} }}</h4></div><div class="col-xs-3 col-sm-offset-1 text-center"><button class="btn btn-primary btn-block" ng-if=!$root.Self.confirmed ng-click=confirmMail() ng-disabled=disableConfirm>{{"send"|translate}}</button> <i ng-if=$root.Self.confirmed class="lo lo-check lo-up-4 fa-2x text-success space-before-sm"></i></div></div></div><div class=list-group-item><div class=row><div class=col-xs-1 ng-class="{\'text-success\': $root.Self.verified, \'text-gray\': !$root.Self.verified}"><i class="lo lo-user-circle lo-left-2 h2 text-normal single-line space-left-sm"></i></div><div class=col-xs-4><h4 class=single-line>{{"fstep.title.verify"|translate}}</h4></div><div class=col-xs-3 ng-switch=$root.Self.verified><h4 ng-switch-when=0 class="single-line text-pink">+{{"dynamic.confirm_credits"|translate:{credits:help.verify.credits}:help.verify.credits}}</h4><h4 ng-switch-when=1 class="single-line text-overflow">{{"credits_earned"|translate:{\'credits\': help.verify.credits} }}</h4></div><div class="col-xs-3 col-sm-offset-1 text-center" ng-switch=$root.Self.verified><button ng-switch-when=2 class="btn btn-primary btn-block" ng-disabled=true>{{"verify_in_progress"|translate}}</button> <i ng-switch-when=1 class="fa-2x lo lo-check lo-up-4 text-success space-before-sm"></i> <a ng-switch-default class="btn btn-primary btn-block" ng-click=close() ng-href={{$root.Ajax.url}}/verify>{{"verify_now"|translate}}</a></div></div></div><div class=list-group-item><form role=form ng-submit=setMessage(this) class=row><div class=col-xs-1 ng-class="{\'text-success\': $root.Self.whazzup, \'text-gray\': !$root.Self.whazzup}"><i class="lo lo-loudspeaker fa-2x single-line space-left-sm"></i></div><div class=col-xs-11 ng-if=!$root.Self.whazzup><h4 class=single-line>{{"fstep.title.whazzup"|translate}} <small class=pull-right>({{"letters_to_go"|translate:{\'letters\': WHAZZUP_MAXLENGTH-$parent.whazzup.length} }})</small></h4><textarea required name=whazzup ng-trim=false ng-model=$parent.whazzup rows=3 class="form-control space-after" maxlength={{WHAZZUP_MAXLENGTH}}></textarea></div><div class=col-xs-8 ng-if=$root.Self.whazzup><h4 class=single-line>{{"fstep.title.whazzup"|translate}}</h4></div><div class="col-xs-3 pull-right text-center"><button type=submit class="btn btn-primary btn-block" ng-if=!$root.Self.whazzup>{{"save"|translate}}</button> <i ng-if=$root.Self.whazzup class="lo lo-check lo-up-4 fa-2x text-success space-before-sm"></i></div></form></div></div></div>')
        }]), angular.module("/template/widget/profile-box.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/widget/profile-box.html", '<div class="sidebar sidebar-left sidebar-profile" ng-init="self = $root.Self"><h1 class="h5 section-title text-overflow" has-emoji><a class=active ng-href={{$root.Ajax.url}}/self>{{self.name}}, {{self.age}}</a></h1><div class="list-group list-group-profile white-box"><div class="list-group-item text-white padding-lg sidebar-blurred-user-image"><div class=blurred-cover width=323 height=120 blur-radius=15 cover-src={{coverSrc}}></div><a ng-href={{$root.Ajax.url}}/self class="cursor-pointer pull-left space-right"><profile-image id={{self.picture}} category=thumb size=m class="img-circle img-md" ng-class="{\'user-vip\': !!self.vip}"></profile-image><svg-verified ng-init="user=self" ng-if="self.verified === true || self.verified === 1" class="user-verificated text-blue"></svg-verified></a><div class=media-body><p id=editWhwazzup class="cursor-pointer line-height-sm" title="{{\'change_stat\'|translate}}" ng-click=editWhazzupMessage()><span has-emoji ng-model=whazzup onsubmit=setMessage maxlength={{WHAZZUP_MAXLENGTH}} ng-bind="self.whazzup || \'custom.whazzup\'|translate"></span> <i class="space-left-sm fa fa-pencil cursor-pointer"></i></p></div></div><a class="list-group-item no-underline" ng-class="{\'active\': $root.activePage==\'visits\'}" ng-href={{$root.Ajax.url}}/self/visits ng-init="hoverVisits = false" ng-mouseenter="hoverVisits = true" ng-mouseleave="hoverVisits = false"><span class="pull-left space-right"><profile-image ng-if=v.pictureId id={{v.pictureId}} category=thumb size=s class="img-circle img-xs" overwrite-title="{{\'Visits\'|translate}}"></profile-image><div ng-if=!v.pictureId class="pull-left circle circle-xs bg-inverted" ng-class="{true: \'bg-black-semi\', false: \'bg-gray-light\'}[hoverVisits || $root.activePage==\'visits\']"><i class="fa fa-2x lo lo-right-1 lo-user-cursor"></i></div></span><h5>{{ "Visits"|translate }}<span class="pull-right h3 text-normal" ng-class="{\'text-danger\': v.new}">{{v.count|formatNumber}}</span></h5><div class=clearfix></div></a> <a class="list-group-item no-underline" ng-href={{$root.Ajax.url}}/fans ng-class="{\'active\': $root.activePage==\'fans\'}" ng-init="hoverFans = false" ng-mouseenter="hoverFans = true" ng-mouseleave="hoverFans = false"><span class="pull-left space-right"><profile-image ng-if=h.pictureId id={{h.pictureId}} category=thumb size=s class="img-circle img-xs" overwrite-title="{{\'fans\'|translate}}"></profile-image><div ng-if=!h.pictureId class="pull-left circle circle-xs bg-inverted" ng-class="{true: \'bg-black-semi\', false: \'bg-gray-light\'}[hoverFans || $root.activePage==\'fans\']"><i class="fa fa-2x lo lo-right-1 lo-user-star"></i></div></span><h5>{{ "Fans"|translate }}<span class="pull-right h3 text-normal" ng-class="{\'text-danger\': h.new}">{{h.count|formatNumber}}</span></h5><div class=clearfix></div></a> <a class="list-group-item no-underline" ng-href={{$root.Ajax.url}}/self/kisses ng-class="{\'active\': $root.activePage==\'kisses\'}" ng-init="hoverKisses = false" ng-mouseenter="hoverKisses = true" ng-mouseleave="hoverKisses = false"><span class="pull-left space-right"><profile-image ng-if=k.pictureId id={{k.pictureId}} category=thumb size=s class="img-circle cursor-pointer img-xs" overwrite-title="{{\'kisses\'|translate}}"></profile-image><div ng-if=!k.pictureId class="pull-left circle circle-xs bg-inverted" ng-class="{true: \'bg-black-semi\', false: \'bg-gray-light\'}[hoverKisses || $root.activePage==\'kisses\']"><i class="fa fa-2x lo lo-left-1 lo-lips"></i></div></span><h5>{{ "Kisses"|translate }}<span class="h3 pull-right text-normal" ng-class="{\'text-danger\': k.new}">{{k.count|formatNumber}}</span></h5><div class=clearfix></div></a> <a class="list-group-item no-underline" ng-href={{$root.Ajax.url}}/flirtmatch/match ng-class="{\'active\': $root.activePage==\'match\'}" ng-init="hoverMatch = false" ng-mouseenter="hoverMatch = true" ng-mouseleave="hoverMatch = false"><span class="pull-left space-right"><profile-image ng-if=mh.pictureId id={{mh.pictureId}} category=thumb size=s class="img-circle cursor-pointer img-xs" overwrite-title="{{\'Matches\'|translate}}"></profile-image><div ng-if=!mh.pictureId class="pull-left circle circle-xs bg-inverted" ng-class="{true: \'bg-black-semi\', false: \'bg-gray-light\'}[hoverMatch || $root.activePage==\'match\']"><i class="fa fa-2x lo lo-left-1 lo-cards-users-check"></i></div></span><h5>{{ "Matches"|translate }} <span ng-if=mh.new class="h3 pull-right text-normal text-danger">{{mh.count|formatNumber}}</span></h5><div class=clearfix></div></a> <a class="list-group-item no-underline" ng-href={{$root.Ajax.url}}/flirtmatch/want-you ng-class="{\'active\': $root.activePage==\'want-you\'}" ng-init="hoverWantYou = false" ng-mouseenter="hoverWantYou = true" ng-mouseleave="hoverWantYou = false"><span class="pull-left space-right"><profile-image ng-if=mv.pictureId id={{mv.pictureId}} category=thumb size=s class="img-circle cursor-pointer img-xs" overwrite-title="{{\'Want you\'|translate}}"></profile-image><div ng-if=!mv.pictureId class="pull-left circle circle-xs bg-inverted" ng-class="{true: \'bg-black-semi\', false: \'bg-gray-light\'}[hoverWantYou || $root.activePage==\'want-you\']"><i class="fa fa-2x lo lo-left-1 lo-cards-question"></i></div></span><h5>{{ "Want you"|translate }} <span ng-if=mv.new class="h3 pull-right text-normal text-danger">{{mv.count|formatNumber}}</span></h5><div class=clearfix></div></a> <a class="list-group-item no-underline" ng-href={{$root.Ajax.url}}/flirtmatch/you-want ng-click=updateWantYou(); ng-class="{\'active\': $root.activePage==\'you-want\'}" ng-init="hoverYouWant = false" ng-mouseenter="hoverYouWant = true" ng-mouseleave="hoverYouWant = false"><span class="pull-left space-right"><profile-image ng-if=yw.pictureId id={{yw.pictureId}} category=thumb size=s class="img-circle cursor-pointer img-xs" overwrite-title="{{\'You want\'|translate}}"></profile-image><div ng-if=!yw.pictureId class="pull-left circle circle-xs bg-inverted" ng-class="{true: \'bg-black-semi\', false: \'bg-gray-light\'}[hoverYouWant || $root.activePage==\'you-want\']"><i class="fa fa-2x lo lo-left-1 lo-cards-check"></i></div></span><h5>{{ "You want"|translate }}</h5><div class=clearfix></div></a></div></div>')
        }]), angular.module("/template/widget/purchase.html", []).run(["$templateCache", function($templateCache) {
            $templateCache.put("/template/widget/purchase.html", '<div class=panel-default><div class="panel-heading text-white relative" ng-class="\'bg-\' + $parent.packageColor"><span class=h3>{{package.idTitle}}</span> <button ng-if=$parent.$parent.changeStep ng-click=$parent.$parent.changeStep(1) class="btn btn-link text-white absolute-right"><span class="fa fa-angle-left fa-fw"></span> {{ ::("Back"|translate) }}</button></div><div class="panel-body relative"><div class=row><div class=col-xs-12><div class="col-xs-4 row space-after-sm"><strong ng-if="package.weeklyPrice > 0" ng-bind-html="\'price_per_week\' | translate: {price: package.localeWeeklyPrice, currency: package.currency}"></strong> {{ ::("purchase.paypal" | translate) }}<div class="row text-bold space-before-sm"><div class=col-xs-9>{{ ::("Sum" | translate)}}</div><div class="col-xs-3 text-right">{{package.price | formatLocalePrice}} {{package.currency}}</div></div></div><div class="col-xs-4 col-xs-offset-4"><form action={{actionUrl}} target="{{ purchaseTarget }}" method=POST class="pull-right space-before-sm"><input ng-repeat="hiddenField in paypalFormScope" name={{hiddenField.name}} value={{hiddenField.value}} type="hidden"> <input type=hidden value="{{ package.id }}" name="{{!paypalFormScope.length ? \'productId\' : \'item_number\'}}"> <input type=hidden value="{{ package.custom }}" name=custom> <input type=hidden value="{{ package.price }}" name=amount> <input type=hidden value="{{ package.currency }}" name=currency_code> <input type=hidden value={{platform}} name=pf> <input type=hidden value="{{ $root.Self.country ? $root.Self.country : \'\' }}_{{ timestamp }}_{{ $root.Self.id }}" name=invoice> <input class=ppButton type=image src=https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif name="sendIt"></form></div></div></div><hr class="devider space-before space-after"><div><a href=# ng-click="isCollapsed = !isCollapsed" class="h4 text-bold collapsed no-underline">{{ ::(\'purchase.alternate\' | translate) }} <span class="fa fa-fw" ng-class="isCollapsed ? \'fa-angle-right\' : \'fa-angle-down\'"></span></a><div collapse=isCollapsed class="space-before collapse"><payment-frame id={{package.packageId}}></payment-frame></div></div></div></div>')
        }]),
        function() {
            "use strict";

            function LovooConfig($rootScopeProvider, $animateProvider, $tooltipProvider) {
                $rootScopeProvider.digestTtl(20), $animateProvider.classNameFilter(/^((?!(fa-spinner)).)*$/), $tooltipProvider.options({
                    animation: !1
                })
            }

            function LovooRun($rootScope, $templateCache) {
                function renderCustomBSTemplates($templateCache) {
                    $templateCache.put("template/tooltip/tooltip-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n'), $templateCache.put("template/datepicker/popup.html", '<ul class="dropdown-menu datepicker" style="z-index: 2500;" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top + \'px\', left: position.left + \'px\'}" ng-keydown="keydown($event)">\n	<li ng-transclude></li>\n	<li ng-if="showButtonBar" style="padding:10px 9px 2px">\n		<span class="btn-group">\n			<button type="button" class="btn btn-sm btn-info" ng-click="select(\'today\')">{{ getText(\'current\') }}</button>\n			<button type="button" class="btn btn-sm btn-danger" ng-click="select(null)">{{ getText(\'clear\') }}</button>\n		</span>\n		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="close()">{{ getText(\'close\') }}</button>\n	</li>\n</ul>\n'), $templateCache.put("template/datepicker/day.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="fa fa-angle-left"></i></button></th>\n      <th colspan="{{5 + showWeeks}}"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="fa fa-angle-right"></i></button></th>\n    </tr>\n    <tr>\n      <th ng-show="showWeeks" class="text-center"></th>\n      <th ng-repeat="label in labels track by $index" class="text-center"><small aria-label="{{label.full}}">{{label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-show="showWeeks" class="text-center h6"><em>{{ weekNumbers[$index] }}</em></td>\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n'), $templateCache.put("template/datepicker/month.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="fa fa-angle-left"></i></button></th>\n      <th><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="fa fa-angle-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n'), $templateCache.put("template/datepicker/year.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="fa fa-angle-left"></i></button></th>\n      <th colspan="3"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="fa fa-angle-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n'), $templateCache.put("template/accordion/accordion-group.html", '<div class="panel panel-default">\n  <div class="panel-heading" ng-class="{\'active\': isOpen}">\n    <h4 class="panel-title">\n      <a href class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading"><span ng-class="{\'text-muted\': isDisabled}">{{heading}}</span></a>\n    </h4>\n  </div>\n  <div class="panel-collapse" collapse="!isOpen">\n	  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>\n')
                }
                $rootScope.AdsEnabled = AdsEnabled, $rootScope.AgeRange = AgeRange, $rootScope.Ajax = Ajax, $rootScope.CaptchaConfig = CaptchaConfig, $rootScope.CostCredits = CostCredits, $rootScope.DeleteReasons = DeleteReasons, $rootScope.ENVIRONMENT = window.ENVIRONMENT, $rootScope.Genders = Genders, $rootScope.Intentions = Intentions, $rootScope.isMobile = isMobile, $rootScope.Languages = Languages, $rootScope.lastResponseTime = LastResponseTime, $rootScope.mailSettingsAction = mailSettingsAction, $rootScope.resultLimit = 36, $rootScope.supportFormAttachmentEnabled = SupportFormAttachmentEnabled, $rootScope.Translator = Translator, $rootScope.TutorialStates = TutorialStates, $rootScope.uniquePageData = uniquePageData, $rootScope.UserDecisions = UserDecisions, $rootScope.UserDetails = UserDetails, $rootScope.userListColumns = 3, $rootScope.websiteVersion = websiteVersion, $rootScope.dateFormat = "de" === Translator.locale ? "dd.MM.yyyy" : "yyyy-MM-dd", $rootScope.staticDomains = function() {
                    return -1 !== $.inArray(Translator.locale, ["de", "en"]) ? Translator.locale : ""
                }, $rootScope.isSecurityUser = function() {
                    return !(void 0 === $rootScope.Self || !$rootScope.Self || !$rootScope.Self.id)
                }, $rootScope.getHelpcenterLink = function() {
                    return "en" === Translator.locale ? "https://support.lovoo.co.uk/hc/en-us" : "https://support.lovoo.com/hc/" + Translator.locale
                }, renderCustomBSTemplates($templateCache), $(".fancybox").fancybox({
                    padding: 0,
                    fitToView: !0,
                    scrolling: "no",
                    openEffect: "elastic",
                    closeEffect: "elastic",
                    template: {
                        closeBtn: '<a title="Close" class="lo lo-close fancybox-close" href="javascript:;"></a>'
                    }
                })
            }

            function initializeGlobalVariables() {
                window.Lovoo = {
                    Templates: {},
                    exceptionCodes: function(code) {
                        switch (code) {
                            case 1e3:
                            case 2202:
                            case 2324:
                            case 2401:
                            case 2403:
                            case 2404:
                            case 2406:
                                return !1;
                            default:
                                return !0
                        }
                    }
                }, window.WEBSITE_VERSION = {
                    uk: 3,
                    global: 3.1
                }
            }
            initializeGlobalVariables(), angular.module("Lovoo", ["angular-loading-bar", "Resize", "Routing", "Lovoo.Templates", "Lovoo.Directives"]).config(LovooConfig).run(LovooRun).constant("PurchaseMethods", [{
                key: "paypal",
                trans: "paypal"
            }, {
                key: "cc",
                trans: "creditcard"
            }, {
                key: "sofortbanktransfer",
                trans: "banktransfer",
                loc: ["de", "at", "ch"]
            }, {
                key: "clickandbuy",
                trans: "Click and buy",
                loc: ["de", "uk", "es", "fr", "it"]
            }, {
                key: "paysafecard",
                trans: "Paysafecard",
                loc: ["de", "uk", "es", "fr", "it"]
            }, {
                key: "directpay",
                trans: "Directpay",
                loc: ["uk", "es", "fr", "it", "at", "ch"]
            }, {
                key: "pagseguro",
                trans: "Pagseguro",
                loc: ["br"]
            }, {
                key: "eprepag",
                trans: "E-Prepag",
                loc: ["br"]
            }])
        }(),
        function(ENVIRONMENT, websiteVersion) {
            "use strict";

            function RoutingConfig(ROUTES, $httpProvider, $locationProvider, $ocLazyLoadProvider, $routeProvider) {
                function resolveFunc(route) {
                    return ["$ocLazyLoad", "$rootScope", function($ocLazyLoad, $rootScope) {
                        var i, moduleAndFile, modulesToInject = $rootScope.routing.modules,
                            lazyLoadedModules = [];
                        for (i = 0; i < modulesToInject.length; ++i) {
                            moduleAndFile = {
                                files: [],
                                name: "Lovoo." + (route.moduleName || modulesToInject[i].charAt(0).toUpperCase() + modulesToInject[i].slice(1))
                            };
                            try {
                                angular.module(moduleAndFile.name)
                            } catch (err) {
                                moduleAndFile.files.push(require.toUrl(modulesToInject[i]) + ".js")
                            } - 1 === $.inArray(moduleAndFile.name, $ocLazyLoad.getModules()) && lazyLoadedModules.push(moduleAndFile)
                        }
                        return lazyLoadedModules.length ? $ocLazyLoad.load(lazyLoadedModules).then({}, function(e) {
                            console.error(e)
                        }) : null
                    }]
                }
                $ocLazyLoadProvider.config({
                    loadedModules: ["Lovoo"],
                    asyncLoader: require,
                    debug: "prod" !== ENVIRONMENT
                }), $locationProvider.html5Mode({
                    enabled: !0,
                    requireBase: !1
                }), _.each(ROUTES, function(route) {
                    var routeOptions = {
                        reloadOnSearch: !1
                    };
                    void 0 !== route.template && (routeOptions.templateUrl = "/template/" + route.template + ".html"), void 0 !== route.controller && (routeOptions.controller = route.controller), void 0 !== route.modules && require && (routeOptions.resolve = resolveFunc(route)), $routeProvider.when(Ajax.url + route.url, routeOptions)
                }), $routeProvider.otherwise({
                    redirectTo: Ajax.url + "/404"
                }), $httpProvider.interceptors.push(InterceptRequestResponse)
            }

            function RoutingRun(ROUTE_DEFAULTS, $rootScope, RoutingService, UpdateSelfService) {
                $rootScope.activePage = "", $rootScope.routing = angular.copy(ROUTE_DEFAULTS), $rootScope.$on("$locationChangeStart", function() {
                    void 0 === $rootScope.Self && UpdateSelfService.update(Self)
                }), $rootScope.$on("$routeChangeStart", RoutingService.onRouteChange)
            }

            function RoutingService(ROUTES, ROUTE_DEFAULTS, $anchorScroll, $location, $rootScope, $route, $timeout) {
                function getContainerClass() {
                    var containerClass = "container-";
                    return containerClass += $rootScope.routing.isResponsive ? "responsive" : "static", containerClass + "-" + $rootScope.routing.container
                }
                var self = this,
                    ignoreRoutingUpdate = !1;
                this.ignoreNextRootScopeRoutingUpdate = function() {
                    ignoreRoutingUpdate = !0
                }, this.updateRootScopeRouting = function(route) {
                    var uniqueData = $rootScope.uniquePageData,
                        loggedState = void 0 !== Self && Self && Self.id ? "loggedIn" : "loggedOut",
                        v3State = $rootScope.websiteVersion >= WEBSITE_VERSION.uk ? "isV3" : "isNotV3";
                    ignoreRoutingUpdate || ("/:uniquePage" === route.url ? ($rootScope.isUniquePage = !0, _.each(ROUTES, function(value) {
                        void 0 !== value.uniquePageType && void 0 !== uniqueData && uniqueData.type === value.uniquePageType && (route = value, "profile" === uniqueData.type && (uniqueData.template = route.template))
                    })) : $rootScope.isUniquePage = !1, $rootScope.routing = $rootScope.routing || {}, _.each(ROUTE_DEFAULTS, function(defaultValue, key) {
                        var newVal;
                        "object" != typeof route[key] || Array.isArray(route[key]) ? newVal = void 0 !== route[key] ? route[key] : defaultValue : (newVal = void 0 !== route[key][loggedState] ? route[key][loggedState] : defaultValue, newVal = void 0 !== route[key][v3State] ? route[key][v3State] : newVal), $rootScope.routing[key] = newVal
                    }), $rootScope.routing.containerClass = getContainerClass(), $rootScope.routing = angular.extend(_.clone(ROUTE_DEFAULTS), $rootScope.routing))
                }, this.onRouteChange = function(event, currentRoute) {
                    var i, path = currentRoute.$$route.originalPath;
                    for (i = 0; i < ROUTES.length; ++i)
                        if (path === $rootScope.Ajax.url + ROUTES[i].url) {
                            self.updateRootScopeRouting(ROUTES[i]);
                            break
                        }
                    $timeout(function() {
                        $("body").trigger("path-change", {
                            url: $location.$$path
                        }), $location.hash() ? $anchorScroll() : ignoreRoutingUpdate || $(window).scrollTop(0), ignoreRoutingUpdate = !1
                    })
                }
            }

            function SelfLocationFactory($location, $route, $rootScope, $window) {
                return $location.isHistoryStopped = !1, $location.replacePath = function(path) {
                    var un, lastRoute = $route.current;
                    return un = $rootScope.$on("$locationChangeSuccess", function() {
                        $route.current = lastRoute, un()
                    }), $location.isHistoryStopped ? $location.path(path).replace() : ($location.isHistoryStopped = !0, $location.path(path)), $location
                }, $location.goBack = function() {
                    return $window.history.back(), $location.isHistoryStopped = !1, $location
                }, $location
            }

            function InterceptRequestResponse(API_VERSION, $injector, $q, $rootScope, $window, NotificationService) {
                function requestErrorFunc(rejection) {
                    return $q.reject(rejection)
                }

                function requestFunc(config) {
                    var regExpTemplateSrc = new RegExp("/template/*", "g");
                    return config.headers = angular.extend({
                        KISSAPI_NOTIHASH: NotificationService.getHash(),
                        DEVICE_TYPE: "web",
                        API_VERSION: API_VERSION,
                        APP: "lovoo"
                    }, config.headers), _.size(Self) || config.headers["public"] || "template" === config.url.split("/")[0] || config.url.match(regExpTemplateSrc) ? config || $q.when(config) : $q.reject({
                        access_denied: !0
                    })
                }

                function responseErrorFunc(rejection) {
                    if (0 === rejection.status) return rejection;
                    if (rejection.access_denied || 403 === rejection.status) return $window.location.href = $rootScope.Ajax.url + "/?show_login=true";
                    var header = Translator.get("action_interrupted"),
                        content = rejection.data.statusMessage ? rejection.data.statusMessage.replace("_", " ") : Translator.get("error_occured");
                    if (!Lovoo.exceptionCodes(rejection.data.statusCode) && rejection.data.dialog) Exception = Exception || $injector.get("ExceptionService"), Exception.handle(rejection.data.statusCode, rejection.data.dialog);
                    else switch (rejection.data.statusCode) {
                        case 2401:
                            break;
                        default:
                            Toaster = Toaster || $injector.get("ToasterService"), Toaster.open("error", header, content, {
                                id: rejection.data.statusCode
                            })
                    }
                    return $q.reject(rejection)
                }

                function responseFunc(response) {
                    var notisFromResponse, data = response.data;
                    return data.notiHash && NotificationService.setHash(data.notiHash), void 0 !== response.headers().date && ($rootScope.lastResponseTime = new Date(response.headers().date).getTime() / 1e3), UpdateSelfService = UpdateSelfService || $injector.get("UpdateSelfService"), UpdateSelfService.update(data.updateUser), data.response && data.response.result && data.response.result.errors && _.each(data.response.result.errors, function(error) {
                        Lovoo.exceptionCodes(error.code) || (Exception = Exception || $injector.get("ExceptionService"), Exception.handle(error.code, {}))
                    }), data.response && data.response.dialog && "tutorial" === data.response.dialog.type ? (DialogTutorial = DialogTutorial || $injector.get("DialogTutorialService"), DialogTutorial.openTutorialDialog(data.response.dialog), DialogTutorial.updateTutorialStates(data.response.tutorialStates), $q.reject(response)) : (notisFromResponse = response.config.data && response.config.data.batch ? data[0][0].noti : data.noti, notisFromResponse && (NotificationService.publish(notisFromResponse), DesktopNotification = DesktopNotification || $injector.get("DesktopNotificationService"), DesktopNotification.handleDesktopNotifications(notisFromResponse)), response || $q.when(response))
                }
                var Toaster, DesktopNotification, DialogTutorial, Exception, UpdateSelfService;
                return {
                    requestError: requestErrorFunc,
                    request: requestFunc,
                    responseError: responseErrorFunc,
                    response: responseFunc
                }
            }

            function getProfileRoutingData(routeType) {
                var isSecurityUser, profileRoutingData;
                return "profile" === routeType ? (isSecurityUser = !(void 0 === Self || !Self || !Self.id), profileRoutingData = {
                    url: "/profile/:userId",
                    uniquePageType: "profile",
                    hasEyeCatcher: {
                        loggedIn: !0,
                        isV3: !1
                    },
                    footer: {
                        isV3: "compact-xs"
                    },
                    topmenu: {
                        isV3: "modern"
                    },
                    isResponsive: {
                        loggedIn: !1,
                        isV3: !0
                    },
                    container: {
                        loggedOut: "lg",
                        isV3: "lg"
                    }
                }, isSecurityUser && 3 !== websiteVersion ? websiteVersion >= 3 ? (profileRoutingData.controller = "ProfileFeedPageCtrl", profileRoutingData.template = "page/profile") : (profileRoutingData.controller = "ProfilePageCtrl", profileRoutingData.template = "page/profile-old") : (profileRoutingData.controller = "ProfilePageCtrl", profileRoutingData.template = "page/profile-guest")) : "self" === routeType ? (profileRoutingData = {
                    url: "/self",
                    modules: {
                        isV3: ["feed"]
                    },
                    hasEyeCatcher: {
                        isNotV3: !0
                    },
                    footer: {
                        isV3: "compact-xs"
                    },
                    topmenu: {
                        isV3: "modern"
                    },
                    isResponsive: {
                        loggedIn: !1,
                        isV3: !0
                    },
                    container: {
                        loggedOut: "lg",
                        isV3: "lg"
                    }
                }, websiteVersion >= 3 ? (profileRoutingData.controller = "ProfileFeedPageCtrl", profileRoutingData.template = "page/profile") : (profileRoutingData.controller = "ProfilePageCtrl", profileRoutingData.template = "page/profile-old")) : "selfAction" === routeType && (profileRoutingData = {
                    url: "/self/:page/:action",
                    template: websiteVersion >= 3.1 ? "page/profile" : "page/profile-old",
                    hasSidebar: !0,
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }), profileRoutingData
            }
            angular.module("Routing", ["ngRoute", "ngCookies", "oc.lazyLoad", "Dialog", "Notifications", "Lovoo.Self"]).config(RoutingConfig).run(RoutingRun).factory("selfLocation", SelfLocationFactory).service("RoutingService", RoutingService).constant("API_VERSION", 1.13).constant("ROUTE_DEFAULTS", {
                    hasSidebar: !1,
                    hasEyeCatcher: !1,
                    isResponsive: !0,
                    container: "md",
                    topmenu: "rainbow-top",
                    footer: "extensive",
                    modules: []
                }).constant("ROUTES", [{
                    url: "/",
                    template: websiteVersion >= 3 ? "page/index" : "page/index-old",
                    modules: {
                        loggedIn: ["flirtmatch"],
                        isV3: ["feed"]
                    },
                    hasSidebar: {
                        loggedIn: !0,
                        isV3: !1
                    },
                    hasEyeCatcher: {
                        loggedIn: !0,
                        isV3: !1
                    },
                    isResponsive: {
                        loggedIn: !1,
                        isV3: !0
                    },
                    container: {
                        loggedOut: "lg",
                        isV3: "lg"
                    },
                    footer: {
                        isV3: "compact-xs"
                    },
                    topmenu: {
                        loggedOut: "transparent",
                        isV3: "modern"
                    }
                }, {
                    url: "/_=_",
                    template: websiteVersion >= 3 ? "page/index" : "page/index-old"
                }, {
                    url: "/challenge/:challenge",
                    controller: "ChallengeCtrl",
                    uniquePageType: "challenge",
                    template: "page/challenge",
                    modules: ["challenge"],
                    footer: "compact-xs",
                    topmenu: "modern"
                }, {
                    url: "/cities/:catchword/:country/:city",
                    template: "page/search-tracking",
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/confirm/:userId/:token"
                }, {
                    url: "/contacts/:page?",
                    controller: "ContactsFavoritesPageCtrl",
                    template: "page/contacts",
                    modules: ["contacts"],
                    hasSidebar: !0,
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/credits/:page?",
                    controller: "CreditsCtrl",
                    modules: ["credits"],
                    template: "page/credits",
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/eyecatcher",
                    template: "page/eyecatcher",
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/fans/:page?",
                    controller: "ContactsFansPageCtrl",
                    template: "page/fans",
                    modules: ["contacts"],
                    hasSidebar: !0,
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/flirtmatch/:page?",
                    controller: "FlirtmatchCtrl",
                    template: "page/flirtmatch",
                    modules: ["flirtmatch"],
                    hasSidebar: !0,
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/hashtag/:hashtag",
                    controller: "HashtagPageCtrl",
                    uniquePageType: "hashtag",
                    template: "page/hashtag",
                    modules: ["hashtag"],
                    footer: "compact-xs",
                    topmenu: "modern",
                    container: "lg"
                }, {
                    url: "/helpcenter",
                    controller: "HelpcenterCtrl",
                    template: "page/helpcenter",
                    modules: ["helpcenter"],
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/imprint",
                    topmenu: "hidden",
                    footer: "hidden"
                }, {
                    url: "/landing/logout",
                    hasEyeCatcher: !0
                }, {
                    url: "/landingpage",
                    container: "lg"
                }, {
                    url: "/landingpage/:page"
                }, {
                    url: "/logout",
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/mail/:userId/unsubscribe/:hash"
                }, {
                    url: "/mail/delete/:userId/:token"
                }, {
                    url: "/mail/confirm/:userId/:token"
                }, {
                    url: "/mail/pwreset/:token"
                }, {
                    url: "/account/deleteconfirm/:userid/:token"
                }, {
                    url: "/pwreset/:token"
                }, {
                    url: "/mail/unsubscribe"
                }, {
                    url: "/confirm/:userId/:token"
                }, {
                    url: "/pay/credits"
                }, {
                    url: "/pay/subscription"
                }, {
                    url: "/post/:postId",
                    controller: "FeedPostPageCtrl",
                    template: "page/post",
                    modules: ["feed"],
                    footer: "compact-xs",
                    topmenu: "modern"
                }, getProfileRoutingData("profile"), {
                    url: "/register"
                }, {
                    url: "/search",
                    template: "page/search",
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, getProfileRoutingData("self"), {
                    url: "/self/:page",
                    template: "page/self-page",
                    hasSidebar: !0,
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, getProfileRoutingData("selfAction"), {
                    url: "/settings/:page?/:subpage?",
                    controller: "SettingsPageCtrl",
                    template: "page/settings",
                    modules: ["helpcenter", "vip"],
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/supportform",
                    topmenu: "hidden",
                    footer: "hidden"
                }, {
                    url: "/test/dialog",
                    template: "testview/dialog_test",
                    isResponsive: !1
                }, {
                    url: "/test/tutorial",
                    template: "testview/tutorial_test",
                    moduleName: "TestView",
                    modules: ["testview", "tutorial"],
                    isResponsive: !1
                }, {
                    url: "/test/icons",
                    controller: "IconsTestCtrl",
                    template: "testview/icons",
                    modules: ["testview"],
                    moduleName: "TestView",
                    isResponsive: !1
                }, {
                    url: "/verify",
                    controller: "VerifyPageCtrl",
                    template: "page/verify",
                    modules: ["verify"],
                    hasSidebar: !0,
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/vip",
                    controller: "VipPageCtrl",
                    template: "page/vip",
                    modules: ["vip"],
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/welcome/:from/:param?",
                    template: websiteVersion >= 3 ? "page/index" : "page/index-old",
                    modules: {
                        loggedIn: ["flirtmatch"],
                        isV3: ["feed"]
                    },
                    hasSidebar: {
                        loggedIn: !0,
                        isV3: !1
                    },
                    hasEyeCatcher: {
                        loggedIn: !0,
                        isV3: !1
                    },
                    isResponsive: {
                        loggedIn: !1,
                        isV3: !0
                    },
                    container: {
                        loggedOut: "lg",
                        isV3: "lg"
                    },
                    footer: {
                        isV3: "compact-xs"
                    },
                    topmenu: {
                        loggedOut: "transparent",
                        isV3: "modern"
                    }
                }, {
                    url: "/404",
                    template: "error",
                    hasEyeCatcher: !0,
                    isResponsive: !1
                }, {
                    url: "/:uniquePage",
                    template: "unique-page",
                    modules: ["hashtag", "challenge"]
                }]), RoutingConfig.$inject = ["ROUTES", "$httpProvider", "$locationProvider", "$ocLazyLoadProvider", "$routeProvider"], RoutingService.$inject = ["ROUTES", "ROUTE_DEFAULTS", "$anchorScroll", "$location", "$rootScope", "$route", "$timeout"], SelfLocationFactory.$inject = ["$location", "$route", "$rootScope", "$window"], InterceptRequestResponse.$inject = ["API_VERSION", "$injector", "$q", "$rootScope", "$window", "NotificationService"],
                function() {
                    function TemplateService($compile, $http, $templateCache) {
                        this.loadTemplate = function(templateSrc, element, scope) {
                            return $http.get("/template/" + templateSrc, {
                                cache: $templateCache
                            }).success(function(data) {
                                element.html(data ? data : "not-found").show(), $compile(element.contents())(scope)
                            })
                        }
                    }
                    angular.module("TemplateLoader", []).service("TemplateService", TemplateService), TemplateService.$inject = ["$compile", "$http", "$templateCache"]
                }()
        }(window.ENVIRONMENT, window.websiteVersion),
        function() {
            "use strict";

            function ToasterConfig(growlProvider) {
                growlProvider.globalTimeToLive(3e3), growlProvider.globalEnableHtml(!0), growlProvider.onlyUniqueMessages(!1)
            }

            function NotificationRequestFctry($rootScope, $resource) {
                return {
                    notification: $resource($rootScope.Ajax.api + "/notifications", {
                        isArray: !1,
                        responseType: "json",
                        cache: !1
                    }, {
                        get: {
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                "KISSAPI-VERSION": 1.15
                            },
                            method: "GET"
                        }
                    })
                }
            }

            function NotifyQueryFctry($rootScope, $resource) {
                return {
                    notification: $resource($rootScope.Ajax.api + "/notifications", {
                        responseType: "json",
                        cache: !1
                    })
                }
            }

            function NotificationNewService(NotificationRequestFctry) {
                this.countNotifications = function(notifications, notificationWhiteList) {
                    var count = 0;
                    return _.each(notifications, function(notification) {
                        _.contains(notificationWhiteList, notification.type) && (count += notification.count)
                    }), count
                }, this.getNotifications = function() {
                    return NotificationRequestFctry.notification.get().$promise
                }
            }

            function NotificationService() {
                var cache = {},
                    hash = 0;
                this.getHash = function() {
                    return hash
                }, this.setHash = function(responseHash) {
                    hash = responseHash
                }, this.subscribe = function(topics, callback) {
                    _.each(topics, function(topic) {
                        cache[topic] || (cache[topic] = []), cache[topic].push(callback)
                    })
                }, this.unsubscribe = function(topics, callback) {
                    var callbackCount;
                    _.each(topics, function(topic) {
                        if (cache[topic])
                            for (callbackCount = cache[topic].length; callbackCount--;) cache[topic][callbackCount] === callback && cache[topic].splice(callbackCount, 1)
                    })
                }, this.publish = function(topics) {
                    var callbackCount, event;
                    $.each(cache, function(topic, funcs) {
                        if (topics[topic] || (topics[topic] = 0), event = funcs, event && event.length > 0)
                            for (callbackCount = event.length; callbackCount--;) event[callbackCount] && event[callbackCount].apply(topics, arguments)
                    })
                }
            }

            function ToastService(growl) {
                function renderTemplate(title, text) {
                    var cntTemplate = "";
                    return title && (cntTemplate = $("<h4></h4>"), cntTemplate.addClass("space-after-sm"), cntTemplate = cntTemplate.html(title)[0].outerHTML), cntTemplate + text
                }
                var _options, _toasts = {
                    success: function(title, cnt) {
                        growl.addSuccessMessage(renderTemplate(title, cnt), _options)
                    },
                    info: function(title, cnt) {
                        growl.addInfoMessage(renderTemplate(title, cnt), _options)
                    },
                    warning: function(title, cnt) {
                        growl.addWarnMessage(renderTemplate(title, cnt), _options)
                    },
                    error: function(title, cnt) {
                        growl.addErrorMessage(renderTemplate(title, cnt), _options)
                    }
                };
                this.open = function(a1, a2, a3, a4) {
                    var title, cnt, type = a1,
                        options = a4;
                    switch (arguments.length) {
                        case 4:
                            title = a2, cnt = a3;
                            break;
                        case 3:
                            angular.isObject(a3) ? (cnt = a2, options = a3) : (title = a2, cnt = a3);
                            break;
                        case 2:
                            cnt = a2;
                            break;
                        case 1:
                            cnt = a1, type = "info";
                            break;
                        default:
                            throw $resourceMinErr("badargs", "Expected up to 4 arguments [type, title, content, options], got {0} arguments", arguments.length)
                    }
                    _options = options || {}, _toasts[type].apply(null, [title, cnt])
                }
            }

            function FavIconService() {
                var tinycon = Tinycon.setOptions({
                    width: 7,
                    height: 9,
                    colour: "#fff",
                    background: "#BD2677",
                    fallback: !0
                });
                this.badge = function(num) {
                    tinycon.setBubble(num)
                }
            }

            function DesktopNotificationService(STORAGE_KEY_HANDLED_NOTIFICATIONS, STORAGE_KEY_PUSH_SETTINGS, $rootScope, $timeout, StorageService) {
                var notiInstances = {},
                    self = this;
                this.isSupportedByBrowser = Notification && "function" == typeof Notification.requestPermission, this.isGranted = Notification && "granted" === Notification.permission, this.isDenied = Notification && "denied" === Notification.permission, this.requestPermission = function(callback) {
                    Notification.requestPermission(function(status) {
                        Notification.permission !== status && (Notification.permission = status), callback && "function" == typeof callback && callback(status)
                    })
                }, this.handleDesktopNotifications = function(notifications) {
                    var storageUserId, localStrgPushSettings, handledNotifications, excludedKeys = $rootScope.websiteVersion < 3 ? ["fr", "g", "mvb", "ne", "pc", "pl", "s", "o", "fo", "pr"] : ["h", "fr", "g", "k", "mh", "mv", "mvb", "pc", "ne", "s"];
                    !self.isDenied && self.isGranted && (storageUserId = $rootScope.Self ? $rootScope.Self.id : "guest", localStrgPushSettings = StorageService.get(STORAGE_KEY_PUSH_SETTINGS + "." + storageUserId), handledNotifications = StorageService.get(STORAGE_KEY_HANDLED_NOTIFICATIONS + "." + storageUserId), handledNotifications || (handledNotifications = notifications), _.each(notifications, function(notiCount, key) {
                        -1 === $.inArray(key, excludedKeys) ? void 0 === handledNotifications[key] ? handledNotifications[key] = 0 : (notiCount > handledNotifications[key] && (!localStrgPushSettings || localStrgPushSettings && localStrgPushSettings[key]) && (notiInstances[key] = new Notification(Translator.get("hello_user", {
                            name: $rootScope.Self.name
                        }), {
                            body: Translator.get("noti_" + key, {
                                count: notiCount
                            }, notiCount),
                            icon: "https://assets.lovoo.com/bundles/website/images/lovoo_logo.png",
                            tag: key
                        }), notiInstances[key].onshow = function() {
                            $timeout(function() {
                                notiInstances[key].close(), notiInstances[key] = null
                            }, 4e3)
                        }), handledNotifications[key] = notiCount) : void 0 !== handledNotifications[key] && delete handledNotifications[key]
                    }), StorageService.set(STORAGE_KEY_HANDLED_NOTIFICATIONS + "." + storageUserId, handledNotifications))
                }
            }
            var $resourceMinErr = angular.$$minErr("$resource");
            return angular.module("Notifications", ["angular-growl", "ngAnimate"]).constant("STORAGE_KEY_HANDLED_NOTIFICATIONS", "handled_notifications").config(ToasterConfig).factory("NotificationRequestFctry", NotificationRequestFctry).factory("NotifyQueryFctry", NotifyQueryFctry).service("NotificationNewService", NotificationNewService).service("NotificationService", NotificationService).service("ToasterService", ToastService).service("FavIconService", FavIconService).service("DesktopNotificationService", DesktopNotificationService)
        }(), angular.module("Dialog", ["ui.bootstrap", "Lovoo.Profile"]).run(runDialogModule).directive("dialogButtons", DialogButtonsDirective).service("DialogService", DialogService).service("ExceptionService", ExceptionService).service("PackageService", PackageService), DialogService.$inject = ["$injector", "$modal", "$rootScope", "RoutingService"], ExceptionService.$inject = ["$injector", "DialogService"], PackageService.$inject = ["DialogService"],
        function(angular, window) {
            "use strict";

            function StorageService($window) {
                var localStorage, isSupported = "localStorage" in $window && null !== $window.localStorage;
                localStorage = isSupported ? $window.localStorage : {
                    setItem: function(k, v) {
                        localStorage[k] = v
                    },
                    getItem: function(k) {
                        return void 0 !== localStorage[k] ? localStorage[k] : null
                    },
                    removeItem: function(k) {
                        delete localStorage[k]
                    },
                    clear: function() {
                        for (var k in localStorage) localStorage.hasOwnProperty[k] && delete localStorage[k]
                    }
                }, this.set = function(key, value) {
                    localStorage.setItem(key, angular.toJson(value))
                }, this.get = function(key) {
                    var value = localStorage.getItem(key);
                    return value ? angular.fromJson(value) : value
                }, this.remove = function(key) {
                    localStorage.removeItem(key)
                }, this.clear = function() {
                    localStorage.clear()
                }, this.subscribe = function(callback) {
                    $window.addEventListener("storage", function(event) {
                        event || (event = window.event), callback(event.key, event.oldValue, event.newValue, event.url ? event.url : event.uri ? event.uri : null)
                    }, !1)
                }
            }

            function CookieService($document) {
                var self = this;
                this.set = function(key, value, options) {
                    var expiresFor;
                    if (void 0 === value || void 0 === key) return "";
                    if (value = "object" == typeof value ? JSON.stringify(value) : String(value), "number" == typeof options.expires) switch (expiresFor = options.expires, options.expires = new Date, options.expireType) {
                        case "hours":
                            options.expires.setHours(options.expires.getHours() + expiresFor);
                            break;
                        case "minutes":
                            options.expires.setMinutes(options.expires.getMinutes() + expiresFor);
                            break;
                        case "seconds":
                            options.expires.setSeconds(options.expires.getSeconds() + expiresFor);
                            break;
                        default:
                            options.expires.setDate(options.expires.getDate() + expiresFor), options.resetTime && options.expires.setHours(0, 0, 0)
                    }
                    var cookieOptions = [encodeURIComponent(key), "=", encodeURIComponent(value), options.expires ? "; expires=" + options.expires.toUTCString() : "", options.path ? "; path=" + options.path : "", options.domain ? "; domain=" + options.domain : "", options.secure ? "; secure" : ""];
                    return $document[0].cookie = cookieOptions.join("")
                }, this.get = function(key) {
                    var cookie, pos, name, value, list = [],
                        cookies = {},
                        hasCookies = !1,
                        all = $document[0].cookie;
                    all && (list = all.split("; "));
                    for (var i = 0; i < list.length; ++i)
                        if (list[i] && (cookie = list[i], pos = cookie.indexOf("="), name = cookie.substring(0, pos), value = decodeURIComponent(cookie.substring(pos + 1)), void 0 === key || key === name)) {
                            try {
                                cookies[name] = JSON.parse(value)
                            } catch (e) {
                                cookies[name] = value
                            }
                            if (key === name) return cookies[name];
                            hasCookies = !0
                        }
                    return hasCookies ? cookies : _.size(cookies)
                }, this.remove = function(key) {
                    return self.get(key) ? void self.set(key, 0, {
                        expires: new Date(0),
                        resetTime: !0,
                        secure: "https:" === window.location.protocol
                    }) : ""
                }
            }
            angular.module("Storage", []).service("StorageService", StorageService).service("CookieService", CookieService), StorageService.$inject = ["$window"], CookieService.$inject = ["$document"]
        }(angular, window),
        function() {
            "use strict";

            function RunResize(AnimationFrameService, BootstrapHelper) {
                BootstrapHelper.setSize("init"), AnimationFrameService.add(null, BootstrapHelper.setSize, ["resize"])
            }

            function AnimationFrameService($rootScope, $timeout) {
                function addEvent(event, scope, callback, id) {
                    event.callbacks[id] = callback, null !== scope && scope.$on("$destroy", function() {
                        delete event.callbacks[id], "number" == typeof event.name && events.splice(events.indexOf(event), 1)
                    })
                }

                function request() {
                    var i, event, callbackId, hasChanged = !1;
                    for (requestAnimFrame(request), i = events.length - 1; i >= 0; --i) {
                        if (event = events[i], void 0 === event) return;
                        if (event.current = event.get(), event.last !== event.current) {
                            event.last = event.current;
                            for (callbackId in event.callbacks) event.callbacks.hasOwnProperty(callbackId) && (event.callbacks[callbackId](), hasChanged = !0)
                        }
                    }
                    hasChanged && !$rootScope.$$phase && $rootScope.$apply()
                }

                function getAnimationFrame() {
                    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
                        window.setTimeout(callback, 1e3 / 60)
                    }
                }
                var jWindow = $(window),
                    jDocument = $(document),
                    requestAnimFrame = getAnimationFrame(),
                    eventCounter = 0,
                    events = [{
                        name: "scroll",
                        get: function() {
                            return jWindow.scrollTop()
                        }
                    }, {
                        name: "resize",
                        get: function() {
                            return jWindow.width() + "x" + jWindow.height()
                        }
                    }, {
                        name: "changeDocumentHeight",
                        get: function() {
                            return jDocument.height()
                        }
                    }];
                _.each(events, function(event) {
                    event.last = event.get(), event.callbacks = {}
                }), this.add = function(scope, callback, listeners) {
                    eventCounter++, $timeout(callback), _.each(listeners, function(listener) {
                        var newEvent;
                        switch (typeof listener) {
                            case "string":
                                _.each(events, function(event) {
                                    return listener === event.name ? (addEvent(event, scope, callback, eventCounter), !1) : void 0
                                });
                                break;
                            case "function":
                                newEvent = {
                                    name: eventCounter,
                                    get: listener,
                                    last: listener(),
                                    callbacks: {}
                                }, events.push(newEvent), addEvent(newEvent, scope, callback, eventCounter)
                        }
                    })
                }, request()
            }

            function BootstrapHelper($rootScope) {
                function getSize() {
                    var i, size, sizes = ["xs", "sm", "md", "lg"],
                        element = $("<div>");
                    for (element.appendTo($("body")), i = sizes.length - 1; i >= 0; i--)
                        if (size = sizes[i], element.addClass("hidden-" + size), element.is(":hidden")) return element.remove(), size
                }
                this.setSize = function(bsSize) {
                    bsSize = bsSize || getSize(), $rootScope.BS = {
                        xs: "xs" === bsSize,
                        sm: "sm" === bsSize,
                        greaterSm: "xs" !== bsSize && "sm" !== bsSize,
                        size: bsSize
                    }
                }
            }
            angular.module("Resize", []).run(RunResize).service("AnimationFrameService", AnimationFrameService).service("BootstrapHelper", BootstrapHelper), AnimationFrameService.$inject = ["$rootScope", "$timeout"], BootstrapHelper.$inject = ["$rootScope"]
        }(), angular.module("ngResource.Batch", ["ngResource"]).factory("$batchRequest", BatchRequestFactory), BatchRequestFactory.$inject = ["$injector", "$q", "$resource"],
        function() {
            "use strict";

            function ApproutingService($rootScope, $window, DialogService, RegisterService) {
                function redirectToAppOrStoreOrWebPage(appPage, pageParams) {
                    pageParams = pageParams || {}, $window.location.href = $rootScope.Ajax.url + "/mailrouting?" + $.param(angular.extend({
                        page: appPage
                    }, pageParams))
                }
                this.openAppOrStoreForMobileOrRegisterForDesktopGuests = function(appPage, pageParams) {
                    pageParams = pageParams || {}, $rootScope.isMobile ? redirectToAppOrStoreOrWebPage(appPage, pageParams) : !$rootScope.isSecurityUser() && $rootScope.websiteVersion >= WEBSITE_VERSION.uk ? DialogService.openDialog({
                        backdrop: !0,
                        templateUrl: "/template/dialog/v3-nologin-info.html"
                    }) : RegisterService.showRegisterDialog()
                }
            }
            angular.module("Lovoo.Approuting", []).service("ApproutingService", ApproutingService), ApproutingService.$inject = ["$rootScope", "$window", "DialogService", "RegisterService"]
        }(),
        function() {
            "use strict";

            function LoginFormController($scope, $window, Auth, FacebookService) {
                $scope.btnDisabled = !1, $scope.displayError = {}, $scope.authEmail = "", $scope.authPassword = "", $scope.authPwRemember = !1, $scope.openRegister = Auth.showRegisterDialog, $scope.openPasswordForgetDialog = Auth.openPasswordForgetDialog, $scope.displayFieldError = function(field) {
                    $scope.displayError[field] = !0
                }, $scope.facebookRegister = function() {
                    FacebookService.tryLoadUserOrConnectOnLogin()
                }, $scope.doLogin = function() {
                    var redirectLocation;
                    return $scope.btnDisabled = !0, Auth.tryAuthorize({
                        _username: $scope.authEmail,
                        _password: $scope.authPassword,
                        _remember_me: $scope.authPwRemember
                    }, function(data) {
                        return $scope.btnDisabled = !1, data.success === !1 ? ($scope.validationErrors = [{
                            label: Translator.get("Login"),
                            message: Translator.get("wrong_input")
                        }], $scope.authPassword = "", $scope.displayError = {}, !1) : (redirectLocation = $scope.redirectUrl || data.referer, void($window.location.href = redirectLocation))
                    }), !1
                }, $scope.switchLoginToRegister = Auth.showRegisterDialog
            }

            function LogoutController($scope, RegisterService, FacebookService) {
                $scope.onRegister = RegisterService.showRegisterDialog, $scope.facebookRegister = FacebookService.tryLoadUserOrConnectOnLogin
            }

            function PasswordForgetController($scope, AuthService, ToasterService) {
                $scope.btnDisabled = !1, $scope.displayError = {}, $scope.email = $scope.email || "", $scope.displayFieldError = function(field) {
                    $scope.displayError[field] = !0
                }, $scope.doReset = function() {
                    $scope.btnDisabled = !0, AuthService.resetPasswordRequest({
                        email: $scope.email
                    }).$promise.then(function() {
                        ToasterService.open("success", Translator.get("pw_forget"), Translator.get("txt.new_pw_sent_to_email")), $scope.dialogOptions.close()
                    }, function() {
                        $scope.btnDisabled = !1
                    })
                }
            }
            angular.module("Lovoo.Auth", ["ngResource", "ui.bootstrap", "vcRecaptcha", "Dialog", "GooglePlus", "Lovoo.Approuting", "Lovoo.Form.Search", "Lovoo.Facebook"]).controller("LoginFormCtrl", LoginFormController).controller("LogoutCtrl", LogoutController).controller("PasswordForgetCtrl", PasswordForgetController), LoginFormController.$inject = ["$scope", "$window", "AuthService", "FacebookService"], LogoutController.$inject = ["$scope", "RegisterService", "FacebookService"], PasswordForgetController.$inject = ["$scope", "AuthService", "ToasterService"]
        }(),
        function() {
            "use strict";

            function LoginFormDirective() {
                return {
                    restrict: "E",
                    scope: {
                        redirectUrl: "@?",
                        isVip: "@?",
                        requireUserid: "@?"
                    },
                    controller: "LoginFormCtrl",
                    templateUrl: function(ele, attr) {
                        return "/template/form/login-" + (attr.type ? attr.type + "-" : "") + "form.html"
                    }
                }
            }
            angular.module("Lovoo.Auth").directive("loginForm", LoginFormDirective)
        }(),
        function(angular) {
            "use strict";

            function AuthQuery($resource) {
                return $resource(Ajax.url + "/:action", {}, {
                    login: {
                        url: Ajax.url + "/login_check",
                        method: "POST",
                        responseType: "json",
                        ignoreLoadingBar: !0,
                        headers: {
                            "public": !0,
                            "Content-Type": "application/x-www-form-urlencoded",
                            "X-Requested-With": "XMLHttpRequest"
                        },
                        transformRequest: function(data) {
                            return void 0 !== data ? $.param(data) : data
                        }
                    },
                    logout: {
                        ignoreLoadingBar: !0,
                        responseType: "json",
                        url: Ajax.url + "/logout",
                        method: "POST"
                    },
                    resetPass: {
                        url: Ajax.api + "/misc/pwreset",
                        method: "PUT",
                        params: {
                            email: "@email"
                        },
                        headers: {
                            "public": !0
                        }
                    }
                })
            }

            function AuthService($rootScope, $window, ApproutingService, AuthQuery, DialogService, RegisterService, SearchFormService) {
                this.tryAuthorize = function(params, callback) {
                    AuthQuery.login(params).$promise.then(function(data) {
                        "function" == typeof callback && callback(data), SearchFormService.removeSearchParams()
                    })
                }, this.resetPasswordRequest = function(params) {
                    return AuthQuery.resetPass(params)
                }, this.showLoginDialog = function() {
                    return $rootScope.websiteVersion === WEBSITE_VERSION.uk ? void ApproutingService.openAppOrStoreForMobileOrRegisterForDesktopGuests("start") : void DialogService.openDialog({
                        size: "sm",
                        controller: "LoginFormCtrl",
                        templateUrl: "/template/dialog/login.html"
                    })
                }, this.doLogout = function() {
                    SearchFormService.removeSearchParams(), $window.location.href = Ajax.url + "/logout"
                }, this.showRegisterDialog = function() {
                    RegisterService.showRegisterDialog()
                }, this.openPasswordForgetDialog = function(email) {
                    DialogService.openDialog({
                        size: "sm",
                        controller: "PasswordForgetCtrl",
                        templateUrl: "/template/dialog/pw-forget.html"
                    }, {
                        email: email
                    })
                }
            }
            angular.module("Lovoo.Auth").factory("AuthQuery", AuthQuery).service("AuthService", AuthService), AuthQuery.$inject = ["$resource"], AuthService.$inject = ["$rootScope", "$window", "ApproutingService", "AuthQuery", "DialogService", "RegisterService", "SearchFormService"]
        }(angular), angular.module("Lovoo.Auth").constant("REGISTERFORM_FIELDS", {
            name: {
                step: 0,
                validateByForm: !0
            },
            birthday: {
                step: 0,
                validateByForm: !0
            },
            city: {
                step: 0
            },
            gender: {
                step: 0,
                validateByForm: !1,
                required: !0
            },
            genderLooking: {
                step: 0,
                validateByForm: !1,
                required: websiteVersion < 3
            },
            email: {
                step: 1
            },
            password: {
                step: 1
            },
            repeatPassword: {
                step: 1
            },
            captcha: {
                step: 1
            }
        }).constant("DATE_FORMATS", {
            "default": "dd/MM/yyyy",
            de: "dd.MM.yyyy"
        }).controller("RegisterFormCtrl", RegisterFormCtrl), RegisterFormCtrl.$inject = ["DATE_FORMATS", "REGISTERFORM_FIELDS", "$filter", "$scope", "$timeout", "$window", "AuthService", "FacebookService", "RegisterService", "RegisterFormStorage", "vcRecaptchaService"], angular.module("Lovoo.Auth").directive("registerForm", RegisterForm), angular.module("Lovoo.Auth").factory("RegisterFctry", RegisterFctry).factory("RegisterFormStorage", RegisterFormStorage).service("RegisterService", RegisterService), RegisterFctry.$inject = ["$resource"], RegisterService.$inject = ["$rootScope", "RegisterFctry", "DialogService"],
        function(angular) {
            "use strict";

            function AutoCompleteFctry($rootScope, $resource) {
                return {
                    request: $resource($rootScope.Ajax.api + "/misc/cities", {
                        isArray: !1,
                        responseType: "json",
                        cache: !0
                    }, {
                        get: {
                            method: "GET",
                            params: {
                                city: "@city",
                                term: "@term",
                                resultLimit: "@limit"
                            },
                            headers: {
                                "public": !0
                            }
                        }
                    })
                }
            }

            function SuggestDirective(AutoCompleteFctry) {
                function getSelector(type, items) {
                    var selector = "cities" === type ? "item as item.title for item" : "item for item";
                    return selector + " in getItems($viewValue, '" + type + "', '" + items + "')"
                }

                function getLookupFunction(type, items) {
                    return "simple" === type ? lookupSimple(items) : lookupCities()
                }

                function lookupCities() {
                    return function(filter) {
                        var params;
                        return filter = trim(filter), params = {
                            city: filter,
                            term: filter,
                            resultLimit: 10
                        }, createPromise(params)
                    }
                }

                function lookupSimple(items) {
                    var itemArray;
                    return itemArray = "string" == typeof items ? items.split(",") : items,
                        function(filter) {
                            var i, out = [];
                            for (filter = trim(filter), i = 0; i < itemArray.length; ++i) - 1 !== itemArray[i].indexOf(filter) && out.push(itemArray[i]);
                            return out
                        }
                }

                function createPromise(params) {
                    return AutoCompleteFctry.request.get(params).$promise.then(function(data) {
                        return data.response.result
                    })
                }

                function trim(text) {
                    return String.prototype.trim ? text.trim() : text.replace(/^\s+|\s+$/g, "")
                }
                return {
                    restrict: "A",
                    replace: !0,
                    transclude: !0,
                    template: function(element, attributes) {
                        var minLength = attributes.minLength || 3,
                            type = attributes.suggest,
                            items = attributes.items || "",
                            onSelected = attributes.onSelected || "",
                            selector = getSelector(type, items);
                        return '<input ng-transclude type="text" autocomplete="off" typeahead="' + selector + '" typeahead-min-length="' + minLength + '" ' + (onSelected ? 'typeahead-on-select="' + onSelected + '($item)" ' : "") + "/>"
                    },
                    link: function($scope) {
                        $scope.getItems = function(filter, type, items) {
                            return getLookupFunction(type, items)(filter)
                        }
                    }
                }
            }
            angular.module("Lovoo.Directives", ["wu.masonry", "TemplateLoader"]).factory("AutoCompleteFctry", AutoCompleteFctry).directive("suggest", SuggestDirective), AutoCompleteFctry.$inject = ["$rootScope", "$resource"], SuggestDirective.$inject = ["AutoCompleteFctry"]
        }(window.angular),
        function(angular, emojify, DfpSlots) {
            "use strict";

            function TextFillDirective(AnimationFrameService) {
                return {
                    restrict: "A",
                    link: function(scope, element, attributes) {
                        var resizeCallback = function() {
                            var widthOnly = void 0 !== attributes.widthOnly;
                            $(element).css({
                                width: $(element).parent().parent().width() + "px",
                                height: widthOnly ? "auto" : "110px"
                            }).textfill({
                                maxFontPixels: parseInt(attributes.maxFontSize),
                                innerTag: attributes.innerTag,
                                widthOnly: widthOnly,
                                explicitWidth: widthOnly ? $(element).parent().parent().width() + 1 : null,
                                success: function() {
                                    $(element).css({
                                        visibility: "visible"
                                    })
                                }
                            })
                        };
                        AnimationFrameService.add(scope, resizeCallback, ["resize"])
                    }
                }
            }

            function ScrollTopDirective(AnimationFrameService) {
                function updateSizes() {
                    scrollTop = jWindow.scrollTop(), footerTop = jFooter.position().top, footerHeight = jFooter.height(), rowHeight = jRow.position().top + parseInt(jRow.css("marginTop")), windowHeight = jWindow.height(), documentHeight = jDocument.height()
                }

                function setButton() {
                    updateSizes(), setButtonPosition(scrollTop + windowHeight > footerTop ? "absolute" : "fixed")
                }

                function setButtonPosition(position) {
                    switch (position) {
                        case "absolute":
                            thisElement.css({
                                bottom: "",
                                position: position,
                                top: documentHeight - footerHeight - rowHeight - thisElement.height() - FOOTER_PADDING + "px"
                            });
                            break;
                        case "fixed":
                            thisElement.css({
                                bottom: "25px",
                                position: position,
                                top: ""
                            })
                    }
                }
                var scrollTop, jRow, windowHeight, documentHeight, thisElement, rowHeight, footerTop, footerHeight, jFooter, jWindow = $(window),
                    jDocument = $(document),
                    FOOTER_PADDING = 24;
                return {
                    restrict: "A",
                    link: function(scope, element, attributes) {
                        {
                            var offset = 250,
                                duration = 250;
                            attributes.scrollTop ? attributes.scrollTop : document
                        }
                        thisElement = element, scrollTop > offset && thisElement.fadeIn(duration), void 0 !== attributes.ignoreFooter && (jRow = $("feed-page > .row-main"), jFooter = $("footer-container").children().first(), setButton(), jWindow.resize(setButton)), AnimationFrameService.add(scope, function() {
                            jWindow.scrollTop() > offset ? thisElement.fadeIn(duration) : thisElement.fadeOut(duration), void 0 !== attributes.ignoreFooter && setButton()
                        }, ["scroll", "changeDocumentHeight"]), thisElement.click(function(event) {
                            event.preventDefault(), $("html, body").animate({
                                scrollTop: 0
                            }, duration)
                        })
                    }
                }
            }

            function EatClickDirective() {
                return {
                    restrict: "A",
                    link: function(scope, element) {
                        $(element).click(function(event) {
                            event.preventDefault()
                        })
                    }
                }
            }

            function ScrollPaneDirective() {
                return {
                    restrict: "A",
                    link: function(scope, element, attributes) {
                        var pane, paneOptions = {};
                        paneOptions.autoReinitialise = !0, paneOptions.stickToBottom = attributes.stickToBottom || !1, void 0 !== attributes.horizontalScrollPaneHidden && (paneOptions.contentWidth = "0px"), element.jScrollPane(paneOptions).addClass("scroll-pane"), attributes.$observe("scrollPane", function(data) {
                            data && (pane || (pane = element.data("jsp")), pane.reinitialise())
                        })
                    }
                }
            }

            function FocusDirective() {
                return {
                    restrict: "A",
                    link: function(scope, element) {
                        scope.$parent.$watch("conversation", function(data) {
                            data && element[0].focus()
                        })
                    }
                }
            }

            function HandleAutofillDirective($timeout) {
                return function(scope, element, attributes) {
                    element.prop("method", "POST"), attributes.ngSubmit || $timeout(function() {
                        element.unbind("submit").submit(function(event) {
                            event.preventDefault(), element.find("input, textarea, select").trigger("input").trigger("change").trigger("keydown"), scope.$apply(attributes.ngSubmit)
                        })
                    })
                }
            }

            function NgEnterDirective() {
                return function(scope, element, attributes) {
                    element.bind("keyup", function(event) {
                        13 === event.keyCode && (event.preventDefault(), scope.$apply(function() {
                            scope.$eval(attributes.ngEnter, {
                                event: event
                            })
                        }))
                    })
                }
            }

            function SvgImageDirective($compile, $rootScope) {
                return {
                    restrict: "E",
                    replace: !0,
                    transclude: !0,
                    template: "<img />",
                    compile: function(element, attributes) {
                        var imagePath = $rootScope.Ajax.imgUrlStatic;
                        element.attr({
                            src: imagePath + attributes.source + ".svg",
                            onerror: "this.src=" + imagePath + attributes.source + attributes.alt,
                            height: attributes.height || "100%",
                            width: attributes.width || "100%"
                        })
                    }
                }
            }

            function TabListDirective($location) {
                return {
                    restrict: "E",
                    transclude: !0,
                    replace: !0,
                    templateUrl: "/template/directives/tab-list.html",
                    scope: {
                        links: "="
                    },
                    link: function(scope) {
                        _.each(scope.links, function(link) {
                            return "vote" === scope.$root.activePage ? (link.isActive = "vote" === link.page, void(link.isHidden = !link.isActive)) : (link.isActive = link.url === $location.path(), void(_.contains(["match", "want-you", "you-want"], scope.$root.activePage) && !link.isActive && "vote" !== link.page && (link.isHidden = !0)))
                        })
                    }
                }
            }

            function HrefReloadDirective() {
                return {
                    restrict: "A",
                    link: function(scope, element, attributes) {
                        element.css({
                            cursor: "pointer"
                        }).click(function() {
                            window.location.href = attributes.hrefReload
                        })
                    }
                }
            }

            function GImageDirective($compile, $rootScope) {
                var galleryId = 0;
                return {
                    restrict: "E",
                    replace: !0,
                    template: '<img class="cursor-pointer" />',
                    scope: {
                        src: "@",
                        id: "@",
                        size: "@",
                        category: "@"
                    },
                    compile: function() {
                        return ++galleryId,
                            function(scope, element, attributes) {
                                scope.$watch("id", function() {
                                    var category = attributes.category ? attributes.category : "thumb",
                                        size = attributes.size ? attributes.size : "m",
                                        src = attributes.id ? Ajax.imgUrl + "/users/pictures/" + attributes.id : attributes.src,
                                        linkElement = $("<a></a>");
                                    if (!src || attributes.id || $rootScope.isSecurityUser()) {
                                        if ("undefined" != typeof attributes.background) return attributes.$set("src", scope.$root.Ajax.imgUrlStatic + "transparent300.png"), void element.css({
                                            background: "url(" + src + "/" + category + "_" + size + ".jpg) center no-repeat",
                                            backgroundSize: "cover"
                                        });
                                        attributes.$set("src", src + "/" + category + "_" + size + ".jpg"), $rootScope.isSecurityUser() && (linkElement.attr({
                                            rel: attributes.gallery || "gallery" + galleryId,
                                            href: src + "/image_l.jpg",
                                            target: "_self"
                                        }).addClass("fancybox").append(element[0].outerHTML), element.replaceWith(linkElement).show()), $compile(element.contents())(scope)
                                    }
                                })
                            }
                    }
                }
            }

            function BlurredCoverDirective() {
                return {
                    restrict: "C",
                    scope: {
                        blurRadius: "=",
                        width: "=",
                        height: "=",
                        coverSrc: "@"
                    },
                    template: '<svg width="100%" height="100%"></svg>',
                    link: function(scope, element) {
                        var svgNs = "http://www.w3.org/2000/svg",
                            updateFilter = function() {
                                var rnd = Math.round(1e6 * Math.random());
                                element.find("filter")[0].setAttributeNS(null, "id", "filter" + rnd), element.find("image")[0].setAttributeNS(null, "filter", "url(#filter" + rnd + ")")
                            },
                            createImage = function() {
                                element.find("svg").empty();
                                var img = new Image;
                                img.onload = function() {
                                    var image, isVertical = scope.width / scope.height > img.width / img.height,
                                        blurRadius = parseInt(scope.blurRadius),
                                        imageWidth = scope.width + 4 * blurRadius,
                                        imageHeight = scope.height + 4 * blurRadius,
                                        scale = isVertical ? imageWidth / img.width : imageHeight / img.height,
                                        filter = document.createElementNS(svgNs, "filter"),
                                        blur = document.createElementNS(svgNs, "feGaussianBlur");
                                    blur.setAttributeNS(null, "stdDeviation", scope.blurRadius.toString()), filter.appendChild(blur), element.find("svg").append(filter), image = document.createElementNS(svgNs, "image"), image.setAttributeNS(null, "width", Math.round(isVertical ? imageWidth : img.width * scale).toString()), image.setAttributeNS(null, "height", Math.round(isVertical ? img.height * scale : imageHeight).toString()), image.setAttributeNS(null, "x", Math.round(isVertical ? 2 * -blurRadius : scope.width / 2 - img.width * scale / 2).toString()), image.setAttributeNS(null, "y", Math.round(isVertical ? scope.height / 2 - img.height * scale / 2 : 2 * -blurRadius).toString()), image.setAttributeNS("http://www.w3.org/1999/xlink", "href", scope.coverSrc), element.find("svg").append(image), updateFilter()
                                }, img.src = scope.coverSrc
                            };
                        element.css({
                            width: scope.width,
                            height: scope.height
                        }), scope.$watch("coverSrc", function() {
                            "default-cover" === scope.coverSrc ? element.addClass("bg-blurred-1") : (element.removeClass("bg-blurred-1"), createImage())
                        }), scope.$on("$locationChangeStart", function() {
                            0 !== element.find("image").length && updateFilter()
                        })
                    }
                }
            }

            function HasEmojiDirective($compile, $timeout) {
                return {
                    restrict: "A",
                    compile: function(element) {
                        function renderEmojis(text, element, scope) {
                            element.html(minEmoji(text)), emojify.run(element[0]), $compile(element.contents())(scope)
                        }
                        return emojify.setConfig({
                                only_crawl_id: null,
                                img_dir: Ajax.imgUrlStatic + "emoji",
                                ignored_tags: {
                                    SCRIPT: 1,
                                    PRE: 1,
                                    CODE: 1
                                }
                            }),
                            function(scope, element, attributes) {
                                function observeFunc(data) {
                                    if (data) {
                                        var i, obj = scope,
                                            str = data.split(".");
                                        for (i = 0; i < str.length; i++)
                                            if (obj = obj[str[i]], !obj || !obj[str[i]]) return;
                                        renderEmojis(obj, element, scope)
                                    }
                                }
                                $timeout(function() {
                                    renderEmojis(element.html(), element, scope)
                                }), attributes.ngBindHtml && attributes.$observe("ngBindHtml", observeFunc), scope.$parent.user && !attributes.ngBindHtml ? scope.$parent.$watch("user", function(data, old) {
                                    angular.equals(data, old) || renderEmojis(data.whazzup, element, scope)
                                }) : attributes.ngBindHtml || $timeout(function() {
                                    element.html().length && renderEmojis(element.html(), element, scope)
                                })
                            }
                    }
                }
            }

            function RepeatCheckDirective() {
                return {
                    restrict: "A",
                    require: "ngModel",
                    link: function(scope, element, attributes, controller) {
                        var compareSelector = attributes.repeatCheck;
                        element.add(compareSelector).on("keyup", function() {
                            scope.$apply(function() {
                                var val = element.val() === $(compareSelector).val();
                                controller.$setValidity("repeatcheck", val)
                            })
                        })
                    }
                }
            }

            function MasonizeDirective($interval) {
                return {
                    restrict: "A",
                    link: function(scope, element, attributes) {
                        if (scope.$last) {
                            var container = element.parent();
                            container.imagesLoaded(function() {
                                var interval = $interval(function() {
                                    element.find("img").height() <= 0 || (container.masonry({
                                        gutter: attributes.masonizeGitter ? parseInt(attributes.masonizeGitter) : 10,
                                        itemSelector: ".masonry-item"
                                    }), container.animate({
                                        opacity: 1
                                    }, 500), container.on("$destroy", function() {
                                        return $interval.cancel(interval), interval = void 0, !0
                                    }), $interval.cancel(interval), interval = void 0)
                                }, 100)
                            })
                        }
                    }
                }
            }

            function DfpBannerDirective() {
                return {
                    restrict: "E",
                    link: function(scope, element, attributes) {
                        var slot = DfpSlots.slots[attributes.slotId];
                        $("#" + attributes.slotId).remove(), element.append($("<div>", {
                            id: attributes.slotId
                        }).css({
                            height: slot.height + "px",
                            width: slot.width + "px"
                        })), slot.initializeSlot()
                    }
                }
            }

            function IncludeTemplateDirective(TemplateService) {
                return {
                    restrict: "E",
                    link: function(scope, element, attributes) {
                        var src = attributes.name + ".html";
                        TemplateService.loadTemplate(src, element, scope), scope.itemAction = function(action, params) {
                            angular.isFunction(scope.$parent.onItemAction) && scope.$parent.onItemAction()(action, params)
                        }
                    }
                }
            }

            function LoadingBarDirective() {
                return {
                    restrict: "E",
                    replace: !0,
                    template: '<div class="loading text-center padding"><i class="heartbeat fa fa-lg fa-2x fa-heart text-gray"></i></div>'
                }
            }

            function ValidationErrorsDirective(DialogService) {
                return {
                    restrict: "E",
                    templateUrl: "/template/directives/validation-errors.html",
                    scope: !0,
                    link: function(scope) {
                        scope.$watch(function() {
                            return scope.$parent.validationErrors
                        }, function(validationErrors) {
                            if (void 0 !== validationErrors) {
                                var modal, showDialog = function(translation, url, error) {
                                    modal = DialogService.openMessage(Translator.get("action_interrupted"), error.message, {
                                        buttons: [{
                                            label: Translator.get(translation),
                                            cssClass: "btn btn-info",
                                            click: function() {
                                                window.location.href = scope.$root.Ajax.url + url, modal.close()
                                            }
                                        }]
                                    })
                                };
                                _.each(scope.$parent.validationErrors, function(error) {
                                    2291 === error.code ? showDialog("verify_now", "/verify", error) : 2292 === error.code && showDialog("become_vip", "/vip", error)
                                })
                            }
                        })
                    }
                }
            }

            function StickyHeaderDirective($timeout) {
                return {
                    restrict: "A",
                    link: function(scope, element) {
                        $timeout(function() {
                            element.parents(".modal").scroll(function() {
                                var stickyShift, stickyMargin, stickyHeader = $(".modal-sticky-header"),
                                    modalContent = element.find(".modal-body").first();
                                stickyHeader.offset().left !== modalContent.offset().left && (stickyShift = stickyHeader.offset().left - modalContent.offset().left, stickyMargin = parseInt(stickyHeader.css("margin-left")), stickyHeader.css("margin-left", stickyMargin - stickyShift)), modalContent.offset().top - $(window).scrollTop() < -180 ? stickyHeader.addClass("compact") : stickyHeader.removeClass("compact")
                            })
                        })
                    }
                }
            }

            function CollapseRefreshDirective() {
                return {
                    restrict: "A",
                    link: function(scope, element) {
                        scope.$watch(function() {
                            return element.hasClass("collapsing")
                        }, function(isCollapsing) {
                            isCollapsing && window.dispatchEvent(new Event("resize"))
                        })
                    }
                }
            }

            function CollapseDomRefreshDirective($timeout, $compile) {
                return {
                    restrict: "A",
                    link: function(scope, element) {
                        var jCollapseElement;
                        $timeout(function() {
                            jCollapseElement = element.parents(".collapse"), scope.$watch(function() {
                                return jCollapseElement.hasClass("collapsing")
                            }, function(isVisible) {
                                isVisible && $compile(element.contents())(scope)
                            })
                        })
                    }
                }
            }

            function CloseAllModalsDirective($modalStack) {
                return {
                    restrict: "C",
                    link: function(scope, element) {
                        element.click(function() {
                            $modalStack.dismissAll()
                        })
                    }
                }
            }
            angular.module("Lovoo.Directives").directive("textFill", TextFillDirective).directive("scrollTop", ScrollTopDirective).directive("eatClick", EatClickDirective).directive("scrollPane", ScrollPaneDirective).directive("loadingBar", LoadingBarDirective).directive("focus", FocusDirective).directive("handleAutofill", HandleAutofillDirective).directive("ngEnter", NgEnterDirective).directive("svgImage", SvgImageDirective).directive("tabList", TabListDirective).directive("hrefReload", HrefReloadDirective).directive("gImage", GImageDirective).directive("blurredCover", BlurredCoverDirective).directive("hasEmoji", HasEmojiDirective).directive("repeatCheck", RepeatCheckDirective).directive("masonize", MasonizeDirective).directive("dfpBanner", DfpBannerDirective).directive("includeTemplate", IncludeTemplateDirective).directive("validationErrors", ValidationErrorsDirective).directive("stickyHeader", StickyHeaderDirective).directive("collapseRefresh", CollapseRefreshDirective).directive("collapseDomRefresh", CollapseDomRefreshDirective).directive("closeAllModals", CloseAllModalsDirective), TextFillDirective.$inject = ["AnimationFrameService"], ScrollTopDirective.$inject = ["AnimationFrameService"], HandleAutofillDirective.$inject = ["$timeout"], SvgImageDirective.$inject = ["$compile", "$rootScope", "$timeout"], TabListDirective.$inject = ["$location"], GImageDirective.$inject = ["$compile", "$rootScope"], HasEmojiDirective.$inject = ["$compile", "$timeout"], MasonizeDirective.$inject = ["$interval"], IncludeTemplateDirective.$inject = ["TemplateService"], ValidationErrorsDirective.$inject = ["DialogService"], StickyHeaderDirective.$inject = ["$timeout"], CollapseDomRefreshDirective.$inject = ["$timeout", "$compile"], CloseAllModalsDirective.$inject = ["$modalStack"]
        }(window.angular, window.emojify, window.DfpSlots),
        function() {
            "use strict";

            function translateFilter() {
                return function(message, params, count) {
                    return Translator.get(message, params, count)
                }
            }

            function dateDiffExtendedFilter() {
                return function(date) {
                    var dateDiff, today = new Date,
                        _date = new Date(date),
                        dd = _date.getDate(),
                        dateDiff = Math.ceil((+today - +_date) / 1e3),
                        oneMinute = 60,
                        oneHour = 60 * oneMinute,
                        oneDay = 24 * oneHour,
                        oneWeek = 7 * oneDay;
                    return dateDiff >= 0 && oneMinute > dateDiff ? "few_seconds_ago" : dateDiff >= oneMinute && 2 * oneMinute > dateDiff ? "one_minute_ago" : dateDiff >= 2 * oneMinute && 30 * oneMinute > dateDiff ? "few_minutes_ago" : dateDiff >= 30 * oneMinute && oneHour > dateDiff ? "half_hour_ago" : dateDiff >= oneMinute && 2 * oneHour > dateDiff ? "one_hour_ago" : dateDiff >= 2 * oneHour && 12 * oneHour > dateDiff ? "few_hours_agos" : dateDiff >= 12 * oneHour && oneDay > dateDiff ? dd == today.getDate() ? "today" : "yesterday" : dateDiff >= oneDay && oneWeek > dateDiff ? dd == today.getDate() - 1 ? "yesterday" : "few_days_ago" : dateDiff >= oneWeek && 2 * oneWeek > dateDiff ? "one_week_ago" : dateDiff >= 2 * oneWeek ? "few_weeks_ago" : $filter("transformToDateString")(_date)
                }
            }

            function dateDiffFilter() {
                return function(date, params) {
                    var today = new Date,
                        _date = new Date(date),
                        dd = _date.getDate(),
                        mm = _date.getMonth() + 1,
                        yyyy = _date.getFullYear(),
                        one_day = 864e5;
                    switch (Math.ceil((+today - +_date) / one_day)) {
                        case 0:
                        case 1:
                            if (dd == today.getDate()) {
                                _date = "today";
                                break
                            }
                        case 2:
                            if (dd == today.getDate() - 1) {
                                _date = "yesterday";
                                break
                            }
                        default:
                            10 > dd && (dd = "0" + dd), 10 > mm && (mm = "0" + mm), _date = dd + "." + mm + "." + yyyy
                    }
                    return _date
                }
            }

            function limitObjFilter() {
                return function(obj, limit) {
                    var count = 0,
                        newObj = {};
                    return $.each(obj, function(key, val) {
                        var curr = {};
                        count >= limit || (curr[key] = val, angular.extend(newObj, curr), ++count)
                    }), newObj
                }
            }

            function skipToFilter() {
                return function(obj, skip) {
                    var count = 0,
                        newObj = {};
                    if (void 0 !== obj) return _.each(obj, function(val, key) {
                        var curr = {};
                        count >= skip && (curr[key] = val, angular.extend(newObj, curr)), ++count
                    }), newObj
                }
            }

            function formatNumberFilter() {
                return function(number) {
                    if (number) {
                        var localeNumber = number;
                        number % 1 !== 0 && (localeNumber = Math.floor(localeNumber)), localeNumber = localeNumber.toLocaleString();
                        var decimalPlaces = 1..toLocaleString().length - 1;
                        0 !== decimalPlaces && (localeNumber = localeNumber.substring(0, localeNumber.length - decimalPlaces))
                    }
                    return localeNumber || 0
                }
            }

            function leadingZeroFilter() {
                return function(n, len) {
                    isNaN(len) && (len = 4);
                    var num = parseInt(n, 10);
                    if (len = parseInt(len, 10), isNaN(num) || isNaN(len)) return n;
                    for (num = "" + num; num.length < len;) num = "0" + num;
                    return num
                }
            }

            function unsafeFilter($sce) {
                return function(val) {
                    return $sce.trustAsHtml(val)
                }
            }

            function currencyCentsFilter() {
                return function(number) {
                    var currencyLast = _.isNumber(number) ? (number % 1).toFixed(2) : "1.00";
                    return currencyLast.slice(2)
                }
            }

            function formatLocalePriceFilter($filter) {
                return function(number) {
                    return $filter("formatNumber")(number) + 1.1.toLocaleString().charAt(1) + $filter("currencyCents")(number)
                }
            }

            function transformToDateStringFilter($filter) {
                return function(dateStr, format) {
                    if (!(dateStr instanceof Date)) {
                        for (var tmpDateObj = {}, dateArray = dateStr.match(/(\d+)/g), formatKeys = format.match(/(dd|MM|yyyy)/g), i = 0; i < dateArray.length; ++i) tmpDateObj[formatKeys[i]] = dateArray[i];
                        dateStr = new Date(tmpDateObj.yyyy, parseInt(tmpDateObj.MM) - 1, tmpDateObj.dd)
                    }
                    return dateStr.getFullYear() + "-" + $filter("leadingZero")(parseInt(dateStr.getMonth()) + 1, 2) + "-" + $filter("leadingZero")(dateStr.getDate(), 2)
                }
            }

            function formatBytes($filter) {
                return function(bytes) {
                    if (0 === bytes) return "0 Bytes";
                    var k = 1024,
                        sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                        i = Math.floor(Math.log(bytes) / Math.log(k)),
                        bytesString = $filter("formatLocalePrice")(bytes / Math.pow(k, i));
                    return bytesString + " " + sizes[i]
                }
            }

            function rangeFilter() {
                return function(targetArray, range) {
                    for (var i = 0; i < parseInt(range); ++i) targetArray.push(i);
                    return targetArray
                }
            }
            return angular.module("Lovoo.Filter", []).filter("translate", translateFilter).filter("date_diff_extended", dateDiffExtendedFilter).filter("date_diff", dateDiffFilter).filter("limitObj", limitObjFilter).filter("skipTo", skipToFilter).filter("formatNumber", formatNumberFilter).filter("leadingZero", leadingZeroFilter).filter("unsafe", unsafeFilter).filter("currencyCents", currencyCentsFilter).filter("formatLocalePrice", formatLocalePriceFilter).filter("transformToDateString", transformToDateStringFilter).filter("formatBytes", formatBytes).filter("range", rangeFilter)
        }(),
        function() {
            "use strict";

            function dialogTutorialModalCtrl(STATE, $location, $scope, $timeout, DialogTutorialService, EyecatcherActionService) {
                function dispatchAction(action, actionCallback) {
                    switch (action) {
                        case "settings_search_filter":
                            "function" == typeof actionCallback && (actionCallback(), DialogTutorialService.setCallbackForExecutingTutorialAction(null)), $location.path(Ajax.url + "/settings/community");
                            break;
                        case "credits_overview":
                            "function" == typeof actionCallback && (actionCallback(), DialogTutorialService.setCallbackForExecutingTutorialAction(null)), $location.path(Ajax.url + "/credits");
                            break;
                        case "eyecatcher":
                            $timeout(function() {
                                EyecatcherActionService.addToEyecatcher()
                            }, 500);
                            break;
                        case "next":
                            $timeout(function() {
                                DialogTutorialService.getDialog()
                            }, 500);
                            break;
                        default:
                            "function" == typeof actionCallback && (actionCallback(), DialogTutorialService.setCallbackForExecutingTutorialAction(null)), $location.path(Ajax.url + "/" + action)
                    }
                }
                $scope.state = STATE, $scope.setState = function(state) {
                    DialogTutorialService.setState({
                        enable: state
                    })
                }, $scope.execute = function(action) {
                    var actionCallback = DialogTutorialService.getCallbackForExecutingTutorialAction();
                    $scope.$close(), dispatchAction(action, actionCallback)
                }
            }
            angular.module("Dialog").constant("STATE", 1).controller("DialogTutorialModalCtrl", dialogTutorialModalCtrl), dialogTutorialModalCtrl.$inject = ["STATE", "$location", "$scope", "$timeout", "DialogTutorialService", "EyecatcherActionService"]
        }(),
        function() {
            "use strict";

            function DialogTutorialQueryFctry($resource) {
                return {
                    tutorial: $resource(Ajax.api + "/tutorial/:action/", {}, {
                        getDialog: {
                            method: "GET",
                            params: {
                                action: "dialog"
                            }
                        },
                        putStates: {
                            method: "PUT",
                            params: {
                                action: "states"
                            }
                        }
                    })
                }
            }

            function DialogTutorialService(TUTORIALS_ACTIVE, $rootScope, DialogService, DialogTutorialQueryFctry) {
                function addTestFlagForApiRequest(params) {
                    return testMode && angular.extend(params, {
                        wt: 1
                    }), params
                }
                var actionCallback, isLoading, oldEnabled, self = this,
                    testMode = !1;
                this.setTestMode = function(testFlag) {
                    testMode = testFlag
                }, this.openAdblockHint = function() {
                    this.openTutorialDialog({
                        actions: [],
                        hideUnsubscriber: !0,
                        picture: "tutorial/adblocker",
                        title: Translator.get("adblocker_hint_title"),
                        text: Translator.get("adblocker_hint_text"),
                        tutorialType: "adblocker",
                        type: "tutorial"
                    })
                }, this.openTutorialDialog = function(tutorialData) {
                    DialogService.openDialog({
                        backdrop: !0,
                        controller: "DialogTutorialModalCtrl",
                        templateUrl: "/template/dialog/tutorial.html"
                    }, addTestFlagForApiRequest({
                        tutorialData: tutorialData
                    }))
                }, this.getDialog = function(params) {
                    params || (params = {}), params = addTestFlagForApiRequest(params), DialogTutorialQueryFctry.tutorial.getDialog(params)
                }, this.setState = function(params) {
                    params || (params = {}), params.enable === oldEnabled || isLoading || (isLoading = !0, params = addTestFlagForApiRequest(params), DialogTutorialQueryFctry.tutorial.putStates(params).$promise.then(function(data) {
                        self.updateTutorialStates(data.response.tutorialStates), oldEnabled = params.enable, isLoading = !1
                    }))
                }, this.loadTutorialIfEnabled = function(tutorialType) {
                    !0 === this.isTutorialAndEvenTypeEnabled(tutorialType) && this.getDialog({
                        type: tutorialType
                    })
                }, this.isTutorialAndEvenTypeEnabled = function(tutorialType) {
                    return _.size($rootScope.TutorialStates) && void 0 !== $rootScope.TutorialStates[tutorialType] && !$rootScope.TutorialStates[tutorialType] && -1 !== $.inArray(tutorialType, TUTORIALS_ACTIVE) ? !0 : !1
                }, this.updateTutorialStates = function(tutorialStates) {
                    $rootScope.TutorialStates = tutorialStates
                }, this.setCallbackForExecutingTutorialAction = function(_actionCallback) {
                    actionCallback = _actionCallback
                }, this.getCallbackForExecutingTutorialAction = function() {
                    return actionCallback
                }
            }
            angular.module("Dialog").constant("TUTORIALS_ACTIVE", ["tutorial_credits", "tutorial_desktop_notifications", "tutorial_eyecatcher", "tutorial_filter", "tutorial_ghost", "tutorial_kiss", "tutorial_notification", "tutorial_online", "tutorial_readconfirm", "tutorial_topchat", "tutorial_verify", "tutorial_vip", "tutorial_whazzup"]).factory("DialogTutorialQueryFctry", DialogTutorialQueryFctry).service("DialogTutorialService", DialogTutorialService), DialogTutorialQueryFctry.$inject = ["$resource"], DialogTutorialService.$inject = ["TUTORIALS_ACTIVE", "$rootScope", "DialogService", "DialogTutorialQueryFctry"]
        }(), angular.module("Lovoo.User", ["Routing", "ngResource", "Lovoo.Filter", "Lovoo.Auth", "Lovoo.Chat", "ngResource.Batch", "Dialog", "ngCollection", "Notifications", "TemplateLoader"]).controller("UserDialogProfileCtrl", UserDialogProfileCtrl).controller("userListCtrl", userListCtrl).controller("ServerGeneratedCtrl", ServerGeneratedCtrl).controller("UserKissCtrl", UserKissCtrl).controller("UserReportCtrl", UserReportCtrl), UserDialogProfileCtrl.$inject = ["$scope", "LoadUserService", "ToasterService", "UserActionsFctry"], userListCtrl.$inject = ["$scope", "UserListService", "UserActionsFctry", "RegisterService", "NotificationService"], ServerGeneratedCtrl.$inject = ["$scope", "FacebookService", "RegisterService"], UserKissCtrl.$inject = ["$scope", "ToasterService", "UserQueryFctry"], UserReportCtrl.$inject = ["$scope", "UserReportService", "ToasterService"], angular.module("Lovoo.User").directive("profileImage", ProfileImage).directive("profileBox", profileBox).directive("userList", userList).directive("scrollRefresh", scrollRefresh).directive("profileBackgroundImage", profileBackgroundImage).directive("userHover", userHover).directive("onlineStatus", onlineStatus).directive("iGender", iGender).directive("svgVerified", svgVerified), ProfileImage.$inject = ["ImageService"], userList.$inject = ["TemplateService"], scrollRefresh.$inject = ["$window", "$timeout", "$interval"], profileBackgroundImage.$inject = ["ImageService"],
        function() {
            "use strict";

            function UserActionFctry($resource, $rootScope) {
                return {
                    follow: $resource($rootScope.Ajax.api + "/users/:userId/follow", {}, {
                        post: {
                            method: "POST",
                            params: {
                                userId: "@userId"
                            }
                        },
                        "delete": {
                            method: "DELETE",
                            params: {
                                userId: "@userId"
                            }
                        }
                    }),
                    visit: $resource($rootScope.Ajax.api + "/users/:userId/visits", {}, {
                        post: {
                            method: "POST",
                            params: {
                                userId: "@userId"
                            }
                        }
                    })
                }
            }

            function UserRequestFctry($resource, $rootScope) {
                return {
                    user: $resource($rootScope.Ajax.api + "/users/:userId", {}, {
                        get: {
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                "public": !0
                            }
                        }
                    }),
                    hashtags: $resource($rootScope.Ajax.api + "/users/:userId/hashtags", {}, {
                        get: {
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                "public": !0
                            }
                        }
                    }),
                    connections: $resource($rootScope.Ajax.api + "/users/:userId/connections"),
                    detailsLoggedIn: $resource($rootScope.Ajax.api + "/users/:userId/details"),
                    detailsLoggedOut: $resource($rootScope.Ajax.api + "/userdetails/:userId", {}, {
                        get: {
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                "public": !0
                            }
                        }
                    }),
                    pictures: $resource($rootScope.Ajax.api + "/users/:userId/pictures", {}, {
                        get: {
                            headers: {
                                method: "GET",
                                "KISSAPI-VERSION": 1.15
                            }
                        }
                    })
                }
            }

            function UserProto(USER_CONNECTIONS, $rootScope, ImageService, UserActionFctry, UserActionsFctry, UserRequestFctry) {
                var User = function(userObject) {
                    angular.extend(this, userObject), this.connections = {
                        isLoading: !1,
                        isLoaded: !1,
                        success: []
                    }, this.interests = [], this.interestsHelper = {
                        isLoading: !1,
                        isLoaded: !1,
                        success: []
                    }, this.isSelf = !1, this.postCount = null
                };
                return User.prototype.getImageUrl = function(size, category) {
                    var options = {
                        category: category,
                        size: size
                    };
                    return this.picture ? options.source = {
                        pictureId: this.picture,
                        gender: this.gender
                    } : options.gender = this.gender, ImageService.createProfileImageUrl(options)
                }, User.prototype.block = function() {
                    $rootScope.isSecurityUser() && !this.isSelf && UserActionsFctry.action(this, "block")
                }, User.prototype.report = function() {
                    $rootScope.isSecurityUser() && !this.isSelf && UserActionsFctry.action(this, "report", {
                        referenceType: "user"
                    })
                }, User.prototype.chat = function() {
                    $rootScope.isSecurityUser() && !this.isSelf && UserActionsFctry.action(this, "chat")
                }, User.prototype.visit = function() {
                    !$rootScope.isSecurityUser() || this.isSelf || $rootScope.Self.admin && nv || UserActionFctry.visit.post({
                        userId: this.id
                    })
                }, User.prototype.getDetails = function() {
                    var userDetailsQuery, self = this;
                    userDetailsQuery = $rootScope.isSecurityUser() ? UserRequestFctry.detailsLoggedIn.get({
                        userId: this.id
                    }) : UserRequestFctry.detailsLoggedOut.get({
                        userId: this.id
                    }), userDetailsQuery.$promise.then(function(result) {
                        var userDetails = result.response.result;
                        $rootScope.isSecurityUser() && userDetails.me instanceof Array && userDetails.flirt instanceof Array && (userDetails = {}), self.details = userDetails
                    })
                }, User.prototype.getInterests = function(params) {
                    var self = this;
                    return this.interestsHelper.isLoaded ? void(void 0 !== params.success && params.success()) : (void 0 !== params.success && self.interestsHelper.success.push(params.success), void(this.interestsHelper.isLoading || (this.interestsHelper.isLoading = !0, UserRequestFctry.hashtags.get({
                        userId: this.id
                    }, function(data) {
                        self.interests = data.response.result, self.interestsHelper.isLoading = !1, self.interestsHelper.isLoaded = !0, _.each(self.interestsHelper.success, function(success) {
                            success()
                        }), self.interestsHelper.success = []
                    }))))
                }, User.prototype.toggleFollow = function() {
                    var params = {
                        userId: this.id
                    };
                    $rootScope.isSecurityUser() && void 0 !== this.connections.hasFollowed && !this.isSelf && (this.connections.hasFollowed ? UserActionFctry.follow["delete"](params) : UserActionFctry.follow.post(params), this.connections.hasFollowed = !this.connections.hasFollowed)
                }, User.prototype.getConnections = function(params) {
                    var setConnections, self = this;
                    return setConnections = function(connections) {
                        self.connections.isLoading = !1, _.each(USER_CONNECTIONS, function(connection) {
                            self.connections[connection] = void 0 !== connections[connection] && 1 === connections[connection]
                        })
                    }, !$rootScope.isSecurityUser() || this.isSelf ? (setConnections({}), void(void 0 !== params.success && params.success())) : self.connections.isLoaded ? void(void 0 !== params.success && params.success()) : (void 0 !== params.success && self.connections.success.push(params.success), void(self.connections.isLoading || (self.connections.isLoading = !0, UserRequestFctry.connections.get({
                        userId: this.id
                    }, function(data) {
                        setConnections(data.response), _.each(self.connections.success, function(success) {
                            success()
                        }), self.connections.success = []
                    }))))
                }, User
            }
            angular.module("Lovoo.User").constant("USER_CONNECTIONS", ["hasFollowed", "hasConversation", "hasLiked", "isMatch"]).factory("UserActionFctry", UserActionFctry).factory("UserRequestFctry", UserRequestFctry).factory("UserProto", UserProto), UserActionFctry.$inject = ["$resource", "$rootScope"], UserRequestFctry.$inject = ["$resource", "$rootScope"], UserProto.$inject = ["USER_CONNECTIONS", "$rootScope", "ImageService", "UserActionFctry", "UserActionsFctry", "UserRequestFctry"]
        }(),
        function() {
            "use strict";

            function UserQueryFctry($resource, $batchRequest) {
                return {
                    info: $batchRequest({
                        isArray: !0
                    }, {
                        get: {
                            method: "GET"
                        }
                    }),
                    self: $resource(Ajax.api + "/self/:type/:subType", {}, {
                        get: {
                            params: {
                                type: "@type",
                                subType: "@subType"
                            },
                            method: "GET"
                        },
                        put: {
                            method: "PUT",
                            params: {
                                type: "@type",
                                subType: "@subType"
                            }
                        }
                    }),
                    unlock: $resource(Ajax.api + "/self/unlockuser", {}, {
                        put: {
                            params: {
                                type: "@type",
                                id: "@id"
                            },
                            method: "PUT"
                        }
                    }),
                    single: $resource(Ajax.api + "/users/:userId/:category", {
                        cache: !0
                    }, {
                        post: {
                            params: {
                                userId: "@userId",
                                category: "@category"
                            },
                            method: "POST"
                        },
                        get: {
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                "public": !0
                            },
                            params: {
                                userId: "@userId",
                                category: "@category"
                            },
                            method: "GET"
                        },
                        "delete": {
                            params: {
                                userId: "@userId",
                                category: "@category"
                            },
                            method: "DELETE"
                        },
                        info: {
                            headers: {
                                "public": !0
                            },
                            url: Ajax.api + "/userdetails/:userId",
                            method: "GET"
                        },
                        block: {
                            method: "POST",
                            params: {
                                userId: "@userId",
                                category: "block"
                            }
                        },
                        unblock: {
                            method: "DELETE",
                            params: {
                                userId: "@userId",
                                category: "block"
                            }
                        },
                        report: {
                            method: "POST",
                            params: {
                                userId: "@userId",
                                type: "@type",
                                text: "@text"
                            }
                        }
                    }),
                    match: $resource(Ajax.api + "/matches/:userId/:type", {}, {
                        get: {
                            headers: {
                                noException: !0
                            },
                            method: "GET"
                        },
                        post: {
                            params: {
                                userId: "@userId",
                                type: "@type"
                            },
                            method: "POST"
                        },
                        list: {
                            url: Ajax.api + "/matches/:page",
                            params: {
                                page: "@page"
                            },
                            method: "GET"
                        }
                    }),
                    eyecatcher: $resource(Ajax.api + "/users", {}, {
                        get: {
                            ignoreLoadingBar: !0,
                            method: "GET",
                            headers: {
                                "public": !0
                            }
                        },
                        add: {
                            url: Ajax.api + "/users/:userId/flirtstar",
                            method: "GET"
                        },
                        put: {
                            url: Ajax.api + "/self/topflirts",
                            method: "PUT"
                        }
                    }),
                    credits: $resource(Ajax.api + "/credits", {
                        cache: !0
                    }, {
                        unlock: {
                            url: Ajax.api + "/credits/:userId/unlock",
                            params: {
                                userId: "@userId"
                            },
                            method: "GET"
                        }
                    }),
                    kiss: $resource(Ajax.api + "/users/:userId/kisstypes", {
                        cache: !0
                    }, {
                        types: {
                            method: "GET",
                            params: {
                                userId: "@userId"
                            }
                        },
                        back: {
                            url: Ajax.api + "/kisses/:kissId/kissback",
                            method: "POST",
                            params: {
                                kissId: "@kissId"
                            }
                        }
                    }),
                    friends: $resource(Ajax.api + "/friends", {
                        cache: !0
                    }, {
                        get: {
                            method: "GET"
                        },
                        remove: {
                            url: Ajax.api + "/friends/:friendshipId",
                            method: "DELETE",
                            params: {
                                friendshipId: "@friendshipId"
                            }
                        }
                    }),
                    awesome: $resource(Ajax.url + "/awesome", {
                        cache: !0,
                        ignoreLoadingBar: !0
                    }, {
                        get: {
                            method: "GET",
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                "public": !0
                            }
                        }
                    }),
                    report: $resource(Ajax.api + "/report", {
                        cache: !0
                    }, {
                        get: {
                            method: "GET",
                            headers: {
                                "public": !1
                            }
                        }
                    })
                }
            }

            function UserActionsFctry($location, $rootScope, $window, DialogService, StorageService, ToasterService, userQuery) {
                var _obj, _options, func, isLoading = !1;
                return func = {
                    like: function() {
                        isLoading !== !0 && (isLoading = !0, userQuery.match.post({
                            userId: _obj.id,
                            vote: 1
                        }).$promise.then(function() {
                            isLoading = !1, ToasterService.open("success", Translator.get("success.info_you_want", {
                                name: _obj.name
                            }))
                        }))
                    },
                    kiss: function() {
                        DialogService.openDialog({
                            templateUrl: "/template/dialog/kisses.html",
                            controller: "UserKissCtrl",
                            backdrop: !0
                        }, {
                            user: _obj
                        })
                    },
                    kiss_back: function() {
                        userQuery.kiss.back({
                            kissId: _obj.id
                        }).$promise.then(function() {
                            ToasterService.open("success", Translator.get("success.info_kiss", {
                                user: _obj.user.name
                            }, 1)), ++_obj.flag
                        })
                    },
                    friend_remove: function() {
                        userQuery.friends.remove({
                            friendshipId: _obj.id,
                            friendEntryId: _obj.id
                        }).$promise.then(function() {
                            ToasterService.open("success", Translator.get("success.title_friendshipstopped"), Translator.get("success.info_friendshipstopped", {
                                user: _obj.user.name
                            })), _obj.listHide = !0
                        })
                    },
                    block: function() {
                        DialogService.openDialog({
                            templateUrl: "/template/dialog/block-user.html"
                        }, {
                            user: _obj,
                            callback: function() {
                                userQuery.single.block({
                                    userId: _obj.id
                                }).$promise.then(function() {
                                    var modal = DialogService.openMessage(Translator.get("success.title_block"), Translator.get("success.info_block", {
                                        user: _obj.name
                                    }), {
                                        buttons: [{
                                            label: Translator.get("to_blocks"),
                                            cssClass: "btn btn-primary",
                                            click: function() {
                                                modal.close(), window.location.href = $rootScope.Ajax.url + "/settings/community/blocked"
                                            }
                                        }]
                                    });
                                    _obj.listHide = !0
                                })
                            }
                        })
                    },
                    unblock: function() {
                        DialogService.openDialog({}, {
                            title: Translator.get("block_undo"),
                            content: Translator.get("txt.block_undo_confirm"),
                            callback: function() {
                                userQuery.single.unblock({
                                    userId: _obj.user.id
                                }).$promise.then(function() {
                                    var modal = DialogService.openMessage(Translator.get(Translator.get("block_undo")), Translator.get("success.info_unblock", {
                                        user: _obj.user.name
                                    }), {
                                        buttons: [{
                                            label: Translator.get("show_profile"),
                                            cssClass: "btn btn-primary",
                                            click: function() {
                                                DialogService.showProfileDialog(_obj.user), modal.close()
                                            }
                                        }]
                                    });
                                    _obj.listHide = !0
                                })
                            }
                        })
                    },
                    report: function() {
                        var params = {
                            referenceObject: _obj
                        };
                        _options && angular.extend(params, _options), DialogService.openDialog({
                            templateUrl: "/template/dialog/report-form.html",
                            controller: "UserReportCtrl",
                            backdrop: !0
                        }, params)
                    },
                    fan: function() {
                        userQuery.single.post({
                            userId: _obj.id,
                            category: "hottie"
                        }, function() {
                            ToasterService.open("success", Translator.get("success.title_hotlist_add"), Translator.get("success.info_hotlist_add", {
                                user: _obj.name
                            }))
                        }, function() {
                            return !1
                        })
                    },
                    fan_remove: function() {
                        userQuery.single["delete"]({
                            userId: _obj.id,
                            category: "hottie"
                        }).$promise.then(function() {
                            ToasterService.open("success", Translator.get("success.title_hotlist_delete"), Translator.get("success.info_hotlist_delete", {
                                user: _obj.user.name
                            })), _obj.listHide = !0
                        })
                    },
                    chat: function() {
                        DialogService.openDialog({
                            controller: "chatRequestCtrl",
                            templateUrl: "/template/dialog/chat-request.html"
                        }, {
                            user: _obj
                        })
                    },
                    profile: function() {
                        var location, i;
                        if (_obj.del) return ToasterService.open("error", Translator.get("account_deleted"), Translator.get("account_deleted_message")), this;
                        if (!$rootScope.isSecurityUser() || $window.opener) return location = $rootScope.Ajax.url + "/profile/" + _obj.id, $window.opener ? $window.opener.location.href = location : $window.location.href = location;
                        if (_options && _options.crypt || _obj && _obj.crypt) func.crypt_profile();
                        else {
                            for (i = 0; i < _obj.flirtInterests.length; ++i) - 1 !== $.inArray(_obj.flirtInterests[i], ["love", "ons", "sex"]) && _obj.flirtInterests.splice(i, 1);
                            DialogService.showProfileDialog(_obj, _options)
                        }
                        return this
                    },
                    crypt_profile: function() {
                        function unlockRequest() {
                            userQuery.unlock.put({
                                id: _obj.id,
                                type: list
                            }).$promise.then(function(data) {
                                var userScope = angular.extend(data.response.user, {
                                    crypt: 0,
                                    hash: data.response.cryptId
                                });
                                angular.extend(_obj, userScope), DialogService.showProfileDialog(userScope)
                            })
                        }
                        var list, type = "unlockuser";
                        switch (_options.list.replace("-", "")) {
                            case "youwant":
                                list = "matchyouwant";
                                break;
                            case "wantyou":
                                list = "matchwantyou";
                                break;
                            case "hotties":
                                list = "hotlistme";
                                break;
                            default:
                                list = _options.list
                        }
                        StorageService.get("general_unlock_confirmation." + Self.id) ? unlockRequest() : userQuery.credits.unlock({
                            userId: Self.id,
                            type: type
                        }).$promise.then(function(data) {
                            DialogService.openDialog({
                                templateUrl: "/template/dialog/unlock-user.html",
                                controller: function($scope) {
                                    $scope.unlockCredits = data.response.credits, $scope.generalConfirmation = !0, $scope.setGeneralConfirmation = function(val) {
                                        $scope.generalConfirmation = val
                                    }, $scope.vip = function() {
                                        $location.path($rootScope.Ajax.url + "/vip"), $scope.$close()
                                    }, $scope.doUnlock = function() {
                                        $scope.generalConfirmation && StorageService.set("general_unlock_confirmation." + Self.id, $scope.generalConfirmation), unlockRequest(), $scope.$close();

                                    }
                                }
                            }, _options)
                        })
                    }
                }, {
                    action: function(target, action, options) {
                        _obj = target, _options = options, func[action].apply()
                    }
                }
            }

            function UserRequestService(UserRequestFctry, UserProto) {
                this.getUserById = function(userId, success) {
                    UserRequestFctry.user.get({
                        userId: userId
                    }, function(data) {
                        success(new UserProto(data.response.user))
                    })
                }, this.getUserPictures = function(userId) {
                    return UserRequestFctry.pictures.get({
                        userId: userId
                    }).$promise
                }
            }

            function ImageService($rootScope) {
                function isEveryImageSquare(images) {
                    var i;
                    for (i = 0; i < images.length; ++i)
                        if (images[i].width !== images[i].height) return !1;
                    return !0
                }

                function getFullSizeImageFromImageArray(images) {
                    var i, highestImage = {
                        width: 0,
                        height: 0,
                        url: ""
                    };
                    for (i = 0; i < images.length; ++i) images[i].height >= highestImage.height && (highestImage = images[i]);
                    return highestImage
                }

                function createDefaultProfileImageUrl(gender) {
                    return $rootScope.Ajax.imgUrlStatic + "default_user_" + $rootScope.Genders[gender].gender + "_300.png"
                }
                var self = this;
                this.createProfileImageUrl = function(options) {
                    var imageUrl = $rootScope.Ajax.imgUrl + "/users/pictures/";
                    return options && options.source ? (options.source.pictureId ? imageUrl += options.source.pictureId : options.source.picture && (imageUrl += options.source.picture), imageUrl += void 0 !== options.category && options.category ? "/" + options.category : "/thumb", imageUrl += void 0 !== options.size && options.size ? "_" + options.size : "_l", imageUrl + ".jpg") : this.getDefaultProfileImageUrl(options)
                }, this.getDefaultProfileImageUrl = function(options) {
                    var gender;
                    return gender = options && options.source && options.source.gender ? options.source.gender : options && options.gender ? options.gender : Self && Self.gender ? Self.gender : 1, gender = void 0 !== $rootScope.Genders[gender] ? gender : 1, createDefaultProfileImageUrl(gender)
                }, this.getProfileImageUrlFromImageArray = function(params) {
                    var imageUrl = this.getImageUrlFromImageArray(params);
                    return "" === imageUrl ? createDefaultProfileImageUrl(params.gender) : imageUrl
                }, this.getImageUrlFromImageArray = function(params) {
                    return self.getImageFromImageArray(params).url
                }, this.getImageFromImageArray = function(params) {
                    var i, image, returnImage = {
                        width: 0,
                        height: 0,
                        url: ""
                    };
                    if (void 0 === params.width) return getFullSizeImageFromImageArray(params.images);
                    for (void 0 === params.category && (params.category = "image"), isEveryImageSquare(params.images) && (params.category = "thumb"), params.images = _.sortBy(params.images, function(sortImage) {
                            return sortImage.width
                        }), i = 0; i < params.images.length && (image = params.images[i], "thumb" === params.category && image.width !== image.height || "image" === params.category && image.width === image.height || (returnImage = image, !(image.width >= params.width))); ++i);
                    return returnImage
                }
            }

            function UserListService($rootScope, $collection, $timeout, userQuery) {
                var _category, _query, _timeout, nav_index, noResults, cached_user, users, i, diff, diff_array, page = 1,
                    self = this;
                users = $collection.getInstance({
                    idAttribute: "id"
                }), this.navigation = function(curr_user, direction, modal) {
                    function findNextUser(start, end) {
                        var condition, _index = start;
                        for (condition = end > start ? "_index < end" : "_index > end", _index; condition; start > end ? _index-- : _index++) {
                            if (cached_user[_index] && !cached_user[_index].crypt) {
                                nav_index = _index;
                                break
                            }
                            if (_index === end) break
                        }
                    }
                    if (!arguments.length) return self.navigation.loadNextUsers = void 0, self.navigation.cached_user = [], !1;
                    for (cached_user = self.navigation.cached_user, self.navigation.cached && users.addAll(self.navigation.cached), cached_user.length < users.size() && (diff = users.size() - cached_user.length, diff_array = users.all().slice(cached_user.length, diff), cached_user.push.apply(cached_user, diff_array)), i = 0; i < cached_user.length; ++i)
                        if (angular.equals(cached_user[i], curr_user)) {
                            nav_index = i;
                            break
                        }
                    return direction ? (-1 !== direction && nav_index + direction < cached_user.length ? findNextUser(nav_index + direction, cached_user.length) : nav_index + direction >= 0 && findNextUser(nav_index + direction, 0), curr_user.id !== cached_user[nav_index].id && (self.navigation.openNext(nav_index, cached_user, curr_user, modal), nav_index > cached_user.length - 5 && ($.isFunction(self.navigation.loadNextUsers) ? self.navigation.loadNextUsers() : "start" !== this.list && self.getList(this.list, self.page_limit, {
                        inCache: !0
                    }))), !0) : !direction && nav_index
                }, this.navigation.cached_user = [], this.enableLoading = function() {
                    return noResults = !1
                }, this.resetList = function() {
                    self.navigation(), page = 1, users.removeAll()
                }, this.get = function() {
                    return users.all()
                }, this.has = function() {
                    return 0 !== users.size()
                }, this.getList = function(category, limit, options) {
                    var searchParams, start, usersFromCache, advancedLists = ["kiss", "friends", "blocks", "favorites"];
                    if (options = options || {}, $timeout.cancel(_timeout), category !== _category && (self.resetList(), _category = category), options.clear && (noResults = !1, page = 1), noResults) return {};
                    switch (category) {
                        case "visited":
                            _query = userQuery.self.get(angular.extend({
                                type: category,
                                resultLimit: limit,
                                resultPage: page
                            }, options));
                            break;
                        case "youwant":
                        case "hit":
                        case "wantyou":
                            _query = userQuery.match.list(angular.extend({
                                page: category,
                                resultPage: page,
                                resultLimit: limit
                            }, options));
                            break;
                        case "hotties":
                            _query = userQuery.self.get(angular.extend({
                                type: category,
                                subType: "me",
                                resultLimit: limit,
                                resultPage: page
                            }, options));
                            break;
                        case "favorites":
                            _query = userQuery.self.get(angular.extend({
                                type: "hotties",
                                resultLimit: limit,
                                resultPage: page
                            }, options));
                            break;
                        case "blocks":
                            _query = userQuery.self.get(angular.extend({
                                type: category,
                                resultLimit: limit,
                                resultPage: page
                            }, options));
                            break;
                        case "start":
                            searchParams = {
                                resultLimit: limit,
                                resultPage: 1,
                                radiusTo: 50,
                                "new": !0,
                                orderBy: "random",
                                "userQuality[0]": "pic",
                                type: "env",
                                cache: !1
                            }, users.removeAll(), _query = userQuery.single.get(angular.extend(searchParams, options));
                            break;
                        case "eyecatcher":
                            _query = userQuery.eyecatcher.get({
                                resultLimit: limit,
                                resultPage: page,
                                type: "top_settings",
                                cache: !1
                            });
                            break;
                        case "eyecatcher_global":
                            _query = userQuery.eyecatcher.get({
                                resultLimit: limit,
                                resultPage: page,
                                type: "top",
                                cache: !1
                            });
                            break;
                        case "friends":
                            _query = userQuery.friends.get({
                                resultLimit: limit,
                                resultPage: page,
                                cache: !1
                            });
                            break;
                        default:
                            _query = userQuery.single.get(angular.extend({
                                userId: $rootScope.Self.id,
                                category: category,
                                resultLimit: limit,
                                resultPage: page
                            }, options))
                    }
                    return self.navigation.cached_user.length > users.size() ? (start = (page - 2) * self.page_limit, usersFromCache = cached_user.slice(start, start + self.page_limit), users.addAll(usersFromCache), {
                        then: function(callback) {
                            callback.apply()
                        }
                    }) : _query.$promise.then(function(data) {
                        var i, tempUsers = [],
                            response = data.response.result;
                        if (++page, options.clear && users.removeAll(), _.size(response))
                            if (-1 !== $.inArray(category, advancedLists)) {
                                if (tempUsers = response, "blocks" === category)
                                    for (i = tempUsers.length - 1; i >= 0; i--) tempUsers[i].user = tempUsers[i].userBlocked
                            } else "user" in response[0] ? $.each(response, function(key, obj) {
                                "undefined" != typeof obj.time && (obj.user.actionTime = obj.time), tempUsers.push(obj.user)
                            }) : tempUsers = response;
                        else noResults = !0;
                        options.inCache ? self.navigation.cached_user.push.apply(self.navigation.cached_user, tempUsers) : users.addAll(tempUsers, {
                            prepend: !1
                        })
                    })
                }
            }

            function UserReportService(UserQueryFctry) {
                var types = {};
                this.getTypes = function() {
                    return types
                }, this.loadTypes = function(params) {
                    params = params || {}, UserQueryFctry.report.get(params).$promise.then(function(data) {
                        types = data.response.types
                    })
                }, this.submitReportUserForm = function(params) {
                    return UserQueryFctry.single.post(params).$promise
                }
            }

            function LoadUserService(UserQueryFctry) {
                var self = this,
                    userData = {};
                this.isLoading = !1, this.clear = function() {
                    userData = {}
                }, this.get = function() {
                    return userData
                }, this.load = function(user, useBatches) {
                    var queries = {},
                        defaultQueries = {
                            details: {
                                path: "/users/:userId/details",
                                params: {
                                    userId: user.id
                                }
                            },
                            gallery: {
                                path: "users/:userId/pictures",
                                params: {
                                    userId: user.id
                                }
                            },
                            visit: {
                                path: "users/:userId/visits",
                                params: {
                                    userId: user.id
                                },
                                method: "POST"
                            }
                        };
                    self.isLoading = !0, $.each(defaultQueries, function(key, batch) {
                        -1 !== $.inArray(key, useBatches) && (queries[key] = defaultQueries[key])
                    }), UserQueryFctry.info.get(queries).$promise.then(function(data) {
                        var query, response = data[0],
                            i = 0;
                        for (query in queries) "visit" !== query && response[i].response && (userData[query] = response[i].response.result), ++i;
                        self.isLoading = !1
                    })
                }
            }
            angular.module("Lovoo.User").factory("UserQueryFctry", UserQueryFctry).factory("UserActionsFctry", UserActionsFctry).service("UserRequestService", UserRequestService).service("ImageService", ImageService).service("UserListService", UserListService).service("UserReportService", UserReportService).service("LoadUserService", LoadUserService), UserQueryFctry.$inject = ["$resource", "$batchRequest"], UserActionsFctry.$inject = ["$location", "$rootScope", "$window", "DialogService", "StorageService", "ToasterService", "UserQueryFctry"], UserRequestService.$inject = ["UserRequestFctry", "UserProto"], ImageService.$inject = ["$rootScope"], UserListService.$inject = ["$rootScope", "$collection", "$timeout", "UserQueryFctry"], UserReportService.$inject = ["UserQueryFctry"], LoadUserService.$inject = ["UserQueryFctry"]
        }()
});