/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    function settingsPageCtrl($injector, $location, $routeParams, $scope, SettingsQuery, SettingsService) {
        $scope.sections = SettingsService.menuSections, $scope.page = $routeParams.page || $scope.sections[0].url, $scope.subpage = $routeParams.subpage, $scope.action = function(index, sectionUrl, subUrl) {
            $scope.sections[index].redirect ? window.open(sectionUrl, "_blank") : void 0 === subUrl ? $location.path(Ajax.url + "/settings/" + sectionUrl) : "delete" == subUrl ? $scope.$root.Self.pwf ? SettingsService.showPreDeleteConfirmMail() : SettingsService.showPreDeleteConfirm(function() {
                $location.path(Ajax.url + "/settings/" + sectionUrl + "/" + subUrl)
            }) : "suspend" == subUrl ? SettingsService.showPreSuspendConfirm() : "mails" == subUrl ? (mailAction = $scope.$root.mailSettingsAction, SettingsService.updateMailSettingsConfirm(function() {
                SettingsQuery.mailSettings.update({
                    action: mailAction
                }).$promise.then(function() {
                    $injector.get("ToasterService").open("success", Translator.get("mail_settings_" + mailAction), Translator.get("changes_saved")), mailAction = "subscribe" == mailAction ? "unsubscribe" : "subscribe", $scope.$root.mailSettingsAction = mailAction, angular.extend($scope.sections[0].sub[1], {
                        title: "mail_settings_" + mailAction
                    })
                })
            })) : $location.path(Ajax.url + "/settings/" + sectionUrl + "/" + subUrl)
        }
    }

    function settingsChatBlockerCtrl($scope, SettingsService, ToasterService) {
        var chatFilterSettingsOld = null;
        $scope.$parent.SettingsPage = "chat_blocker", $scope.btnDisabled = !0, $scope.filteredMsg = 0, SettingsService.getChatFilterSettings(function(res) {
            return (chatFilterSettingsOld = res.filter) ? ($scope.filter = {}, _.each(chatFilterSettingsOld, function(filter, key) {
                chatFilterSettingsOld[key] = !!filter, $scope.filter[key] = !!filter
            }), $scope.btnDisabled = !1, void($scope.filteredMsg = res.filteredMsg)) : $scope.nothingFound = !0
        }), $scope.saveFilterSettings = function() {
            var sthChanged = !1;
            return _.size($scope.filter) > 0 && (_.each($scope.filter, function(filter, key) {
                chatFilterSettingsOld[key] != filter && (sthChanged = !0)
            }), !sthChanged) ? SettingsService.oldEqualsNew() : ($scope.btnDisabled = !0, SettingsService.saveChatFilterSettings($scope.filter, function() {
                $scope.btnDisabled = !1, _.each($scope.filter, function(value, key) {
                    chatFilterSettingsOld[key] = value
                }), ToasterService.open("success", Translator.get("chat_blocker"), Translator.get("changes_saved"))
            }), !1)
        }, $scope.handleClickInformation = function(filterKey) {
            if ($scope.filter[filterKey]) switch (filterKey) {
                case "isVip":
                    ToasterService.open("info", Translator.get("chat_blocker") + " - " + Translator.get("vip_settings"), Translator.get("vips_allowed"));
                    break;
                case "searchSettings":
                    ToasterService.open("info", Translator.get("chat_blocker") + " - " + Translator.get("vip_settings"), Translator.get("vips_allowed"))
            }
        }
    }

    function settingsLinkedAccountsCtrl($scope, FacebookService, GooglePlusService) {
        $scope.$parent.SettingsPage = "linked_accounts", $scope.facebookConnect = FacebookService.connectAccountToLovoo, $scope.facebookDisconnect = FacebookService.disconnectAccountFromLovoo, $scope.googlePlus = GooglePlusService.inBetweenConnection
    }

    function settingsAccountMainCtrl($scope) {
        $scope.$parent.SettingsPage = "my_account", $scope.edit = !1, $scope.formFields = [{
            key: "name",
            label: Translator.get("Name")
        }, {
            key: "birthday",
            label: Translator.get("Birthday")
        }, {
            key: "city",
            label: Translator.get("City")
        }, {
            key: "languages",
            label: Translator.get("Languages")
        }, {
            key: "email",
            label: Translator.get("Email")
        }, {
            key: "password",
            label: Translator.get("Password"),
            condition: 2 == $scope.$root.Self.pwf
        }, {
            key: "user_id",
            label: Translator.get("your_user_id")
        }]
    }

    function settingsChangePasswordCtrl($scope, SettingsService, ToasterService) {
        var resetPasswordForm = function() {
            $scope.btnDisabled = !1, $scope.passwordOld = "", $scope.password = "", $scope.repeatPassword = "", $scope.displayError = {}
        };
        resetPasswordForm(), $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, $scope.savePassword = function() {
            var changePasswordForm = $scope.changePasswordForm;
            return 2 != $scope.$root.Self.pwf && changePasswordForm.passwordOld.$viewValue == changePasswordForm.password.$viewValue ? void SettingsService.oldEqualsNew() : (passwordUpdate = {
                password_old: changePasswordForm.passwordOld.$viewValue ? changePasswordForm.passwordOld.$viewValue : "",
                password_new: {
                    first: changePasswordForm.password.$viewValue,
                    second: changePasswordForm.password.$viewValue
                }
            }, $scope.btnDisabled = !0, void SettingsService.saveAccountChanges("pw", passwordUpdate, function() {
                resetPasswordForm(), ToasterService.open("success", Translator.get("change_password"), Translator.get("changes_saved"))
            }, function() {
                resetPasswordForm(), $scope.changePasswordForm.$setPristine()
            }))
        }
    }

    function settingsChangeOriginCtrl($scope, SettingsService, ToasterService) {
        var newHomeLocation = {};
        $scope.btnDisabled = !1, $scope.displayError = {}, $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, $scope.setLocation = function(location) {
            location && (newHomeLocation = {
                latitude: location.coordinates.latitude,
                longitude: location.coordinates.longitude,
                city: location.city,
                country: location.country
            })
        }, $scope.languages = [], _.each($scope.$root.Self.languages, function(lang) {
            null !== lang && $scope.languages.push(lang)
        }), $scope.toggleLanguageSelection = function(lang) {
            var i = $scope.languages.indexOf(lang); - 1 != i ? $scope.languages.splice(i, 1) : $scope.languages.push(lang)
        }, $scope.save = function() {
            var updateData = {};
            return angular.equals($scope.languages, $scope.$root.Self.languages) || angular.extend(updateData, {
                languages: $scope.languages
            }), newHomeLocation && angular.extend(updateData, newHomeLocation), _.size(updateData) <= 0 ? void SettingsService.oldEqualsNew() : ($scope.btnDisabled = !0, void SettingsService.saveAccountChanges("", updateData, function() {
                newHomeLocation = {}, $scope.btnDisabled = !1, ToasterService.open("success", Translator.get("change_origin"), Translator.get("changes_saved"))
            }, function() {
                $scope.btnDisabled = !1, $scope.changeOriginForm.$setPristine()
            }))
        }
    }

    function settingsChangeBirthdayCtrl($filter, $scope, SettingsService, ToasterService) {
        $scope.btnDisabled = !1, $scope.displayError = {}, $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, $scope.dateSelectGroup = DateSelectGroup;
        var splitDate = $scope.$root.Self.birthday.split("."),
            dateObj = new Date(splitDate[2], splitDate[1] - 1, splitDate[0]);
        $scope.birthday = {
            day: dateObj.getDate(),
            month: dateObj.getMonth(),
            year: dateObj.getFullYear()
        }, $scope.translateBirthdayOptions = function(key, val) {
            if ("day" == key) return $filter("leadingZero")(val, 2);
            if ("month" == key) {
                var helperval = val + 1;
                return Translator.get("month_" + helperval)
            }
            return val
        }, $scope.save = function() {
            $scope.btnDisabled = !0;
            var birthday = {
                    month: $scope.birthday.month,
                    day: $scope.birthday.day,
                    year: $scope.birthday.year
                },
                birthdate = new Date(birthday.year, birthday.month, birthday.day + 1);
            return dateObj.getDate() == birthday.day && dateObj.getMonth() == birthday.month && dateObj.getFullYear() == birthday.year ? ($scope.btnDisabled = !1, void SettingsService.oldEqualsNew()) : void SettingsService.saveAccountChanges("changebirthday", {
                birthday: birthdate
            }, function() {
                $scope.btnDisabled = !1, ToasterService.open("success", Translator.get("change_birthday"), Translator.get("changes_saved"))
            }, function() {
                $scope.btnDisabled = !1, $scope.changeBirthdayForm.$setPristine()
            })
        }
    }

    function settingsChangeGeneralCtrl($scope, SettingsService, ToasterService) {
        $scope.name = $scope.$root.Self.name, $scope.email = $scope.$root.Self.email, $scope.displayError = {}, $scope.btnDisabled = {
            email: !1,
            name: !1
        }, $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, $scope.saveName = function() {
            return $scope.$root.Self.name == $scope.name ? void SettingsService.oldEqualsNew() : ($scope.btnDisabled.name = !0, void SettingsService.saveAccountChanges("changename", {
                name: $scope.name
            }, function() {
                $scope.btnDisabled.name = !1, ToasterService.open("success", Translator.get("change_name"), Translator.get("changes_saved"))
            }, function() {
                $scope.btnDisabled.name = !1, $scope.changeNameForm.$setPristine()
            }))
        }, $scope.saveEmail = function() {
            return $scope.$root.Self.email == $scope.email ? void SettingsService.oldEqualsNew() : ($scope.btnDisabled.email = !0, void SettingsService.saveAccountChanges("changemail", {
                email: $scope.email
            }, function() {
                $scope.btnDisabled.email = !1, ToasterService.open("success", Translator.get("change_email"), Translator.get("changes_saved"))
            }, function() {
                $scope.btnDisabled.email = !1, $scope.changeEmailForm.$setPristine()
            }))
        }
    }

    function settingsSearchFilterCtrl($scope, SettingsService, SearchFormService, ToasterService) {
        $scope.btnDisabled = !1, $scope.ageFrom = SearchSettings.ageFrom, $scope.ageTo = Math.min(100, SearchSettings.ageTo), $scope.gender = 0 == SearchSettings.gender ? 3 : SearchSettings.gender, $scope.save = function() {
            var sthChanged = !1,
                data = {
                    ageFrom: $scope.ageFrom,
                    ageTo: $scope.ageTo
                },
                mappedSearchFilterGenders = SettingsService.mapSearchFilterGenders(parseInt($scope.gender));
            return angular.extend(data, mappedSearchFilterGenders), _.each(data, function(setting, key) {
                SearchSettings[key] != setting && (sthChanged = !0)
            }), sthChanged ? ($scope.btnDisabled = !0, void SettingsService.saveSearchSettings(data, function() {
                $scope.btnDisabled = !1, angular.extend(SearchSettings, data), SearchFormService.removeSearchParams(), $scope.$root.Self.genderLooking = data.gender, $scope.$root.Self.searchSettings = SearchSettings, ToasterService.open("success", Translator.get("searchSettings"), Translator.get("changes_saved"))
            })) : void SettingsService.oldEqualsNew()
        }, $scope.checkButton = function(value, field) {
            $scope[field] = value
        }, $scope.addYearsToString = function(value) {
            return void 0 === value ? "" : (value = value.toString(), Translator.get("dynamic.age_years", {
                years: value
            }, value))
        }
    }

    function settingsInterestsCtrl($scope, SettingsService, ToasterService) {
        $scope.btnDisabled = !1, $scope.intentionsList = [], $scope.interests = {}, _.each($scope.$root.Intentions, function(value) {
            $scope.intentionsList.push(value)
        }), _.each($scope.$root.Self.flirtInterests, function(val) {
            $scope.interests[val] = !0
        }), $scope.save = function() {
            var newInterests = [];
            if (_.each($scope.interests, function(val, key) {
                    val && newInterests.push(key)
                }), _.size(newInterests) == _.size($scope.$root.Self.flirtInterests)) {
                var sthChanged = !1;
                if (_.each(newInterests, function(val) {
                        sthChanged || -1 != $scope.$root.Self.flirtInterests.indexOf(val) || (sthChanged = !0)
                    }), !sthChanged) return void SettingsService.oldEqualsNew()
            }
            return _.size(newInterests) <= 0 ? void ToasterService.open("error", Translator.get("searchSettings"), Translator.get("validate.least_one")) : ($scope.btnDisabled = !0, void SettingsService.saveFlirtInterests(newInterests, function() {
                $scope.btnDisabled = !1, $scope.$root.Self.flirtInterests = newInterests, ToasterService.open("success", Translator.get("searchSettings"), Translator.get("changes_saved"))
            }))
        }
    }

    function settingsCommunityMainCtrl($scope) {
        $scope.$parent.SettingsPage = "community_settings"
    }

    function settingsAccountDeleteCtrl($scope, SettingsService) {
        $scope.$parent.SettingsPage = "delete_account", $scope.btnDisabled = !1, $scope.displayError = {}, $scope.delReasons = {}, $scope.delText = "", $scope.delPassword = "", $scope.showConfirmationDialog2 = function() {
            var delReasons = [];
            return $.each($scope.delReasons, function(key, value) {
                delReasons.push(key)
            }), SettingsService.showFinalDeleteConfirm({
                delReasons: delReasons,
                delText: $scope.delText,
                password: $scope.delPassword
            }, function() {
                $scope.delPassword = ""
            }), !1
        }, $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }
    }

    function settingsNotificationsCtrl(PUSH_OPTIONS, STORAGE_KEY_PUSH_SETTINGS, $scope, DialogTutorialService, DesktopNotificationService, SettingsService, StorageService, ToasterService) {
        var oldPushSettings = {},
            storageUserId = $scope.$root.Self ? $scope.$root.Self.id : "guest",
            localStrgPushSettings = StorageService.get(STORAGE_KEY_PUSH_SETTINGS + "." + storageUserId);
        $scope.$parent.SettingsPage = "settings_notifications", $scope.isGranted = DesktopNotificationService.isGranted, $scope.isDenied = DesktopNotificationService.isDenied, $scope.setDesktopNotifications = function() {
            $scope.isGranted || $scope.isDenied ? $scope.isGranted ? ToasterService.open("success", Translator.get("desktop_notifications_enabled_info"), Translator.get("browser_reset_info")) : $scope.isDenied && ToasterService.open("error", Translator.get("desktop_notifications_disabled_info"), Translator.get("browser_reset_info")) : DesktopNotificationService.requestPermission(function(status) {
                return "default" !== status ? window.location.href = window.location.pathname : void 0
            })
        }, $scope.pushSettings = {}, $scope.btnDisabled = !1, $scope.pushOptions = PUSH_OPTIONS, localStrgPushSettings ? $scope.pushSettings = localStrgPushSettings : (_.each($scope.pushOptions, function(item, key) {
            $scope.pushSettings[key] = item.enabled
        }), StorageService.set(STORAGE_KEY_PUSH_SETTINGS + "." + storageUserId, $scope.pushSettings)), _.each($scope.pushSettings, function(val, key) {
            oldPushSettings[key] = val
        }), $scope.save = function() {
            var sthChanged = !1;
            return _.each($scope.pushSettings, function(val, key) {
                oldPushSettings[key] != val && (sthChanged = !0)
            }), sthChanged ? ($scope.btnDisabled = !0, StorageService.set(STORAGE_KEY_PUSH_SETTINGS + "." + storageUserId, $scope.pushSettings), _.each($scope.pushSettings, function(val, key) {
                oldPushSettings[key] = val
            }), $scope.btnDisabled = !1, void ToasterService.open("success", Translator.get("desktop_notification_enable"), Translator.get("changes_saved"))) : void SettingsService.oldEqualsNew()
        }, DialogTutorialService.loadTutorialIfEnabled("tutorial_desktop_notifications")
    }

    function languagesFilter($rootScope) {
        return function(val) {
            for (var out = [], i = val.length - 1; i >= 0; i--) out.push($rootScope.Languages[val[i]]);
            return out.join(", ")
        }
    }
    angular.module("Lovoo.Settings", ["ngResource", "GooglePlus", "TemplateLoader"]).constant("STORAGE_KEY_PUSH_SETTINGS", "push_settings").constant("PUSH_OPTIONS", {
            h: {
                label: "Fans",
                enabled: !0
            },
            k: {
                label: "Kisses",
                enabled: !0
            },
            m: {
                label: "Chats",
                enabled: !0
            },
            mh: {
                label: "Matches",
                enabled: !0
            },
            mr: {
                label: "chat_requests",
                enabled: !0
            },
            mv: {
                label: "match_votes",
                enabled: !0
            },
            v: {
                label: "Visits",
                enabled: !0
            }
        }).controller("SettingsPageCtrl", settingsPageCtrl).controller("SettingsChatBlockerCtrl", settingsChatBlockerCtrl).controller("SettingsLinkedAccountsCtrl", settingsLinkedAccountsCtrl).controller("SettingsAccountMainCtrl", settingsAccountMainCtrl).controller("SettingsChangePasswordCtrl", settingsChangePasswordCtrl).controller("SettingsChangeOriginCtrl", settingsChangeOriginCtrl).controller("SettingsChangeBirthdayCtrl", settingsChangeBirthdayCtrl).controller("SettingsChangeGeneralCtrl", settingsChangeGeneralCtrl).controller("SettingsSearchFilterCtrl", settingsSearchFilterCtrl).controller("SettingsInterestsCtrl", settingsInterestsCtrl).controller("SettingsCommunityMainCtrl", settingsCommunityMainCtrl).controller("SettingsAccountDeleteCtrl", settingsAccountDeleteCtrl).controller("SettingsNotificationsCtrl", settingsNotificationsCtrl), settingsPageCtrl.$inject = ["$injector", "$location", "$routeParams", "$scope", "SettingsQuery", "SettingsService"], settingsChatBlockerCtrl.$inject = ["$scope", "SettingsService", "ToasterService"], settingsLinkedAccountsCtrl.$inject = ["$scope", "FacebookService", "GooglePlusService"], settingsAccountMainCtrl.$inject = ["$scope"], settingsChangePasswordCtrl.$inject = ["$scope", "SettingsService", "ToasterService"], settingsChangeOriginCtrl.$inject = ["$scope", "SettingsService", "ToasterService"], settingsChangeBirthdayCtrl.$injector = ["$filter", "$scope", "SettingsService", "ToasterService"], settingsChangeGeneralCtrl.$inject = ["$scope", "SettingsService", "ToasterService"], settingsSearchFilterCtrl.$inject = ["$scope", "SettingsService", "SearchFormService", "ToasterService"], settingsInterestsCtrl.$inject = ["$scope", "SettingsService", "ToasterService"], settingsCommunityMainCtrl.$inject = ["$scope"], settingsAccountDeleteCtrl.$inject = ["$scope", "SettingsService"], settingsNotificationsCtrl.$inject = ["PUSH_OPTIONS", "STORAGE_KEY_PUSH_SETTINGS", "$scope", "DialogTutorialService", "DesktopNotificationService", "SettingsService", "StorageService", "ToasterService"],
        function() {
            "use strict";

            function SettingsDirective(TemplateService) {
                return {
                    restrict: "E",
                    replace: !0,
                    transclude: !0,
                    link: function(scope, element, attr) {
                        var src, sub = "";
                        void 0 !== attr.subpage && "" !== attr.subpage && (sub = "-" + attr.subpage), src = "page/settings/" + attr.page + sub + ".html", TemplateService.loadTemplate(src, element, scope)
                    }
                }
            }
            angular.module("Lovoo.Settings").directive("settings", SettingsDirective), SettingsDirective.$inject = ["TemplateService"]
        }(), angular.module("Lovoo.Settings").filter("languages", languagesFilter), languagesFilter.$inject = ["$rootScope"],
        function() {
            "use strict";

            function settingsQuery($resource, $batchRequest) {
                var jsonToFormData = function(data) {
                    return void 0 !== data ? $.param(data) : data
                };
                return {
                    searchSettings: $batchRequest({}, {
                        put: {
                            method: "PUT"
                        }
                    }),
                    chatSettings: $resource(Ajax.api + "/self/chatfiltersettings", {}, {
                        save: {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            transformRequest: jsonToFormData
                        }
                    }),
                    mailSettings: $resource(Ajax.api + "/self/mailsettings", {}, {
                        update: {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            transformRequest: jsonToFormData
                        }
                    }),
                    accountSuspend: $resource(Ajax.api + "/self/suspend", {}, {
                        put: {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            transformRequest: jsonToFormData
                        }
                    }),
                    accountDelete: $resource(Ajax.api + "/self/delete", {}, {
                        post: {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            transformRequest: jsonToFormData
                        }
                    }),
                    self: $resource(Ajax.api + "/self", {}, {
                        put: {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            transformRequest: jsonToFormData
                        },
                        change: {
                            url: Ajax.api + "/self/:type",
                            method: "PUT",
                            params: {
                                type: "@type"
                            },
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            transformRequest: jsonToFormData
                        },
                        confirmmail: {
                            url: Ajax.api + "/self/confirmmail",
                            method: "POST"
                        }
                    })
                }
            }

            function settingsService($rootScope, AuthService, DesktopNotificationService, DialogService, SettingsQuery, ToasterService, UpdateSelfService, userQuery) {
                this.menuSections = [{
                    title: "my_account",
                    icon: "lo lo-user-circle",
                    url: "account",
                    sub: [{
                        title: "linked_accounts",
                        url: "linked",
                        condition: 2 === $rootScope.Self.pwf
                    }, {
                        title: "mail_settings_" + $rootScope.mailSettingsAction,
                        url: "mails"
                    }, {
                        title: "suspend_account",
                        url: "suspend"
                    }, {
                        title: "delete_account",
                        url: "delete"
                    }]
                }, {
                    title: "community_settings",
                    icon: "lo lo-users",
                    url: "community",
                    sub: [{
                        title: "chat_blocker",
                        url: "chat-blocker"
                    }, {
                        title: "blocked_members",
                        url: "blocked"
                    }]
                }, {
                    title: "settings_notifications",
                    icon: "lo lo-loudspeaker",
                    url: "notifications",
                    condition: !DesktopNotificationService.isSupportedByBrowser
                }, {
                    title: "Private",
                    icon: "fa-lock",
                    url: "private",
                    condition: !$rootScope.Self.vip
                }, {
                    redirect: !0,
                    title: "support_n_help",
                    icon: "lo lo-file-text-question-o",
                    url: $rootScope.getHelpcenterLink()
                }, {
                    title: "About us",
                    icon: "fa-info-circle",
                    url: "about"
                }], this.oldEqualsNew = function() {
                    ToasterService.open("error", Translator.get("action_interrupted"), Translator.get("old_equals_new"))
                }, this.getChatFilterSettings = function(callback) {
                    SettingsQuery.chatSettings.get().$promise.then(function(res) {
                        callback(res.response)
                    })
                }, this.saveChatFilterSettings = function(data, callback) {
                    var sendData = {};
                    _.each(data, function(value, key) {
                        sendData[key] = value ? 1 : 0
                    }), SettingsQuery.chatSettings.save(sendData).$promise.then(function(res) {
                        callback(res.result)
                    })
                }, this.updateMailSettingsConfirm = function(callback) {
                    DialogService.openDialog({
                        backdrop: !0
                    }, {
                        title: Translator.get("mail_settings_" + $rootScope.mailSettingsAction),
                        content: Translator.get("mail_settings_text_" + $rootScope.mailSettingsAction),
                        callback: callback
                    })
                }, this.showPreSuspendConfirm = function() {
                    DialogService.openDialog({
                        backdrop: !0
                    }, {
                        title: Translator.get("suspend_account"),
                        content: Translator.get("txt.suspend_account"),
                        closeLabel: Translator.get("cancel"),
                        callback: function() {
                            SettingsQuery.accountSuspend.put({}, function() {
                                $rootScope.Self = null, AuthService.doLogout()
                            })
                        }
                    })
                }, this.showPreDeleteConfirm = function(callback) {
                    DialogService.openDialog({
                        backdrop: !0
                    }, {
                        title: Translator.get("delete_account"),
                        content: Translator.get("txt.delete_account"),
                        closeLabel: Translator.get("cancel"),
                        callback: callback
                    })
                }, this.showPreDeleteConfirmMail = function() {
                    DialogService.openDialog({
                        backdrop: !0
                    }, {
                        title: Translator.get("delete_account"),
                        content: Translator.get("mail_delete_text"),
                        callbackLabel: Translator.get("mail_delete_ok"),
                        closeLabel: Translator.get("prefer_continuing"),
                        callback: function() {
                            SettingsQuery.accountDelete.post({}, function() {
                                ToasterService.open("success", Translator.get("delete_account"), Translator.get("mail_sent"))
                            })
                        }
                    })
                }, this.showFinalDeleteConfirm = function(formData, errorCallback) {
                    DialogService.openDialog({
                        backdrop: !0
                    }, {
                        title: Translator.get("delete_account"),
                        content: Translator.get("txt.delete_account_confirm"),
                        callback: function() {
                            SettingsQuery.accountDelete.post(formData, function() {
                                $rootScope.Self = null, AuthService.doLogout()
                            }, errorCallback)
                        }
                    })
                }, this.saveFlirtInterests = function(interests, callback) {
                    SettingsQuery.self.put({
                        interests: interests
                    }).$promise.then(function(res) {
                        callback(res.result)
                    })
                }, this.saveAccountChanges = function(type, data, successCallback, errorCallback) {
                    return userQuery.self.put({
                        type: type
                    }, data).$promise.then(function(res) {
                        UpdateSelfService.requestAndUpdateSelfUser(), "function" == typeof successCallback && successCallback(res)
                    }, function(err) {
                        "function" == typeof errorCallback && errorCallback(err)
                    })
                }, this.sendConfirmMail = function(callback) {
                    return SettingsQuery.self.confirmmail({}, function(res) {
                        "function" == typeof callback && callback(res), ToasterService.open("info", $rootScope.Translator.get("confirmmail"), $rootScope.Translator.get("send_confirmation_success", {
                            email: $rootScope.Self.email
                        }))
                    })
                }, this.saveSearchSettings = function(searchSettings, callback) {
                    var queries = {
                        self: {
                            path: "/self",
                            params: {
                                genderLooking: 0 === searchSettings.gender ? 3 : searchSettings.gender
                            }
                        },
                        searchsettings: {
                            path: "self/searchsettings",
                            params: searchSettings
                        }
                    };
                    SettingsQuery.searchSettings.put(queries).$promise.then(function(res) {
                        "function" == typeof callback && callback(res)
                    })
                }, this.mapSearchFilterGenders = function(searchFilterGender) {
                    var user = $rootScope.Self,
                        mappedSearchFilterGenders = {
                            gender: user.genderLooking,
                            genderLooking: user.gender
                        };
                    switch (user.gender) {
                        case 1:
                            mappedSearchFilterGenders = 1 === searchFilterGender ? {
                                gender: 1,
                                genderLooking: 1
                            } : 2 === searchFilterGender ? {
                                gender: 2,
                                genderLooking: 1
                            } : {
                                gender: 0,
                                genderLooking: 1
                            };
                            break;
                        default:
                            mappedSearchFilterGenders = 1 === searchFilterGender ? {
                                gender: 1,
                                genderLooking: 2
                            } : 2 === searchFilterGender ? {
                                gender: 2,
                                genderLooking: 2
                            } : {
                                gender: 0,
                                genderLooking: 2
                            }
                    }
                    return mappedSearchFilterGenders
                }
            }
            angular.module("Lovoo.Settings").factory("SettingsQuery", settingsQuery).service("SettingsService", settingsService), settingsQuery.$inject = ["$resource", "$batchRequest"], settingsService.$inject = ["$rootScope", "AuthService", "DesktopNotificationService", "DialogService", "SettingsQuery", "ToasterService", "UpdateSelfService", "UserQueryFctry"]
        }()
});