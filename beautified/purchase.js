/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    ! function() {
        "use strict";

        function PurchaseController($scope, $sce, PurchaseService, PurchasePaymentService, PAYPAL_DATA) {
            $scope.isCollapsed = !0, $scope["package"] = $scope.$parent["package"], $scope.paypalFormScope = [], $scope.timestamp = (new Date).getTime(), $scope.purchaseTarget = PurchaseService.getPurchaseTarget(), $scope.purchaseStart = function(method) {
                dataLayer.push({
                    event: "sendEvent",
                    category: "Start Payment",
                    label: method.key
                })
            }, $scope.platform = "web", $scope.$watch("packageType", function(packageType) {
                1 === Number(packageType) ? $scope.actionUrl = $scope.$root.Ajax.url + "/paypal/ec" : 2 === Number(packageType) && ($scope.actionUrl = $sce.trustAsResourceUrl("https://www.paypal.com/cgi-bin/webscr"), _.each(PAYPAL_DATA, function(value, key) {
                    var inputVar = {
                        name: key
                    };
                    "item_name" === key ? angular.extend(inputVar, {
                        value: value + " - " + $scope["package"].idTitle + " Package"
                    }) : angular.extend(inputVar, {
                        value: value + ("return" === key ? "/credits?status=Success" : "")
                    }), $scope.paypalFormScope.push(inputVar)
                }))
            }), PurchasePaymentService.load($scope["package"])
        }
        angular.module("Lovoo.Purchase", []).constant("PAYPAL_DATA", {
            cmd: "_xclick",
            business: "pay@lovoo.net",
            form_url: "https://www.paypal.com/cgi-bin/webscr",
            item_name: "Lovoo",
            no_shipping: "1",
            "return": "https://www.lovoo.com",
            cancel_return: "https://www.lovoo.com",
            no_note: "1",
            lc: "DE",
            bn: "PP-BuyNowBF",
            notify_url: "https://www.lovoo.com/paypal/ipn"
        }).controller("PurchaseCtrl", PurchaseController), PurchaseController.$inject = ["$scope", "$sce", "PurchaseService", "PurchasePaymentService", "PAYPAL_DATA"]
    }(),
    function() {
        "use strict";

        function PurchaseDirective(methods, PAYPAL_DATA) {
            return {
                restrict: "E",
                replace: !0,
                controller: "PurchaseCtrl",
                scope: {
                    packageType: "@"
                },
                templateUrl: "/template/widget/purchase.html",
                compile: function() {
                    var doc = document.documentElement;
                    return doc.setAttribute("data-useragent", navigator.userAgent),
                        function(scope) {
                            var locatedPayments = [],
                                hiddenFields = [];
                            _.each(PAYPAL_DATA, function(value, name) {
                                if ("item_name" === name) {
                                    var itemName = value + " - " + scope["package"].idTitle + " Package";
                                    hiddenFields.push({
                                        type: "hidden",
                                        name: name,
                                        val: itemName
                                    })
                                } else hiddenFields.push({
                                    type: "hidden",
                                    name: name,
                                    val: value + ("return" === name ? "/credits?status=Success" : "")
                                })
                            }), _.each(methods, function(method) {
                                -1 === $.inArray(PAYPAL_DATA.lc.toLowerCase(), method.loc) && method.loc || locatedPayments.push(method)
                            }), scope.methods = locatedPayments, scope.paypalForm = angular.extend(PAYPAL_DATA, {
                                hiddenFields: hiddenFields
                            })
                        }
                }
            }
        }

        function PaymentFrameDirective(PurchasePaymentService) {
            var size = {
                height: "385",
                width: "100%"
            };
            return {
                restrict: "E",
                template: '<iframe src="" height="" width="" style="border: none;"></iframe><loading-bar ng-if="iframeLoad"></loading-bar>',
                link: function(scope, ele) {
                    var iframe = ele.find("iframe");
                    ele.css({
                        display: "block",
                        height: size.height,
                        width: size.width
                    }), scope.iframeLoad = !0, iframe.load(function() {
                        iframe.height(size.height), iframe.width(size.width), scope.iframeLoad = !1, scope.$digest()
                    }), scope.$watch(function() {
                        return PurchasePaymentService.get()
                    }, function(urlParams) {
                        urlParams && iframe.attr("src", "https://wallapi.com/api/subscription/?" + $.param(urlParams))
                    })
                }
            }
        }
        angular.module("Lovoo.Purchase").directive("purchase", PurchaseDirective).directive("paymentFrame", PaymentFrameDirective), PurchaseDirective.$inject = ["PurchaseMethods", "PAYPAL_DATA"], PaymentFrameDirective.$inject = ["PurchasePaymentService"]
    }(),
    function() {
        "use strict";

        function PurchaseQuery($resource) {
            return $resource(Ajax.api + "/purchase/paymentwall", {
                cache: !0
            }, {
                get: {
                    method: "GET"
                }
            })
        }

        function PurchaseService($cookieStore) {
            var postfix = "_purchase";
            this.setPackage = function(type, pckg) {
                $cookieStore.put(type + postfix, pckg)
            }, this.getPackage = function(type) {
                return $cookieStore.get(type + postfix) || null
            }, this.getPurchaseTarget = function() {
                return window.location !== window.parent.location ? "_parent" : "_self"
            }
        }

        function PurchasePaymentService(PurchaseQuery) {
            var paymentFrame;
            this.get = function() {
                return paymentFrame
            }, this.load = function(pkg) {
                var query = {
                    id: pkg.packageId,
                    ag_name: pkg.idTitle
                };
                PurchaseQuery.get(query).$promise.then(function(data) {
                    paymentFrame = data.response
                })
            }
        }
        angular.module("Lovoo.Purchase").factory("PurchaseQuery", PurchaseQuery).service("PurchaseService", PurchaseService).service("PurchasePaymentService", PurchasePaymentService), PurchaseQuery.$inject = ["$resource"], PurchaseService.$inject = ["$cookieStore"], PurchasePaymentService.$inject = ["PurchaseQuery"]
    }()
});