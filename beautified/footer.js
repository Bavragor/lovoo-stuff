/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    ! function() {
        "use strict";

        function footerCtrl($scope) {
            $scope.locale = $scope.$root.Translator.locale, $scope.currentYear = new Date(1e3 * $scope.$root.lastResponseTime).getFullYear(), "de" === $scope.locale ? ($scope.switchLang = "de", $scope.switchDomain = "net") : ($scope.switchLang = "en", $scope.switchDomain = "com")
        }
        angular.module("Lovoo.Footer", []).controller("FooterCtrl", footerCtrl), footerCtrl.$inject = ["$scope"]
    }(),
    function() {
        "use strict";

        function FooterContainerDirective($rootScope, AnimationFrameService) {
            return {
                restrict: "E",
                templateUrl: "/template/footer/footer.html",
                scope: {},
                controller: "FooterCtrl",
                link: function(scope) {
                    var resizeCallback = function() {
                        scope.style = void 0 !== $rootScope.routing.footer ? $rootScope.routing.footer : "extensive", scope.style = "compact-xs" === scope.style && $rootScope.BS.xs ? "compact" : "extensive"
                    };
                    AnimationFrameService.add(scope, resizeCallback, ["resize"])
                }
            }
        }

        function FooterSidebarDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/footer/footer-sidebar.html",
                controller: "FooterCtrl"
            }
        }
        angular.module("Lovoo.Footer").directive("footerContainer", FooterContainerDirective).directive("footerSidebar", FooterSidebarDirective), FooterContainerDirective.$inject = ["$rootScope", "AnimationFrameService"]
    }()
});