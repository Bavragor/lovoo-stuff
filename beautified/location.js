/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    ! function() {
        "use strict";

        function LocationFieldCtrl($scope, LocationFieldService) {
            function geoSuccess(data) {
                $scope.geoLocationIsLoading = !1, data.length && $scope.setLocation(data[0])
            }

            function geoError() {
                $scope.geoLocationIsLoading = !1
            }
            $scope.geoLocationIsSupported = Boolean("geolocation" in navigator), $scope.geoLocationIsLoading = !1, $scope.requestGeolocation = function() {
                $scope.geoLocationIsLoading = !0, LocationFieldService.getLocation(geoSuccess, geoError)
            }, $scope.setLocation = function(location) {
                $scope.locationTitle = location.title, void 0 !== $scope.presetLocation && ($scope.presetLocation.title = location.title), $scope.callback(location)
            }, $scope.presetLocation && $scope.setLocation($scope.presetLocation), $scope.$watch(function() {
                return void 0 !== $scope.presetLocation ? $scope.presetLocation.title : ""
            }, function(locationTitle) {
                "" !== locationTitle && ($scope.locationTitle = locationTitle)
            })
        }
        angular.module("Lovoo.Form.Location", []).controller("LocationFieldCtrl", LocationFieldCtrl), LocationFieldCtrl.$inject = ["$scope", "LocationFieldService"]
    }(),
    function() {
        "use strict";

        function LocationFieldDirective() {
            return {
                restrict: "E",
                transclude: !1,
                replace: !0,
                controller: "LocationFieldCtrl",
                scope: {
                    callback: "=",
                    presetLocation: "="
                },
                templateUrl: "/template/form/location.html"
            }
        }
        angular.module("Lovoo.Form.Location").directive("locationField", LocationFieldDirective)
    }(),
    function() {
        "use strict";

        function LocationFctry($resource) {
            return {
                requestLocation: $resource(Ajax.url + "/citiesfromgeo", {
                    cache: !0
                }, {
                    get: {
                        method: "GET",
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "public": !0
                        }
                    }
                })
            }
        }

        function LocationFieldService(LocationFctry, ToasterService) {
            var self = this;
            this.geoLocationIsSupported = Boolean("geolocation" in navigator), this.getLocation = function(geoSuccess, geoError) {
                this.geoLocationIsSupported && navigator.geolocation.getCurrentPosition(function(location) {
                    var requestLocationParams = {
                        lat: location.coords.latitude,
                        "long": location.coords.longitude
                    };
                    self.getCityByCoordinates(requestLocationParams).then(function(response) {
                        geoSuccess(response)
                    })
                }, function(reject) {
                    ToasterService.open("error", Translator.get(reject.message)), geoError()
                })
            }, this.getCityByCoordinates = function(location) {
                return LocationFctry.requestLocation.get(location).$promise.then(function(data) {
                    return data.response.result
                })
            }
        }
        angular.module("Lovoo.Form.Location").factory("LocationFctry", LocationFctry).service("LocationFieldService", LocationFieldService), LocationFctry.$inject = ["$resource"], LocationFieldService.$inject = ["LocationFctry", "ToasterService"]
    }()
});