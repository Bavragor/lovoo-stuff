/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    "use strict";

    function flirtmatchCtrl($scope, $routeParams, DialogTutorialService) {
        $scope.links = [{
            page: "vote",
            url: $scope.$root.Ajax.url + "/",
            title: Translator.get("Flirtmatch"),
            icon: "lo lo-cards-users"
        }, {
            page: "match",
            url: $scope.$root.Ajax.url + "/flirtmatch/match",
            title: Translator.get("Matches"),
            icon: "lo lo-cards-users-check"
        }, {
            page: "want-you",
            url: $scope.$root.Ajax.url + "/flirtmatch/want-you",
            title: Translator.get("Want you"),
            icon: "lo lo-cards-question"
        }, {
            page: "you-want",
            url: $scope.$root.Ajax.url + "/flirtmatch/you-want",
            title: Translator.get("You want"),
            icon: "lo lo-cards-check"
        }], $scope.$root.activePage = $routeParams.page || "vote", "match" === $scope.$root.activePage && DialogTutorialService.loadTutorialIfEnabled("tutorial_match")
    }

    function GameController($scope, $timeout, FlirtmatchGameService) {
        function updateUserScope(data) {
            "object" == typeof data ? (angular.extend($scope, data), $scope.gameStatus = 1) : $scope.gameStatus = "number" == typeof data ? data : 2401
        }

        function onScopeDestroy() {
            jBody.unbind("keydown.flirtMatch keyup.flirtMatch")
        }

        function bindKeydownMatch(event) {
            var vote;
            if (!areKeysBlocked && 0 === $(".modal.in").length) {
                switch (event.keyCode) {
                    case 37:
                        vote = 0;
                        break;
                    case 39:
                        vote = 1;
                        break;
                    default:
                        return
                }
                areKeysBlocked = !0, $scope.popAnimation = 1 === vote ? "yes" : "no", $scope.vote($scope.user, vote), $timeout(function() {
                    $scope.popAnimation = !1
                }, 300)
            }
        }

        function bindKeyupMatch() {
            areKeysBlocked = !1
        }
        var jBody = $("body"),
            areKeysBlocked = !1;
        FlirtmatchGameService.resetService(), $scope.gameStatus = 0, $scope.vote = function(id, vote) {
            var updateUser = FlirtmatchGameService.voteUser(id, vote);
            updateUserScope(updateUser)
        }, jBody.unbind("keydown.flirtMatch").bind("keydown.flirtMatch", bindKeydownMatch), jBody.unbind("keyup.flirtMatch").bind("keyup.flirtMatch", bindKeyupMatch), $scope.$on("$destroy", onScopeDestroy), FlirtmatchGameService.render(updateUserScope)
    }

    function MatchHitController($scope, UserActionsFctry) {
        var parent = $scope.$parent;
        $scope.user = parent.user, $scope.action = UserActionsFctry.action
    }

    function matchDirective() {
        return {
            restrict: "E",
            transclude: !1,
            replace: !1,
            controller: "FlirtmatchGameCtrl",
            templateUrl: "/template/flirtmatch.html"
        }
    }

    function matchPageDirective() {
        return {
            restrict: "E",
            transclude: !1,
            replace: !1,
            templateUrl: "/template/page/flirtmatch.html",
            link: function(scope) {
                scope.$root.activePage = "vote"
            }
        }
    }

    function matchProfileDirective(UserActionsFctry) {
        return function(scope, ele) {
            ele.bind("click", function(e) {
                e.target === e.currentTarget && UserActionsFctry.action(scope.user, "profile")
            })
        }
    }

    function matchQueryFctry(userQuery) {
        return {
            preview: function() {
                return userQuery.match.get({
                    preview: 1
                })
            },
            get: function(limit) {
                return userQuery.match.get({
                    limit: limit
                })
            },
            post: function(userId, vote) {
                return userQuery.match.post({
                    userId: userId,
                    vote: vote
                })
            }
        }
    }

    function FlirtmatchGameService(limit, $collection, query, DialogService) {
        function addUsers() {
            var hasUsers = 0 !== users.size();
            if (!(loading || !hasVoted && hasUsers)) {
                loading = !0;
                var _query = hasVoted ? query.get(reloadAmount()) : query.preview();
                _query.$promise.then(function(data) {
                    var result = data.response.result;
                    if (hasVoted)
                        for (var i = 0; i < result.length; ++i)
                            if (result[i].id === previewUserId) {
                                result.splice(i, 1);
                                break
                            }
                    users.addAll(result, hasUsers ? {
                        prepend: !1
                    } : {}), votesRemaining = -1 === votesRemaining ? data.response.votesLeft : votesRemaining, $.isFunction(callback) && callback({
                        user: users.at(0),
                        users: users.all()
                    }), loading = !1
                }, function(rejection) {
                    return votesRemaining = !1, users.removeAll(), loading = !1, $.isFunction(callback) && callback(rejection.data.statusCode), rejection
                })
            }
        }

        function reloadAmount() {
            var amount, hasUsers = 0 !== users.size();
            return limit > votesRemaining - 1 && votesRemaining - 1 > 0 && -1 !== votesRemaining ? amount = votesRemaining - 1 : votesRemaining && (amount = hasUsers ? limit : 2 * limit), amount
        }
        var callback, previewUserId, loading, hasVoted, votesRemaining, users, self = this;
        this.resetService = function() {
            loading = !1, hasVoted = !1, votesRemaining = -1, users = $collection.getInstance({
                idAttribute: "id"
            })
        }, this.resetService(), this.render = function(func) {
            callback = func || callback || {}, votesRemaining && users.size() <= limit + 4 && (0 > votesRemaining || users.size() < votesRemaining) && addUsers()
        }, this.voteUser = function(user, vote) {
            return user ? (hasVoted = !0, DfpSlots.slots["div-gpt-ad-1404396289262-1"].refreshSlot(), 0 !== votesRemaining && users.size() && (votesRemaining--, previewUserId = users.at(0).id, users.remove(users.at(0))), query.post(user.id, vote), user.possibleMatch && 1 === vote && DialogService.openDialog({
                templateUrl: "/template/dialog/match-hit.html",
                controller: "MatchHitCtrl"
            }, {
                user: user
            }), self.render(), users.size() && votesRemaining ? {
                user: users.at(0),
                users: users.all()
            } : {}) : {}
        }
    }
    angular.module("Lovoo.Flirtmatch", ["ngCollection", "ngResource.Batch"]).constant("LIMIT", 6).controller("FlirtmatchCtrl", flirtmatchCtrl).controller("FlirtmatchGameCtrl", GameController).controller("MatchHitCtrl", MatchHitController), flirtmatchCtrl.$inject = ["$scope", "$routeParams", "DialogTutorialService"], GameController.$inject = ["$scope", "$timeout", "FlirtmatchGameService"], MatchHitController.$inject = ["$scope", "UserActionsFctry"], angular.module("Lovoo.Flirtmatch").directive("flirtmatch", matchDirective).directive("flirtmatchPage", matchPageDirective).directive("showProfile", matchProfileDirective), matchProfileDirective.$inject = ["UserActionsFctry"], angular.module("Lovoo.Flirtmatch").factory("fmQueryFctry", matchQueryFctry).service("FlirtmatchGameService", FlirtmatchGameService), matchQueryFctry.$inject = ["UserQueryFctry"], FlirtmatchGameService.$inject = ["LIMIT", "$collection", "fmQueryFctry", "DialogService"]
});