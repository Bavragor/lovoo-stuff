/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "hashtag"], function(angular, hashtag) {
    ! function(SearchSettings) {
        "use strict";

        function ProfileFeedPageCtrl($scope, FeedRequestService, ProfileFeedService, UserRequestService) {
            function initProfileUser(user) {
                $scope.user = user, $scope.user.getConnections({}), $scope.user.getInterests({}), $scope.user.getDetails(), $scope.user.ageRange = {
                    from: SearchSettings.ageFrom,
                    to: SearchSettings.ageTo
                }, addProfileWidgets()
            }

            function addProfileWidgets() {
                $scope.feedOptions.feed && $scope.user && !hasAddedWidgets && ($scope.feedOptions.isHidden = !0, hasAddedWidgets = !0, ProfileFeedService.addProfileDetailsWidget($scope.feedOptions, $scope.user), UserRequestService.getUserPictures($scope.user.id).then(function(data) {
                    ProfileFeedService.addProfilePicturesWidget($scope.feedOptions, $scope.user, data.response.result), $scope.feedOptions.isHidden = !1
                }))
            }
            var profileUserId = ProfileFeedService.getProfileUserId(),
                hasAddedWidgets = !1;
            $scope.$root.activePage = "profile", $scope.feedOptions = FeedRequestService.getFeedOptions(function() {
                FeedRequestService.getFeedByUserId({
                    userId: profileUserId,
                    resultPage: $scope.feedOptions.resultPage
                }).then(function(feed) {
                    $scope.feedOptions.updateFeed(feed), addProfileWidgets()
                })
            }), $scope.$root.Self && $scope.$root.Self.id === profileUserId ? initProfileUser($scope.$root.Self) : UserRequestService.getUserById(profileUserId, function(user) {
                user.visit(), initProfileUser(user)
            })
        }

        function ProfilePageController($scope, ApproutingService, ProfileService, RegisterService, UserQueryFctry) {
            function setUserDataIntoScope() {
                var userData = ProfileService.getUserData();
                void 0 !== userData.user && void 0 !== userData.details && ($scope.user = userData.user, angular.extend($scope.user, {
                    details: userData.details
                }), $scope.isSelf && _.each($scope.user.details, function(entry) {
                    _.each(entry, function(val, key) {
                        $scope.saveData[key] = val.id
                    })
                }), $scope.coverSrc = void 0 !== $scope.user.picture && "" !== $scope.user.picture ? $scope.$root.Ajax.imgUrl + "/users/pictures/" + $scope.user.picture + "/image_l.jpg" : "")
            }
            var profileUserId = ProfileService.getProfileUserId();
            $scope.$root.activePage = "profile", $scope.displayV3Profile = $scope.$root.websiteVersion >= WEBSITE_VERSION.uk || !$scope.$root.isSecurityUser(), $scope.coverSrc = "", $scope.isSelf = ProfileService.isSelfProfile(profileUserId), ProfileService.resetUserData(), $scope.isSelf ? ProfileService.setUser($scope.$root.Self) : (ProfileService.createVisit(profileUserId), ProfileService.requestUser(profileUserId, setUserDataIntoScope)), ProfileService.requestUserDetails(profileUserId, setUserDataIntoScope), $scope.displayV3Profile && ($scope.freeText = null, ProfileService.requestUserFreeText(profileUserId, function(freeText) {
                $scope.freeText = freeText
            }), UserQueryFctry.single.get({
                userId: profileUserId,
                category: "hashtags"
            }, function(data) {
                $scope.hashtags = data.response.result
            })), $scope.isEditMode = !1, $scope.userDetailOpts = ProfileService.getUserDetailOpts(), $scope.saveData = ProfileService.createSaveDataObject(), $scope.showRegisterDialog = RegisterService.showRegisterDialog, $scope.tryApprouting = ApproutingService.openAppOrStoreForMobileOrRegisterForDesktopGuests, $scope.userAction = function(user, userAction, options) {
                $scope.$root.isSecurityUser() && ProfileService.userAction(user, userAction, options)
            }, $scope.toggleEditMode = function(isEditMode) {
                $scope.isEditMode = isEditMode
            }, $scope.saveProfile = function() {
                ProfileService.updateScopeUserDetailLabels($scope.saveData, $scope.user.details, $scope.userDetailOpts), ProfileService.saveProfile($scope.saveData), $scope.isEditMode = !1
            }
        }

        function ProfileEditDetailsCtrl(FREETEXT_MAX_LENGTH, HASHTAG_VALIDATE, $scope, FeedActionService, FeedRequestService, FeedService) {
            var self = this;
            this.trendingHashtags = [], this.freetextMaxLength = FREETEXT_MAX_LENGTH, this.freetext = $scope.user.freetext, this.isLoading = !0, FeedRequestService.getTrendingHashtags({
                success: function(hashtags) {
                    self.isLoading = !1, self.trendingHashtags = hashtags
                }
            }), this.likeHashtag = function(hashtag) {
                var i, validatedHashtag = hashtag.replace(HASHTAG_VALIDATE, "");
                if ($scope.$root.isSecurityUser()) {
                    if (_.contains($scope.$root.Self.interests, validatedHashtag) || FeedActionService.toggleHashtagLike(validatedHashtag), hashtag === this.newHashtag) return void(this.newHashtag = "");
                    for (i = 0; i < self.trendingHashtags.length; ++i) self.trendingHashtags[i] === hashtag && (self.trendingHashtags.splice(i, 1), --i)
                }
            }, this.unlikeHashtag = function(hashtag) {
                FeedActionService.toggleHashtagLike(hashtag), _.contains(self.trendingHashtags, hashtag) || self.trendingHashtags.push(hashtag)
            }
        }

        function ProfileEditInterviewCtrl($scope, ProfileEditService) {
            var bodySizeStepsLength, DEFAULT_BODY_SIZE = 160,
                self = this,
                REGEX_REMOVE_NON_INTEGER = /[^\d.]/g;
            this.detailOptions = ProfileEditService.getUserDetailOpts(), this.gender = $scope.user.gender, this.languages = [], this.transformedLanguages = [], _.each($scope.user.languages, function(lang) {
                self.languages.push(lang)
            }), _.each($scope.$root.Languages, function(value, key) {
                self.transformedLanguages.push({
                    key: key,
                    value: value
                })
            }), this.location = {
                title: $scope.user.location || $scope.user.city || "",
                coordinates: {
                    latitude: $scope.user.coord.latitude,
                    longitude: $scope.user.coord.longitude
                }
            }, this.age = {
                from: parseInt(SearchSettings.ageFrom),
                to: parseInt(SearchSettings.ageTo),
                convertString: function(age) {
                    return void 0 !== age ? Translator.get("dynamic.age_years", {
                        years: age
                    }, age) : ""
                }
            }, bodySizeStepsLength = self.detailOptions.me.size.opts.length, this.size = {
                min: parseInt(self.detailOptions.me.size.opts[1].name.replace(REGEX_REMOVE_NON_INTEGER, "")),
                max: parseInt(self.detailOptions.me.size.opts[bodySizeStepsLength - 1].name.replace(REGEX_REMOVE_NON_INTEGER, "")),
                current: DEFAULT_BODY_SIZE,
                convertString: function(size) {
                    var convertedString;
                    if (void 0 === size) return "";
                    switch (parseInt(size)) {
                        case self.size.min:
                            convertedString = self.detailOptions.me.size.opts[1].name;
                            break;
                        case self.size.max:
                            convertedString = self.detailOptions.me.size.opts[bodySizeStepsLength - 1].name;
                            break;
                        default:
                            convertedString = Translator.get("profile_edit_details.body_size", {
                                size: size
                            })
                    }
                    return convertedString
                }
            }, $scope.user.details.me && (this.size.current = $scope.user.details.me.size ? parseInt($scope.user.details.me.size.label.replace(REGEX_REMOVE_NON_INTEGER, "")) : DEFAULT_BODY_SIZE), this.toggleLanguageSelection = function(language) {
                var i;
                return (i = -1 !== self.languages.indexOf(language)) ? void self.languages.splice(i, 1) : void self.languages.push(language)
            }, this.updateLocation = function(form) {
                var validatedData = ProfileEditService.validateUpdatedData({
                    city: form.city
                });
                _.size(validatedData) && ProfileEditService.updateAccount("", self.location.current).then(function() {
                    $scope.user.location = self.location.current.city, $scope.user.homeLocation.location = self.location.current.city
                })
            }, this.updateLanguages = function() {
                ProfileEditService.updateAccount("", {
                    languages: self.languages
                }).then(function() {
                    $scope.user.languages = self.languages
                })
            }, this.updateSexualityAndAgeRange = function(form) {
                var newAgeRange, newGenderLooking, saveData = {};
                saveData.genderLooking = form.genderLooking, (newGenderLooking = ProfileEditService.validateUpdatedData(saveData)) && $scope.user.genderLooking !== parseInt(newGenderLooking.genderLooking) && (saveData.genderLooking = parseInt(newGenderLooking.genderLooking), saveData = angular.extend(saveData, newAgeRange || {}), ProfileEditService.updateAccount("", saveData, !1).then(function() {
                    $scope.user.genderLooking = newGenderLooking.genderLooking
                })), (newAgeRange = ProfileEditService.validateUpdatedAge(self.age, $scope.$root.AgeRange)) && (saveData = angular.extend({}, newAgeRange, newGenderLooking || {}), ProfileEditService.updateAccount("searchsettings", saveData, !0).then(function() {
                    $scope.user.ageRange = {
                        from: newAgeRange.ageFrom,
                        to: newAgeRange.ageTo
                    }, angular.extend(SearchSettings, newAgeRange)
                }))
            }, this.updateDetails = function(form) {
                var saveData = {};
                switch (form.$name) {
                    case "FormAppearance":
                        saveData.fig = form.figure, saveData.size = form.size, saveData.size.$valid && (saveData.size.$viewValue = ProfileEditService.getSizeIdFromObject(self.size.current, self.detailOptions.me.size.opts));
                        break;
                    case "FormRelationship":
                        saveData.status = form.flirtStatus;
                        break;
                    case "FormChildren":
                        saveData.child = form.children;
                        break;
                    case "FormSmoke":
                        saveData.smoke = form.smoke;
                        break;
                    case "FormReligion":
                        saveData.religion = form.religion;
                        break;
                    case "FormLife":
                        saveData.life = form.life;
                        break;
                    case "FormEducation":
                        saveData.educ = form.education;
                        break;
                    case "FormJob":
                        saveData.job = form.job
                }
                ProfileEditService.updateDetails($scope.user.details, ProfileEditService.validateUpdatedData(saveData))
            }, this.saveLocationToProfile = function(data) {
                self.location.current = {
                    city: data.city || "",
                    country: data.country,
                    latitude: data.coordinates.latitude,
                    longitude: data.coordinates.longitude
                }
            }
        }
        angular.module("Lovoo.Profile", ["Lovoo.User", "Lovoo.Feed"]).constant("FREETEXT_MAX_LENGTH", 250).controller("ProfileFeedPageCtrl", ProfileFeedPageCtrl).controller("ProfilePageCtrl", ProfilePageController).controller("ProfileEditDetailsCtrl", ProfileEditDetailsCtrl).controller("ProfileEditInterviewCtrl", ProfileEditInterviewCtrl), ProfileFeedPageCtrl.$inject = ["$scope", "FeedRequestService", "ProfileFeedService", "UserRequestService"], ProfilePageController.$inject = ["$scope", "ApproutingService", "ProfileService", "RegisterService", "UserQueryFctry"], ProfileEditDetailsCtrl.$inject = ["FREETEXT_MAX_LENGTH", "HASHTAG_VALIDATE", "$scope", "FeedActionService", "FeedRequestService", "FeedService"], ProfileEditInterviewCtrl.$inject = ["$scope", "ProfileEditService"]
    }(window.SearchSettings),
    function(FeedHeaderBg) {
        "use strict";

        function ProfileHeaderDirective() {
            return {
                restrict: "E",
                replace: !0,
                templateUrl: "/template/profile/header-content.html",
                scope: {
                    user: "="
                },
                link: function(scope) {
                    scope.coverSrc = FeedHeaderBg || ""
                }
            }
        }

        function ProfileLocationDirective() {
            return {
                restrict: "E",
                scope: {
                    user: "=",
                    iconClass: "@"
                },
                template: '<span ng-if="user.location || user.city"><i class="fa fa-fw fa-map-marker" ng-class="iconClass"></i> <span ng-if="user.location">{{ user.location }} <span ng-if="$root.isSecurityUser() && user.distance > 0">({{ \'distance\' | translate }}: {{ user.distance }} km)</span></span><span ng-if="!user.location && user.city">{{ user.city }}</span></span>'
            }
        }

        function ProfileEditDetailsDirective() {
            return {
                restrict: "E",
                controller: "ProfileEditDetailsCtrl",
                controllerAs: "editDetails",
                templateUrl: "/template/profile/profile-edit-details.html",
                scope: {
                    user: "="
                }
            }
        }

        function ProfileEditInterviewDirective() {
            return {
                restrict: "E",
                replace: !0,
                scope: {
                    user: "="
                },
                controller: "ProfileEditInterviewCtrl",
                controllerAs: "editInterview",
                templateUrl: "/template/profile/edit-interview.html"
            }
        }
        angular.module("Lovoo.Profile").directive("profileHeader", ProfileHeaderDirective).directive("profileLocation", ProfileLocationDirective).directive("profileEditDetails", ProfileEditDetailsDirective).directive("profileEditInterview", ProfileEditInterviewDirective)
    }(window.FeedHeaderBg),
    function() {
        "use strict";

        function ProfileUserDataStorage() {
            var userData = {};
            return {
                getUserData: function() {
                    return userData
                },
                resetUserData: function() {
                    userData = {}
                },
                setUser: function(user) {
                    userData.user = user
                },
                setUserDetails: function(userDetails) {
                    userData.details = userDetails
                }
            }
        }

        function ProfileFeedService($rootScope, $route, $routeParams, DialogService, FeedPostProto) {
            this.getProfileUserId = function() {
                return $rootScope.uniquePageData.id ? $rootScope.uniquePageData.id : $routeParams.userId ? $routeParams.userId : $rootScope.isSecurityUser() && $route.current.$$route.originalPath === $rootScope.Ajax.url + "/self" ? $rootScope.Self.id : ""
            }, this.addProfileDetailsWidget = function(feedOptions, user) {
                var detailsPost = {
                    _type: "profile-details",
                    owner: user,
                    openEditProfileModal: function() {
                        return DialogService.openDialog({
                            backdrop: !0,
                            size: "",
                            templateUrl: "/template/dialog/edit-profile.html",
                            windowClass: "modal-responsive"
                        }, {
                            user: user
                        })
                    }
                };
                feedOptions.feed.extendFeedPost(new FeedPostProto(detailsPost), !0)
            }, this.addProfilePicturesWidget = function(feedOptions, user, pictures) {
                var i, picturePost = {
                    _type: "profile-picture",
                    owner: user,
                    images: [],
                    pictures: []
                };
                for (i = 0; i < pictures.length; ++i) pictures[i].id === user.picture ? picturePost.images = pictures[i].images : picturePost.pictures.push(pictures[i].images);
                picturePost.images.length || picturePost.images.push({
                    width: 300,
                    height: 300,
                    url: $rootScope.Ajax.imgUrlStatic + "default_user_" + (2 === user.gender ? "female" : "male") + "_300.png"
                }), feedOptions.feed.extendFeedPost(new FeedPostProto(picturePost), !0)
            }
        }

        function ProfileEditService($rootScope, ToasterService, UserQueryFctry) {
            function createSaveDataObject(userDetails, updatedDetails) {
                function updateDetailsObject(entryVal, entryKey) {
                    updatedDetails[entryKey] ? saveData[entryKey] = updatedDetails[entryKey] : userDetailCategoryObject && userDetailCategoryObject[entryKey] && (saveData[entryKey] = userDetailCategoryObject[entryKey].id)
                }
                var userDetailCategoryObject, saveData = {};
                return _.each($rootScope.UserDetails, function(groupVal, groupKey) {
                    userDetailCategoryObject = userDetails[groupKey], _.each(groupVal, updateDetailsObject)
                }), saveData
            }

            function putUpdateUser(type, updateData, showToast) {
                return UserQueryFctry.self.put(angular.extend({
                    type: type
                }, updateData), function() {
                    showToast && ToasterService.open("success", Translator.get("edit_profile"), Translator.get("changes_saved"))
                }).$promise
            }
            var self = this;
            this.updateDetails = function(userDetails, saveData) {
                var updatedDetails;
                _.size(saveData) && (updatedDetails = createSaveDataObject(userDetails, saveData), self.updateScopeUserDetailLabels(updatedDetails, userDetails), putUpdateUser("details", updatedDetails, !0))
            }, this.updateAccount = function(type, updatedData, showToast) {
                return putUpdateUser(type || "", updatedData, showToast)
            }, this.updateScopeUserDetailLabels = function(saveData, scopeUserDetails) {
                var userDetailOpts = self.getUserDetailOpts();
                _.each(userDetailOpts, function(groupVal, groupKey) {
                    _.each(saveData, function(saveVal, saveKey) {
                        void 0 !== groupVal[saveKey] && _.each(groupVal[saveKey].opts, function(optVal) {
                            optVal.id === saveVal && (scopeUserDetails[groupKey] = scopeUserDetails[groupKey] || {}, scopeUserDetails[groupKey][saveKey] = scopeUserDetails[groupKey][saveKey] || {}, scopeUserDetails[groupKey][saveKey].label = optVal.name, scopeUserDetails[groupKey][saveKey].id = optVal.id)
                        })
                    })
                })
            }, this.getUserDetailOpts = function() {
                var userDetailOpts = {};
                return _.each($rootScope.UserDetails, function(groupVal, groupKey) {
                    userDetailOpts[groupKey] = {}, _.each(groupVal, function(entryVal, entryKey) {
                        userDetailOpts[groupKey][entryKey] = {
                            opts: [],
                            title: entryVal.title
                        }, _.each(entryVal.opts, function(optsVal, optsKey) {
                            userDetailOpts[groupKey][entryKey].opts.push({
                                id: optsKey,
                                name: optsVal
                            })
                        })
                    })
                }), userDetailOpts
            }, this.getSizeIdFromObject = function(size, sizeObject) {
                var i, regexp = /[^\d.]/g;
                for (i = 0; i < sizeObject.length; ++i)
                    if (parseInt(size) === parseInt(sizeObject[i].name.replace(regexp, ""))) return sizeObject[i].id
            }, this.validateUpdatedData = function(formDataObject) {
                var isAllValid = !0,
                    validatedData = {};
                return _.each(formDataObject, function(data, key) {
                    return data.$invalid || "" === data.$viewValue ? void(isAllValid = !1) : void(validatedData[key] = data.$viewValue)
                }), isAllValid || _.size(validatedData) || ToasterService.open("error", Translator.get("action_interrupted"), Translator.get("error_try_again")), validatedData
            }, this.validateUpdatedAge = function(ageObject, defaultAgeObject) {
                var allValid = !0,
                    validatedAge = {};
                return ageObject.from <= defaultAgeObject.min && ageObject.to >= defaultAgeObject.max && (allValid = !1, ToasterService.open("error", Translator.get("action_interrupted"), Translator.get("out_of_range"))), allValid && (validatedAge.ageFrom = ageObject.from, validatedAge.ageTo = ageObject.to), validatedAge
            }
        }

        function ProfileService($rootScope, $routeParams, $route, RegisterService, ToasterService, UserActionsFctry, UserQueryFctry, ProfileUserDataStorage) {
            var self = this;
            this.getUserData = ProfileUserDataStorage.getUserData, this.resetUserData = ProfileUserDataStorage.resetUserData, this.setUser = ProfileUserDataStorage.setUser, this.getProfileUserId = function() {
                return $rootScope.uniquePageData.id ? $rootScope.uniquePageData.id : $routeParams.userId ? $routeParams.userId : $rootScope.isSecurityUser() && $route.current.$$route.originalPath === $rootScope.Ajax.url + "/self" ? $rootScope.Self.id : ""
            }, this.requestUserDetails = function(userId, success) {
                var userDetailsQuery;
                userDetailsQuery = $rootScope.isSecurityUser() ? UserQueryFctry.single.get({
                    userId: userId,
                    category: "details"
                }) : UserQueryFctry.single.info({
                    userId: userId
                }), userDetailsQuery.$promise.then(function(result) {
                    var userDetails = result.response.result;
                    $rootScope.isSecurityUser() && userDetails.me instanceof Array && userDetails.flirt instanceof Array && (userDetails = {}), ProfileUserDataStorage.setUserDetails(userDetails), success()
                })
            }, this.requestUser = function(userId, success) {
                UserQueryFctry.single.get({
                    userId: userId
                }).$promise.then(function(result) {
                    ProfileUserDataStorage.setUser(result.response.user), success()
                })
            }, this.requestUserFreeText = function(userId, success) {
                UserQueryFctry.single.get({
                    userId: userId,
                    category: "freetext"
                }, function(data) {
                    var freeText = null;
                    void 0 !== data.response.user.freetext && (freeText = data.response.user.freetext), success(freeText)
                })
            }, this.isSelfProfile = function(userId) {
                return $rootScope.isSecurityUser() ? $rootScope.Self.id === userId : !1
            }, this.userAction = function(user, userAction, options) {
                self.isSelfProfile(user) ? ToasterService.open("error", Translator.get("action_interrupted"), Translator.get("self-dealing")) : $rootScope.isSecurityUser() ? UserActionsFctry.action(user, userAction, options) : RegisterService.showRegisterDialog()
            }, this.getUserDetailOpts = function() {
                var userDetailOpts = {};
                return _.each($rootScope.UserDetails, function(groupVal, groupKey) {
                    userDetailOpts[groupKey] = {}, _.each(groupVal, function(entryVal, entryKey) {
                        userDetailOpts[groupKey][entryKey] = {
                            opts: [],
                            title: entryVal.title
                        }, _.each(entryVal.opts, function(optsVal, optsKey) {
                            userDetailOpts[groupKey][entryKey].opts.push({
                                id: optsKey,
                                name: optsVal
                            })
                        })
                    })
                }), userDetailOpts
            }, this.createSaveDataObject = function() {
                var saveData = {};
                return _.each($rootScope.UserDetails, function(groupVal) {
                    _.each(groupVal, function(entryVal, entryKey) {
                        saveData[entryKey] = "no"
                    })
                }), saveData
            }, this.saveProfile = function(saveData) {
                UserQueryFctry.self.put(angular.extend({
                    type: "details"
                }, saveData)).$promise.then(function() {
                    ToasterService.open("success", Translator.get("edit_profile"), Translator.get("changes_saved"))
                })
            }, this.updateScopeUserDetailLabels = function(saveData, scopeUserDetails, userDetailOpts) {
                _.each(userDetailOpts, function(groupVal, groupKey) {
                    _.each(saveData, function(saveVal, saveKey) {
                        void 0 !== groupVal[saveKey] && _.each(groupVal[saveKey].opts, function(optVal) {
                            optVal.id === saveVal && (scopeUserDetails[groupKey] = scopeUserDetails[groupKey] || {}, scopeUserDetails[groupKey][saveKey] = scopeUserDetails[groupKey][saveKey] || {}, scopeUserDetails[groupKey][saveKey].label = optVal.name)
                        })
                    })
                })
            }, this.createVisit = function(userId) {
                !$rootScope.isSecurityUser() || $rootScope.Self.admin && nv || UserQueryFctry.single.post({
                    userId: userId,
                    category: "visits"
                })
            }
        }
        angular.module("Lovoo.Profile").factory("ProfileUserDataStorage", ProfileUserDataStorage).service("ProfileFeedService", ProfileFeedService).service("ProfileEditService", ProfileEditService).service("ProfileService", ProfileService), ProfileFeedService.$inject = ["$rootScope", "$route", "$routeParams", "DialogService", "FeedPostProto"], ProfileEditService.$inject = ["$rootScope", "ToasterService", "UserQueryFctry"], ProfileService.$inject = ["$rootScope", "$routeParams", "$route", "RegisterService", "ToasterService", "UserActionsFctry", "UserQueryFctry", "ProfileUserDataStorage"]
    }()
});