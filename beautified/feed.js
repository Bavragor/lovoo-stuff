/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    ! function() {
        "use strict";

        function FeedPageCtrl(FEED_MAX_DISTANCE, $scope, FeedRequestService, UserRequestService) {
            function getFeed(resetFeed) {
                resetFeed && (feedCount++, scrollToFeedTop()), searchOptions = {
                    type: $scope.type,
                    lat: $scope.lat,
                    lon: $scope.lon,
                    distance: parseInt($scope.distance),
                    ageFrom: parseInt($scope.ageFrom),
                    ageTo: parseInt($scope.ageTo),
                    trendingHashtags: $scope.trendingHashtags,
                    interests: $scope.interests,
                    genders: $scope.genders,
                    feedCount: feedCount,
                    resultPage: $scope.feedOptions.resultPage
                }, searchOptions.before = $scope.feedOptions.feed ? $scope.feedOptions.feed.getLastPostTimeStamp() : null, FeedRequestService.getFeedByFeedPageSearchOptions(searchOptions, function(feed) {
                    feed.feedCount < feedCount || $scope.feedOptions.updateFeed(feed)
                })
            }

            function scrollToFeedTop() {
                var feedPageTop = $(".feed-page").offset().top - $(".navbar-lovoo").outerHeight(!0);
                jWindow.scrollTop() > feedPageTop && jWindow.scrollTop(feedPageTop)
            }
            var searchOptions, jWindow = $(window),
                feedCount = 0,
                HASHTAG_COUNT = 6,
                DEFAULT_DISTANCE = 50;
            $scope.$root.activePage = "feed", $scope.type = "timeline", $scope.lat = null, $scope.lon = null, $scope.distance = DEFAULT_DISTANCE, $scope.maxDistance = FEED_MAX_DISTANCE, $scope.ageFrom = $scope.$root.AgeRange.from, $scope.ageTo = $scope.$root.AgeRange.to, $scope.trendingHashtags = [], FeedRequestService.getTrendingHashtags({
                resultLimit: HASHTAG_COUNT,
                success: function(hastags) {
                    _.each(hastags, function(hashtag) {
                        $scope.trendingHashtags.push({
                            hashtag: hashtag,
                            ngModel: !1
                        })
                    })
                }
            }), $scope.interests = [], $scope.$root.Self.getInterests({
                success: function() {
                    var hashtags = $scope.$root.Self.interests.slice(0, HASHTAG_COUNT);
                    _.each(hashtags, function(hashtag) {
                        $scope.interests.push({
                            hashtag: hashtag,
                            ngModel: !1
                        })
                    })
                }
            }), $scope.genders = [], _.each($scope.$root.Genders, function(value, key) {
                "3" !== key && $scope.genders.push({
                    id: key,
                    name: value.genderGroupTranslated,
                    ngModel: "1" !== key || 1 !== $scope.$root.Self.gender
                })
            }), $scope.feedOptions = FeedRequestService.getFeedOptions(getFeed), $scope.$root.isSecurityUser() && void 0 !== Self.coord && void 0 !== Self.coord.latitude && ($scope.presetLocation = {
                title: Self.location || Self.city || "",
                coordinates: {
                    latitude: Self.coord.latitude,
                    longitude: Self.coord.longitude
                }
            }), $scope.setType = function($event, type) {
                var jSiblings = $($event.currentTarget).siblings().find(".inner");
                $scope.type !== type && (jSiblings.find("> label").addClass("collapsed").end().find("> ul").collapse({
                    toggle: !1
                }).collapse("hide"), $scope.type = type, $scope.feedOptions.getFeed(!0))
            }, $scope.setLocation = function(location) {
                $scope.lat = location.coordinates.latitude, $scope.lon = location.coordinates.longitude, $scope.location = location.city, $scope.feedOptions.feed && $scope.feedOptions.getFeed(!0)
            }, $scope.convertDistanceToString = function(distance) {
                return void 0 === distance ? "" : distance >= FEED_MAX_DISTANCE ? Translator.get("equal") : distance + " km"
            }, $scope.convertAgeToString = function(age) {
                return void 0 === age ? "" : Translator.get("dynamic.age_years", {
                    years: age
                }, age)
            }, $scope.onChangeSlider = function() {
                $scope.feedOptions.getFeed(!0)
            }
        }

        function FeedPostPageCtrl($routeParams, $scope, ApproutingService, FeedRequestService, ImageService) {
            $scope.$root.activePage = "feedPost", $scope.isLoading = !0, $scope.coverSrc = "", FeedRequestService.getFeedByPhotoId($routeParams.postId).then(function(post) {
                $scope.isLoading = !1, $scope.post = post, $scope.user = post.owner, $scope.tryApprouting = ApproutingService.openAppOrStoreForMobileOrRegisterForDesktopGuests, $scope.coverSrc = ImageService.getImageUrlFromImageArray({
                    images: post.images,
                    width: 1200
                }), FeedRequestService.getFeedByCaption(post.caption).$promise.then(function(feed) {
                    for (var i = feed.posts.length - 1; i >= 0; --i) post.id !== feed.posts[i].id ? feed.posts[i].imageUrl = ImageService.getImageUrlFromImageArray({
                        images: feed.posts[i].images,
                        width: 120
                    }) : feed.posts.splice(i, 1);
                    $scope.similarPosts = feed.posts
                })
            })
        }

        function PostModalCtrl($scope, FeedRequestService) {
            FeedRequestService.getFeedByUserId({
                userId: $scope.post.owner.id,
                resultLimit: 1
            }).then(function(feed) {
                $scope.post.owner.postCount = feed.allCount
            })
        }
        angular.module("Lovoo.Feed", ["ngResource", "Lovoo.Approuting", "Lovoo.User", "Storage", "uiSlider"]).constant("FEED_MAX_DISTANCE", 500).constant("FEED_PHOTO_WIDTH", 336).controller("FeedPageCtrl", FeedPageCtrl).controller("FeedPostPageCtrl", FeedPostPageCtrl).controller("PostModalCtrl", PostModalCtrl), FeedPageCtrl.$inject = ["FEED_MAX_DISTANCE", "$scope", "FeedRequestService", "UserRequestService"], FeedPostPageCtrl.$inject = ["$routeParams", "$scope", "ApproutingService", "FeedRequestService", "ImageService"], PostModalCtrl.$inject = ["$scope", "FeedRequestService"]
    }(),
    function(Translator) {
        "use strict";

        function FeedDirective(FEED_INFINITE_SCROLL_DISTANCE, $rootScope, AnimationFrameService) {
            var jWindow = $(window),
                jDocument = $(document);
            return {
                restrict: "E",
                templateUrl: function() {
                    return $rootScope.isSecurityUser() ? "/template/feed/advanced-feed.html" : "/template/feed/simple-feed.html"
                },
                scope: {
                    feedOptions: "="
                },
                link: function(scope, element, attributes) {
                    function feedScrollEvent() {
                        scope.feedOptions.isLoadingFeed || jWindow.scrollTop() + jWindow.height() + FEED_INFINITE_SCROLL_DISTANCE > jDocument.height() && scope.feedOptions.getFeed(!1)
                    }
                    AnimationFrameService.add(scope, feedScrollEvent, ["scroll"]), scope.type = attributes.type, scope.feedOptions.isLoadingFeed = !1, scope.feedOptions.jContainer = element, scope.feedOptions.getFeed(!0)
                }
            }
        }

        function FeedPageDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/feed/feed-page.html",
                controller: "FeedPageCtrl"
            }
        }

        function FeedPageSidebarDirective($rootScope, AnimationFrameService) {
            function checkSidebarPosition() {
                documentHeight = jDocument.height(), sidebarHeight = jSidebar.outerHeight(!0), sidebarTop = jSidebar.offset().top, contentHeight = jContent.height(), contentTop = jContent.offset().top, windowTop = jWindow.scrollTop(), windowHeight = jWindow.height(), topMenuHeight = jHeader.outerHeight(!0), footerHeight = jFooter.children().first().height(), $rootScope.BS.xs || $rootScope.BS.sm || sidebarHeight > contentHeight || contentTop - topMenuHeight > windowTop ? setSidebarPosition({
                    position: "static-top"
                }) : sidebarHeight + topMenuHeight > windowHeight ? checkScrollableSidebarPosition() : checkNonScrollableSidebarPosition(), lastWindowTop = windowTop
            }

            function checkNonScrollableSidebarPosition() {
                setSidebarPosition(windowTop + sidebarHeight > documentHeight - footerHeight - topMenuHeight - FOOTER_PADDING ? {
                    position: "relative-bottom"
                } : {
                    position: "fixed-top"
                })
            }

            function checkScrollableSidebarPosition() {
                windowTop + windowHeight > contentTop + contentHeight - FOOTER_PADDING ? setSidebarPosition({
                    position: "relative-bottom"
                }) : "static-top" === lastPosition && windowTop > contentTop - topMenuHeight ? setSidebarPosition({
                    position: "relative-top",
                    top: 0
                }) : "relative-top" === lastPosition && windowTop + windowHeight > sidebarTop + sidebarHeight ? setSidebarPosition({
                    position: "fixed-bottom"
                }) : "relative-top" === lastPosition && sidebarTop - topMenuHeight > windowTop ? setSidebarPosition({
                    position: "fixed-top"
                }) : "fixed-bottom" === lastPosition && lastWindowTop > windowTop ? setSidebarPosition({
                    position: "relative-top",
                    top: windowTop + windowHeight - sidebarHeight - contentTop
                }) : "fixed-top" === lastPosition && windowTop > lastWindowTop ? setSidebarPosition({
                    position: "relative-top",
                    top: windowTop - contentTop + topMenuHeight
                }) : "relative-bottom" === lastPosition && sidebarTop - topMenuHeight > windowTop && setSidebarPosition({
                    position: "fixed-top"
                })
            }

            function setSidebarPosition(styles) {
                switch (lastPosition = styles.position, styles.position) {
                    case "static-top":
                        jSidebar.css({
                            top: "auto",
                            bottom: "auto",
                            position: "static",
                            width: "auto"
                        });
                        break;
                    case "fixed-top":
                        jSidebar.css({
                            top: topMenuHeight,
                            bottom: "auto",
                            position: "fixed",
                            width: jSidebar.parent().width()
                        });
                        break;
                    case "relative-top":
                        jSidebar.css({
                            top: styles.top,
                            bottom: "auto",
                            position: "relative",
                            width: "auto"
                        });
                        break;
                    case "fixed-bottom":
                        jSidebar.css({
                            top: "auto",
                            bottom: 0,
                            position: "fixed",
                            width: jSidebar.parent().width()
                        });
                        break;
                    case "relative-bottom":
                        jSidebar.css({
                            top: documentHeight - contentTop - footerHeight - sidebarHeight - FOOTER_PADDING,
                            bottom: "auto",
                            position: "relative",
                            width: "auto"
                        })
                }
            }
            var topMenuHeight, footerHeight, sidebarTop, contentHeight, sidebarHeight, contentTop, lastPosition, windowTop, windowHeight, documentHeight, jSidebar, jContent, jHeader, jFooter, jWindow, jDocument, FOOTER_PADDING = 22,
                lastWindowTop = 0;
            return {
                restrict: "C",
                replace: !1,
                link: function(scope, element) {
                    jSidebar = element, jContent = element.parent().siblings().first(), jHeader = $(".navbar-lovoo"), jFooter = $("footer-container"), jWindow = $(window), jDocument = $(document), AnimationFrameService.add(scope, checkSidebarPosition, ["scroll", "resize", "changeDocumentHeight", function() {
                        return jSidebar.height() + "-" + jContent.height()
                    }])
                }
            }
        }

        function FeedImageDirective(FEED_PHOTO_WIDTH, $timeout) {
            return {
                restrict: "A",
                scope: {
                    post: "="
                },
                link: function(scope, element) {
                    $timeout(function() {
                        var containerWidth = element.parent().width(),
                            image = scope.post.getImage({
                                width: FEED_PHOTO_WIDTH,
                                category: "image"
                            });
                        element.css({
                            width: containerWidth,
                            height: containerWidth * image.height / image.width,
                            opacity: 0,
                            scale: .98
                        }).attr({
                            src: image.url
                        }).imagesLoaded(function() {
                            element.css({
                                width: "",
                                height: ""
                            }).transition({
                                opacity: 1,
                                scale: 1
                            }, 1200)
                        })
                    })
                }
            }
        }

        function MasonizeFeedDirective($timeout, AnimationFrameService) {
            return {
                restrict: "A",
                link: function(scope, element) {
                    $timeout(function() {
                        AnimationFrameService.add(scope, function() {
                            element.parent().masonry({
                                gutter: 0,
                                itemSelector: ".masonry-item",
                                transitionDuration: 0
                            })
                        }, ["resize"]), element.removeClass("invisible")
                    })
                }
            }
        }

        function MasonizeUpdateHeightDirective($timeout) {
            return {
                restrict: "A",
                link: function(scope, element, attributes) {
                    var targetElement = attributes.masonizeUpdateHeight;
                    element.find(targetElement + " a").on("click", function(event) {
                        var jTarget = $(event.target);
                        jTarget.parent().hasClass("active") || $timeout(function() {
                            element.parents(".masonryContainer").masonry()
                        }, 15)
                    })
                }
            }
        }

        function HeaderBackgroundCoverDirective() {
            return {
                restrict: "C",
                link: function(scope, element, attributes) {
                    var coverSrc = scope.$root.isUniquePage ? scope.$root.Ajax.imgUrlStatic + "talent-header.jpg" : attributes.coverSrc;
                    "" !== attributes.coverSrc && element.css({
                        backgroundImage: "url(" + coverSrc + ")"
                    })
                }
            }
        }

        function ShowMorePostsDirective() {
            return {
                restrict: "E",
                templateUrl: "/template/feed/show-more-posts.html"
            }
        }

        function ReadMoreTextDirective() {
            return {
                restrict: "E",
                scope: {
                    text: "@"
                },
                templateUrl: "/template/feed/read-more-text.html",
                link: function(scope, element, attributes) {
                    scope.showFullText = !1, attributes.$observe("chars", function(chars) {
                        chars = parseInt(chars), scope.text.length <= chars ? scope.texts = [scope.text] : (chars += scope.text.substr(chars).indexOf(" "), scope.texts = [scope.text.substr(0, chars), scope.text.substr(chars)])
                    }), scope.toggleText = function() {
                        scope.showFullText = !scope.showFullText
                    }
                }
            }
        }

        function HashtagChecklistDirective(HASHTAG_VALIDATE) {
            return {
                restrict: "A",
                scope: {
                    hashtags: "=hashtagChecklist",
                    feedOptions: "="
                },
                templateUrl: "/template/feed/hashtag-checklist.html",
                controller: function($scope) {
                    $scope.newHashtagName = "", $scope.addHashtag = function() {
                        var hashtag = $scope.newHashtagName.replace(HASHTAG_VALIDATE, "");
                        $scope.newHashtagName = "", "" !== hashtag && ($scope.hashtags.push({
                            hashtag: hashtag,
                            ngModel: !0
                        }), $scope.feedOptions.getFeed(!0))
                    }
                }
            }
        }

        function LinkHashtagsDirective($timeout, FeedService) {
            return {
                restrict: "A",
                link: function(scope, element) {
                    $timeout(function() {
                        element.html(FeedService.linkHashtagsInText(element.html()))
                    })
                }
            }
        }

        function FollowButtonDirective(FeedActionService) {
            return {
                restrict: "E",
                replace: !0,
                scope: {
                    type: "@",
                    value: "="
                },
                templateUrl: "/template/feed/follow-button.html",
                link: function(scope) {
                    switch (scope.hasFollowed = !1, scope.isLoading = !1, scope.type) {
                        case "hashtag":
                            if (scope.actionName = Translator.get("follow_btn.hashtag"), !scope.$root.isSecurityUser()) {
                                scope.hasFollowed = !1;
                                break
                            }
                            scope.$root.Self.interestsHelper.isLoaded || (scope.isLoading = !0), scope.$root.Self.getInterests({
                                success: function() {
                                    scope.isLoading = !1, scope.hasFollowed = _.contains(scope.$root.Self.interests, scope.value)
                                }
                            });
                            break;
                        case "user":
                            scope.actionName = Translator.get("follow_btn.user"), scope.value.connections.isLoaded || (scope.isLoading = !0), scope.value.getConnections({
                                success: function() {
                                    scope.isLoading = !1, scope.hasFollowed = scope.value.connections.hasFollowed
                                }
                            })
                    }
                    scope.clickButton = function() {
                        if (scope.$root.isSecurityUser() && !scope.isLoading) switch (scope.hasFollowed = !scope.hasFollowed, scope.type) {
                            case "hashtag":
                                FeedActionService.toggleHashtagLike(scope.value);
                                break;
                            case "user":
                                scope.value.toggleFollow()
                        }
                    }
                }
            }
        }
        angular.module("Lovoo.Feed").constant("FEED_INFINITE_SCROLL_DISTANCE", 2e3).directive("feed", FeedDirective).directive("feedPage", FeedPageDirective).directive("feedPageSidebar", FeedPageSidebarDirective).directive("feedImage", FeedImageDirective).directive("masonizeFeed", MasonizeFeedDirective).directive("masonizeUpdateHeight", MasonizeUpdateHeightDirective).directive("headerBackgroundCover", HeaderBackgroundCoverDirective).directive("readMoreText", ReadMoreTextDirective).directive("showMorePosts", ShowMorePostsDirective).directive("hashtagChecklist", HashtagChecklistDirective).directive("linkHashtags", LinkHashtagsDirective).directive("followButton", FollowButtonDirective), FeedDirective.$inject = ["FEED_INFINITE_SCROLL_DISTANCE", "$rootScope", "AnimationFrameService"], FeedPageSidebarDirective.$inject = ["$rootScope", "AnimationFrameService"], FeedImageDirective.$inject = ["FEED_PHOTO_WIDTH", "$timeout"], MasonizeFeedDirective.$inject = ["$timeout", "AnimationFrameService"], MasonizeUpdateHeightDirective.$inject = ["$timeout"], HashtagChecklistDirective.$inject = ["HASHTAG_VALIDATE"], LinkHashtagsDirective.$inject = ["$timeout", "FeedService"], FollowButtonDirective.$inject = ["FeedActionService"]
    }(window.Translator),
    function() {
        "use strict";

        function FeedActionFctry($resource, $rootScope) {
            return {
                photoLike: $resource($rootScope.Ajax.api + "/photos/:photoId/likes", {}, {
                    post: {
                        method: "POST",
                        params: {
                            photoId: "@photoId"
                        }
                    },
                    "delete": {
                        method: "DELETE",
                        params: {
                            photoId: "@photoId"
                        }
                    }
                }),
                photoReport: $resource($rootScope.Ajax.api + "/photos/:photoId/report", {}, {
                    post: {
                        method: "POST",
                        params: {
                            photoId: "@photoId"
                        }
                    }
                }),
                hashtagLike: $resource($rootScope.Ajax.api + "/hashtags/:hashtag/like", {}, {
                    post: {
                        method: "POST",
                        params: {
                            hashtag: "@hashtag"
                        }
                    },
                    "delete": {
                        method: "DELETE",
                        params: {
                            hashtag: "@hashtag"
                        }
                    }
                })
            }
        }

        function FeedFctry($resource, $rootScope, FeedProto, FeedPostProto) {
            return {
                photos: $resource($rootScope.Ajax.api + "/photos/:photoId", {}, {
                    get: {
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "public": !0
                        },
                        interceptor: {
                            response: function(response) {
                                return new FeedPostProto(response.data.response)
                            }
                        }
                    }
                }),
                photoReplies: $resource($rootScope.Ajax.api + "/photos/:photoId/replies", {}, {
                    get: {
                        interceptor: {
                            response: function(response) {
                                return new FeedProto(response.data.response)
                            }
                        }
                    }
                }),
                search: $resource($rootScope.Ajax.api + "/feed/search", {}, {
                    get: {
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "public": !0
                        },
                        interceptor: {
                            response: function(response) {
                                return new FeedProto(response.data.response)
                            }
                        }
                    }
                }),
                feed: $resource($rootScope.Ajax.api + "/users/:userId/feed", {}, {
                    get: {
                        interceptor: {
                            response: function(response) {
                                return new FeedProto(response.data.response)
                            }
                        }
                    }
                }),
                localfeed: $resource($rootScope.Ajax.api + "/users/:userId/localfeed", {}, {
                    get: {
                        interceptor: {
                            response: function(response) {
                                return new FeedProto(response.data.response)
                            }
                        }
                    }
                }),
                timeline: $resource($rootScope.Ajax.api + "/users/:userId/timeline", {}, {
                    get: {
                        interceptor: {
                            response: function(response) {
                                return new FeedProto(response.data.response)
                            }
                        }
                    }
                }),
                trendingHashtags: $resource($rootScope.Ajax.api + "/hashtags/trending", {}, {
                    get: {
                        interceptor: {
                            response: function(response) {
                                return response.data.response
                            }
                        }
                    }
                }),
                hashtagInfo: $resource($rootScope.Ajax.api + "/hashtags/:hashtag/info", {}, {
                    get: {
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                            "public": !0
                        }
                    }
                }),
                widgets: $resource($rootScope.Ajax.api + "/widgets", {}, {
                    get: {
                        interceptor: {
                            response: function(response) {
                                return response.data.response.result
                            }
                        }
                    }
                })
            }
        }

        function FeedProto(FeedPostProto, ImageService, UserProto) {
            var Feed = function(response) {
                var self = this;
                this.allCount = response.allCount || 0, this.posts = [], _.each(response.result, function(post) {
                    var feedPost;
                    "story" === post._type && (post = post.object), post.owner = void 0 !== post.owner ? new UserProto(post.owner) : null, feedPost = new FeedPostProto(post), "empty" !== feedPost._type && self.posts.push(feedPost)
                })
            };
            return Feed.prototype.getBackgroundCoverSrc = function() {
                var i;
                for (i = 0; i < this.posts.length; ++i)
                    if ("photo" === this.posts[i]._type && this.posts[i].images.length > 0) return ImageService.getImageUrlFromImageArray({
                        images: this.posts[i].images,
                        width: 1200
                    });
                return ""
            }, Feed.prototype.getLastPostTimeStamp = function() {
                return this.posts.length > 0 ? this.posts[this.posts.length - 1].createdAt : null
            }, Feed.prototype.setFirstPhotoToMostPopular = function() {
                var i;
                for (i = 0; i < this.posts.length; ++i)
                    if ("photo" === this.posts[i]._type) {
                        this.posts[i].isMostPopular = !0;
                        break
                    }
                return this
            }, Feed.prototype.extendFeedPost = function(feedPost, prepend) {
                var isNewPost = !0;
                return prepend = !!prepend, _.each(this.posts, function(oldPost) {
                    return feedPost.id === oldPost.id ? (isNewPost = !1, !1) : void 0
                }), isNewPost ? (prepend ? this.posts.unshift(feedPost) : this.posts.push(feedPost), !0) : !1
            }, Feed.prototype.extendFeedPosts = function(feedPosts, prepend) {
                var appendedNewPost, self = this,
                    extendCount = 0;
                return prepend = prepend || !1, _.each(feedPosts, function(newPost) {
                    appendedNewPost = self.extendFeedPost(newPost, prepend), extendCount = appendedNewPost ? ++extendCount : extendCount
                }), extendCount
            }, Feed.prototype.removeFeedPosts = function(posts) {
                var i, ii;
                for (i = 0; i < this.posts.length; ++i)
                    for (ii = 0; ii < posts.length; ++ii)
                        if (this.posts[i].id === posts[ii].id) {
                            this.posts.splice(i, 1), --i;
                            break
                        }
            }, Feed
        }

        function FeedPostProto($injector, $rootScope, DialogService, FeedActionFctry, FeedService, ImageService, UserActionsFctry) {
            var FeedPost = function(data) {
                switch (this._type = data._type || "empty", this.owner = data.owner || null, this.isMostPopular = !1, data._type) {
                    case "widget":
                        this._type = "widget-" + data.type, this.position = data.position, delete this.owner;
                        break;
                    case "profile-details":
                        this.openEditProfileModal = data.openEditProfileModal, this.id = data._type;
                        break;
                    case "profile-picture":
                        this.images = data.images || [], this.pictures = data.pictures || [], this.id = data._type, this.getImageFromImageArray = ImageService.getImageFromImageArray;
                        break;
                    case "photo":
                        this.caption = data.caption || "", this.challenge = data.challenge || null, this.createdAt = data.createdAt || null, this.distance = data.distance || null, this.hasLiked = data.hasLiked || !1, this.id = data.id || "", this.images = data.images || [], this.likeCount = data.likeCount || 0, this.place = data.place || null, this.relativeXPosition = data.relativeXPosition || 0, this.relativeYPosition = data.relativeYPosition || 0, this.replies = null, this.replyCount = data.replyCount || 0, this.topReplies = null, this.updatedAt = data.updatedAt || 0, this.location = data.location || {
                            city: null,
                            distance: null
                        }, null === this.location.city && null !== this.owner && (this.location.city = data.owner.location)
                }
            };
            return FeedPost.prototype.toggleLike = function() {
                var params;
                return $rootScope.isSecurityUser() && this.id && (params = {
                    photoId: this.id
                }, this.hasLiked = !this.hasLiked, this.hasLiked ? (this.likeCount++, FeedActionFctry.photoLike.post(params)) : (this.likeCount--, FeedActionFctry.photoLike["delete"](params))), this
            }, FeedPost.prototype.getPostTypeDescription = function() {
                if ("photo" === this._type) {
                    var profileUrl = Ajax.url + "/profile/" + this.owner.id;
                    return Translator.get("feed.user_has_added_photo", {
                        user: '<a href="' + profileUrl + '">' + this.owner.name + "</a>"
                    })
                }
                return ""
            }, FeedPost.prototype.getCaptionWithLinks = function() {
                return FeedService.linkHashtagsInText(this.caption)
            }, FeedPost.prototype.getFormattedTime = function() {
                var time;
                return null === this.createdAt ? "" : (time = $rootScope.lastResponseTime - this.createdAt, 60 > time ? time + "s" : 3600 > time ? Math.floor(time / 60) + "m" : 86400 > time ? Math.floor(time / 60 / 60) + "h" : 604800 > time ? Math.floor(time / 60 / 60 / 24) + "d" : 31536e3 > time ? Math.floor(time / 60 / 60 / 24 / 7) + "w" : Math.floor(time / 60 / 60 / 24 / 365) + "y")
            }, FeedPost.prototype.getImage = function(params) {
                return params.images = this.images, ImageService.getImageFromImageArray(params)
            }, FeedPost.prototype.openReportFormModal = function() {
                $rootScope.isSecurityUser() && UserActionsFctry.action(this, "report", {
                    referenceType: "photo"
                })
            }, FeedPost.prototype.submitReportForm = function(params) {
                return FeedActionFctry.photoReport.post(params).$promise
            }, FeedPost.prototype.openPostModal = function() {
                return this.getReplies(), DialogService.openDialog({
                    backdrop: !0,
                    controller: "PostModalCtrl",
                    size: $rootScope.BS.xs ? "" : "lg",
                    templateUrl: "/template/dialog/post-modal.html",
                    windowClass: $rootScope.BS.xs ? "modal-cover" : ""
                }, {
                    post: this
                })
            }, FeedPost.prototype.getReplies = function() {
                var self = this,
                    FeedRequestService = $injector.get("FeedRequestService");
                return FeedRequestService.getFeedRepliesByPhotoId(this.id, function(response) {
                    self.replies = response.replies, self.topReplies = response.topReplies
                })
            }, FeedPost
        }

        function FeedOptionsProto($timeout) {
            var FeedOptions = function(getFeed) {
                this.feed = null, this.jContainer = null, this.isLoadingFeed = !1, this.isFeedCompletelyLoaded = !1, this.isHidden = !1, this.resultPage = 0, this.requestFeed = getFeed
            };
            return FeedOptions.prototype.getFeed = function(resetFeed) {
                resetFeed && (this.isFeedCompletelyLoaded = !1, this.isLoadingFeed = !1, this.resultPage = 0, this.feed = null), this.isLoadingFeed || this.isFeedCompletelyLoaded || (this.resultPage++, this.isLoadingFeed = !0, this.requestFeed(resetFeed))
            }, FeedOptions.prototype.refreshMasonry = function(params) {
                var self = this;
                return params.count = void 0 !== params.count ? params.count : 1, params.prepend = params.prepend || !1, params.waitForImages = params.waitForImages || !1, 0 === params.count ? params : ($timeout(function() {
                    var refreshMasonry, jMasonryItems, jMasonryContainer = self.jContainer.find(".masonryContainer");
                    jMasonryContainer.length && self.feed && (refreshMasonry = function() {
                        jMasonryContainer.masonry(params.prepend ? "prepended" : "appended", jMasonryItems)
                    }, jMasonryItems = params.prepend ? jMasonryContainer.children().slice(0, params.count) : jMasonryContainer.children().slice(-params.count), params.waitForImages ? jMasonryItems.imagesLoaded(refreshMasonry) : refreshMasonry())
                }, 10), params)
            }, FeedOptions.prototype.updateFeed = function(feed) {
                var self = this,
                    newPostsCount = 0;
                null === this.feed ? (newPostsCount = feed.posts.length, this.feed = feed) : (newPostsCount = this.feed.extendFeedPosts(feed.posts, !1), this.refreshMasonry({
                    count: newPostsCount
                })), $timeout(function() {
                    self.isLoadingFeed = !1
                }, 100), 0 === newPostsCount && (this.isFeedCompletelyLoaded = !0)
            }, FeedOptions
        }
        angular.module("Lovoo.Feed").factory("FeedActionFctry", FeedActionFctry).factory("FeedFctry", FeedFctry).factory("FeedProto", FeedProto).factory("FeedPostProto", FeedPostProto).factory("FeedOptionsProto", FeedOptionsProto), FeedActionFctry.$inject = ["$resource", "$rootScope"], FeedFctry.$inject = ["$resource", "$rootScope", "FeedProto", "FeedPostProto"], FeedProto.$inject = ["FeedPostProto", "ImageService", "UserProto"], FeedPostProto.$inject = ["$injector", "$rootScope", "DialogService", "FeedActionFctry", "FeedService", "ImageService", "UserActionsFctry"], FeedOptionsProto.$inject = ["$timeout"]
    }(),
    function() {
        "use strict";

        function FeedRequestService(FEED_MAX_DISTANCE, FEED_RESULT_LIMIT, $injector, $rootScope, FeedFctry, FeedOptionsProto, FeedPostProto, FeedProto, FeedService) {
            function getPostFromWidget(widget) {
                var widgetObject, post = new FeedPostProto(widget);
                switch (widget.post = post, post._type) {
                    case "widget-hashtag":
                        return widgetObject = widget.children[0].object, void 0 === widgetObject.headerImage ? null : (post.allCount = widgetObject.hashtag.count, post.images = widgetObject.headerImage || [], post.hashtag = widgetObject.hashtag.name, self.getFeed({
                            tags: post.hashtag,
                            resultLimit: 3
                        }).then(function(feed) {
                            post.hashtagPosts = feed.posts
                        }), post);
                    default:
                        return null
                }
            }

            function getFeedResultLimit() {
                var feedTemplate = $rootScope.isSecurityUser() ? "advanced" : "simple";
                return FEED_RESULT_LIMIT[feedTemplate]
            }

            function getParamsFromSearchOptions(searchOptions) {
                var gender, tags = "",
                    params = {
                        resultLimit: getFeedResultLimit()
                    };
                return "pictures" === searchOptions.type ? tags = getTagsStringFromHashtags(searchOptions.trendingHashtags, !1) : "people" === searchOptions.type && (tags = getTagsStringFromHashtags(searchOptions.interests, !0)), "" !== tags && (params.tags = tags), ("timeline" === searchOptions.type || "pictures" === searchOptions.type && "" === tags) && (params.userId = $rootScope.Self.id), "timeline" === searchOptions.type ? null !== searchOptions.before && (params.before = searchOptions.before) : searchOptions.resultPage > 1 && (params.resultPage = searchOptions.resultPage), gender = getGenderFromSearchOptions(searchOptions), "people" !== searchOptions.type || 1 !== gender && 2 !== gender || (params.gender = gender), "people" === searchOptions.type && (params.ageFrom = searchOptions.ageFrom, params.ageTo = searchOptions.ageTo), ("pictures" === searchOptions.type || "people" === searchOptions.type) && "number" == typeof searchOptions.lat && "number" == typeof searchOptions.lon && "number" == typeof searchOptions.distance && searchOptions.distance > 0 && searchOptions.distance < FEED_MAX_DISTANCE && (params.lat = searchOptions.lat, params.lon = searchOptions.lon, params.distance = searchOptions.distance), params
            }

            function getTagsStringFromHashtags(hashtags, appendMe) {
                var tags = "";
                return _.each(hashtags, function(hashtag) {
                    hashtag.ngModel && (tags += hashtag.hashtag + ",")
                }), appendMe && (tags += "me,"), "" !== tags && (tags = tags.slice(0, -1)), tags
            }

            function getGenderFromSearchOptions(searchOptions) {
                var unifiedGender = 0;
                return _.each(searchOptions.genders, function(gender) {
                    gender.ngModel && (unifiedGender += parseInt(gender.id))
                }), unifiedGender
            }
            var self = this;
            this.getFeed = function(params) {
                return void 0 === params.resultLimit && (params.resultLimit = getFeedResultLimit()), void 0 !== params.resultPage && params.resultPage <= 1 && delete params.resultPage, FeedFctry.search.get(params).$promise
            }, this.getFeedByUserId = function(params) {
                return void 0 === params.resultLimit && (params.resultLimit = getFeedResultLimit()), void 0 !== params.resultPage && params.resultPage <= 1 && delete params.resultPage, FeedFctry.feed.get(params).$promise
            }, this.getFeedByPhotoId = function(photoId) {
                return FeedFctry.photos.get({
                    photoId: photoId,
                    resultLimit: 1
                }).$promise
            }, this.getFeedRepliesByPhotoId = function(photoId, success) {
                var checkResponse, response = {
                    replies: null,
                    topReplies: null
                };
                return checkResponse = function() {
                    response.replies && response.topReplies && (response.replies.removeFeedPosts(response.topReplies.posts), success(response))
                }, FeedFctry.photoReplies.get({
                    photoId: photoId
                }).$promise.then(function(feed) {
                    response.replies = feed, checkResponse()
                }), FeedFctry.photoReplies.get({
                    photoId: photoId,
                    type: "top"
                }).$promise.then(function(topFeed) {
                    response.topReplies = topFeed, checkResponse()
                }), response
            }, this.getFeedByCaption = function(caption) {
                var hashtags = FeedService.getHashtagsFromText(caption);
                return 0 === hashtags.length ? new FeedProto({}) : FeedFctry.search.get({
                    resultLimit: getFeedResultLimit(),
                    tags: hashtags[0]
                })
            }, this.getFeedByFeedPageSearchOptions = function(searchOptions, success) {
                var requestSuccess, params = getParamsFromSearchOptions(searchOptions);
                switch (requestSuccess = function(feed) {
                    feed.feedCount = searchOptions.feedCount, success(feed)
                }, searchOptions.type) {
                    case "timeline":
                        self.getFeedWithWidgets({
                            $promise: FeedFctry.timeline.get(params).$promise,
                            success: requestSuccess,
                            widgetParams: {
                                type: "timeline"
                            }
                        });
                        break;
                    case "pictures":
                        void 0 === params.tags ? self.getFeedWithWidgets({
                            $promise: FeedFctry.localfeed.get(params).$promise,
                            success: requestSuccess,
                            widgetParams: {
                                type: "local"
                            }
                        }) : FeedFctry.search.get(params).$promise.then(requestSuccess);
                        break;
                    case "people":
                        FeedFctry.search.get(params).$promise.then(requestSuccess)
                }
                return params
            }, this.getFeedWithWidgets = function(widgetOptions) {
                var feedAndWidgetRequestSuccess;
                widgetOptions.feed = null, widgetOptions.widgets = null, feedAndWidgetRequestSuccess = function(type, data) {
                    widgetOptions[type] = data, widgetOptions.feed && widgetOptions.widgets && (delete widgetOptions.$promise, self.appendWidgetsToFeed(widgetOptions))
                }, widgetOptions.$promise.then(function(feed) {
                    feedAndWidgetRequestSuccess("feed", feed)
                }), self.getWidgets(widgetOptions.widgetParams).then(function(widgets) {
                    feedAndWidgetRequestSuccess("widgets", widgets)
                })
            }, this.getTrendingHashtags = function(params) {
                var requestTrendingHashtags, TRENDING_HASHTAGS_STORAGE_KEY = $injector.get("TRENDING_HASHTAGS_STORAGE_KEY"),
                    StorageService = $injector.get("StorageService"),
                    storage = StorageService.get(TRENDING_HASHTAGS_STORAGE_KEY);
                return requestTrendingHashtags = function() {
                    var requestParams = void 0 !== params.resultLimit ? {
                        resultLimit: params.resultLimit
                    } : {};
                    FeedFctry.trendingHashtags.get(requestParams).$promise.then(function(data) {
                        var newStorage = {
                            timestamp: (new Date).getTime(),
                            hashtags: []
                        };
                        _.each(data.result, function(hashtag) {
                            newStorage.hashtags.push(hashtag.hashtag.name)
                        }), StorageService.set(TRENDING_HASHTAGS_STORAGE_KEY, newStorage), params.success(newStorage.hashtags)
                    })
                }, null === storage ? void requestTrendingHashtags() : void(void 0 !== storage.hashtags && ((new Date).getTime() - 864e5 > storage.timestamp ? requestTrendingHashtags() : params.success(storage.hashtags)))
            }, this.getHashtagInfo = function(hashtag) {
                return FeedFctry.hashtagInfo.get({
                    hashtag: hashtag
                }).$promise
            }, this.getWidgets = function(params) {
                return FeedFctry.widgets.get(params).$promise
            }, this.appendWidgetsToFeed = function(widgetOptions) {
                _.each(widgetOptions.widgets, function(widget) {
                    var post = getPostFromWidget(widget);
                    null !== post && (post.position > widgetOptions.feed.posts.length ? widgetOptions.feed.posts.push(post) : widgetOptions.feed.posts.splice(post.position - 1, 0, post))
                }), widgetOptions.success(widgetOptions.feed)
            }, this.getFeedOptions = function(getFeed) {
                return new FeedOptionsProto(getFeed)
            }
        }

        function FeedActionService($rootScope, FeedActionFctry, FeedService) {
            this.toggleHashtagLike = function(hashtag) {
                var interestIndex;
                return $rootScope.isSecurityUser() ? (_.contains($rootScope.Self.interests, hashtag) ? (FeedActionFctry.hashtagLike["delete"]({
                    hashtag: hashtag
                }), interestIndex = $rootScope.Self.interests.indexOf(hashtag), -1 !== interestIndex && $rootScope.Self.interests.splice(interestIndex, 1)) : (FeedActionFctry.hashtagLike.post({
                    hashtag: hashtag
                }), $rootScope.Self.interests.push(hashtag)), $rootScope.Self.interests.length) : 0
            }
        }

        function FeedService(HASHTAG_FIND) {
            this.getHashtagsFromText = function(text) {
                var hashtags = text.match(HASHTAG_FIND);
                return _.each(hashtags, function(hashtag, key) {
                    hashtags[key] = hashtag.substr(1)
                }), hashtags
            }, this.linkHashtagsInText = function(text) {
                return text.replace(HASHTAG_FIND, function(hashtag) {
                    return hashtag = hashtag.substr(1), '<a class="close-all-modals" href="' + Ajax.url + "/hashtag/" + hashtag + '">#' + hashtag + "</a>"
                })
            }
        }
        angular.module("Lovoo.Feed").constant("FEED_RESULT_LIMIT", {
            simple: 29,
            advanced: 12
        }).constant("HASHTAG_VALIDATE", /[ +-.:,;#!?/\\"'â€˜â€™`Â´Â§$â‚¬Â¥Â£%&(){}[\]|=<>@*\n^Â°]+/g).constant("HASHTAG_FIND", /#[^ +-.:,;#!?/\\"'â€˜â€™`Â´Â§$â‚¬Â¥Â£%&(){}[\]|=<>@*\n^Â°]+/g).constant("TRENDING_HASHTAGS_STORAGE_KEY", "trendingHashtags").service("FeedRequestService", FeedRequestService).service("FeedActionService", FeedActionService).service("FeedService", FeedService), FeedRequestService.$inject = ["FEED_MAX_DISTANCE", "FEED_RESULT_LIMIT", "$injector", "$rootScope", "FeedFctry", "FeedOptionsProto", "FeedPostProto", "FeedProto", "FeedService"], FeedActionService.$inject = ["$rootScope", "FeedActionFctry", "FeedService"], FeedService.$inject = ["HASHTAG_FIND"]
    }()
});