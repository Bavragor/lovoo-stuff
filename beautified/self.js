/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "fileupload", "list", "settings"], function(angular, fileupload, list, settings) {
    ! function() {
        "use strict";

        function selfProfileBoxCtrl($scope, FirstStepsService, NotificationService, SelfCountsService) {
            var _self = $scope.$root.Self,
                notifications = ["v", "h", "k", "mh", "mv"];
            $scope.WHAZZUP_MAXLENGTH = FirstStepsService.WHAZZUP_MAXLENGTH, $scope.setMessage = FirstStepsService.setWhazzupMessage, $scope.editWhazzupMessage = FirstStepsService.openWhazzupMessageForm, $scope.coverSrc = _self.picture ? $scope.$root.Ajax.imgUrl + "/users/pictures/" + _self.picture + "/thumb_l.jpg" : "default-cover", $scope.$watch(function() {
                return SelfCountsService.get()
            }, function(data) {
                void 0 !== data && angular.extend($scope, data)
            }, !0), $scope.updateWantYou = function() {
                SelfCountsService.set(_self, "yw")
            }, NotificationService.subscribe(notifications, function(key) {
                var noti = this,
                    update = {
                        "new": noti[key] ? !0 : !1,
                        count: noti[key] ? "+" + noti[key] : _self.counts[key]
                    };
                SelfCountsService.update(key, update)
            }), SelfCountsService.set(_self)
        }

        function selfFirstStepsCtrl($modalInstance, $scope, FirstStepsService) {
            $scope.sel = "email", $scope.WHAZZUP_MAXLENGTH = FirstStepsService.WHAZZUP_MAXLENGTH, $scope.setMessage = FirstStepsService.setWhazzupMessage, $scope.close = $modalInstance.close, $scope.dismiss = $modalInstance.dismiss, $scope.confirmMail = function() {
                FirstStepsService.sendConfirmMail().$promise.then(function() {
                    $scope.disableConfirm = !0
                })
            }, $scope.selectItem = function(item) {
                $scope.sel = item
            }, $scope.help = {
                email: {
                    credits: 25
                },
                verify: {
                    credits: 100
                },
                photo: {
                    credits: 25
                },
                fullprofile: {
                    credits: 100
                }
            }
        }

        function selfGalleryCtrl($scope, GalleryService, UserActionsFctry) {
            $scope.setFirst = GalleryService.setFirst, $scope.action = UserActionsFctry.action, $scope.$watch(function() {
                return GalleryService.get()
            }, function(data, old) {
                (data !== old || data && !$scope.images) && ($scope.images = data)
            }, !0), $scope.removeImage = function(image) {
                GalleryService.remove(image, function() {
                    $scope.images.remove(image)
                })
            }
        }

        function selfPagesCtrl($routeParams, $scope, $rootScope, DialogTutorialService, UserActionsFctry) {
            switch ($rootScope.activePage = $routeParams.page, $scope.userParams = {
                userId: Self.id
            }, $scope.action = $routeParams.action || UserActionsFctry.action, $scope.openProfile = function() {
                UserActionsFctry.action($rootScope.Self, "profile")
            }, $rootScope.activePage) {
                case "kisses":
                    DialogTutorialService.loadTutorialIfEnabled("tutorial_kiss"), $scope.links = [{
                        page: "kisses",
                        url: $scope.$root.Ajax.url + "/self/kisses",
                        title: Translator.get("Kisses"),
                        icon: "lo lo-lips"
                    }];
                    break;
                case "visited":
                case "visits":
                    $scope.links = [{
                        page: "visits",
                        url: $scope.$root.Ajax.url + "/self/visits",
                        title: Translator.get("Visits"),
                        icon: "lo lo-user-cursor"
                    }, {
                        page: "visited",
                        url: $scope.$root.Ajax.url + "/self/visited",
                        title: Translator.get("visited_profiles"),
                        icon: "lo lo-users"
                    }]
            }
        }
        angular.module("Lovoo.Self", ["ngResource", "Lovoo.List", "Lovoo.Settings", "Lovoo.Fileupload"]).constant("WHAZZUP_MAXLENGTH", 90).controller("SelfProfileBoxCtrl", selfProfileBoxCtrl).controller("SelfFirstStepsCtrl", selfFirstStepsCtrl).controller("SelfGalleryCtrl", selfGalleryCtrl).controller("SelfPagesCtrl", selfPagesCtrl), selfProfileBoxCtrl.$inject = ["$scope", "FirstStepsService", "NotificationService", "SelfCountsService"], selfFirstStepsCtrl.$inject = ["$modalInstance", "$scope", "FirstStepsService"], selfGalleryCtrl.$inject = ["$scope", "GalleryService", "UserActionsFctry"], selfPagesCtrl.$inject = ["$routeParams", "$scope", "$rootScope", "DialogTutorialService", "UserActionsFctry"]
    }(),
    function() {
        "use strict";

        function firstStepsDirective(CookieService, DialogService) {
            return {
                restrict: "E",
                replace: !1,
                link: function(scope) {
                    var localStorageKey = "first_steps." + scope.$root.Self.id;
                    if (!(scope.$root.Self.confirmed && scope.$root.Self.picture || CookieService.get(localStorageKey))) {
                        var modal = DialogService.openDialog({
                            backdrop: !0,
                            windowClass: "modal-xlg",
                            controller: "SelfFirstStepsCtrl",
                            templateUrl: "/template/widget/first-steps.html"
                        });
                        modal.result["catch"](function() {
                            CookieService.set(localStorageKey, 1, {
                                expires: 1,
                                resetTime: !0,
                                secure: "https:" === window.location.protocol
                            })
                        })
                    }
                }
            }
        }

        function detailCountDirective() {
            return {
                restrict: "E",
                replace: !1,
                transclude: !1,
                templateUrl: "/template/widget/detail-count.html",
                link: function(scope, ele) {
                    scope.detailsCount = 100 * scope.$root.Self.counts.details, scope.detailsCount = 5 * Math.floor(scope.detailsCount / 5);
                    var pointerWidth = scope.detailsCount <= 95 ? scope.detailsCount - 7 : 95;
                    pointerWidth = Math.max(0, pointerWidth), ele.find("#pointerWidth").css({
                        "margin-left": pointerWidth + "%"
                    }), ele.find("#pointerProgress").css({
                        width: scope.detailsCount + "%"
                    })
                }
            }
        }

        function selfGalleryDirective() {
            return {
                restrict: "E",
                controller: "SelfGalleryCtrl",
                templateUrl: "/template/page/self-gallery.html"
            }
        }
        angular.module("Lovoo.Self").directive("firstSteps", firstStepsDirective).directive("detailCount", detailCountDirective).directive("selfGallery", selfGalleryDirective), firstStepsDirective.$inject = ["CookieService", "DialogService"]
    }(),
    function() {
        "use strict";

        function SelfActionFctry($resource, $rootScope) {
            return {
                freetext: $resource($rootScope.Ajax.api + "/self/freetext", {}, {
                    put: {
                        method: "PUT",
                        params: {
                            freetext: "@freetext"
                        }
                    }
                })
            }
        }

        function SelfProto(SelfActionFctry, UserProto) {
            var Self = function(userObject) {
                UserProto.call(this, userObject), this.isSelf = !0
            };
            return Self.prototype = Object.create(UserProto.prototype), Self.prototype.constructor = UserProto, Self.prototype.setFreetext = function(freetext) {
                this.freetext !== freetext && (this.freetext = freetext, SelfActionFctry.freetext.put({
                    freetext: freetext
                }))
            }, Self
        }
        angular.module("Lovoo.Self").factory("SelfActionFctry", SelfActionFctry).factory("SelfProto", SelfProto), SelfActionFctry.$inject = ["$resource", "$rootScope"], SelfProto.$inject = ["SelfActionFctry", "UserProto"]
    }(),
    function() {
        "use strict";

        function SelfQueryFctry($resource) {
            return {
                firstSteps: $resource(Ajax.api + "/self/confirmmail", {}, {
                    post: {
                        method: "POST"
                    }
                }),
                self: $resource(Ajax.api + "/self", {}, {
                    get: {
                        method: "GET"
                    }
                })
            }
        }

        function GalleryService($collection, $rootScope, DialogService, PicturesQuery, ToasterService, UserQueryFctry) {
            var hasNoPicture, loading = !1,
                self = this,
                imageCollection = $collection.getInstance({
                    idAttribute: "id"
                });
            this.setFirst = function(pictureId) {
                PicturesQuery.setFirst({
                    userId: $rootScope.Self.id,
                    pictureEntry: pictureId
                }).$promise.then(function() {
                    self.updateSelfMainPicture(pictureId), ToasterService.open("success", Translator.get("new_profile_pic"), Translator.get("changes_saved"))
                })
            }, this.get = function() {
                return imageCollection.size() || loading || hasNoPicture || (loading = !0, UserQueryFctry.single.get({
                    userId: $rootScope.Self.id,
                    category: "pictures"
                }).$promise.then(function(data) {
                    imageCollection.addAll(data.response.result, {
                        reverse: !0
                    }), imageCollection.size() || (hasNoPicture = !0), loading = !1
                })), imageCollection
            }, this.remove = function(image, callback) {
                DialogService.openDialog({}, {
                    title: Translator.get("delete_pic"),
                    content: Translator.get("delete_confirm"),
                    callback: function() {
                        PicturesQuery.remove({
                            userId: $rootScope.Self.id,
                            pictureEntry: image.id
                        }).$promise.then(function(response) {
                            imageCollection.remove(response), callback()
                        })
                    }
                })
            }, this.updateSelfMainPicture = function(pictureId) {
                $rootScope.Self.picture = pictureId
            }
        }

        function SelfCountsService(UserQueryFctry) {
            var _counts = {};
            this.get = function() {
                return _counts
            }, this.update = function(key, update) {
                angular.extend(_counts[key], update)
            }, this.set = function(user, key, update) {
                update = update || !1;
                var counts = {},
                    queries = {
                        h: {
                            path: "self/hotties/me",
                            params: {
                                resultLimit: 1,
                                preview: !0
                            }
                        },
                        k: {
                            path: "users/:userId/kiss",
                            params: {
                                userId: user.id,
                                resultLimit: 1,
                                preview: !0
                            }
                        },
                        v: {
                            path: "users/:userId/visits",
                            params: {
                                userId: user.id,
                                resultLimit: 1,
                                preview: !0
                            }
                        },
                        mh: {
                            path: "matches/hit",
                            params: {
                                resultLimit: 1,
                                preview: !0
                            }
                        },
                        mv: {
                            path: "matches/wantyou",
                            params: {
                                resultLimit: 1,
                                preview: !0
                            }
                        },
                        yw: {
                            path: "matches/youwant",
                            params: {
                                resultLimit: 1,
                                preview: !0
                            }
                        }
                    };
                if (key) {
                    var query = queries[key];
                    queries = {}, queries[key] = query
                }
                _.each(queries, function(value, key) {
                    counts[key] = {
                        "new": !1,
                        count: user.counts[key]
                    }
                }), UserQueryFctry.info.get(queries).$promise.then(function(data) {
                    var response = data[0],
                        i = 0;
                    for (var query in queries) {
                        if (response[i].response) {
                            var result = response[i].response.result;
                            result.length && angular.extend(counts[query], {
                                pictureId: result[0].user.picture
                            }, update && query === key ? update : {})
                        }++i
                    }
                    angular.equals(counts, _counts) || angular.extend(_counts, counts)
                }), _.size(_counts) || angular.extend(_counts, counts)
            }
        }

        function FirstStepsService(WHAZZUP_MAXLENGTH, $rootScope, DialogService, SettingsService, ToasterService, UserQueryFctry) {
            var self = this;
            this.sendConfirmMail = SettingsService.sendConfirmMail, this.WHAZZUP_MAXLENGTH = WHAZZUP_MAXLENGTH, this.setWhazzupMessage = function(form) {
                form.$viewValue === form.whazzup && form.whazzup || UserQueryFctry.self.put({
                    type: "whazzup",
                    whazzup: form.whazzup
                }).$promise.then(function() {
                    $rootScope.Self.whazzup = form.whazzup
                })
            }, this.openWhazzupMessageForm = function() {
                DialogService.openDialog({
                    size: "sm",
                    templateUrl: "/template/dialog/edit-whazzup.html",
                    controller: function($scope, DialogTutorialService) {
                        $scope.WHAZZUP_MAXLENGTH = self.WHAZZUP_MAXLENGTH, $scope.displayError = {}, $scope.displayFieldError = function(field) {
                            $scope.displayError[field] = !0
                        }, $scope.whazzup = $rootScope.Self.whazzup, $scope.doSubmit = function(form) {
                            form.whazzup.$invalid || UserQueryFctry.self.put({
                                type: "whazzup",
                                whazzup: form.whazzup.$modelValue
                            }).$promise.then(function() {
                                $rootScope.Self.whazzup = form.whazzup.$modelValue, ToasterService.open("success", Translator.get("edit_status"), Translator.get("changes_saved")), $scope.$close()
                            })
                        }, DialogTutorialService.loadTutorialIfEnabled("tutorial_whazzup")
                    }
                })
            }
        }

        function VipBenefitService(IS_NOT_VIP_STORAGE_KEY, $rootScope, StorageService, DialogService, UserActionsFctry, UserQueryFctry) {
            function loadVisitorsAndShowVipBenefitModal() {
                UserQueryFctry.self.get({
                    subType: "visited",
                    resultLimit: 9,
                    preview: !0
                }).$promise.then(showVipBenefitModal)
            }

            function showVipBenefitModal(response) {
                var userVisits = response.response.result,
                    showDefaultImage = userVisits.length < 6,
                    dialogOptions = {
                        backdrop: !0,
                        templateUrl: "/template/dialog/vip-benefits.html",
                        controller: function($scope) {
                            $scope.action = UserActionsFctry.action, $scope.showDefaultImage = showDefaultImage, $scope.users = [], _.each(userVisits, function(userVisit) {
                                $scope.users.push(userVisit.user)
                            })
                        }
                    };
                showDefaultImage ? dialogOptions.windowClass = "modal-xs" : dialogOptions.size = "sm", DialogService.openDialog(dialogOptions)
            }
            this.checkIsNewVip = function(updateUser) {
                var storageKey, storageValue;
                void 0 !== updateUser.vip && void 0 !== $rootScope.Self.id && (storageKey = IS_NOT_VIP_STORAGE_KEY + "." + $rootScope.Self.id, storageValue = StorageService.get(storageKey), 0 === updateUser.vip && 1 !== storageValue ? StorageService.set(storageKey, 1) : 1 === updateUser.vip && 1 === storageValue && (StorageService.remove(storageKey), loadVisitorsAndShowVipBenefitModal()))
            }
        }

        function UpdateSelfService($injector, $rootScope, $timeout, ListService, SelfQueryFctry, ToasterService, VipBenefitService) {
            function toastLastCreditsHistory(firstToast) {
                function callback(data) {
                    if (0 !== data.length) {
                        var item = data[0];
                        if (item.time !== lastCreditsHistoryTime) {
                            lastCreditsHistoryTime = item.time;
                            var itemType = "freecredits" === item.type ? item.subtype : item.type;
                            if (firstToast) {
                                if ("dailylogin" !== itemType) return;
                                if ($rootScope.lastResponseTime - item.time > 10) return
                            }
                            switch (itemType) {
                                case "dailylogin":
                                    $timeout(function() {
                                        ToasterService.open("success", Translator.get("get_credits.dailylogin", {
                                            credits: item.credits
                                        }))
                                    }, 2e3);
                                    break;
                                case "sponsorpay":
                                case "supersonicads":
                                case "trialpay":
                                    var providers = {
                                        sponsorpay: "Sponsorpay",
                                        supersonicads: "SupersonicAds",
                                        trialpay: "TrialPay"
                                    };
                                    ToasterService.open("success", Translator.get("get_credits.freecredits", {
                                        credits: item.credits,
                                        provider: providers[itemType]
                                    }))
                            }
                        }
                    }
                }
                var typeService = ListService.initCollection("credits");
                typeService.load({
                    type: "credits",
                    resultLimit: 1,
                    resultPage: 1
                }, !0, !1, callback)
            }
            var lastCreditsHistoryTime, self = this;
            this.requestAndUpdateSelfUser = function() {
                return SelfQueryFctry.self.get().$promise.then(function(data) {
                    self.update(data.response.self)
                })
            }, this.update = function(updateUser) {
                var isFirstUpdate = void 0 === $rootScope.Self;
                if (void 0 !== updateUser) {
                    if (void 0 !== updateUser.credits && (isFirstUpdate || !isFirstUpdate && updateUser.credits > $rootScope.Self.credits) && toastLastCreditsHistory(isFirstUpdate), isFirstUpdate) {
                        var SelfProto = $injector.get("SelfProto");
                        $rootScope.Self = new SelfProto(updateUser)
                    } else angular.extend($rootScope.Self, updateUser);
                    VipBenefitService.checkIsNewVip(updateUser)
                }
            }
        }
        angular.module("Lovoo.Self").constant("IS_NOT_VIP_STORAGE_KEY", "is_not_vip").factory("SelfQueryFctry", SelfQueryFctry).service("GalleryService", GalleryService).service("SelfCountsService", SelfCountsService).service("FirstStepsService", FirstStepsService).service("VipBenefitService", VipBenefitService).service("UpdateSelfService", UpdateSelfService), SelfQueryFctry.$inject = ["$resource"], GalleryService.$inject = ["$collection", "$rootScope", "DialogService", "PicturesQuery", "ToasterService", "UserQueryFctry"], SelfCountsService.$inject = ["UserQueryFctry"], FirstStepsService.$inject = ["WHAZZUP_MAXLENGTH", "$rootScope", "DialogService", "SettingsService", "ToasterService", "UserQueryFctry"], VipBenefitService.$inject = ["IS_NOT_VIP_STORAGE_KEY", "$rootScope", "StorageService", "DialogService", "UserActionsFctry", "UserQueryFctry"], UpdateSelfService.$inject = ["$injector", "$rootScope", "$timeout", "ListService", "SelfQueryFctry", "ToasterService", "VipBenefitService"]
    }()
});