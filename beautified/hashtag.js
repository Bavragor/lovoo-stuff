/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "feed"], function(angular, feed) {
    ! function() {
        "use strict";

        function HashtagPageCtrl(FEED_PHOTO_WIDTH, $routeParams, $scope, ApproutingService, FeedRequestService, ImageService) {
            $scope.$root.activePage = "hashtag", $scope.hashtagInfo = null, $scope.tryApprouting = ApproutingService.openAppOrStoreForMobileOrRegisterForDesktopGuests, $scope.hashtag = "" !== $scope.$root.uniquePageData ? $scope.$root.uniquePageData.hashtag : $routeParams.hashtag, $scope.Math = window.Math, $scope.thumbWidth = 110, $scope.thumbBorder = 2, $scope.thumbPadding = 14, $scope.thumbResolution = FEED_PHOTO_WIDTH, FeedRequestService.getHashtagInfo($scope.hashtag).then(function(data) {
                $scope.hashtagInfo = {
                    allCount: data.response.result.count,
                    coverSrc: ImageService.getImageUrlFromImageArray({
                        width: 704,
                        images: data.response.result.previewImage || []
                    })
                }
            }), $scope.feedOptions = FeedRequestService.getFeedOptions(function() {
                FeedRequestService.getFeed({
                    tags: $scope.hashtag,
                    resultPage: $scope.feedOptions.resultPage
                }).then(function(feed) {
                    $scope.feedOptions.updateFeed(feed), $scope.feedOptions.feed.setFirstPhotoToMostPopular()
                })
            })
        }
        angular.module("Lovoo.Hashtag", ["Lovoo.Approuting", "Lovoo.Feed"]).controller("HashtagPageCtrl", HashtagPageCtrl), HashtagPageCtrl.$inject = ["FEED_PHOTO_WIDTH", "$routeParams", "$scope", "ApproutingService", "FeedRequestService", "ImageService"]
    }()
});