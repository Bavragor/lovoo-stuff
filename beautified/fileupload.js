/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular"], function(angular) {
    "use strict";

    function photoUploadCtrl($scope, FileUploadService, GalleryService) {
        $scope.uploadProgress = !1, $scope.btnDisabled = !0, $scope.doUpload = function(setFirst) {
            FileUploadService.doUpload({
                data: {
                    first: !$scope.$root.Self.picture || setFirst ? "true" : "false"
                }
            }, function(evt) {
                $scope.uploadProgress = parseInt(100 * evt.loaded / evt.total)
            }, function(evt) {
                $scope.uploadProgress = 0, $scope.error = evt.data && evt.data.statusMessage ? evt.data.statusMessage : Translator.get("pic_error_info")
            }, function(data) {
                var uploadedPic = data.response;
                GalleryService.get().add(uploadedPic), $scope.btnDisabled = !1, $scope.uploadProgress = 100, $scope.$close(), setFirst && ($scope.$root.Self.picture = data.response.id)
            })
        }
    }

    function photoUploadButtonCtrl($scope, FileUploadService) {
        $scope.selectFile = function(file) {
            FileUploadService.selectFile(file, void 0 !== $scope.hideDialog)
        }
    }

    function photoUploadButton() {
        return {
            restrict: "E",
            replace: !1,
            controller: "PhotoUploadButtonCtrl",
            scope: {
                watchId: "@",
                hideDialog: "@"
            },
            template: '<i class="fa fa-camera space-right-sm"></i>{{::"upload_pic"|translate}}<input type="file" ng-file-select="selectFile($files)" required="required" name="picture" id="{{::watchId}}">'
        }
    }

    function fileUploadPreview(FileUploadService) {
        var size = 128;
        return {
            restrict: "E",
            replace: !1,
            scope: {
                imgsizing: "@"
            },
            template: '<img ng-if="file" ng-cloak ng-src="{{file.thumbnailUrl}}"/>',
            link: function($scope, ele, attrs) {
                var fileInput = attrs.watchInput;
                return $scope.imgsizing && (size = $scope.imgsizing), fileInput ? ($scope.file = {}, void $scope.$watchCollection(function() {
                    var fileInputEle = angular.element(fileInput)[0];
                    return fileInputEle ? fileInputEle.files : !1
                }, function(newVal) {
                    $scope.file = newVal[0], FileUploadService.getPreview($scope.file, size, function(url) {
                        attrs.$set("width", size), ele.css({
                            width: size
                        }), $scope.$apply(function() {
                            $scope.file.thumbnailUrl = url
                        }), $scope.$digest()
                    })
                })) : (console.log("Couldn't find file input with selector: " + attrs.watchInput), !1)
            }
        }
    }
    angular.module("Lovoo.Fileupload", ["angularFileUpload"]).constant("FILE_FORMAT_VALID", ["jpg", "jpeg", "png", "bmp", "gif"]).controller("PhotoUploadCtrl", photoUploadCtrl).controller("PhotoUploadButtonCtrl", photoUploadButtonCtrl), photoUploadCtrl.$inject = ["$scope", "FileUploadService", "GalleryService"], photoUploadButtonCtrl.$inject = ["$scope", "FileUploadService"], angular.module("Lovoo.Fileupload").directive("photoUploadButton", photoUploadButton).directive("fileUploadPreview", fileUploadPreview), fileUploadPreview.$inject = ["FileUploadService"],
        function() {
            function PicturesQuery($resource) {
                return $resource(Ajax.api + "/users/:userId/pictures", {
                    isArray: !0,
                    cache: !0
                }, {
                    get: {
                        method: "GET",
                        params: {
                            userId: "@userId",
                            ignoreLoadingBar: !0
                        }
                    },
                    remove: {
                        url: Ajax.api + "/users/:userId/pictures/:pictureEntry",
                        method: "DELETE",
                        params: {
                            userId: "@userId",
                            pictureEntry: "@pictureEntry"
                        }
                    },
                    setFirst: {
                        url: Ajax.api + "/users/:userId/pictures/:pictureEntry/first",
                        method: "PUT",
                        params: {
                            userId: "@userId",
                            pictureEntry: "@pictureEntry"
                        }
                    }
                })
            }

            function FileUpload(FILE_FORMAT_VALID, $upload, $window, DialogService, ToasterService) {
                function isImageTypeValid(files) {
                    for (var regEx = new RegExp("image/(" + FILE_FORMAT_VALID.join("|") + ")$"), allValid = !0, i = files.length - 1; i >= 0; i--)
                        if (void 0 !== files[i].type && !files[i].type.match(regEx)) {
                            allValid = !1;
                            break
                        }
                    return allValid
                }

                function uploadFile(options) {
                    var callback = angular.identity,
                        opts = angular.extend({
                            transformRequest: function(val) {
                                return val
                            }
                        }, options);
                    return opts.onError = opts.onError || angular.identity, opts.progress = opts.progress || angular.identity, opts.onSuccess = opts.onSuccess || angular.identity, self.hideDialog || (callback = function() {
                        var modal = DialogService.openMessage(Translator.get("upload_pic"), Translator.get("pic_uploaded_info"));
                        modal.result["finally"](function() {
                            return window.location.href = window.location.pathname
                        })
                    }), $upload.upload(opts).success(opts.onSuccess).error(opts.onError).progress(opts.progress).then(callback)
                }
                var self = this,
                    isHTML5UploadEnabled = void 0 !== $window.FormData;
                this.files = [], this.hasPreview = !1, this.selectFile = function(files, hideDialog) {
                    return isImageTypeValid(files) ? (self.files = files, self.errors = "", self.hasPreview = !0, self.hideDialog = hideDialog, void(hideDialog || DialogService.openDialog({
                        templateUrl: "/template/dialog/photo-upload.html",
                        controller: "PhotoUploadCtrl",
                        windowClass: "modal-xs"
                    }))) : void ToasterService.open("error", Translator.get("fileupload.invalid.title"), Translator.get("fileupload.invalid.text", {
                        formats: FILE_FORMAT_VALID.join(", ")
                    }))
                }, this.getPreview = function(file, size, callback) {
                    if (file && 0 === file.type.indexOf("image")) {
                        var reader = new FileReader;
                        reader.onload = function(e) {
                            var img = $("<img>", {
                                    src: e.target.result
                                }),
                                cnvs = $('<canvas width="' + size + '" height="' + size + '"></canvas>')[0],
                                context = cnvs.getContext("2d");
                            img.load(function() {
                                context.drawImage(this, 0, 0, size, size * this.height / this.width), callback && callback(cnvs.toDataURL())
                            })
                        }, reader.readAsDataURL(file)
                    }
                }, this.doUpload = function(options, progressCallback, errorCallback, successCallback) {
                    if (isHTML5UploadEnabled)
                        for (var uploadParams = angular.extend({
                                url: Ajax.api + "/users/" + Self.id + "/pictures",
                                fileFormDataName: "picture",
                                progress: progressCallback || {},
                                onError: errorCallback || {},
                                onSuccess: successCallback || {}
                            }, options), i = 0; i < self.files.length; ++i) uploadParams.file = self.files[i], uploadFile(uploadParams)
                }
            }
            angular.module("Lovoo.Fileupload").factory("PicturesQuery", PicturesQuery).service("FileUploadService", FileUpload), PicturesQuery.$inject = ["$resource"], FileUpload.$inject = ["FILE_FORMAT_VALID", "$upload", "$window", "DialogService", "ToasterService"]
        }()
});