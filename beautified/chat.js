/*! lovoo - v3.1.0 - 2015-04-01 06:04:40 - Copyright LOVOO GmbH */
define(["angular", "location"], function(angular, location) {
    "use strict";

    function ListController($scope, UserActionsFctry, convActionService, page) {
        function renderConversation(data) {
            var lastMsgId;
            switch ($scope.type || activePage) {
                case page.chat:
                    lastMsgId = single.getLocal() ? single.getLocal() : data.id || null;
                    break;
                case page.request:
                    lastMsgId = data.id || null
            }
            list[activePage].get(lastMsgId) || (lastMsgId = data.id || null), $scope.active = lastMsgId, single.query(lastMsgId)
        }
        var list, single, activePage, self = this;
        $scope.startOnOpen && convActionService.startOnOpen(!0), list = convActionService.list, single = convActionService.single, $scope.next = convActionService.nextPage, this.single = function(id, index) {
            $scope.startOnOpen && ($scope.sidebarOpen = !1), id === single.getLocal() && ++single.page, void 0 !== index && (self.listIndex = index, single.page = 1), single.query(id, single.page)
        }, this.remove = function(id, next) {
            id === single.getLocal() && single.setLocal(next), list["delete"](id, next, function() {
                $scope.active = single.getLocal()
            })
        }, this.action = UserActionsFctry.action, this.filter = [{
            name: "chat_first_latest",
            value: "lastMessage.time"
        }, {
            name: "chat_first_online",
            value: "[user.isOnline, user.isMobile]"
        }, {
            name: "chat_first_unread",
            value: "countNewMessages"
        }], this.orderFilter = this.filter[0], $scope.$watch(function() {
            return list.loaded
        }, function(data) {
            self.loaded = data
        }), $scope.$watch("type", function(data, old) {
            angular.equals(data, old) || (single.reset(), $scope.active = "", self.orderFilter = self.filter[0]), activePage = data, convActionService.page = data
        }), $scope.$watchCollection(function() {
            var conversations = list[activePage];
            return conversations && !conversations.length && _.size(single.get()) && single.set(), conversations
        }, function(data) {
            if (void 0 !== data && self.loaded && (self.conversations = data.all(), _.size(data.last()) && !$scope.active && renderConversation(data.last()), !data.size())) switch (self.nothingFound = [], $scope.type || activePage) {
                case page.chat:
                    self.nothingFound[page.chat] = !0;
                    break;
                case page.request:
                    self.nothingFound[page.request] = !0
            }
        })
    }

    function SingleController($scope, $timeout, convActionService, DialogTutorialService, UserActionsFctry, StorageService, ToasterService, LocationFieldService, page) {
        var list, single, self = this,
            root = $scope.$root,
            constantSubmitOnEnter = root.Self.id + "_submitOnEnter",
            fileTypesSupported = ["jpg", "jpeg", "png", "bmp", "gif"],
            fileReaderSupported = null !== window.FileReader && (null === window.FileAPI || FileAPI.html5 !== !1),
            messageReset = function() {
                self.message = {
                    pinned: !1,
                    text: ""
                }
            };
        $scope.counts = {}, list = convActionService.list, single = convActionService.single, messageReset(), this.confirmTopchat = function(eve) {
            var ele = angular.element(eve.target);
            self.message.pinned = ele.prop("checked"), self.message.pinned && (DialogTutorialService.setCallbackForExecutingTutorialAction($scope.$close), DialogTutorialService.loadTutorialIfEnabled("tutorial_topchat"), root.Self.vip || ToasterService.open("info", Translator.get("activate_topchat"), Translator.get("txt.topchat_text", {
                credits: root.CostCredits.topchat
            })))
        }, this.toggleSubmit = function() {
            var toggle = !self.submitOnEnter;
            StorageService.set(constantSubmitOnEnter, toggle), self.submitOnEnter = toggle
        }, self.fileError = !1, this.dragAttachment = function($event) {
            var regExp = new RegExp("image/(" + fileTypesSupported.join("|") + ")$"),
                items = $event.dataTransfer.items,
                hasFile = self.fileError = !1;
            if (null !== items) {
                for (var i = 0; i < items.length; i++)
                    if ("file" === items[i].kind && items[i].type.match(regExp)) {
                        hasFile = !0;
                        break
                    }
            } else hasFile = !0;
            return self.fileError = hasFile, $scope.$digest(), hasFile ? "dragover" : "dragover error"
        }, this.onFileSelect = function($files) {
            var regExp = new RegExp("image/(" + fileTypesSupported.join("|") + ")$"),
                scopeMessage = self.message;
            if (!$files[0].type.match(regExp)) return void ToasterService.open("error", Translator.get("fileupload.invalid.title"), Translator.get("fileupload.invalid.text", {
                formats: fileTypesSupported.join(", ")
            }));
            scopeMessage.dataUrls = [], scopeMessage.attachments = $files;
            for (var i = 0; i < $files.length; ++i) {
                var $file = $files[i];
                if (fileReaderSupported && $file.type.indexOf("image") > -1) {
                    var fileReader = new FileReader;
                    fileReader.readAsDataURL($files[i]),
                        function(fileReader, index) {
                            fileReader.onload = function(e) {
                                $timeout(function() {
                                    scopeMessage.dataUrls[index] = e.target.result
                                })
                            }
                        }(fileReader, i)
                } else scopeMessage.dataUrls = [], scopeMessage.attachments = []
            }
        }, this.fileSize = function(bytes) {
            if (0 === bytes) return "0 Byte";
            var k = 1e3,
                sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                i = Math.floor(Math.log(bytes) / Math.log(k));
            return (bytes / Math.pow(k, i)).toPrecision(3) + " " + sizes[i]
        }, $scope.accepted = !1, this.submitOnEnter = StorageService.get(constantSubmitOnEnter) || !1, this.attachmentLocation = function(coordinates) {
            var params = {
                lat: coordinates.latitude,
                "long": coordinates.longitude
            };
            return LocationFieldService.getCityByCoordinates(params).then(function(data) {
                return data[0].city
            })
        }, this.removeAttachment = function() {
            delete self.message.dataUrls, delete self.message.attachments, delete single.picture
        }, this.reject = function(id, requestList, index) {
            var nextRequest = requestList[index + 1] || requestList[index - 1] || {};
            single.reset(), $scope.active = nextRequest.id, list.reject(id, nextRequest.id)
        }, this.removeMessage = single.removeMsg, this.reportUser = function() {
            var options = {
                text: Translator.get("last_msg") + ": " + self.messages.last().content,
                user: self.conversation.user,
                referenceType: "user"
            };
            UserActionsFctry.action(self.conversation.user, "report", options)
        }, this.send = function(id) {
            var send = single.send(self.message, id, $scope.type, $scope.$root.Self.id);
            self.sendInProgress = !0, send.then(function() {
                if ($scope.scrollOnSend(), self.sendInProgress = !1, self.message.pinned && ToasterService.open("info", Translator.get("topchat"), Translator.get("topchat.send")), $scope.type === page.request) {
                    var oldConv = single.get();
                    $scope.type = page.chat, single.set(oldConv)
                }
                messageReset()
            }, function() {
                self.sendInProgress = !1
            })
        }, $scope.$watch(function() {
            return single.loaded
        }, function(data) {
            self.loaded = data
        }), $scope.$watchCollection(function() {
            return single.get()
        }, function(data, old) {
            if ($scope.type === page.request && ($scope.accepted = !1), _.size(data) && angular.equals(data, old)) angular.extend(self, data);
            else if (!_.size(data) || angular.equals(data, old) || angular.equals(data.conversation, old.conversation)) !_.size(data) && _.size(old) && (delete self.conversation, delete self.messages);
            else {
                var conversation = data.conversation;
                if (conversation.id !== single._id) return;
                delete self.messages, $scope.user = conversation.user, self.message = {}, $scope.type !== page.request && conversation && conversation.id && single.getLocal() !== conversation.id && single.setLocal(conversation.id), (_.size(old) && data.conversation.id !== old.conversation.id || !_.size(old)) && (self.page = 1), DialogTutorialService.setCallbackForExecutingTutorialAction($scope.$close), DialogTutorialService.loadTutorialIfEnabled("tutorial_readconfirm")
            }
            $timeout(function() {
                angular.extend(self, data)
            }, 0)
        })
    }

    function TabController($scope, NotificationService, page) {
        var self = this,
            notifications = ["m", "mr"];
        this.counts = {}, this.links = [{
            page: page.chat,
            title: "my_chats",
            icon: "fa fa-comments",
            badge: "m"
        }, {
            page: page.request,
            title: "chat_requests",
            icon: "lo lo-comment-hi",
            badge: "mr"
        }], $scope.$parent.type = this.links[0].page, NotificationService.subscribe(notifications, function() {
            var noti = this;
            _.each(notifications, function(key) {
                self.counts[key] = noti[key]
            })
        }), $scope.$parent.counts = this.counts
    }

    function RequestController($scope, $window, convActionService, DialogService, DialogTutorialService, ToasterService, vcRecaptchaService) {
        function checkMessagesCredits() {
            convActionService.single.chatrequestCredits({
                userId: $scope.user.id
            }).$promise.then(function(data) {
                data.response.dialog && ToasterService.open("info", data.response.dialog.title, data.response.dialog.text, {
                    ttl: 1e4
                })
            })
        }
        $scope.displayError = {}, $scope.btnDisabled = !1, $scope.displayFieldError = function(field) {
            $scope.displayError[field] = !0
        }, $scope.addCaptcha = 0, $scope.sendRequest = function() {
            var params = {
                userId: $scope.user.id,
                text: $scope.message,
                pinned: $scope.pinned ? 1 : 0
            };
            $.each(vcRecaptchaService.data(), function(key, val) {
                params["recaptcha_" + key + "_field"] = val
            }), $scope.btnDisabled = !0, convActionService.single.request(params).$promise.then(function(data) {
                $scope.btnDisabled = !1;
                var response = data.response.result;
                if (_.size(response.errors) > 0) {
                    var errors = response.errors;
                    _.each(errors, function(error, key) {
                        2277 === error.code && DialogService.openMessage(Translator.get("action_interrupted"), error.message, {
                            buttons: [{
                                cssClass: "btn btn-warning",
                                label: Translator.get("become_vip"),
                                click: function() {
                                    $window.location.href = Ajax.url + "/vip"
                                }
                            }]
                        })
                    }), $scope.validationErrors = errors, response.addCaptcha && ($scope.addCaptcha = response.addCaptcha, vcRecaptchaService.reload(!0))
                } else $scope.$close(), ToasterService.open("success", Translator.get("msg_sent_to", {
                    user: $scope.user.name
                }))
            })
        }, $scope.confirmTopchat = function(eve) {
            var ele = angular.element(eve.target);
            $scope.pinned = ele.prop("checked"), $scope.pinned && (DialogTutorialService.setCallbackForExecutingTutorialAction($scope.$close), DialogTutorialService.loadTutorialIfEnabled("tutorial_topchat"), $scope.$root.Self.vip || ToasterService.open("info", Translator.get("activate_topchat"), Translator.get("txt.topchat_text", {
                credits: $scope.$root.CostCredits.topchat
            })))
        }, checkMessagesCredits()
    }

    function conversationListDirective() {
        return {
            restrict: "E",
            transclude: !1,
            replace: !1,
            controllerAs: "convList",
            controller: "convUserListCtrl",
            templateUrl: "/template/chat/list.html"
        }
    }

    function conversationSingleDirective() {
        return {
            restrict: "E",
            transclude: !1,
            replace: !1,
            controllerAs: "convSingle",
            controller: "convUserSingleCtrl",
            templateUrl: "/template/chat/messages.html"
        }
    }

    function stickToBottomDirective() {
        function scrollToBottom(ele) {
            ele.hasClass("scroll-pane") ? (ele.data("jsp").scrollToPercentY(1), ele.scrollTop() + ele.innerHeight() == ele[0].scrollHeight && (prevent = !0)) : ele.scrollTop(ele[0].scrollHeight)
        }
        var prevent;
        return {
            priority: 1,
            restrict: "A",
            link: function(scope, ele, attr) {
                var stickyScroll = !0;
                prevent && (prevent = !1), scope.$parent.$parent.scrollOnSend = function() {
                    scrollToBottom(ele)
                }, scope.$watch(function() {
                    scope.$eval(attr.stickToBottom) && stickyScroll && scrollToBottom(ele)
                }), ele.bind("scroll", function(e) {
                    stickyScroll = !prevent && ele.scrollTop() + ele.innerHeight() >= ele[0].scrollHeight
                }), scope.$on("destroy", function() {
                    ele.unbind("scroll")
                })
            }
        }
    }

    function chatListRefreshDirective($window) {
        return function(scope, ele, attr) {
            function handler() {
                var pane = ele.data("jsp"),
                    containerBottom = ele.offset().top + ele.height(),
                    scrollUnitHeight = $(".media.chat", ele).height(),
                    realScrollTop = ele.scrollTop() + ele.height(),
                    reloadPass = containerBottom - scrollUnitHeight,
                    shouldScroll = realScrollTop >= reloadPass;
                (shouldScroll || pane && 100 * pane.getPercentScrolledY() >= 90) && (scope.$root.$$phase ? scope.$eval(attr.chatListRefresh) : scope.$apply(attr.chatListRefresh))
            }
            var func = attr.chatListRefresh.replace(/\(\)/i, "");
            $window = angular.element($window), $.isFunction(scope[func]) && (ele.unbind("scroll").bind("scroll", ele, handler), scope.$on("$destroy", function() {
                ele.unbind("scroll", handler)
            }))
        }
    }

    function flexibleBottomDirective() {
        return function(scope, ele, attr) {
            var prevSibling = ele.prev("form");
            scope.$watch(function() {
                return prevSibling.height()
            }, function(data, old) {
                data && ele.css("bottom", data + "px")
            })
        }
    }

    function checkAttachmentDirective($interval) {
        return function(scope, ele, attr) {
            var newImageSrc, intervalPromise, triedLoading = !1,
                image = new Image;
            scope.isLoading = !0, image.onload = function() {
                attr.$set("src", image.src + "?" + (new Date).getTime()), triedLoading && ele.addClass("image-160"), scope.isLoading = !1, $interval.cancel(intervalPromise), image = null
            }, image.onerror = function() {
                return triedLoading ? void(attr.src = newImageSrc) : (image.src = newImageSrc = image.src.replace(/thumb_l/, "image_l"), void(triedLoading = !0))
            }, attr.src && (image.src = attr.src, intervalPromise = $interval(function() {
                image.src = newImageSrc
            }, 3e4, !1))
        }
    }

    function chatQueryFactory($resource, $batchRequest) {
        return {
            batch: $batchRequest({
                ignoreLoadingBar: !0
            }, {
                get: {
                    method: "GET"
                }
            }),
            list: $resource(Ajax.api + "/conversations/:convId/:subPage", {
                cache: !0
            }, {
                "delete": {
                    params: {
                        convId: "@id"
                    },
                    method: "DELETE"
                }
            }),
            single: $resource(Ajax.api + "/conversations/:convId", {}, {
                set: {
                    url: Ajax.api + "/users/:userId/write",
                    params: {
                        userId: "@userId"
                    },
                    method: "POST"
                },
                get: {
                    url: Ajax.api + "/users/:userId/messages",
                    params: {
                        userId: "@userId"
                    },
                    method: "GET"
                },
                request: {
                    url: Ajax.url + "/chats/:userId/request",
                    method: "POST",
                    params: {
                        userId: "@userId"
                    },
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "X-Requested-With": "XMLHttpRequest"
                    },
                    transformRequest: function(data) {
                        return void 0 !== data ? $.param(data) : data
                    }
                },
                "delete": {
                    url: Ajax.api + "/conversations/:convId/messages/:messageId",
                    params: {
                        convId: "@convId",
                        messageId: "@messageId"
                    },
                    method: "DELETE"
                },
                upload: {
                    url: Ajax.api + "/users/:userId/pictureattachment",
                    method: "POST",
                    isArray: !0,
                    params: {
                        userId: "@userId",
                        data: "@data"
                    },
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            })
        }
    }

    function ConversationActionService($collection, $interval, $upload, query, DialogService, StorageService, PAGE, MESSAGE_LIMIT) {
        function selectNextItemInList(id, oldObj) {
            self.list[self.page].remove(oldObj), self.list[self.page].sort(), !id && self.list[self.page].size() && (id = self.list[self.page].at(self.list[self.page].size() - 1).id), id ? self.single.query(id) : self.single.set()
        }

        function doRequest(reset) {
            var mergedQuery, request = {
                chat: {
                    path: "/conversations",
                    params: {}
                },
                requests: {
                    path: "/conversations",
                    params: {
                        requests: "only"
                    }
                }
            };
            "boolean" == typeof reset && reset && (!singleObj || _.size(singleObj) && !singleObj.conversation.request) && (queries = {}, self.list.set(), self.single.set()), (!self.loading || !self.loading && queries.single && !self.list[self.page].get(queries.single.params.convId)) && (self.loading = !0, queries[self.page] && (self.page === PAGE.request && (queries[self.page].params.requests = "only"), angular.extend(request[self.page], queries[self.page])), mergedQuery = angular.extend(queries, request), void 0 === mergedQuery[self.page] || mergedQuery[self.page].params.resultPage || angular.extend(mergedQuery[self.page].params, {
                since: (new Date).getTime()
            }), query.batch.get(mergedQuery).$promise.then(function(data) {
                var response = data[0],
                    i = 0;
                reset && (self.list[self.page].size() || self.single.get() && self.single.get().conversation) && self.single.set(), response && _.each(mergedQuery, function(value, key) {
                    -1 !== $.inArray(key, [PAGE.chat, PAGE.request]) && mergedQuery[key].params && mergedQuery[key].params.resultPage && (listPage.last = !response[i].response.result.length, disableScroll = !1), response[i] && handleRequest[key] && (!request[key] || request[key] && angular.equals(value.params, request[key].params)) && handleRequest[key].apply(response[i].response), ++i
                }), queries[self.page] && (queries[self.page].params = {}), self.loading = !1
            }))
        }
        var singleObj, intervalPromise, isPopup, disableScroll = !1,
            listPage = {
                current: 1,
                last: !1
            },
            self = this,
            queries = {},
            storedConversations = {},
            handleRequest = {
                chat: function() {
                    self.list.set(PAGE.chat, this), self.list.loaded = !0
                },
                requests: function() {
                    self.list.set(PAGE.request, this), self.list.loaded = !0
                },
                single: function() {
                    self.single.set(this), self.single.loaded = !0
                }
            };
        this.loading = !1, this.nextPage = function() {
            if (self.list[self.page].size() && !listPage.last && self.list.loaded && !disableScroll) {
                var pageQuery = {};
                disableScroll = !0, pageQuery[self.page] = {
                    params: {
                        resultPage: ++listPage.current
                    }
                }, angular.extend(queries, pageQuery), doRequest()
            }
        }, this.openChatDialog = function() {
            var modal = DialogService.openDialog({
                templateUrl: "/template/dialog/conversations.html",
                backdrop: !0,
                windowClass: "modal-chat"
            }, {
                popup: function() {
                    var popupOptions = {
                        fullscreen: 0,
                        titlebar: 0,
                        width: 1080,
                        resizable: 1,
                        height: 565,
                        scrollbars: 0
                    };
                    popupOptions = $.map(Object.getOwnPropertyNames(popupOptions), function(v) {
                        return [v, popupOptions[v]].join("=")
                    }).join(","), window.name = "main";
                    var popup = window.open(Ajax.url + "/chat", "_blank", popupOptions);
                    popup.onbeforeunload = function() {
                        self.openChatDialog()
                    }, modal.close()
                }
            });
            modal.result["finally"](function() {
                listPage = {
                    current: 1,
                    last: !1
                }, self.list.set(), self.single.set(), queries = {}, $interval.cancel(intervalPromise)
            }), self.page = "chat", self.startOnOpen(!1)
        }, this.startOnOpen = function(popup) {
            doRequest(!0), intervalPromise = $interval(doRequest, 6e3, 0, !1), isPopup = popup || !1
        }, this.list = {
            loaded: !1,
            chat: $collection.getInstance({
                idAttribute: "id"
            }),
            requests: $collection.getInstance({
                idAttribute: "id"
            }),
            set: function(type, data) {
                return data ? (this[type].addAll(data.result), this) : (this.loaded = !1, type ? this[type].removeAll() : (this.chat.removeAll(), this.requests.removeAll()), this)
            },
            reject: function(id, next) {
                var nextObj, obj = self.list[self.page].get(id);
                delete queries.single, self.list[self.page].sort();
                for (var i = 0, length = self.list[self.page].size(); length > i; ++i)
                    if (self.list[self.page].at(i).id === id) {
                        var index = i === length - 1 ? i - 1 : i + 1;
                        nextObj = self.list[self.page].at(index);
                        break
                    }
                return query.list["delete"]({
                    convId: id
                }).$promise.then(function() {
                    selectNextItemInList(next, obj)
                })
            },
            "delete": function(id, next, cb) {
                var singleQuery = queries.single;
                delete queries.single;
                var modal = DialogService.openDialog({}, {
                    closeLabel: Translator.get("cancel"),
                    content: Translator.get("delete_chat"),
                    title: Translator.get("delete_confirm"),
                    callback: function() {
                        var obj = self.list[self.page].get(id);
                        query.list["delete"]({
                            convId: id
                        }).$promise.then(function() {
                            selectNextItemInList(self.single.getLocal(), obj), cb()
                        })
                    }
                });
                modal.result["catch"](function() {
                    void 0 !== next && angular.extend(queries, {
                        single: singleQuery
                    })
                })
            }
        }, this.single = {
            page: 1,
            loaded: !1,
            query: function(id, page) {
                var loadStoredConversation = !1;
                return _.size(singleObj) && !page && singleObj.conversation.id !== id && (self.single.reset(), storedConversations[id] && (loadStoredConversation = !0)), null === id ? (self.single._id = null, this) : (self.single._id = id, angular.extend(queries, {
                    single: {
                        path: "/conversations/:convId",
                        params: {
                            convId: id,
                            resultLimit: MESSAGE_LIMIT,
                            resultPage: page || 1
                        }
                    }
                }), loadStoredConversation ? (self.single.set(), self.single.set(storedConversations[id])) : (self.single.loaded = !1, doRequest()), this)
            },
            get: function() {
                return singleObj
            },
            set: function(data) {
                var messages = data ? data.messages || data.result : data;
                if (!messages && self.single._id && self.list[self.page].size()) return self.single.query(self.single._id), this;
                if (data === window) return self.single.reset(), this;
                if (this.loaded = !0, data && data.conversation && (self.page === PAGE.request && !data.conversation.request || self.page === PAGE.chat && data.conversation.request)) return this.loaded = !1, this;
                if ((!data || data && _.size(singleObj) && singleObj.conversation.id !== data.conversation.id) && (self.single.reset(), !data)) return this;
                if (_.size(singleObj.conversation) ? angular.extend(singleObj.conversation, data.conversation) : singleObj = {
                        conversation: data.conversation,
                        messages: $collection.getInstance({
                            idAttribute: "id"
                        }),
                        complete: !1
                    }, singleObj.complete || (singleObj.complete = !messages.length || messages.length < 14), queries.single && queries.single.params && self.single.page && (queries.single.params.resultPage = 1), messages.hash) singleObj.messages = messages;
                else if (singleObj.messages.size()) {
                    var shouldPrepend, newMessages = [];
                    if (jQuery.grep(messages, function(el) {
                            singleObj.messages.get(el) || newMessages.push(el)
                        }), !newMessages.length) return this;
                    shouldPrepend = newMessages[0].time < singleObj.messages.last().time, singleObj.messages.addAll(messages, {
                        prepend: shouldPrepend
                    })
                } else singleObj.messages.addAll(messages, {
                    reverse: !0
                });
                return self.single.store(data), isPopup && void 0 !== singleObj.conversation.user && $("title").text(singleObj.conversation.user.name), this
            },
            store: function(data, override) {
                var id = data.conversation.id;
                storedConversations[id] && !override ? angular.extend(storedConversations[id], data) : storedConversations[id] = data
            },
            reset: function() {
                self.single.loaded = !1, singleObj = {}
            },
            setLocal: function(id) {
                StorageService.set("conversation", id)
            },
            getLocal: function() {
                return StorageService.get("conversation")
            },
            send: function(message, id, page, selfId) {
                var queryParams = {
                        userId: id,
                        text: message.text,
                        pinned: message.pinned
                    },
                    promiseSendQuery = function(data) {
                        if (singleObj && (singleObj.conversation.pinnedOther = message.pinned ? 1 : 0, singleObj.conversation.readconfirm = 0, singleObj.messages.add(data.response.message, {
                                prepend: !1
                            })), page === PAGE.request) {
                            var chatRequest = singleObj.conversation;
                            self.single.setLocal(chatRequest.id), self.list[PAGE.chat].add(chatRequest), self.list[PAGE.request].remove(self.list[page].get(chatRequest.id))
                        }
                        return singleObj.messages.length > MESSAGE_LIMIT && singleObj.messages.remove(singleObj.messages.at(0)), delete self.single.picture, data
                    };
                if (message.attachments && !self.single.picture) {
                    var pictureUploadQuery = self.single.upload(selfId, message.attachments);
                    return pictureUploadQuery.then(function(response) {
                        return self.single.picture = response.data.response.id, query.single.set(angular.extend(queryParams, {
                            pictureId: self.single.picture
                        })).$promise.then(promiseSendQuery)
                    })
                }
                return query.single.set(self.single.picture ? angular.extend(queryParams, {
                    pictureId: self.single.picture
                }) : queryParams).$promise.then(promiseSendQuery)
            },
            removeMsg: function(convId, msgId) {
                if (1 === singleObj.messages.length) {
                    var nextObj = {},
                        currId = self.single.getLocal();
                    self.list[self.page].sort();
                    for (var i = 0, length = self.list[self.page].size(); length > i; ++i)
                        if (self.list[self.page].at(i).id === currId && self.list[self.page].size() > 1) {
                            var index = i === length - 1 ? i - 1 : i + 1;
                            nextObj = self.list[self.page].at(index);
                            break
                        }
                    return void self.list["delete"](currId, nextObj.id)
                }
                DialogService.openDialog({}, {
                    closeLabel: Translator.get("cancel"),
                    content: Translator.get("delete_msg"),
                    title: Translator.get("delete_confirm"),
                    callback: function() {
                        query.single["delete"]({
                            convId: convId,
                            messageId: msgId
                        }).$promise.then(function() {
                            singleObj.messages.remove(singleObj.messages.get(msgId)), self.single.store(singleObj, !0)
                        })
                    }
                })
            },
            request: function(params) {
                return query.single.request(params)
            },
            upload: function(id, data) {
                return $upload.upload({
                    url: Ajax.api + "/users/" + id + "/pictureattachment",
                    headers: {
                        "Content-Type": data.type
                    },
                    method: "POST",
                    data: {
                        picture: data[0]
                    }
                })
            },
            chatrequestCredits: function(params) {
                return query.single.get(params)
            }
        }
    }
    angular.module("Lovoo.Chat", ["Lovoo.User", "Lovoo.Form.Location", "Storage", "ngResource", "ngAnimate", "angularFileUpload"]).constant("PAGE", {
        chat: "chat",
        request: "requests"
    }).controller("convUserListCtrl", ListController).controller("convUserSingleCtrl", SingleController).controller("ChatTabCtrl", TabController).controller("chatRequestCtrl", RequestController), ListController.$inject = ["$scope", "UserActionsFctry", "convActionService", "PAGE"], SingleController.$inject = ["$scope", "$timeout", "convActionService", "DialogTutorialService", "UserActionsFctry", "StorageService", "ToasterService", "LocationFieldService", "PAGE"], TabController.$inject = ["$scope", "NotificationService", "PAGE"], RequestController.$inject = ["$scope", "$window", "convActionService", "DialogService", "DialogTutorialService", "ToasterService", "vcRecaptchaService"], angular.module("Lovoo.Chat").directive("conversationList", conversationListDirective).directive("conversationSingle", conversationSingleDirective).directive("stickToBottom", stickToBottomDirective).directive("chatListRefresh", chatListRefreshDirective).directive("flexibleBottom", flexibleBottomDirective).directive("checkAttachment", checkAttachmentDirective), chatListRefreshDirective.$inject = ["$window"], checkAttachmentDirective.$inject = ["$interval"], angular.module("Lovoo.Chat").constant("MESSAGE_LIMIT", 14).factory("chatQuery", chatQueryFactory).service("convActionService", ConversationActionService), chatQueryFactory.$inject = ["$resource", "$batchRequest"], ConversationActionService.$inject = ["$collection", "$interval", "$upload", "chatQuery", "DialogService", "StorageService", "PAGE", "MESSAGE_LIMIT"]
});